/*
Copyright (c) 2017
------------------------------------------------------------------
[Master Javascript]
-------------------------------------------------------------------*/
var base_url = $('#base_url').val();
(function ($) {
	"use strict";
	var caferia = {
		initialised: false,
		version: 1.0,
		mobile: false,
		init: function () {

			if(!this.initialised) {
				this.initialised = true;
			} else {
				return;
			}

			/*-------------- caferia Functions Calling ---------------------------------------------------
			------------------------------------------------------------------------------------------------*/
			this.RTL();
			this.Custom_scroll();
			this.Nav_dropdown_toggle();
			this.Sidebar_toggle();
			this.Popups();
			this.fd_timepicker();
			//this.DataTable();
			
		},
		
		/*-------------- caferia Functions definition ---------------------------------------------------
		---------------------------------------------------------------------------------------------------*/
		RTL: function () {
			// On Right-to-left(RTL) add class 
			var rtl_attr = $("html").attr('dir');
			if(rtl_attr){
				$('html').find('body').addClass("rtl");	
			}		
		},
		Custom_scroll: function(){
			/* custom scroll bar start */
			if($(".hs_custom_scrollbar").length){
				$(".hs_custom_scrollbar").mCustomScrollbar({
					scrollInertia:200,
				});
			}
			if($(".hs_custom_scrollbar_x").length){
				$(".hs_custom_scrollbar_x").mCustomScrollbar({
					scrollInertia:200,
					axis:"x"
				});
			}
			/* custom scroll bar end */
		},
		Nav_dropdown_toggle: function(){
			$('.hs_nav_dropdown > a').on('click', function(){
				$('.hs_nav_dropdown > ul').slideUp(200);
				$(this).next().slideDown(200);				
			});
		},
		Sidebar_toggle: function(){
			$('.hs_sidebar_toggle').on('click', function(){
				$('body').toggleClass('sidebar_hide');
			});
		},
		Popups: function(){
			if($('.hs_popup_link').length){
				$('.hs_popup_link').magnificPopup({
					callbacks: {
						open: function() {
							$('body').addClass('open_popup');
						},
						close: function() {
							$('body').removeClass('open_popup');
						}
					}
				});
			}
		},
		// DataTable: function(){
			// if($('.hs_datatable').length){
				// $('.hs_datatable').DataTable({
					
					// "scrollX": true
				// });
			// }			
		// }
		
		fd_timepicker: function(){
		 $('.fd_time').timepicker();
		},
	};

	

	// Load Event
	$(window).on('load', function() {
		/* add class on load start */
		setTimeout(function(){
			$('body').addClass('site_loaded');
		}, 500);
		/* add class on load end */		
	});

	// ready function
	$(document).ready(function() {
		caferia.init();
		toastr.options = {
				"closeButton": true,
				"debug": false,
				"newestOnTop": false,
				"progressBar": false,
				"positionClass": "toast-top-right",
				"preventDuplicates": true,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "5000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			  }		
	});
	
	$(document).ready(function() {
		//submit form on enter
		$("input").keypress(function(event) {
			if (event.which == 13) {  
				event.preventDefault();
				$(this).parent().parent().parent().find('.target-submit').trigger('click');
			}
		});
		 //logn section
		$('a[button-type="login"]').click(function(){
			var user_email = $('#login_form #user_email').val().trim();
			var user_pass = $('#login_form #user_pass').val().trim();
			var email_rex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/;
            if(user_email == '' || user_pass == ''){
			  toastr.error($('#emptyerr_text').val()); return false;	
			}
            
			if(!email_rex.test(user_email)){
			  toastr.error('Please enter valid Email'); return false;	
			}
			var rem = ($('#user_remember').is(':checked'))?1:0;
		     $.ajax({
					method : 'post',
					url : base_url+'authentication/login/web',
					data : {'user_email' : user_email, 'user_pass' : user_pass , 'rem' :rem }
				}).done(function(resp){ 
					var resp = JSON.parse(resp);
					if(resp['status'] == 'true'){
						location.reload();
						toastr.success(resp['message']); 
					}else{
						toastr.error(resp['message']); 
					}
			});	
		});	
		
		if($('#latest_order_type').length){	
			if($('#latest_order_type').val()=='latest'){
				setInterval(function(){ window.location.reload(); }, 1000*100);
			}
			if($('#flagNewOrders').length){
				if($('#flagNewOrders').val()=='1'){
					toastr.success('Nuevo Pedido registrado');
					document.getElementById('xyz').play();
				}
			}
		}
		
		 
		if($('.hs_datatable').length){
				$('.hs_datatable').DataTable({
					
					"scrollX": true

				});
			}
		
		
		
		
		/***********************Discount Coupon Js Start*********************/
	/* $("#apply_discount_to").change(function(){
	 var apply_to = $(this).val();
	 if(apply_to == 'Total_amount'){
		$('#discounted_product').attr('disabled', 'disabled');
		$('#discounted_product').val(0); 
		$("#discounted_product" ).trigger( "change");
	 }else{
		$('#discounted_product').removeAttr('disabled'); 
	 }
	 
	 });
	 
	 $("#up_apply_discount_to").change(function(){
	 var apply_to = $(this).val();
	 if(apply_to == 'Total_amount'){
		$('#up_discounted_product').attr('disabled', 'disabled');
		$('#up_discounted_product').val(0); 
		$("#up_discounted_product" ).trigger( "change");
	 }else{
		$('#up_discounted_product').removeAttr('disabled');
	 }
	 
	 }); */	
	 
	 
		
	$("#add_offer_form").submit(function(){

	var baseurl =$("#baseurl").val();
	var offer_heading =$.trim($("#offer_heading").val());
	var offer_title =$.trim($("#offer_title").val());
	var offer_descr =$.trim($("#offer_descr").val());
	var offer_image =$.trim($("#offer_image").val());
	var coupen_code =$.trim($("#coupen_code").val());
	var coupen_limit =$.trim($("#coupen_uses_limit").val());
	var discount_type = $.trim($("#discount_type").val());
	var discount_amount = $.trim($("#discount_amount").val());
	//var apply_discount = $.trim($("#apply_discount_to").val());
	//var discounted_prod = $.trim($("#discounted_product").val());
	var expire_date = $.trim($("#coupen_expire_date").val());
	var error=0;
	var  extension = offer_image.split('.').pop();
	event.preventDefault();

	if(offer_heading == "")                       
	{
		toastr.error('Please Fill All Fileds');
		error++;

	}else if(offer_title == "")                       
	{
		toastr.error('Please Fill All Fileds');
		error++;

	}else if(offer_descr == "")                       
	{
		toastr.error('Please Fill All Fileds');
		error++;

	}else if(coupen_code == "")                       
	{
		toastr.error('Please Enter Coupen Code');
		error++;

	}else if(coupen_limit == "")                       
	{
		toastr.error('Please Enter Limit');
		error++;

	}else if(discount_type == 0)                       
	{
		toastr.error('Please Select Discount Type');
		error++;

	}else if(discount_amount == "")                       
	{
		toastr.error('Please Enter Discount Amount');
		error++;

	}else if(expire_date == "")                       
	{
		toastr.error('Please Select Expire Date');
		error++;
	}else if(offer_image == "")                       
	{
		toastr.error('Please Select Image');
		error++;
	}else if (extension  == "gif" || extension == "png" || extension == "bmp" || extension == "jpeg" || extension == "jpg") {
			error = 0;
	}else
	{	toastr.error('Photo only allows file types of GIF, PNG, JPG, JPEG');
		error++;
	}    

	if(error == 0){ 
	    jQuery.ajax({
		type: "POST",
		url: baseurl + "admin/add_offer",
		data: new FormData(this),
		contentType: false,       
		cache: false,             
		processData:false,
		success: function(resp) {
					if(resp == 0){
						toastr.error('data not valid.');
					}else if(resp == 1){
						toastr.success('Offer add successfully.');
						setTimeout(function(){ 
								window.location.href = baseurl + "admin/discount_coupon";
							}, 1000);
						 
					}else if(resp == 2){
						toastr.error('This coupen code already exist. please enter another coupon code');
					}  
				}
		}); 
	}    
	 
	});
		
		
	$("#update_offer_form").submit(function(){
	  
	var baseurl =$("#baseurl").val();
	var offer_heading =$.trim($("#up_offer_heading").val());
	var offer_title =$.trim($("#up_offer_title").val());
	var offer_descr =$.trim($("#up_offer_descr").val());
	var offer_image =$.trim($("#up_offer_image").val()); 
	var coupen_code =$.trim($("#up_coupen_code").val());
	var coupen_limit =$.trim($("#up_coupen_uses_limit").val());
	var discount_type = $.trim($("#up_discount_type").val());
	var discount_amount = $.trim($("#up_discount_amount").val());
	//var apply_discount = $.trim($("#up_apply_discount_to").val());
	//var discounted_prod = $.trim($("#up_discounted_product").val());
	var expire_date = $.trim($("#up_coupen_expire_date").val());
	var error=0;
	event.preventDefault();

	if(offer_heading == "")                       
	{
		toastr.error('Please Fill All Fileds');
		error++;

	}else if(offer_title == "")                       
	{
		toastr.error('Please Fill All Fileds');
		error++;

	}else if(offer_descr == "")                       
	{
		toastr.error('Please Fill All Fileds');
		error++;

	}else if(coupen_code == "")                       
	{
		toastr.error('Please Enter Coupen Code');
		error++;

	}else if(coupen_limit == "")                       
	{
		toastr.error('Please Enter Limit');
		error++;

	}else if(discount_type == 0)                       
	{
		toastr.error('Please Select Discount Type');
		error++;

	}else if(discount_amount == "")                       
	{
		toastr.error('Please Enter Discount Amount');
		error++;

	}else if(expire_date == "")                       
	{
		toastr.error('Please Select Expire Date');
		error++;
	}else if(offer_image !== "")                       
	{
		var  extension = offer_image.split('.').pop();
		if (extension  == "gif" || extension == "png" || extension == "bmp" || extension == "jpeg" || extension == "jpg") {
			error = 0;
		}else
		{
			toastr.error('Photo only allows file types of GIF, PNG, JPG, JPEG');
			error++;
		}
	}   

	if(error == 0){ 

		jQuery.ajax({
		type: "POST",
		url: baseurl + "admin/update_offer",
		data: new FormData(this),
		contentType: false,       
		cache: false,             
		processData:false,
		success: function(resp){
			 
					if(resp == 0){
						toastr.error('data not valid.');
					}else if(resp == 1){
						toastr.success('Offer update successfully.');
						setTimeout(function(){ 
								window.location.href = baseurl + "admin/discount_coupon";
							}, 1000);
						
					}  
				}
		}); 
	}    
	 
  });	
		
	/***********************Discount Coupon Js End*********************/	



		
		
});
	
	
	
/***********************Combo  Offer  Js Start*********************/	
	
	$(document).ready(function(){
	    
	$( "#offer_date" ).datepicker({ dateFormat: 'yy-mm-dd', minDate: new Date() });
	
	
        $("#search-offer-product").click(function(e){
			e.preventDefault();
			e = $.Event('keydown');
			e.keyCode= 13; // enter
			$('#kf_search_offer_products').trigger(e);
		});
		$("#kf_search_offer_products").keydown(function(e)
		{
			if(e.keyCode == 13)
			{
				$(".error").remove();
				
				var searchterm = $('#kf_search_offer_products').val();

				if (searchterm == '')
				{
					$('#kf_products_results').prepend('<div class="message error">Please enter a search term</div>');
				}
				else
				{
					$("#kf_products_results").slideUp("normal", function()
				{
					$('#kf_products_results').html('<p>loading...</p>');
				});
				$("#kf_products_results").slideDown();
				
					$.post(base_url+"admin/search_items",
					{searchterm: searchterm},
						function(data)
						{
						$("#kf_products_results").slideUp("normal", function()
						{
							$("#kf_products_results").html(data);
						});

						$("#kf_products_results").slideDown();
					}
				);  
					
				}
				
				return false;
			}
		});
        
        
        $('body').on('click', '.kf_add_offer_items', function(){ 
			
			var item_id = $(this).attr('data-itemid');
			var item_name = $(this).attr('data-itemname');
			var item_price = $(this).attr('data-itemprice');

			if ($('#product-added-'+item_id).length != 0)
			{
				$('#product-added-'+item_id).animate({opacity:0.1},150);
				$('#product-added-'+item_id).animate({opacity:1},150);
				$('#product-added-'+item_id).animate({opacity:0.1},150);
				$('#product-added-'+item_id).animate({opacity:1},150);
				return;
			}
			
			$('#no-products-message').fadeOut('slow', function() { $(this).remove(); });
		
			var html = '<div id="product-added-'+item_id+'" style="display:none;">';
			html = html+'<input type="hidden" name="offer_products[]" value="'+item_id+'" />';
			html = html+'<div class="kf_item_box">';
			html = html+'<div class="kf_item_title">';
			html = html+'<a id="product-remove-'+item_id+'" class="delete">';
			html = html+'<i class="fa fa-minus-circle" aria-hidden="true"></i>';
			html = html+'</a>';
			html = html+'<span>'+item_name+'</span>';
			html = html+'</div>';
			html = html+'<div class="kf_item_quantity"><span>Quantity: </span>';
			html = html+'<input type="number" class="kf_product_quantity" name="product_quantity[]" min="1" max="10" value="1" data-price="'+item_price+'"></div>';
			html = html+'</div>';
			html = html+'</div>';

			$('#kfp_selected_products').append(html);
			$('#product-added-'+item_id).fadeIn();
			$('.kf_product_quantity').trigger('change');
			
			$('#product-remove-'+item_id).click(function()
			{
				$(this).parent().fadeOut('slow', function() { 
				    $(this).parent().remove();
				    var numItems = $('.kf_product_quantity').length;
				    if(numItems){
				        $('.kf_product_quantity').trigger('change');   
				    }else{
				        $('.kf_tmt_price').html('');
				    }
				});
			});
		});
        
        $("body").on('change', '.kf_product_quantity', function(){
		    var tot = 0;
		    $(".kf_product_quantity").each(function(){
		        var quant = $(this).val();
		        if(quant == ''){
		            quant = 1;
		        }
		        var pr = $(this).attr('data-price');
		        tot = tot + (quant * pr);
		    });
		    
		    $(".kf_tmt_price").html(tot);
		});

	});
	
	$(".kf_update_offer_item").on('change', function(){
			var offer_id = $(this).attr('data-offerid');
			var status_val = $(this).val();
			
			$.ajax({
				type: 'POST',
				url: base_url+'admin/update_offer_status_fun',
				data: {offer_id: offer_id, status_val: status_val},
				success: function(response){
					var result = JSON.parse(response);
					if(result.status){
						toastr.success(result.msg);
					}
				}
			});
		});
	
	
/***********************Combo  Offer  Js End*********************/		
	
	
	
	
	
	
	
/*********** Add Item Products START  *********************/

function add_user($this){
	var email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/;
    var mobile = /^[\s()+-]*([0-9][\s()+-]*){6,20}$/;
    var number = /^[\s()+-]*([0-9][\s()+-]*){0,20}$/;
    
    var errCount = 0; 
    $('.required').each(function(){
    	var id_val = $.trim($(this).val());
    	if( id_val == '' || id_val == '0' ) {
			 toastr.error("You missed out some fields.");
    		 errCount++;
			 $(this).focus();
			 return false;
    	}
		if( id_val != '' || id_val != '0' ) {
			var valid = $(this).attr('data-valid'); 
			if(typeof valid != 'undefined'){
				if(!eval(valid).test($(this).val().trim())){
					toastr.error($(this).attr('data-error') , 'Error');
					 errCount++;
					 $(this).focus();
					return false; 
				}
			}
		}
    	
    });
	
	
    if( errCount != 0 ) {  return false;}

    var errCount = 0;
   
	if( errCount == 0 ) {
	  if($('#cl_name').length){
		  $('#add_clinic_form').submit();
	  }else{
		
	   var user_id=$('#user_id').val();
	   var user_email=$('#user_email').val().trim();
       if(user_id!=0){
		   $('#add_user_form').submit();
	   }else{
		    var allData = {};
            var basepath = $('#base_url').val();
            allData [ 'user_email' ] = user_email;
		  $.post(basepath+'ajax/check_user',allData,function(data, status) {
				if(data == 1){
					 $('#add_user_form').submit();
				}
				else {
					 toastr.error("This email already exists,Please try other email ");
				}
			}); 
	   }	   
    }
  }
}	

})(jQuery);

function add_item($this){
    var number = /^[\s()+-]*([0-9][\s()+-]*){0,20}$/;
    var errCount = 0; 
    $('.required').each(function(){
    	var id_val = $.trim($(this).val());
    	if( id_val == '' || id_val == '0' ) {
			 toastr.error("You missed out some fields.");
    		 errCount++;
			 $(this).focus();
			 return false;
    	}
		if( id_val != '' || id_val != '0' ) {
			var valid = $(this).attr('data-valid'); 
			if(typeof valid != 'undefined'){
				if(!eval(valid).test($(this).val().trim())){
					toastr.error($(this).attr('data-error') , 'Error');
					 errCount++;
					 $(this).focus();
					return false; 
				}
			}
		}
    	
    });
	
	
    if( errCount != 0 ) {  return false;}

    var errCount = 0;
   
	if( errCount == 0 ) {
	  $('#add_item_form').submit();
     }
}



function add_offer($this)
{
	var number = /^[\s()+-]*([0-9][\s()+-]*){0,20}$/;
    var errCount = 0; 
    $('.required').each(function(){
    	var id_val = $.trim($(this).val());
    	if( id_val == '' || id_val == '0' ) {
			 toastr.error("You missed out some fields.");
    		 errCount++;
			 $(this).focus();
			 return false;
    	}
		if( id_val != '' || id_val != '0' ) {
			var valid = $(this).attr('data-valid'); 
			if(typeof valid != 'undefined'){
				if(!eval(valid).test($(this).val().trim())){
					toastr.error($(this).attr('data-error') , 'Error');
					 errCount++;
					 $(this).focus();
					return false; 
				}
			}
		}
    	
    });
	
	
    if( errCount != 0 ) {  return false;}

    var errCount = 0;
   
	if( errCount == 0 ) {
	  $('#add_combo_offer_form').submit();
     }
}



/********************* Add New User STARTS **********************/

	function addnewuser(){
		var err_count = 0;
		var dataArr = {};
		$('.addnew_wait').removeClass('hide');
		$('.addnewuser').each(function() {

			if( $.trim($(this).val()) == '' ) {
				toastr.error('All fields are Required.');
				err_count++;
			}
			else {
				var clsStr = $(this).attr('class');
				if( clsStr.search('user_email') != -1 ) {

					var em = $.trim($(this).val());
					var emRegex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,15}(?:\.[a-z]{2})?)$/i;

					if(!emRegex.test(em)) {
						toastr.error('Email should be correct.');
						 err_count++;
					}
				}

				if( clsStr.search('user_pass') != -1 ) {

					var pwd = $.trim($(this).val());

					if(pwd.length < 7) {
						toastr.error('Password should contain minimum 7 characters.');
						err_count++;
					}
				}
			}

			if( err_count == 0 ) {
				dataArr[ $(this).attr('id') ] = $(this).val() ;
			}
		});
		

		if( err_count == 0 ){
			
			if( $('#sendemail').length > 0 ) {
				var sendemail = $('#sendemail:checked').length;
				dataArr [ 'sendemail' ] = sendemail;
			}
			dataArr [ 'u_accesslevel' ] = $('.tp_catset_box #u_accesslevel').val() ;
		    var basepath = $('#base_url').val();
			$.post(basepath+"admin/addnewuser_backend",dataArr,function(data, status) {
		
				var resStr = data.split('#');
				if(resStr[1] == 'register'){
					toastr.success('User registered successfully.');
					setInterval(function(){
					  window.location.reload(1);
					}, 3000);
				}
				else if(resStr[1] == 'exists'){
					toastr.success('We already have user with these details.');
					
				}
				else if(resStr[0] == 404){
					toastr.error('Page will refreshed in 3 seconds.');
					setInterval(function(){
					   window.location.reload(1);
					}, 3000);
				}
				$('.ts_submit_wait').addClass('hide');
	
			});
			
		}
		else {
			$('.addnew_wait').addClass('hide');
			
		}
	}

/********************* Add New User ENDS **********************/

/************* Wallet credit STARTS *******************/
function openWalletcreditPopup(user_id , user_uname , request_id ='') {
   var basepath = $('#base_url').val();	
   var dataArr = {};
   dataArr['user_id'] = user_id;
   $('#wallet_credit #user_unameTxt').text(user_uname);
   $('#wallet_credit #wallet_user_id').val(user_id);
   
   if(request_id !=''){
		$('#wallet_credit #wallet_request_id').val(request_id);
		 
   }
   
   $.post(basepath+"admin/user_wallet_detail",dataArr,function(data, status) {
		$('#wallet_credit #wallet_amountTxt').text(data);
   });
   
   $('#wallet_credit').modal('show');
}



function update_wallet_fun(){
   var basepath = $('#base_url').val();	
   var dataArr = {};
   var wc_amount= $('#wallet_credit #wc_amount').val();
   var wc_note= $('#wallet_credit #wc_note').val();
   var wc_uid=   $('#wallet_credit #wallet_user_id').val();
   if(wc_amount==''){
	  toastr.error('Amount is Required.');
      return false;	  
   }
   if (isNaN(wc_amount) || wc_amount < 1) {
       toastr.error('Please enter numeric amount.');
      return false;	 
   } 
   dataArr['wc_amount'] = wc_amount;
   dataArr['wc_note'] = wc_note;
   dataArr['wc_uid'] = wc_uid;
   
   if($('#wallet_credit #wallet_request_id').length > 0 ){
	   var request_id = $('#wallet_credit #wallet_request_id').val();
	   dataArr['request_id'] = request_id;
   }
   
   $.post(basepath+"admin/update_user_wallet",dataArr,function(data, status) {
	if (isNaN(data)) {
		toastr.error('Page will refreshed in 3 seconds.');
		setInterval(function(){
		   window.location.reload(1);
		}, 5000);
	}else{
		$('#wallet_credit #wallet_amountTxt').text(data);
		$('#wallet_credit #wc_amount').val('');
        $('#wallet_credit #wc_note').val('');
		toastr.success('Wallet update successfully.');
		setInterval(function(){
		  window.location.reload(1);
		}, 3000);
	}

});	
}

/********************* Update User STARTS **********************/
	function updateuser(){
		var err_count = 0;
		var dataArr = {};
		//$('.addnew_wait').removeClass('hideme');
		$('.update_user').each(function() {
				var clsStr = $(this).attr('class');
				if( clsStr.search('email') != -1 ) {

					var em = $.trim($(this).val());
					var emRegex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,15}(?:\.[a-z]{2})?)$/i;

					if(!emRegex.test(em)) {
						toastr.error('Email should be correct.');
						 err_count++;
					}
				}
			

			if( err_count == 0 ) {
				dataArr[ $(this).attr('id') ] = $(this).val() ;
			}
		});
		if( err_count == 0 ){
			var basepath = $('#base_url').val();
			$.post(basepath+"admin/updateuser_backend",dataArr,function(data, status) {
		
				toastr.success('User update successfully.');
					setInterval(function(){
					  window.location.reload(1); 
					}, 3000);
			});
			
		}
		else {
			toastr.error('Page will refreshed in 3 seconds.');
			$('.ts_message_popup').addClass('ts_popup_error');
		}
	}
	
	function updateuserpass(){
	    var dataArr = {};
		var user_id=$('#changepass #user_id').val();
		var user_pwd=$('#changepass #user_pwd').val().trim();
		var user_cpass=$('#changepass #user_cpass').val().trim();
		if(user_pwd==''){
		  $('.ts_message_popup_text').text('New password is required');
		  $('.ts_message_popup').addClass('ts_popup_error');
		  $('#changepass #user_pwd').focus();
          removeMessage();
          return false;		  
		}
		if(user_pwd.length<8){
		  $('.ts_message_popup_text').text('New password should be 8 characters long');
		  $('.ts_message_popup').addClass('ts_popup_error');
		  $('#changepass #user_pwd').focus();
          removeMessage();
          return false;		  
		}
		if(user_pwd!=user_cpass){
		  $('.ts_message_popup_text').text('New password and Confirm pasword must be same');
		  $('.ts_message_popup').addClass('ts_popup_error');
		  $('#changepass #user_cpass').focus();
          removeMessage();
          return false;		  
		}
		dataArr['user_id'] = user_id;
		dataArr['user_pwd'] = user_pwd;
		var basepath = $('#basepath').val();
		$.post(basepath+"backend/updateuserpass",dataArr,function(data, status) {
		 if(data==1){
		  $('.ts_message_popup_text').text('Password changed successfully.');
		  $('.ts_message_popup').addClass('ts_popup_success');
		  $('#changepass #user_cpass').focus();
          removeMessage();
		 }
        });
			
	}
	
	
	/*********** Update basic values of tables STARTS ******************/
function updatethevalue($this,type){
    var dataArr = {};
    var id = $($this).attr('id');
   
   if( type == 'categories' ) {
        var vlu = $($this).is(':checked') ? '1' : '0' ;
    }
    else {
        var vlu = $($this).val();
    }
	
    var basepath = $('#base_url').val();
    dataArr [ 'id' ] = id;
    dataArr [ 'type' ] = type;
    dataArr [ 'vlu' ] = vlu;
	var serverUrl = basepath+"settings/updatethevalue";
    $.post(serverUrl,dataArr,function(data, status) {
    console.log(data);
        if(data == '1'){  
			toastr.success("Data updated successfully");
        }
        else {
			toastr.error("Data can not be updated.");
        }


    });
}
/*********** Update basic values of tables ENDS ******************/
function updateSettings(commonclass) {
    if( commonclass == 'logoform' ) {
        // image upload
        $('#logoform').submit();
    }
	 else if( commonclass == 'languageSettings' ) {
        // language settings
        var addnewlanguage = ($.trim($('#addnewlanguage').val())).toLowerCase();
        var existinglanguage = $('#existinglanguage').val();
        if( addnewlanguage != '' ) {
            if( existinglanguage.search(addnewlanguage) != '-1' ) {
                // already exists
               toastr.error(addnewlanguage+' is already added.');
                return false;
            }
            else if( existinglanguage.search(' ') != '-1' ) {
                // Space cannot be allowed
                toastr.error('Space is not allowed.');
                return false;
            }
        }
        $('#addnewlanguage').val(addnewlanguage);
        $('#languageForm').submit();
    }
    else if( commonclass == 'add_cate_form' ) {
        // Category Section
        $(this).removeAttr('onlick');
        var err = 0;
        $('.add_cate_form').each(function(){
            if($.trim($(this).val()) == '') {
                err++;
            }
        });

        if(err == 0) {
            $('#add_cate_form').submit();
        }
        else {
            toastr.error("Category name is required");
            
        }

    }
    else if( commonclass == 'add_sub_cate_form' ) {
        // Sub Category Section
        $(this).removeAttr('onlick');
        var err = 0;
        $('.add_sub_cate_form').each(function(){
            if($.trim($(this).val()) == '') {
                err++;
            }
        });

        

        if(err == 0) {
            $('#add_sub_cate_form').submit();
        }
        else {
            $(this).attr('onlick',"updateSettings('add_sub_cate_form')");
			toastr.error("Please, fill in the details.");
        }

    }
	else if( commonclass == 'add_plan_form' ) {
        // Sub Category Section
        $(this).removeAttr('onlick');
        var err = 0;
        $('.add_plan_form').each(function(){
            if($.trim($(this).val()) == '') {
                err++;
            }
        });

        

        if(err == 0) {
            $('#add_plan_form').submit();
        }
        else {
            $(this).attr('onlick',"updateSettings('add_plan_form')");
			toastr.error("Please, fill in the details.");
        }

    }
	else if( commonclass == 'add_spe_form' ) {
        // Sub Category Section
        $(this).removeAttr('onlick');
        var err = 0;
        $('.add_spe_form').each(function(){
            if($.trim($(this).val()) == '') {
                err++;
            }
        });

        

        if(err == 0) {
            $('#add_spe_form').submit();
        }
        else {
            $(this).attr('onlick',"updateSettings('add_spe_form')");
			toastr.error("Please, fill in the details.");
        }

    }
    else {
        var allData = {};
        var dataArr = {};
        $('.'+commonclass).each(function(){
            if( $(this).attr('id').search("_checkbox") != '-1' ) {
                var chk = $('#'+$(this).attr('id')).is(':checked') ? '1' : '0' ;
                allData[ $(this).attr('id') ] = chk;
            }else if ( $(this).attr('id').search("_status") != '-1' ) {
                var chk = $('#'+$(this).attr('id')).is(':checked') ? '1' : '0' ;
                allData[ $(this).attr('id') ] = chk;
            }
            else {
                allData[ $(this).attr('id') ] = $.trim($(this).val()) ;
            }
        });
        var basepath = $('#base_url').val();
        dataArr [ 'updateform' ] = 'yes';
        dataArr [ 'updatedata' ] = JSON.stringify(allData);
        $.post(basepath+"settings/update_settingsdetails",dataArr,function(data, status) {
            if(data == '1'){
                $('.ts_message_popup_text').text('Data updated successfully.');
                 toastr.success("Data updated successfully.");
            }
            else {
                toastr.error("Data can not be updated.");
            }
           
        });
    }
}

 function add_category(cat_id = "" , thiss = ""){
	 $('#addnewcat #cateimage').val('');
	 if(cat_id==''){
		 $('#addnewcat .modal-title span').text('Add New') ;
		 $('#addnewcat .modal-body .btn').text('Add');
		 $('#addnewcat #catename').val('');
		 $('#addnewcat #old_cateid').val(0);
	 }else{
		$('#addnewcat .modal-title span').text('Update');
		$('#addnewcat .modal-body .btn').text('Update');
		$('#addnewcat #catename').val($(thiss).attr('data-name'));
		$('#addnewcat #old_cateid').val(cat_id);
		 
	 }
     $('#addnewcat').modal('show');	   
 }



function delete_user(user_id='',user_level=''){
   var cnf=confirm('Are you sure?');
   if(cnf){
	   var basepath = $('#base_url').val();
	   window.location.href=basepath+'admin/delete_user/'+user_id+'/'+user_level;
   }   
}
function delete_item(item_id=''){
   var cnf=confirm('Are you sure?');
   if(cnf){
	   var basepath = $('#base_url').val();
	   window.location.href=basepath+'admin/delete_item/'+item_id;
   }   
}

function delete_item_offer(offer_id=''){
   var cnf=confirm('Are you sure?');
   if(cnf){
	   var basepath = $('#base_url').val();
	   window.location.href=basepath+'admin/delete_item_offer/'+offer_id;
   }   
}


function delete_request(req_id=''){
   var cnf=confirm('Are you sure?');
   if(cnf){
	   var basepath = $('#base_url').val();
	   window.location.href=basepath+'admin/delete_request/'+req_id;
   }   
}

function accept_request(req_id='',){
   var cnf=confirm('Are you want to add this as a user sure?');
   if(cnf){
	   var basepath = $('#base_url').val();
	   window.location.href=basepath+'admin/accept_request/'+req_id;
   }   
}
function meta_info_update()
{
	var site_title=$('#site_title').val();
	var site_keyword=$('#site_keyword').val();
	var author_name=$('#author_name').val();
	var site_description=$('#site_description').val();
	var basepath = $('#base_url').val();
	if(site_title=='')
	{
		toastr.error("Enter Site Title");
	}
	else if(site_keyword=='')
	{
		toastr.error("Enter Site Keyword");
	}
	else if(author_name=='')
	{
		 toastr.error("Enter Author Name");
	}
	else if(site_description=='')
	{
		toastr.error("Enter Site Description");
	}
	else
	{
			var url=basepath+"settings/site_setting";
			$.post(url,{'site_title':site_title,'site_keyword':site_keyword,'author_name':author_name,'site_description':site_description},function(fb){
			if(fb.match('1'))
			{
				toastr.success("Information Successfully Updated");
			}
			else
			{
				toastr.error("Information Not Updated");
			}
			})
		
	}
}
function google_ads()
{
	var add_s=$('#add_s').val();
	var add_mob=$('#add_mob').val();
	var basepath = $('#base_url').val();
	var url=basepath+"settings/ads_integration";
	$.post(url,{'add_s':add_s,'add_mob':add_mob},function(fb){
		if(fb.match('1'))
		{
			toastr.success("Setting Successfully Updated");
		}
		else 
		{
			toastr.error("Setting Not Updated");
		}
	});
}
function google_analytics()
{
	var google_a=$('#google_a').val();
	var basepath = $('#base_url').val();
	var url=basepath+"settings/google_analytics";
	$.post(url,{'google_a':google_a},function(fb){
		if(fb.match('1'))
		{
			toastr.success("Setting Successfully Updated");
		}
		else 
		{
			toastr.error("Setting Not Updated");
		}
	});
}


function blog_setting()
{	
	var open_blog 		= $("input[name='open_blog']:checked").val();
	var comment_section = $("input[name='comment_section']:checked").val();

	var basepath = $('#base_url').val();
	var url=basepath+"settings/blog_setting";
	$.post(url,{'open_blog':open_blog,'comment_section':comment_section},function(res){
		if(res.match('1'))
		{
			toastr.success("Blog Setting Successfully Updated");
		}
		else 
		{
			toastr.error("Blog Setting Not Updated");
		}
	});
}
function credentials()
{
	var email=$('#email').val();
	var password1=$('#password').val();
	if(email=='')
	{
		toastr.error("Enter E-Mail");
	}
	else if(password1!='' && password1.length<4)
	{
			toastr.error("Password is Short");
	}
	else
	{
		var basepath = $('#base_url').val();
		var url1=basepath+"settings/credentials";
		if(password1=='')
		{
			password1=$('#ps1').val();
		}
		$.post(url1,{'email':email,'ps':password1},function(fb){
			if(fb.match('1'))
			{
				toastr.success("Information Successfully Updated");
			}
			else
			{
				toastr.error("Information Not Updated");
			}
		});
	}
}


function saveCookies()
{	
	var cookie_title=$('#cookie_title').val();
	var cookie_text=$('#cookie_text').val();
	var cookie_link=$('#cookie_link').val();
	var cookie_link_title=$('#cookie_link_title').val();
	var cookie_button=$('#cookie_button').val();
	if(cookie_title=='' || cookie_text=='' || cookie_link=='' || cookie_link_title=='' || cookie_button=='')
	{
		toastr.error("All field must be required");
	}
	else
	{
		var basepath = $('#base_url').val();
		var url1=basepath+"settings/saveCookies";
		$.post(url1,{'cookie_title'		:	cookie_title,
					 'cookie_text'		:	cookie_text,
					 'cookie_link'		:	cookie_link,
					 'cookie_link_title':	cookie_link_title,
					 'cookie_button'	:	cookie_button},function(fb){
			if(fb.match('1'))
			{
				toastr.success("Information Successfully Updated");
			}
			else
			{
				toastr.error("Information Not Updated");
			}
		});
	}
}


function checkRequire(formId){
	//if($('#myMessage').length){ $('#myMessage').remove(); } 
	var email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/;	 
	var url = /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/;
	var image = /\.(jpe?g|gif|png|PNG|JPE?G)$/;
	var mobile = /^[\s()+-]*([0-9][\s()+-]*){6,20}$/;
	var facebook = /^(https?:\/\/)?(www\.)?facebook.com\/[a-zA-Z0-9(\.\?)?]/;
	var twitter = /^(https?:\/\/)?(www\.)?twitter.com\/[a-zA-Z0-9(\.\?)?]/;
	var google_plus = /^(https?:\/\/)?(www\.)?plus.google.com\/[a-zA-Z0-9(\.\?)?]/;
	var check = 0;
	$('#er_msg').remove();
	var target = (typeof formId == 'object')? $(formId):$('#'+formId);
	target.find('input , textarea').each(function(){
		if($(this).hasClass('require')){
			if($(this).val().trim() == ''){
				check = 1;
				toastr.error('You missed out some fields' , 'Error');
				$(this).parent().addClass('error');
				return false; 
			}else{
				$(this).parent().removeClass('error');
			} 
		}   
		 
		
		if($(this).val().trim() != ''){
			var valid = $(this).attr('data-valid'); 
			if(typeof valid != 'undefined'){
				if(!eval(valid).test($(this).val().trim())){
					toastr.error($(this).attr('data-error') , 'Error');
					$(this).parent().addClass('error');	
					check = 1;
					return false; 
				}else{
					$(this).parent().removeClass('error');
				}
			}
		}
	});
	return check;
}
function terms_condation()
{
				var data = CKEDITOR.instances.terms_condition.getData();
				$.post('compliamce_info',{'tc':data,'field':'terms_condition','date_type':'terms_condition_update_date'},function(data){
					if(data.match('Update'))
						toastr.success('Terms Condation Updated');
					else
						toastr.error('Terms Condation Not Updated');
					
				});
}
function privacy_policy()
{
				var data = CKEDITOR.instances.privacy_policy.getData();
				$.post('compliamce_info',{'tc':data,'field':'privacy_policy','date_type':'privacy_policy_update_date'},function(data){
					if(data.match('Update'))
						toastr.success('Privacy Policy Updated');
					else
						toastr.error('Privacy Policy Not Updated');
					
				});
}
function read_more(msg,name)
{
	$('#rname').text(name);
	$('#read_more_text').text(msg);
	$('#myModal').modal('toggle');
}

function edit_offers(offerid){
	var baseurl =$("#baseurl").val();
	if(offerid !== ''){
		jQuery.ajax({
		type: "POST",
		url: baseurl + "admin/edit_offer",
		data: {offer_id: offerid},
		success: function(resp) {
			 var respData = JSON.parse(resp);
			 if(respData['status'] == 1){ 
				 $("#old_offer_id").val(respData['edit_offer'][0]['offer_id']);
				 $("#up_offer_heading").val(respData['edit_offer'][0]['offer_heading']);
				 $("#up_offer_title").val(respData['edit_offer'][0]['offer_title']);
				 $("#up_offer_descr").val(respData['edit_offer'][0]['offer_description']); 
				 $("#up_coupen_code").val(respData['edit_offer'][0]['coupen_code']); 
				 $("#up_coupen_uses_limit").val(respData['edit_offer'][0]['coupen_uses_limt']); 
				 $("#up_discount_type").val(respData['edit_offer'][0]['discount_type']);
				 $( "#up_discount_type" ).trigger( "change" );				 
				 $("#up_discount_amount").val(respData['edit_offer'][0]['discount_amount']); 
				 //$("#up_apply_discount_to").val(respData['edit_offer'][0]['apply_discount_to']); 
				 //$( "#up_apply_discount_to" ).trigger( "change" );
				  //if(respData['edit_offer'][0]['apply_discount_to'] == 'single_product'){
						//$("#up_discounted_product").val(respData['edit_offer'][0]['discounted_product']);  
					    //$("#up_discounted_product" ).trigger( "change"); 
						 
				 // } 
				 if(respData['edit_offer'][0]['user_used_limit'] == 1){
						$("#up_user_usedlimit_yes").val(respData['edit_offer'][0]['user_used_limit']).attr('checked',true);
				 }else{
					 $("#up_user_usedlimit_no").val(respData['edit_offer'][0]['user_used_limit']).attr('checked',true);
				 }
				 $("#up_user_usedlimit" ).trigger( "change"); 
				 //$("#up_discounted_product").val(respData['edit_offer'][0]['discounted_product']);
				 $("#up_coupen_expire_date").val(respData['edit_offer'][0]['coupen_expire_date']); 
				 $("#update_offer").modal('show');  
			 }else{
				  
				 toastr.error(respData['error']);
			 }
					  
		  }
		});
		 		
	}
}

//update offer status
function update_offer_status(offer_id){
	 
	var baseurl =$("#baseurl").val();
	var isChecked = $("#offerstatus_"+offer_id).is(":checked");
	if (isChecked == true) {
	   
		var val = 1;
	 
	} else {
		var val = 0;
	   
	}
	 
	  
	if(val !== '' && offer_id !== ''){      
		jQuery.ajax({
		type: "POST",
		url: baseurl + "admin/update_offer_status",
		data: {offer_value: val, offer_id: offer_id},
		success: function(resp){
		 
				if(resp == 0){
					 toastr.error('data not update.');
				}else if(resp == 1){
					 toastr.success('data update successfully.');
				}  
										
			}
		});  
	 } 
	  
}

//update offer status
function update_offer_featured(offer_id){
	 
	var baseurl =$("#baseurl").val();
	var isChecked = $("#offerfeatured_"+offer_id).is(":checked");
	if (isChecked == true) {
	    
		var val = 1;
	 
	} else {
		var val = 0;
	   
	}
	 
	  
	if(val !== '' && offer_id !== ''){      
		jQuery.ajax({
		type: "POST",
		url: baseurl + "admin/update_offer_featured",
		data: {offer_value: val, offer_id: offer_id},
		success: function(resp){
		 
				if(resp == 0){
					 toastr.error('data not update.');
				}else if(resp == 1){
					 toastr.success('data update successfully.');
				}  
										
			}
		});  
	 } 
	  
}




function delete_offer(){
   var cnf=confirm('Are you sure?');
   if(cnf){
	    return true;
   }else{
		return false;
   }   
   
   
}

 function initilize_table(){
		  var pickuporder = 'pickup_order';
		  var basepath = $('#base_url').val();
	   window.location.href=basepath+'admin/orders/'+pickuporder;
	  }

function initilize_table_order(){
		  var order = '';
		  var basepath = $('#base_url').val();
	   window.location.href=basepath+'admin/orders/'+order;
	  }
	  
	  
function getItemOption_list(mainitem_id){
	var basepath = $('#base_url').val();
	$('#option_list').html('');
	if(mainitem_id !='')
	{
		$.ajax({
			type: "POST",
			url: basepath + '/admin/get_itemoptionsList',
			data:{'item_id':mainitem_id},
			success: function(data)
			{ 	obj = JSON.parse(data);
				if(obj['status'] == 1){
					$('#option_list').html(obj['option_list']);
					$('#item_optionlist_modal').modal('show');
				}					
			} 
		});
	}
}	  
	  
	  
	 
	 
function prodoptions_checkbox($this){
	if($($this).is(':checked')){
		$('#mainprod_price').html('');
		add_optionfileds();
	}else{
		$('#mainprod_price').html('<div class="hs_input"><label>Item Price </label><input type="text" class="form-control required" id="main_item_price" name="main_item_price" value="" data-valid="number" data-error="Please enter valid number"></div>');
		$('#prod_option_div').html('');
	}
} 
    
   
	
function add_optionfileds(id=0){ 
	if($('.counter').length < 3){
	var add_btn='';
	if($('#prodoption_'+id).length == 0 ){
		var id=0;
		var div_id = 'prodoption_'+id;
		add_btn += '<div class="col-md-3 col-sm-12 col-xs-12"><div class="hs_input"><a onclick="add_optionfileds('+id+');" id="prouductopt_btn" class="btn" ><i class="fa fa-plus"></i></a></div></div>';
	}else{
		id = 0 + $('.counter').length;
		var div_id = 'prodoption_'+id;
		add_btn += '<div class="col-md-3 col-sm-12 col-xs-12"><div class="hs_input"><a onclick="remove_optionfileds('+id+',0);" id="prouductopt_btn" class="btn" ><i class="fa fa-minus"></i></a></div></div>';
	}
 
	var box_html = '<div id="'+div_id+'" class="row counter"><div class="col-md-3 col-sm-12 col-xs-12"><div class="hs_input"><label>Item Sizes </label><input type="text" class="form-control required" id="item_size" name="item_size[]" value=""></div></div><div class="col-md-3 col-sm-12 col-xs-12"><div class="hs_input"><label>Item Price </label><input type="text" class="form-control required" id="item_price" name="item_price[]" value="" data-valid="number" data-error="Please enter valid price"></div></div>'+add_btn+'</div>';
	$('#prod_option_div').append(box_html);
	}
}
	
	
function remove_optionfileds(box_id, item_option_id =''){
		
	 if(item_option_id !='' && item_option_id !=0){
		 var cnf=confirm('Are you sure remove this option from database ?');
		 if(cnf)
		 {
			var basepath = $('#base_url').val();
			dataArr = {};
			dataArr['item_opt_id']= item_option_id;  
			$.post(basepath+"admin/delete_item_options",dataArr,function(data) {
				if(data == 1)
				{
					if(box_id > 0)
					{
						$('#prodoption_'+box_id).remove();
					}
					toastr.success('Data remove successfully'); 
				}
			});
		}
	}else{
		if(box_id > 0)
		{
			$('#prodoption_'+box_id).remove();
		}
	}		
}

$(document).ready(function(){
	if($('#dtOrderDelivery').length){
		var dtOrderDelivery = $('#dtOrderDelivery').DataTable({
			"pageLength": 5,
			language: {
				"decimal": "",
				"emptyTable": "No se encontraron datos",
				"info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
				"infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
				"infoFiltered": "(Filtrado de _MAX_ total entradas)",
				"infoFiltered": "(Filtrado de _MAX_ total entradas)",
				"infoPostFix": "",
				"thousands": ",",
				"lengthMenu": "Mostrar _MENU_ Entradas",
				"loadingRecords": "Cargando...",
				"processing": "Procesando...",
				"search": "Buscar:",
				"zeroRecords": "Sin resultados encontrados"
			},
			"scrollX": true,
			select: true,
			"columnDefs": [
				{ "visible": false, "targets": 14 }
			]
		});

		$('#dtOrderDelivery tbody').on('dblclick', 'tr', function () {
			//var data = $dtOperaciones.row(this).data();
			var closestRow = $(this).closest('tr');
			var data = dtOrderDelivery.row(closestRow).data();

			var Id = data[0];
			var PedidoId = data[1];
			var Cliente = data[2];
			var Celular = data[3];
			var Productos = data[4];
			var EstadoPedido = data[5];
			var DireccionDelivery = data[6];
			var DeliveryBoy = data[7];
			var EstadoPago = data[8];
			var ModoPago = data[9];
			var MontoPago = data[10];
			var Descuento = data[11];
			var FechaPedido = data[12];
			var FechaDelivery = data[13];
			var InstruccionesEspeciales = data[16];

			// EstadoPedido = EstadoPedido.replace(/<\/?select>/g, '');
			// EstadoPedido = EstadoPedido.replace(/<\/?option>/g, '');
			if(EstadoPago == 'Pagado'){
				EstadoPago = '<span class="label label-success">Pagado</span>';
			}else{
				EstadoPago = '<span class="label label-danger">No pagado</span>';
			}

			if(ModoPago =='cod'){
				ModoPago = "Pago Contra Entrega";
			}else{
				ModoPago = "Tarjeta Debito/Credito";
			}

			$("#Id").val(Id);
			$("#PedidoId").html(PedidoId);
			$("#Cliente").html(Cliente);
			$("#Celular").html(Celular);
			$("#Productos").html(Productos);
			$("#EstadoPedido").html(EstadoPedido);
			$("#DirecciónDelivery").html(DireccionDelivery);
			$("#DeliveryBoy").html(DeliveryBoy);
			$("#EstadoPago").html(EstadoPago);
			$("#ModoPago").html(ModoPago);
			$("#MontoPago").html("S/ " + MontoPago);
			$("#Descuento").html(Descuento);

			$("#FechaPedido").html(FechaPedido);
			$("#FechaDelivery").html(FechaDelivery);
			$("#InstruccionesEspeciales").html(InstruccionesEspeciales);

			$('#popupOrderId').modal('show');
			//$("#MontoAprob").focus();

			$('#popupOrderId').on('shown.bs.modal', function () {
				//$("#MontoAprob").focus();
			})

		});
	}

	$('#btnImprimirPedido').on("click", function () {
		$('#printPedido').printThis({
			base: "https://jasonday.github.io/printThis/",
			header: "<h1>Detalle del Pedido:</h1>",
			importCSS: true,            // import parent page css
			importStyle: true,         // import style tags
			canvas: true              // copy canvas content
		});
	});
});

function printDiv()
{

	var divToPrint=document.getElementById('printPedido');

	var newWin=window.open('','Print-Window');

	newWin.document.open();

	newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

	newWin.document.close();

	setTimeout(function(){newWin.close();},10);

}