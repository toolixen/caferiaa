<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Ts_functions {
    public function __construct() {
        $this->CI = get_instance();
    }
    /****
    getsettings : Function responsible to fetch the basic settings of the application,
    First Param : Pagename eg., login, register...
    Second Param : Type eg., title , metatags...
    ****/
    public function getsettings($pagename = '', $type = '') {
        $whrParam = $pagename . '_' . $type;
        $whrArray = array(
            'key_text' => $whrParam
        );
        // $resArray = $this->CI->DatabaseModel->access_database('ts_settings','select','',$whrArray);
        $resArray = $this->CI->DatabaseModel->select_data('value_text', 'rt_settings', $whrArray, 1);
        return (!empty($resArray) ? $resArray[0]['value_text'] : 'NF');
    }
    /****
    updatesettings : Function responsible to update the settings of the application,
    First Param : key ...
    Second Param : value ...
    ****/
    public function updatesettings($key = '', $value = '') {
        $whrArray    = array(
            'key_text' => $key
        );
        $updateArray = array(
            'value_text' => $value
        );
        $this->CI->DatabaseModel->access_database('rt_settings', 'update', $updateArray, $whrArray);
    }
    public function sendnotificationemails($type, $to, $subject, $username, $linkhref = '', $password = '', $website_link = '') {
        
        
        $this->CI->load->library('parser');
        $link       = "<a href='" . $linkhref . "'>" . $linkhref . "</a>";
        $emContent  = $this->getsettings($type, 'text');
        $emContent  = str_replace("[linktext]", $link, $emContent);
        $emContent  = str_replace("[username]", $username, $emContent);
        $emContent  = str_replace("[password]", $password, $emContent);
        $emContent  = str_replace("[break]", "<br/>", $emContent);  
		$emContent  = str_replace("[space]", "&nbsp;", $emContent);
		
		$emContent  = str_replace("[s1]", "<a href='".base_url('front/product_rating/'.$linkhref.'/1/'.$password)."'><img src='".base_url('assets/images/Star1.png')."'></a>", $emContent);
		$emContent  = str_replace("[s2]", "<a href='".base_url('front/product_rating/'.$linkhref.'/2/'.$password)."'><img src='".base_url('assets/images/Star1.png')."'></a>", $emContent);
		$emContent  = str_replace("[s3]", "<a href='".base_url('front/product_rating/'.$linkhref.'/3/'.$password)."'><img src='".base_url('assets/images/Star1.png')."'></a>", $emContent);
		$emContent  = str_replace("[s4]", "<a href='".base_url('front/product_rating/'.$linkhref.'/4/'.$password)."'><img src='".base_url('assets/images/Star1.png')."'></a>", $emContent);
		$emContent  = str_replace("[s5]", "<a href='".base_url('front/product_rating/'.$linkhref.'/5/'.$password)."'><img src='".base_url('assets/images/Star1.png')."'></a>", $emContent);
		
        $email_body = '<table><tr><td>' . $emContent . '</td></tr></table>';
        $email_data = array(
            'email_body' => $email_body
        );
        $body       = $this->CI->parser->parse('email_template', $email_data, true);
        $subject    = $this->getsettings($type, 'subject');
        $this->sendUserEmailCI($to, '', '', $subject, $body);
        //return $body; 
    }
	function email2($type,$to,$emContent)
	{
		 $this->CI->load->library('parser');
		 $email_body = '<table><tr><td>' . $emContent . '</td></tr></table>';
		  $subject    = $this->getsettings($type, 'subject');
        $email_data = array(
            'email_body' => $email_body
        );
        $body       = $this->CI->parser->parse('email_template', $email_data, true);
        $subject    = $this->getsettings($type, 'subject');
        $this->sendUserEmailCI($to, '', '', $subject, $body);
	}
    public function sendbookingemails($payment_id = '', $type = '') {
        $email_bodyUser = $email_bodyDoctor = '';
        $this->CI->load->library('parser');
        $paymentDetails = $this->CI->DatabaseModel->select_data('*', 'rt_paymentdetails', array(
            'payment_id' => $payment_id
        ), 1);
        if (!empty($paymentDetails)) {
            $order_status = $paymentDetails[0]['payment_order_status'];
            $user_id      = $paymentDetails[0]['payment_uid'];
            $userDetails  = $this->CI->DatabaseModel->select_data('*', 'rt_users', array(
                'user_id' => $user_id
            ), 1);
            $username     = $userDetails[0]['user_name'];
            $user_mobile  = $userDetails[0]['user_mobile'];
            //$user_device_token	=$userDetails[0]['user_device_token'];
            $subject      = $this->getsettings('bookingemail', 'subject');
			
            // Booking start
            if ($order_status == 0) {
                $emContent       = $this->getsettings('bookorder', 'text');
                $email_bodyAdmin = $this->parseBotemp($paymentDetails, $userDetails, $emContent);
            }
            // Booking End
			
            // Intransist start
            if ($order_status == 1) {
                $emContentUser    = $this->getsettings('intransits', 'text');
                $email_bodyUser   = $this->parseBotemp($paymentDetails, $userDetails, $emContentUser);
                
            }
			
            // Intransist End
			
            // cancel order by admin start
            if ($order_status == 2) {
                $emContentUser  = $this->getsettings('delivered', 'text');
                $email_bodyUser = $this->parseBotemp($paymentDetails, $userDetails,  $emContentUser);
            }
			if ($order_status == 3) {
                $emContentUser  = $this->getsettings('canceled', 'text');
                $email_bodyUser = $this->parseBotemp($paymentDetails, $userDetails,  $emContentUser);
            }
            // cancel order by admin end
			//echo $email_bodyUser;die;  
			
            if ($email_bodyUser != '') {
                $bodyUser = $this->CI->parser->parse('email_template', array(
                    'email_body' => $email_bodyUser
                ), true);
                $this->sendUserEmailCI($userDetails[0]['user_email'], '', '', $subject, $bodyUser);
                /*if ($user_mobile != '') {
                    $this->send_msg($user_mobile, strip_tags($email_bodyUser));
                }
                if ($user_device_token != '') {
                    $this->firebase_notification($user_device_token, strip_tags($email_bodyUser), $subject);
                }*/
            }
            if ($email_bodyAdmin != '') {
				
                $bodyAdmin = $this->CI->parser->parse('email_template', array(
                    'email_body' => $email_bodyAdmin
                ), true);
				
                $this->sendUserEmailCI($this->getsettings('email', 'contactemail'), '', '', $subject, $bodyAdmin);
                //$this->send_msg($this->getsettings('admin' , 'mobile') ,strip_tags($email_bodyAdmin)); 
                /*   if($doctor_device_token!=''){
                $this->firebase_notification($doctor_device_token, strip_tags($email_bodyDoctor),$subject);
                } */
            }
        }
    }
    function parseBotemp($paymentDetails, $userDetails, $emContent) {
        $order_date   = date("d-M-Y", strtotime($paymentDetails[0]['payment_date']));
        $order_number = $paymentDetails[0]['payment_uniqid'];
        $username     = ucfirst($userDetails[0]['user_name']);
        $emContent    = str_replace("[username]", $username, $emContent);
        $emContent    = str_replace("[order_date]", $order_date, $emContent);
        $emContent    = str_replace("[order_number]", $order_number, $emContent);
        $emContent    = str_replace("[break]", "<br/>", $emContent);
        $email_body   = '<table><tr><td class="main_reason">' . $emContent . '</td></tr></table>';
        return $email_body;
    }
    public function sendUserEmailCI($to, $fromname, $fromemail, $subject = '', $body = '', $attachments = array(), $filePath = '', $emailtype = '') {
        $fromname           = ($fromname ? $fromname : $this->getsettings('email', 'fromname'));
        $fromemail          = ($fromemail ? $fromemail : $this->getsettings('email', 'fromemail'));
      
        /*if($this->getsettings('smtp','status')==1){
        $config = Array(
        'protocol' => 'smtp',
        'smtp_host' =>$this->getsettings('smtp','host'),
        'smtp_port' =>intval($this->getsettings('smtp','port')), //465,
        'smtp_timeout' => '20',
        );
        if($this->getsettings('smtp','authentication')==1){
        $config['smtp_user']=$this->getsettings('smtp','username');
        $config['smtp_pass']=$this->getsettings('smtp','password');
        }
        if($this->getsettings('smtp','encrptionstatus')==1){
        $config['smtp_crypto']=$this->getsettings('smtp','encrption');
        }
        }*/
        $config['mailtype'] = "html";
        if ($this->getsettings('email', 'lanformat') == '1') {
            $config['charset'] = "utf-8";
        } else {
            $config['charset'] = "iso-8859-1";
        }
       
        $this->CI->load->library('email');
        $this->CI->email->initialize($config);
        $this->CI->email->from($fromemail, $fromname);
        $this->CI->email->to($to);
        $this->CI->email->subject($subject);
        $this->CI->email->message($body);
        if($this->CI->email->send()){
			return true;
		}
    }
    function send_msg($mob_no, $MESSAGE) {
        $msg91_status = $this->getsettings('msg91', 'status');
        $AUTHKEY      = $this->getsettings('msg91', 'key');
        if ($msg91_status != 0 && $AUTHKEY != '') {
            $SENDER = $this->getsettings('msg91', 'sender');
            $ROUTE  = "4";
            $url    = 'https://control.msg91.com/api/sendhttp.php?authkey=' . $AUTHKEY . '&mobiles=' . $mob_no . '&message=' . urlencode($MESSAGE) . '&sender=' . $SENDER . '&response=json&route=' . $ROUTE;
            $ch     = curl_init($url);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $resp = curl_exec($ch);
            $json = json_decode($resp);
            curl_close($ch);
			 
        }
		 
    }
    function upload_image($upPath, $name, $postFix = NULL) {
        $this->CI                	= get_instance();
		
		$basePath 					= explode('application',dirname(__FILE__))[0];
        $uploadPath              	= $basePath . $upPath;
        $config['upload_path']   	= $uploadPath;
        $config['allowed_types'] 	= 'gif|jpg|png';
        $this->CI->load->library('upload', $config);
		$this->CI->upload->initialize($config);
        if ($this->CI->upload->do_upload($name)) {
            //$this->upload->display_errors();	
            $uploaddata    = $this->CI->upload->data();
            $imgName       = $uploaddata['raw_name'];
            $imgExt        = $uploaddata['file_ext'];
            $randomstr     = substr(md5(microtime()), 0, 10);
            $uploadedImage = $randomstr . $postFix . $imgExt;
            rename($uploadPath . $imgName . $imgExt, $uploadPath . $uploadedImage);
            return $uploadedImage;
        } else {
            return '';
        }
    }
	
	public function resizeImage($width,$height,$source_path,$target_path)
	{
			$config_manip = array(
				'image_library' => 'gd2',
				'source_image' => $source_path,
				'new_image' => $target_path,
				'maintain_ratio' => FALSE,
				'create_thumb' => TRUE,
				'thumb_marker' => '',
				'width' => $width,
				'height' => $height
				);


			$this->CI->load->library('image_lib', $config_manip);
			
			$this->CI->image_lib->initialize($config_manip);
			if (!$this->CI->image_lib->resize()) {
				return $this->CI->image_lib->display_errors();
			}else{
			return true;
			}

			$this->CI->image_lib->clear();
	}
}
/* End of file Ts_functions.php */
?>
