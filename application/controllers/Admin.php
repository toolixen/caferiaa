<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (isset($_POST) && !empty($_POST)) {
            if (!isset($_SERVER['HTTP_REFERER'])) {
                die('Direct Access Not Allowed!!');
            }
        }

        if (!isset($this->session->userdata['rt_uid'])) {
            redirect(base_url());
        }
        if (isset($this->session->userdata['ts_uid'])) {
            if ($this->session->userdata['rt_level'] != 1) {
                redirect(base_url());
            }
        }
        $this->load->library('ts_functions');
        $this->load->helper(array('form', 'url', 'info_helper'));
        $this->load->library('form_validation');


    }

    function general_setting()
    {
        $data['general_setting'] = 1;
        $meta_info = $this->DatabaseModel->access_database('rt_genral_setting', 'select', '', array('id' => '1'));
        $blog_set = $this->DatabaseModel->access_database('rt_blog_setting', 'select', '');

        $data['page_title'] = 'Ajustes generales';
        $this->load->view('common/header', $data);
        $this->load->view('setting/general_setting', ['meta_info' => $meta_info, 'blog_set' => $blog_set]);
        $this->load->view('common/footer');
    }

    public function index()
    {

        redirect(base_url() . 'admin/dashboard');
    }

    /* Common Logout */
    public function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url());
    }

    public function dashboard()
    {

        $data['userCount'] = $this->DatabaseModel->aggregate_data('rt_users', 'user_id', 'count', array('user_accesslevel' => 2));
        $data['itemCount'] = $this->DatabaseModel->aggregate_data('rt_items', 'item_id', 'count');
        $data['orderCount'] = $this->DatabaseModel->aggregate_data('rt_paymentdetails', 'payment_id', 'count');
        $data['revenueCount'] = $this->DatabaseModel->aggregate_data('rt_paymentdetails', 'payment_amount', 'sum', array('payment_status' => 1));

        $data['accountrequestCount'] = $this->DatabaseModel->aggregate_data('rt_request', 'req_id', 'count', array('req_status' => 0));

        $join_arr = array('rt_users', 'rt_users.user_id = rt_paymentdetails.payment_uid');
        $data['ordersList'] = $this->DatabaseModel->select_data('*', 'rt_paymentdetails', '', 20, $join_arr, array('payment_id', 'desc'));

        $data['dashboard_active'] = 1;
        $data['page_title'] = 'Dashboard';
        $this->load->view('common/header', $data);
        $this->load->view('dashboard', $data);
        $this->load->view('common/footer', $data);
    }

    public function users()
    {

        $data['userList'] = $this->DatabaseModel->select_data('*', 'rt_users', array('user_accesslevel !=' => 1, 'user_deleted' => 0));
        $data['user_active'] = 1;
        $data['page_title'] = 'Usuarios';
        $this->load->view('common/header', $data);
        $this->load->view('users', $data);
        $this->load->view('common/footer', $data);
    }


    public function users_wallet_recharge_request()
    {
        $join_arr = array('rt_users', 'rt_users.user_id = rt_userwallet_recharge_request.user_id');
        $data['userList'] = $this->DatabaseModel->select_data('rt_userwallet_recharge_request.*,rt_users.user_name', 'rt_userwallet_recharge_request', array('status' => 1), '', $join_arr);

        $data['user_active'] = 1;
        $data['page_title'] = 'Users Request';
        $this->load->view('common/header', $data);
        $this->load->view('users_request', $data);
        $this->load->view('common/footer', $data);
    }


    /************* Single User details STARTS ********************/
    public function single_user($uid = '')
    {
        $data['basepath'] = base_url();
        $cond = "user_accesslevel in(2,3) AND user_id=$uid";

        $data['userdetails'] = $this->DatabaseModel->access_database('rt_users', 'select', '', $cond);
        $data['wallet_credit'] = $this->DatabaseModel->select_data('*', 'rt_wallet_credit', array('wc_uid' => $uid));

        $this->load->view('common/header', $data);
        $this->load->view('single_user', $data);
        $this->load->view('common/footer', $data);
    }

    /************* Update User STARTS **********************/
    function updateuser_backend()
    {
        $update_arr = $_POST;
        $user_id = $_POST['user_id'];
        $this->DatabaseModel->access_database('rt_users', 'update', $update_arr, array('user_id' => $user_id));
        echo 1;

    }


    /************* Offer Section Start ********************/

    public function search_items()
    {
        if (isset($_POST['searchterm'])) {
            $searchterm = trim($_POST['searchterm']);

            if (!$searchterm) {
                ?>
                <p>No search term was entered.</p>
                <?php
                exit();
            }

            $products = array();
            $finished = false;

            $likecol = 'item_name';
            $itemList = $this->DatabaseModel->get_data('rt_items', array('item_options' => 'NO'), 0, array(), array(), '', $likecol, $searchterm);

            if ($itemList) {
                ?>
                <table class="hs_datatable table">
                    <thead>
                    <tr>
                        <th>Item Name</th>
                        <th>Price</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($itemList as $soloItem) {
                        ?>
                        <tr>
                            <td>
                                <a class="kf_add_offer_items" title="Delete" data-itemid="<?= $soloItem['item_id'] ?>"
                                   data-itemname="<?= $soloItem['item_name'] ?>"
                                   data-itemprice="<?= $soloItem['item_price'] ?>"><i class="fa fa-plus-circle"
                                                                                      aria-hidden="true"></i></a>
                                <?= (strlen($soloItem['item_name']) > 55 ? substr($soloItem['item_name'], 0, 53) . '..' : $soloItem['item_name']) ?>
                            </td>
                            <td><?= $soloItem['item_price'] ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
                <?php
            } else {
                ?>
                <p>No Items were found for "<?= $searchterm ?>".</p>
                <?php
                exit();
            }

        }
    }

    public function offers()
    {
        $data['offerList'] = $this->DatabaseModel->select_data('*', 'rt_offers');
        $data['offer_active'] = 1;
        $data['page_title'] = 'Ofertas';

        $this->load->view('common/header', $data);
        $this->load->view('offers', $data);
        $this->load->view('common/footer', $data);
    }

    public function add_item_offer($offer_id = '')
    {
        $data['page_title'] = 'Añadir oferta';
        if ($offer_id != '') {
            $offer_details = $this->DatabaseModel->select_data('*', 'rt_offers', array('id' => $offer_id), '');
            if (empty($offer_details)) {
                redirect(base_url() . 'admin/add_item_offer');
            }
            $data['offer_details'] = $offer_details;
            $data['page_title'] = 'Update Offer';

        }

        $data['itemList'] = $this->DatabaseModel->select_data('*', 'rt_items');

        $data['offer_active'] = 1;

        $this->load->view('common/header', $data);
        $this->load->view('add_item_offer', $data);
        $this->load->view('common/footer', $data);
    }

    public function update_item_offer()
    {
        if ($_POST) {
            $dataArr = array(
                'offer_name' => trim($_POST['offer_name']),
                'offer_date' => trim($_POST['offer_date']),
                'offer_status' => 1
            );

            if (isset($_POST['offer_products']) && is_array($_POST['offer_products'])) {
                $ct = count($_POST['offer_products']);
                $offers_arr = array();
                if ($ct) {
                    for ($i = 0; $i < $ct; $i++) {
                        $offers_arr[] = array('item_id' => $_POST['offer_products'][$i], 'quantity' => $_POST['product_quantity'][$i]);
                    }

                    $offer_pro = json_encode($offers_arr);
                    $dataArr['offer_products'] = $offer_pro;
                }

            }

            if (empty($offer_pro)) {
                $dataArr['offer_products'] = '';
            }

            if (isset($_POST['offer_price']) && !empty($_POST['offer_price'])) {
                $dataArr['offer_price'] = trim($_POST['offer_price']);
            }

            if ($_FILES['offer_pic']['name'] != '') {
                $imagename = $this->ts_functions->upload_image('assets/admin/images/offers/', 'offer_pic');
                if ($imagename != '') {
                    if ($_POST['offer_id'] != '0') {
                        $item_detail = $this->DatabaseModel->select_data('offer_pic', 'rt_offers', array('id' => $_POST['offer_id']), 1);
                        if ($item_detail[0]['offer_pic'] != '') {
                            $path = dirname(__FILE__);
                            $abs_path = explode('application', $path);
                            unlink($abs_path[0] . $item_detail[0]['offer_pic']);
                        }
                    }
                    $dataArr['offer_pic'] = 'assets/admin/images/offers/' . $imagename;
                    $dataArr['image_thumb'] = $imagename;


                    $upload_path = 'assets/admin/images/offers/' . $imagename;
                    $target_path = 'assets/admin/images/offers/thumb';
                    $this->ts_functions->resizeImage("370", "393", $upload_path, $target_path);
                }
            }


            if ($_POST['offer_id'] == '0') {
                $user_id = $this->DatabaseModel->access_database('rt_offers', 'insert', $dataArr, '');
                $this->session->userdata['ts_success'] = 'Offer added successfully.';

            } else {

                $this->DatabaseModel->access_database('rt_offers', 'update', $dataArr, array('id' => $_POST['offer_id']));

                $this->session->userdata['ts_success'] = 'Offer updated successfully.';
            }
            redirect(base_url() . 'admin/offers');

        }
    }

    public function update_offer_status_fun()
    {
        if (isset($_POST['offer_id']) && isset($_POST['status_val'])) {
            $offer_id = $_POST['offer_id'];
            $status_val = $_POST['status_val'];

            if ($status_val != 1) {
                $status_val = 0;
            }

            $data = array('offer_status' => $status_val);

            $this->DatabaseModel->access_database('rt_offers', 'update', $data, array('id' => $offer_id));

            echo json_encode(array('status' => 1, 'msg' => 'Update Successfully'));
        }
    }

    function delete_item_offer($offer_id = '')
    {
        $offer_detail = $this->DatabaseModel->select_data('*', 'rt_offers', array('id' => $offer_id), 1);

        if ($offer_detail) {
            if ($offer_detail[0]['item_pic'] != '') {
                $path = dirname(__FILE__);
                $abs_path = explode('application', $path);
                unlink($abs_path[0] . $offer_detail[0]['item_pic']);
            }

            $this->DatabaseModel->access_database('rt_offers', 'delete', '', array('id' => $offer_id));

            $this->session->userdata['ts_success'] = ' Delete successfully.';
            redirect(base_url() . 'admin/offers');
        }

    }



    /************* Offer Section ENDS ********************/


    /************* Single User details ENDS ********************/
    public function orders($type = "")
    {
        $join_arr = array('rt_users', 'rt_users.user_id = rt_paymentdetails.payment_uid');

        $limit = '';

        if ($type == 'latest') {
            $limit = 30;
            $data['order_active_latest'] = 1;
            $data['page_title'] = 'Últimos Pedidos';
        } else {
            $data['order_active'] = 1;
            $data['page_title'] = 'Pedidos';
        }

        if ($type == 'pickup_order') {
            $data['pickuporder_tab'] = 1;
        } else if ($type == '') {
            $data['pickuporder_tab'] = 0;
        } else {
            $data['pickuporder_tab'] = 0;
        }


        $data['latest_order_type'] = $type;

        if ($type == 'latest') {
            $where_arr = array('payment_order_status <=' => '1');
            $data['ordersList'] = $this->DatabaseModel->select_data('*', 'rt_paymentdetails', $where_arr, $limit, $join_arr, array('payment_id', 'desc'));
//            if((int) $_SESSION["countOrdersList"] == count($data['ordersList'])){
//                $_SESSION["newOrders"] = "0";
//            }else{
//                $_SESSION["newOrders"] = "1";
//            }
            $flagNewOrders = 0;
            $idLastOrder = 0;
            if (count($data['ordersList']) > 0) {
                $idLastOrder = $data['ordersList'][0]['payment_id'];
                if ($idLastOrder > (int)$_SESSION["idLastOrder"]) {
                    $flagNewOrders = 1;
                } else {
                    $flagNewOrders = 0;
                }
            }
            $_SESSION["idLastOrder"] = $idLastOrder;
            $_SESSION["flagNewOrders"] = $flagNewOrders;
        } else {
            $data['ordersList'] = $this->DatabaseModel->select_data('*', 'rt_paymentdetails', '', $limit, $join_arr, array('payment_id', 'desc'));
        }
        $data['type'] = $type;
        $data['deliveryBoy'] = $this->DatabaseModel->select_data('user_id,user_name', 'rt_users', array('user_accesslevel' => 3));
        $this->load->view('common/header', $data);
        $this->load->view('orders', $data);
        $this->load->view('common/footer', $data);
    }

    /************************Items Section Start *******************/
    public function items()
    {
        $data['itemList'] = $this->DatabaseModel->select_data('*', 'rt_items');
        $data['item_active'] = 1;
        $data['page_title'] = 'Productos';

        $this->load->view('common/header', $data);
        $this->load->view('items', $data);
        $this->load->view('common/footer', $data);
    }

    public function add_item($item_id = '')
    {
        $data['page_title'] = 'Add Producto';
        if ($item_id != '') {
            $item_details = $this->DatabaseModel->select_data('*', 'rt_items', array('item_id' => $item_id), '');
            if (empty($item_details)) {
                redirect(base_url() . 'admin/add_item');
            }
            $data['item_details'] = $item_details;
            $data['page_title'] = 'Update Item';
            $data['item_optiondetails'] = $this->DatabaseModel->select_data('*', 'rt_item_options', array('main_item_id' => $item_id), '');
            // print_R($data['item_optiondetails']);die;
        }
        $data['catlist'] = $this->DatabaseModel->select_data('*', 'rt_categories');
        $data['item_active'] = 1;

        $this->load->view('common/header', $data);
        $this->load->view('add_item', $data);
        $this->load->view('common/footer', $data);
    }

    function update_item()
    {

        if ($_POST) {
            $dataArr = array(
                'item_name' => trim($_POST['item_name']),
                //'item_price' => trim($_POST['main_item_price']),
                'item_desc' => trim($_POST['item_desc']),
                'item_cat' => trim($_POST['item_cat']),
                'item_quantity' => trim($_POST['item_quantity']),
                'item_sku' => trim($_POST['item_sku']),
            );
            if (isset($_POST['main_item_price']) && $_POST['main_item_price'] != '') {
                $dataArr['item_price'] = trim($_POST['main_item_price']);
                $dataArr['item_options'] = 'NO';
            } else {
                $dataArr['item_price'] = '';
                $dataArr['item_options'] = 'YES';
            }

            if ($_FILES['item_pic']['name'] != '') {
                $imagename = $this->ts_functions->upload_image('assets/admin/images/menus/', 'item_pic');
                if ($imagename != '') {
                    if ($_POST['item_id'] != '0') {
                        $item_detail = $this->DatabaseModel->select_data('item_pic', 'rt_items', array('item_id' => $_POST['item_id']), 1);
                        if ($item_detail[0]['item_pic'] != '') {
                            $path = dirname(__FILE__);
                            $abs_path = explode('application', $path);
                            unlink($abs_path[0] . $item_detail[0]['item_pic']);
                        }
                    }
                    $dataArr['item_pic'] = 'assets/admin/images/menus/' . $imagename;
                    $dataArr['image_thumb'] = $imagename;


                    $upload_path = 'assets/admin/images/menus/' . $imagename;
                    $target_path = 'assets/admin/images/menus/thumb';
                    $this->ts_functions->resizeImage("370", "393", $upload_path, $target_path);
                }
            }


            if ($_POST['item_id'] == '0') {
                $main_item_id = $this->DatabaseModel->access_database('rt_items', 'insert', $dataArr, '');

                if (isset($_POST['prod_optionscheckbox'])) {
                    if (isset($_POST['item_size']) && $_POST['item_size'] != '') {
                        foreach ($_POST['item_size'] as $key => $value) {
                            $dataOptionArr = array('main_item_id' => $main_item_id, 'item_size' => $value, 'item_price' => $_POST['item_price'][$key]);
                            $prod_option_id = $this->DatabaseModel->access_database('rt_item_options', 'insert', $dataOptionArr, '');
                        }
                    }
                }
                $this->session->userdata['ts_success'] = 'Artículo agregado exitosamente.';
            } else {

                $this->DatabaseModel->access_database('rt_items', 'update', $dataArr, array('item_id' => $_POST['item_id']));
                $main_item_id = $_POST['item_id'];
                if (isset($_POST['prod_optionscheckbox'])) {
                    if (isset($_POST['item_size']) && $_POST['item_size'] != '') {
                        foreach ($_POST['item_size'] as $key => $value) {
                            $dataOptionArr = array('main_item_id' => $main_item_id, 'item_size' => $value, 'item_price' => $_POST['item_price'][$key]);

                            if (isset($_POST['olditem_optionid'][$key]) && $_POST['olditem_optionid'][$key] != '') {
                                $this->DatabaseModel->access_database('rt_item_options', 'update', $dataOptionArr, array('item_option_id' => $_POST['olditem_optionid'][$key]));

                            } else {
                                $prod_option_id = $this->DatabaseModel->access_database('rt_item_options', 'insert', $dataOptionArr, '');
                            }
                        }
                    }
                }
                $this->session->userdata['ts_success'] = 'Artículo actualizado con éxito.';
            }
            redirect(base_url() . 'admin/items');

        }
    }


    function delete_item($item_id = '')
    {
        $item_detail = $this->DatabaseModel->select_data('*', 'rt_items', array('item_id' => $item_id), 1);

        if ($item_detail) {
            if ($item_detail[0]['item_pic'] != '') {
                $path = dirname(__FILE__);
                $abs_path = explode('application', $path);
                unlink($abs_path[0] . $item_detail[0]['item_pic']);
            }

            $this->DatabaseModel->access_database('rt_items', 'delete', '', array('item_id' => $item_id));
            $this->DatabaseModel->access_database('rt_item_options', 'delete', '', array('main_item_id' => $item_id));
            $this->session->userdata['ts_success'] = ' Eliminado con éxito.';
            redirect(base_url() . 'admin/items');
        }

    }


    function delete_item_options()
    {
        if ($_POST['item_opt_id'] != '' && $_POST['item_opt_id'] != 0) {
            $this->DatabaseModel->access_database('rt_item_options', 'delete', '', array('item_option_id' => $_POST['item_opt_id']));
            echo 1;
        } else {
            echo 0;
        }
    }


    function delete_user($user_id = '', $user_level = '')
    {
        $user_detail = $this->DatabaseModel->select_data('*', 'rt_users', array('user_id' => $user_id, 'user_accesslevel!=' => 1), 1);
        if ($user_detail) {
            if ($user_detail[0]['user_pic'] != '') {
                $path = dirname(__FILE__);
                $abs_path = explode('application', $path);
                unlink($abs_path[0] . $user_detail[0]['user_pic']);
            }
            $this->DatabaseModel->access_database('rt_users', 'delete', '', array('user_id' => $user_id));
            $this->session->userdata['ts_success'] = ' Eliminado con éxito.';
            redirect(base_url() . 'admin/users');
        }

    }


    /************* Add New User STARTS **********************/

    function addnewuser_backend()
    {
        if (isset($_POST['user_email'])) {
            if (isset($_POST['user_email'])) {
                $user_email = $_POST['user_email'];
                $user_name = $_POST['user_name'];
                $user_pass = $_POST['user_pass'];
                $user_mobile = $_POST['user_mobile'];
                $user_accesslevel = $_POST['user_accesslevel'];

                if (filter_var($user_email, FILTER_VALIDATE_EMAIL) && strlen($user_pass) > 7 && preg_match("/^[a-zA-Z0-9\d]+$/", $user_name)) {


                    $checkEmail = $this->DatabaseModel->access_database('rt_users', 'select', '', array('user_email' => $user_email));
                    $checkMobile = $this->DatabaseModel->access_database('rt_users', 'select', '', array('user_mobile' => $user_mobile));

                    if (empty($checkEmail) && empty($checkMobile)) {

                        $data_arr = array('user_name' => $user_name, 'user_email' => $user_email, 'user_pass' => md5($user_pass), 'user_status' => 1, 'user_mobile' => $user_mobile, 'user_accesslevel' => $user_accesslevel);

                        $uid = $this->DatabaseModel->access_database('rt_users', 'insert', $data_arr, '');


                        $this->ts_functions->sendnotificationemails('addnewuseremail', $user_email, 'New Account Created', $user_name, base_url(), $user_pass);

                        echo '7#register';
                        // Register success

                    } else {
                        echo '7#exists';
                        // Email exists
                    }

                } else {
                    echo '404#js_mistake';
                    // Server Error exists
                }
            } else {
                echo '0#error';
                // Login credentials don't match
            }


        } else {
            echo '404#error';
            // False access
        }
        die();
    }

    /************* Add New User ENDS **********************/
    /************* Update User STARTS **********************/
    function user_wallet_detail()
    {
        $user_id = $_POST['user_id'];
        $wallet_amount = 0;
        $wallet_detail = $this->DatabaseModel->select_data('*', 'rt_wallet', array('wallet_uid' => $user_id), 1);
        if ($wallet_detail) {
            if ($wallet_detail[0]['wallet_amount'] != '') {
                $wallet_amount = $wallet_detail[0]['wallet_amount'];
            }
        }
        echo $wallet_amount;
    }

    function update_user_wallet()
    {
        $user_id = $_POST['wc_uid'];
        $wc_amount = $_POST['wc_amount'];
        $wc_note = $_POST['wc_note'];
        $wallet_detail = $this->DatabaseModel->select_data('*', 'rt_wallet', array('wallet_uid' => $user_id), 1);
        $user_detail = $this->DatabaseModel->select_data('user_mobile', 'rt_users', array('user_id' => $user_id), 1);
        if ($wallet_detail) {
            $nw_amount = $wallet_detail[0]['wallet_amount'] + $wc_amount;
            $this->DatabaseModel->access_database('rt_wallet', 'update', array('wallet_amount' => $nw_amount), array('wallet_id' => $wallet_detail[0]['wallet_id']));
            $wallet_amount = $nw_amount;
        } else {
            $this->DatabaseModel->access_database('rt_wallet', 'insert', array('wallet_uid' => $user_id, 'wallet_amount' => $wc_amount), '');
            $wallet_amount = $wc_amount;
        }


        if (isset($_POST['request_id'])) {
            $this->DatabaseModel->access_database('rt_userwallet_recharge_request', 'update', array('status' => 2), array('id' => $_POST['request_id']));

        }
        if (!empty($user_detail)) {
            $mobile_no = $user_detail[0]['user_mobile'];
            if ($mobile_no != '') {
                $wallet_recharge_Msg = 'Dear user Your wallet credited by Rs ' . $wc_amount . ' on ' . date('d-m-Y h:i:s');
                $this->ts_functions->send_msg($mobile_no, $wallet_recharge_Msg);
            }
        }

        $insData = array(
            'wc_uid' => $user_id,
            'wc_amount' => $wc_amount,
            'wc_note' => $wc_note,
            'wc_date' => date('Y-m-d h:i:s')
        );
        $this->DatabaseModel->access_database('rt_wallet_credit', 'insert', $insData, '');
        echo $wallet_amount;
    }


    /********* Category Settings STARTS **********/

    function categories()
    {
        $data['basepath'] = base_url();
        $data['cateList'] = $this->DatabaseModel->select_data('*', 'rt_categories', '', '');
        $data['category_active'] = 1;

        $this->load->view('common/header', $data);
        $this->load->view('category', $data);
        $this->load->view('common/footer', $data);
    }

    function add_categories()
    {
        if (isset($_POST['catename'])) {

            $cateDataArr = array();
            if ($_POST['catename'] != '') {
                $cat_info = $this->DatabaseModel->access_database('rt_categories', 'select', '', array('cat_name' => $_POST['catename']));
                if ($cat_info) {

                    if ($_FILES['cateimage']['name'] != '') {
                        $imagename = $this->ts_functions->upload_image('assets/admin/images/category/', 'cateimage');
                        $upload_path = 'assets/admin/images/category/' . $imagename;
                        $target_path = 'assets/admin/images/category/';
                        $this->ts_functions->resizeImage("150", "150", $upload_path, $target_path);
                        $cateDataArr['cat_image'] = 'assets/admin/images/category/' . $imagename;
                        $this->DatabaseModel->access_database('rt_categories', 'update', $cateDataArr, array('cat_id' => $_POST['old_cateid']));
                        $this->session->userdata['ts_success'] = 'Imagen actualizada con éxito.';
                        redirect('admin/categories');

                    } else {
                        redirect('admin/categories');
                    }

                } else {
                    $cateDataArr['cat_name'] = $_POST['catename'];
                    $cateDataArr['cat_slug'] = strtolower(str_replace(' ', '_', $_POST['catename']));
                    if ($_FILES['cateimage']['name'] != '') {
                        $imagename = $this->ts_functions->upload_image('assets/admin/images/category/', 'cateimage');
                        $upload_path = 'assets/admin/images/category/' . $imagename;
                        $target_path = 'assets/admin/images/category/';
                        $this->ts_functions->resizeImage("150", "150", $upload_path, $target_path);

                        if ($imagename != '') {

                            if ($_POST['old_cateid'] != '0') {
                                echo "die";
                                $cat_detail = $this->DatabaseModel->access_database('cat_image', 'rt_categories', '', array('cat_id' => $_POST['old_cateid']));
                                if ($cat_detail[0]['cat_image'] != '') {
                                    $path = dirname(__FILE__);
                                    $abs_path = explode('application', $path);
                                    //$pathToImages = $abs_path[0];
                                    unlink($abs_path[0] . $cat_detail[0]['cat_image']);
                                }
                            }
                            $cateDataArr['cat_image'] = 'assets/admin/images/category/' . $imagename;
                        }
                    }

                    if ($_POST['old_cateid'] == '0') {


                        $this->DatabaseModel->access_database('rt_categories', 'insert', $cateDataArr, '');

                        $this->session->userdata['ts_success'] = 'Categoría añadida con éxito.';
                    } else {
                        $this->DatabaseModel->access_database('rt_categories', 'update', $cateDataArr, array('cat_id' => $_POST['old_cateid']));

                        $this->session->userdata['ts_success'] = 'Categoría actualizada con éxito.';
                    }

                }
            } else {
                $this->session->userdata['ts_error'] = "Categoría requerida.";
            }

            redirect(base_url() . 'admin/categories');
        } else {
            echo '0';
        }
        die();
    }


    function account_request()
    {
        $data['requestList'] = $this->DatabaseModel->select_data('*', 'rt_request');
        $data['account_request'] = 1;
        $data['page_title'] = 'Solicitud de cuenta';

        $this->load->view('common/header', $data);
        $this->load->view('account_request', $data);
        $this->load->view('common/footer', $data);
    }

    function accept_request($req_id)
    {
        $requestDetail = $this->DatabaseModel->select_data('*', 'rt_request', array('req_id' => $req_id), 1);
        if ($requestDetail) {
            $user_email = $requestDetail[0]['req_email'];
            $user_mobile = $requestDetail[0]['req_mobile'];
            $user_name = explode('@', $user_email)[0];
            $user_pass = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 8);
            $user_accesslevel = 2;

            $checkEmail = $this->DatabaseModel->access_database('rt_users', 'select', '', array('user_email' => $user_email));
            $checkMobile = $this->DatabaseModel->access_database('rt_users', 'select', '', array('user_mobile' => $user_mobile));

            if (empty($checkEmail) && empty($checkMobile)) {

                $data_arr = array('user_name' => $user_name, 'user_email' => $user_email, 'user_pass' => md5($user_pass), 'user_status' => 1, 'user_mobile' => $user_mobile, 'user_accesslevel' => $user_accesslevel);

                $uid = $this->DatabaseModel->access_database('rt_users', 'insert', $data_arr, '');
                $this->ts_functions->sendnotificationemails('addnewuseremail', $user_email, 'New Account Created', $user_name, $user_pass, base_url());

                $this->DatabaseModel->access_database('rt_users', 'rt_request', array('req_status' => 1), array('req_id' => $req_id));

                $this->session->userdata['ts_error'] = "User created successfully";

            } else {
                $this->session->userdata['ts_error'] = "User Email or Mobile already exists";
            }

        }
        redirect(base_url() . 'admin/account_request');
    }

    function delete_request($req_id)
    {
        $this->DatabaseModel->access_database('rt_request', 'delete', '', array('req_id' => $req_id));

        $this->session->userdata['ts_success'] = ' Eliminado con éxito.';
        redirect(base_url() . 'admin/account_request');
    }


    function Front_menu_setting()
    {
        if (!empty($_POST['status'])) {
            if ($this->DatabaseModel->access_database('rt_header_menu', 'update', array('status' => $_POST['status']), array('id' => $_POST['id']))) {

                echo 1;
            }
        } elseif (!empty($_POST)) {
            $id = $_POST['id'];
            unset($_POST['id']);
            if ($this->DatabaseModel->access_database('rt_header_menu', 'update', $_POST, array('id' => $id))) {
                $this->session->userdata['ts_success'] = 'Menú actualizado con éxito.';
                redirect(base_url() . 'admin/Front_menu_setting');
            }

        } else {

            $data['menus'] = $this->DatabaseModel->select_data('*', ' rt_header_menu', '', '');
            $data['Front_menu_setting'] = 1;
            $data['img'] = $this->DatabaseModel->select_data('*', 'rt_images', array('id' => 1), '');
            $data['img'] = $data['img'][0];
            $data['page_title'] = 'Ajuste del menú';
            $this->load->view('common/header', $data);
            $this->load->view('setting/menu', $data);
            $this->load->view('common/footer', $data);
        }
    }

    function testimonial_setting($id = null)
    {
        if (!empty($_POST)) {

            if ($_FILES['image']['name'] != '') {
                $imagename = $this->ts_functions->upload_image('assets/admin/images/testimonial/', 'image');
                if ($imagename != '') {
                    $upload_path = 'assets/admin/images/testimonial/' . $imagename;
                    $target_path = 'assets/admin/images/testimonial/thumb';
                    $this->ts_functions->resizeImage("150", "150", $upload_path, $target_path);
                    $_POST['image'] = $imagename;
                }
            }


            if (!empty($_POST['id'])) {
                $id = $_POST['id'];
                unset($_POST['id']);
                if ($this->DatabaseModel->access_database('rt_testimonial', 'update', $_POST, array('id' => $id))) {
                    $this->session->userdata['ts_success'] = 'Artículo actualizado con éxito.';
                }

            } else {
                $this->DatabaseModel->access_database('rt_testimonial', 'insert', $_POST, '');
                $this->session->userdata['ts_success'] = 'Artículo agregado exitosamente.';
            }
            redirect(base_url() . 'admin/testimonial_setting');


        } else {

            if (!empty($id)) {
                if ($this->DatabaseModel->access_database('rt_testimonial', 'delete', '', array('id' => $id))) {
                    $this->session->userdata['ts_success'] = 'Artículo actualizado con éxito.';
                    redirect(base_url() . 'admin/testimonial_setting', 'refresh');
                }
            }

            $data['testimonials'] = $this->DatabaseModel->select_data('*', 'rt_testimonial', '', '');
            $data['testimonial_setting'] = 1;
            $data['img'] = $this->DatabaseModel->select_data('*', 'rt_images', array('id' => 1), '');
            $data['img'] = $data['img'][0];
            $data['page_title'] = 'Testimonial ';
            $this->load->view('common/header', $data);
            $this->load->view('setting/testimonial', $data);
            $this->load->view('common/footer', $data);
        }
    }

    public function delete_testimonial()
    {
        if ($this->DatabaseModel->access_database('rt_testimonial', 'update', array('status' => $_POST['status']), array('id' => $_POST['id']))) {

            echo 1;
        }
    }

    function info_setting()
    {
        if (!empty($_POST['status'])) {
            if ($this->DatabaseModel->access_database('rt_info', 'update', array('status' => $_POST['status']), array('id' => $_POST['id']))) {

                echo 1;
            }
        } elseif (!empty($_POST)) {
            $id = $_POST['id'];
            unset($_POST['id']);
            if ($this->DatabaseModel->access_database('rt_info', 'update', $_POST, array('id' => $id))) {
                $this->session->userdata['ts_success'] = 'Información actualizada con éxito.';
                redirect(base_url() . 'admin/info_setting');
            }

        } else {

            $data['infos'] = $this->DatabaseModel->select_data('*', ' rt_info', '', '');
            $data['info_setting'] = 1;
            $data['page_title'] = 'Configuración de información ';
            $this->load->view('common/header', $data);
            $this->load->view('setting/info', $data);
            $this->load->view('common/footer', $data);
        }
    }


    function Manage_time_schedule($id = null)
    {

        if (!empty($_POST)) {
            if (isset($_POST['tid'])) {
                if ($this->DatabaseModel->access_database('rt_time_schedule', 'update', array('status' => $_POST['status']), array('id' => $_POST['tid']))) {
                    echo 1;
                }
            } elseif (!empty($_POST['id'])) {

                $time_arr['open_time'] = $_POST['opening_time'];
                $time_arr['close_time'] = $_POST['closing_time'];
                $_POST['description'] = json_encode($time_arr);
                unset($_POST['opening_time']);
                unset($_POST['closing_time']);
                $id = $_POST['id'];
                unset($_POST['id']);
                if ($this->DatabaseModel->access_database('rt_time_schedule', 'update', $_POST, array('id' => $id))) {
                    $this->session->userdata['ts_success'] = 'Horario actualizado correctamente.';
                    redirect(base_url() . 'admin/Manage_time_schedule');
                }

            } else {
                $time_arr['open_time'] = $_POST['opening_time'];
                $time_arr['close_time'] = $_POST['closing_time'];
                $_POST['description'] = json_encode($time_arr);
                unset($_POST['opening_time']);
                unset($_POST['closing_time']);

                $this->DatabaseModel->access_database('rt_time_schedule', 'insert', $_POST, '');
                $this->session->userdata['ts_success'] = 'Horario de tiempo agregado con éxito.';
                redirect(base_url() . 'admin/Manage_time_schedule');
            }


        } else {

            if (!empty($id)) {
                if ($this->DatabaseModel->access_database('rt_time_schedule', 'delete', '', array('id' => $id))) {
                    $this->session->userdata['ts_success'] = 'Horario de tiempo eliminado correctamente.';
                    redirect(base_url() . 'admin/Manage_time_schedule', 'refresh');
                }
            }

            $data['timess'] = $this->DatabaseModel->select_data('*', 'rt_time_schedule', '', '');
            $data['time_setting'] = 1;
            $data['page_title'] = 'Agregar Tiempo ';
            $this->load->view('common/header', $data);
            $this->load->view('setting/time', $data);
            $this->load->view('common/footer', $data);
        }
    }

    function blog_category($id = null)
    {

        if (!empty($_POST)) {
            if (!empty($_POST['status'])) {
                if ($this->DatabaseModel->access_database('rt_blog_category', 'update', array('status' => $_POST['status']), array('id' => $_POST['id']))) {
                    echo 1;
                }
            } elseif (!empty($_POST['id'])) {

                $_POST['slug'] = strtolower(str_replace(' ', '_', $_POST['title']));

                $id = $_POST['id'];
                unset($_POST['id']);
                if ($this->DatabaseModel->access_database('rt_blog_category', 'update', $_POST, array('id' => $id))) {
                    $this->session->userdata['ts_success'] = 'Blog Category updated successfully.';
                    redirect(base_url('admin/blog_category'));
                }

            } else {

                $this->form_validation->set_rules('title', 'Category Name', 'required|is_unique[rt_blog_category.title]');

                if ($this->form_validation->run() == FALSE) {
                    $data['categories'] = $this->DatabaseModel->select_data('*', 'rt_blog_category', array('is_delete' => 1), '');
                    $data['blog_category'] = 1;
                    $data['img'] = $this->DatabaseModel->select_data('*', 'rt_images', array('id' => 1), '');
                    $data['img'] = $data['img'][0];
                    $this->load->view('common/header', $data);
                    $this->load->view('blog/category');
                    $this->load->view('common/footer');
                } else {
                    $_POST['slug'] = strtolower(str_replace(' ', '_', $_POST['title']));

                    $this->DatabaseModel->access_database('rt_blog_category', 'insert', $_POST, '');
                    $this->session->userdata['ts_success'] = 'Blog Category added successfully.';
                    redirect(base_url('admin/blog_category'));
                }
            }


        } else {

            if (!empty($id)) {
                if ($this->DatabaseModel->access_database('rt_blog_category', 'update', array('is_delete' => 0), array('id' => $id))) {
                    $this->session->userdata['ts_success'] = 'Blog Category Deleted successfully.';
                    redirect(base_url('admin/blog_category'));
                }
            }

            $data['categories'] = $this->DatabaseModel->select_data('*', 'rt_blog_category', array('is_delete' => 1), '');
            $data['blog_category'] = 1;
            $data['page_title'] = 'Blog Category';
            $this->load->view('common/header', $data);
            $this->load->view('blog/category', $data);
            $this->load->view('common/footer', $data);
        }
    }


    function add_blog($id = '')
    {
        if (!empty($_POST)) {
            if (empty($id)) {
                $this->form_validation->set_rules('title', 'Title', 'required|max_length[50]|is_unique[rt_blogs.title]');
            } else {
                $this->form_validation->set_rules('title', 'Title', 'required|max_length[50]');
            }
            $this->form_validation->set_rules('tags', 'Tags', 'required');
            $this->form_validation->set_rules('category', 'Category', 'required');
            $this->form_validation->set_rules('content[]', 'Content', 'required');
            $this->form_validation->set_rules('author_name', 'Author Name', 'required');
            $this->form_validation->set_rules('author_profile', 'Author Profile', 'required');
            $this->form_validation->set_rules('author_quote', 'Author Quote', 'required');
            $this->form_validation->set_rules('facebook_url', 'Facebook URL', "trim|required|prep_url|valid_url");
            $this->form_validation->set_rules('twitter_url', 'Twitter URL', "trim|required|prep_url|valid_url");
            $this->form_validation->set_rules('linkedin_url', 'Linkedin URL', "trim|required|prep_url|valid_url");

            if ($this->form_validation->run() == FALSE) {
                if (!empty($id)) {
                    $data['blog'] = $this->DatabaseModel->select_data('*', 'rt_blogs', array('id' => $id), '');
                }
                $data['blog_cats'] = $this->DatabaseModel->select_data('*', 'rt_blog_category', '', '');
                $data['page_title'] = 'Blogs';
                $data['add_blog'] = 1;
                $this->load->view('common/header', $data);
                $this->load->view('blog/add_blog');
                $this->load->view('common/footer');
            } else {


                if ($_FILES['blog_img']['name'] != '') {
                    $imagename = $this->ts_functions->upload_image('assets/admin/images/blog/', 'blog_img');
                    if ($imagename != '') {
                        $_POST['blog_img'] = $imagename;
                        $upload_path = 'assets/admin/images/blog/' . $imagename;
                        $target_path = 'assets/admin/images/blog/';
                        $this->ts_functions->resizeImage("850", "450", $upload_path, $target_path);

                        $upload_path = 'assets/admin/images/blog/' . $imagename;
                        $target_path = 'assets/admin/images/blog/thumb';
                        $this->ts_functions->resizeImage("370", "274", $upload_path, $target_path);
                    }
                }
                if ($_FILES['author_img']['name'] != '') {
                    $imagename = $this->ts_functions->upload_image('assets/admin/images/blog/', 'author_img');
                    if ($imagename != '') {
                        $upload_path = 'assets/admin/images/blog/' . $imagename;
                        $target_path = 'assets/admin/images/blog';
                        $this->ts_functions->resizeImage("120", "140", $upload_path, $target_path);
                        $_POST['author_img'] = $imagename;
                    }
                }
                $_POST['content'] = serialize($_POST['content']);
                $_POST['slug'] = strtolower(str_replace(' ', '-', $_POST['title']));
                if (!empty($id)) {

                    if ($this->DatabaseModel->access_database('rt_blogs', 'update', $_POST, array('id' => $id))) {
                        $this->session->userdata['ts_success'] = 'Blog updated successfully.';
                        redirect(base_url('admin/add_blog'));
                    }
                } else {


                    $this->DatabaseModel->access_database('rt_blogs', 'insert', $_POST, '');
                    $this->session->userdata['ts_success'] = 'Blog added successfully.';
                    redirect(base_url('admin/add_blog'));
                }

            }
        } else {
            $data = array();
            $data['img'] = $this->DatabaseModel->select_data('*', 'rt_images', array('id' => 1), '');
            $data['img'] = $data['img'][0];
            $data['blog_cats'] = $this->DatabaseModel->select_data('*', 'rt_blog_category', '', '');
            $data['page_title'] = 'Blogs';
            $data['add_blog'] = 1;

            if ($id == 'add') {

                $this->load->view('common/header', $data);
                $this->load->view('blog/add_blog');
                $this->load->view('common/footer');
            } elseif (!empty($id)) {
                $data['blog'] = $this->DatabaseModel->select_data('*', 'rt_blogs', array('id' => $id), '');
                $this->load->view('common/header', $data);
                $this->load->view('blog/add_blog');
                $this->load->view('common/footer');

            } else {


                $data['blogs'] = $this->DatabaseModel->select_data('*', 'rt_blogs', '', '');
                $this->load->view('common/header', $data);
                $this->load->view('blog/blog');
                $this->load->view('common/footer');
            }

        }

    }

    function delete_blog($id = '')
    {
        if (!empty($_POST)) {
            if (!empty($_POST['status'])) {
                if ($this->DatabaseModel->access_database('rt_blogs', 'update', array('status' => $_POST['status']), array('id' => $_POST['id']))) {
                    echo 1;
                }
            }
        } else {

            $this->DatabaseModel->access_database('rt_blogs', 'delete', '', array('id' => $id));
            $this->session->userdata['ts_success'] = ' Eliminado con éxito.';
            redirect(base_url() . 'admin/add_blog');
        }

    }

    function contact_us()
    {
        if (empty($_POST)) {
            $data['contacts'] = $this->DatabaseModel->select_data('*', 'rt_contact_us', '', '');

            $data['contactus'] = 1;
            $data['page_title'] = 'Customer Queries';
            $this->load->view('common/header', $data);
            $this->load->view('contactus/index');
            $this->load->view('common/footer');
        } else {
            if ($this->DatabaseModel->access_database('rt_contact_us', 'update', array('status' => $_POST['status']), array('id' => $_POST['id']))) {
                echo 1;
            }
        }

    }

    function feedback()
    {
        $data['customer_feedback'] = $this->DatabaseModel->select_data('*', 'rt_feedback', '', '', '', array('id', 'desc'));
        if ($data['customer_feedback']) {
            $i = 0;
            foreach ($data['customer_feedback'] as $cust_feed) {
                $user_details = $this->DatabaseModel->select_data('*', 'rt_users', array('user_id' => $cust_feed['user_id']), '');
                if ($user_details) {
                    $data['customer_feedback'][$i]['user_name'] = $user_details[0]['user_name'];

                } else {
                    $data['customer_feedback'][$i]['user_name'] = '';
                }
                $i++;
            }
        }
        $data['feedback'] = 1;
        $data['page_title'] = 'Comentarios del cliente';
        $this->load->view('common/header', $data);
        $this->load->view('contactus/feedback');
        $this->load->view('common/footer');
    }

    function booking()
    {
        if (empty($_POST)) {
            $data['bookings'] = $this->DatabaseModel->select_data('*', 'rt_booking', '', '');
            $data['cust_booking'] = 1;
            $data['page_title'] = 'Customer Booking';
            $this->load->view('common/header', $data);
            $this->load->view('booking/index');
            $this->load->view('common/footer');
        } else {
            $bookings_details = $this->DatabaseModel->select_data('mobile', 'rt_booking', array('id' => $_POST['id']), '');

            if ($this->DatabaseModel->access_database('rt_booking', 'update', array('status' => $_POST['status']), array('id' => $_POST['id']))) {
                if ($_POST['status'] == '1') {
                    if (!empty($bookings_details)) {
                        $mobile_no = $bookings_details[0]['mobile'];

                        $user_details = $this->DatabaseModel->select_data('user_id,user_name', 'rt_users', array('user_mobile' => $mobile_no), '');

                        if (!empty($user_details)) {
                            $uname = $user_details[0]['user_name'];
                            $msg = 'Hi, ' . $uname . ' Your Table Booked Suucessfully.';
                            $insert_arr = array(
                                'message' => $msg,
                                'userid' => $user_details[0]['user_id'],
                                'date' => date('d/m/y'),
                            );
                            $this->DatabaseModel->access_database('rt_notification', 'insert', $insert_arr);
                        }
                    }
                }
                echo 1;
            }
        }

    }

    function admin_setting()
    {

        if (!empty($_POST)) {
            $uniq_id = $_POST['uniq_id'];

            if ($this->DatabaseModel->access_database('rt_settings', 'update', $_POST, array('uniq_id' => $uniq_id))) {
                $this->session->userdata['ts_success'] = 'Configuración actualizada con éxito.';
                redirect(base_url() . 'admin/admin_setting');
            }

        } else {

            $data['infos'] = $this->DatabaseModel->select_data('*', ' rt_settings', '', '', '', array('uniq_id', 'ASC'));
            $data['admin_setting'] = 1;
            $data['page_title'] = 'Configuración de administrador ';
            $this->load->view('common/header', $data);
            $this->load->view('setting/admin_setting', $data);
            $this->load->view('common/footer', $data);
        }
    }

    public function multiple_upload()
    {

        $data['img'] = $this->DatabaseModel->select_data('*', 'rt_images', array('id' => 1), '');
        $data['img'] = $data['img'][0];
        $data['multiple_upload'] = 1;
        $data['page_title'] = 'Subir imágenes';

        $this->load->view('common/header', $data);
        $this->load->view('setting/multiple_upload', $data);
        $this->load->view('common/footer', $data);
    }

    public function addImages()
    {

        if ($_POST['document'] == 'img_logo') {
            $config['width'] = '134';
            $config['height'] = '60';

        }
        if ($_POST['document'] == 'img_logo_footer') {
            $config['width'] = '114';
            $config['height'] = '63';
        }
        if ($_POST['document'] == 'img_special_offer') {
            $config['width'] = '467';
            $config['height'] = '279';
        }
        if ($_POST['document'] == 'img_contact_us') {
            $config['width'] = '440';
            $config['height'] = '717';
        }
        /*if($_POST['document'] == 'img_banner_down'){
						  $config['max_width']     = '1921';
						  $config['max_height']    = '951';
					}
					if($_POST['document'] == 'img_banner_up'){
						  $config['max_width']     = '100';
						  $config['max_height']    = '150';
					}*/
        if ($_POST['document'] == 'fevicon') {
            $config['width'] = '32';
            $config['height'] = '32';
        }

        $config['upload_path'] = './assets/front/images/front_imges/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['encrypt_name'] = TRUE;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('image')) {
            echo "error not";
            die;
            $error = array('error' => $this->upload->display_errors());

            $this->session->userdata['ts_error'] = $error['error'];
            redirect(base_url() . 'admin/multiple_upload');
        } else {


            $upload_data = $this->upload->data();
            $image = 'assets/front/images/front_imges/' . $upload_data['file_name'];
            $upload_path = $image;
            $target_path = 'assets/front/images/front_imges/';
            $this->ts_functions->resizeImage($config['width'], $config['height'], $upload_path, $target_path);
            $document = $this->input->post('document');
            $data = ["$document" => $image];

            if ($this->DatabaseModel->access_database('rt_images', 'update', $data, array('id' => 1))) {
                $this->session->userdata['ts_success'] = 'Imagen actualizada con éxito.';
                redirect(base_url() . 'admin/multiple_upload');
            }

        }

    }

    public function send_invoice($order_no = null)
    {
        $this->load->model('Front_model');
        $this->load->library('email');
        if (!empty($order_no)) {
            $invoice = $this->Front_model->GenerateInvoice($order_no);
            $p_info = $invoice['invoice'];
            $pay_address = explode('|', $invoice['payment_address']);

            $img = $this->DatabaseModel->select_data('*', 'rt_images', array('id' => 1), '');
            $img = $img[0];

            //
//            $config = array(
//                'protocol' => 'smtp',
//                'smtp_host' => 'smtp.gmail.com',
//                'smtp_port' => 587, //465,
//                'smtp_timeout' => '20',
//            );
//            $config['smtp_user'] = 'lv.store.offer@gmail.com';
//            $config['smtp_pass'] = 'lv2019store..';
//            $config['smtp_crypto'] = 'tls';
            //

            $config['charset'] = 'utf-8';
            $config['wordwrap'] = TRUE;
            $config['mailtype'] = 'html';

            //$config['protocol'] = 'sendmail';

            $this->email->initialize($config);
            $from = $this->ts_functions->getsettings('email', 'fromemail');
            $fromn = $this->ts_functions->getsettings('email', 'fromname');
            $this->email->from($from, $fromn);
            $this->email->to($invoice['user_email']);
            $this->email->subject('' . $fromn . ' Order Invoice');

            $tablerow = "";
            foreach ($p_info as $order) {
                $tablerow .= '
			<tr>
			<td class="service">' . $order['item_name'] . '</td>
			<td class="desc">' . $order['item_sku'] . '</td>
			<td class="unit">' . key_text("portalcurreny_symbol") . ' ' . $order['item_price'] . '</td>
			<td class="qty">' . $order['qty'] . '</td>
			<td class="total">' . key_text("portalcurreny_symbol") . ' ' . $order['subtotal'] . '</td>
			</tr>';


            }
            $this->email->message('

				<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

				<style>
				.clearfix:after {
				content: "";
				display: table;
				clear: both;
				}

				a {
				color: #5D6975;
				text-decoration: underline;
				}

				body {
				position: relative;
				width: 21cm; 
				height: 29.7cm; 
				margin: 0 auto; 
				color: #001028;
				background: #FFFFFF; 
				font-family: Arial, sans-serif; 
				font-size: 12px; 
				font-family: Arial;
				}

				header {
				padding: 10px 0;
				margin-bottom: 30px;
				}

				#logo {
				text-align: center;
				margin-bottom: 10px;
				}

				#logo img {
				width: 90px;
				}

				h1 {
				border-top: 1px solid #5D6975;
				border-bottom: 1px solid #5D6975;
				color: #5D6975;
				font-size: 2.4em;
				line-height: 1.4em;
				font-weight: normal;
				text-align: center;
				margin: 0 0 20px 0;
				background: url(dimension.png);
				}

				#project {
				float: left;
				}

				#project span {
				color: #5D6975;
				text-align: right;
				width: 52px;
				margin-right: 10px;
				display: inline-block;
				font-size: 0.8em;
				}

				#company {
				float: right;
				text-align: right;
				}

				#project div,
				#company div {
				white-space: nowrap; 
				}

				table {
				width: 100%;
				border-collapse: collapse;
				border-spacing: 0;
				margin-bottom: 20px;
				}

				table tr:nth-child(2n-1) td {
				background: #F5F5F5;
				}

				table th,
				table td {
				text-align: center;
				}

				table th {
				padding: 5px 20px;
				color: #5D6975;
				border-bottom: 1px solid #C1CED9;
				white-space: nowrap; 
				font-weight: normal;
				}

				table .service,
				table .desc {
				text-align: left;
				}

				table td {
				padding: 20px;
				text-align: right;
				}

				table td.service,
				table td.desc {
				vertical-align: top;
				}

				table td.unit,
				table td.qty,
				table td.total {
				font-size: 1.2em;
				}

				table td.grand {
				border-top: 1px solid #5D6975;;
				}

				#notices .notice {
				color: #5D6975;
				font-size: 1.2em;
				}

				footer {
				color: #5D6975;
				width: 100%;
				height: 30px;
				position: absolute;
				bottom: 0;
				border-top: 1px solid #C1CED9;
				padding: 8px 0;
				text-align: center;
				}
				</style>
				</head>


				<body>
				<header class="clearfix">
				<div id="logo">
				<img src="' . base_url($img["img_logo"]) . '">
				</div>
				<h1>INVOICE PEDIDO NO: ' . $invoice['payment_uniqid'] . '</h1>

				<div id="company" class="clearfix">
				<b>Facturado a:</b>
				<div>' . $invoice['user_name'] . '</div>
				<div>' . $pay_address[0] . '</div>
				<div>' . $pay_address[2] . '</div>
				<div>' . $pay_address[3] . '</div>
				<div>' . $pay_address[1] . ' - ' . $pay_address[4] . '</div>
				<br>
				<br>
				<b>Fecha de orden:</b>
				<div> ' . date_format(date_create($invoice['payment_date']), "Y/m/d") . '</div>
				</div>

				<div id="project">
				<b>Enviado a:</b>
				<div>' . $invoice['user_name'] . '</div>
				<div>' . $pay_address[0] . '</div>
				<div>' . $pay_address[2] . '</div>
				<div>' . $pay_address[3] . '</div>
				<div>' . $pay_address[1] . ' - ' . $pay_address[4] . '</div>
				<br>
				<br>
				<b>Método de pago:</b>
				<div> ' . $invoice['payment_mode'] . '</div>
				<div> ' . $invoice['user_email'] . '</div>
				<div> ' . $invoice['user_mobile'] . '</div>
				</div>
				</header>

				<main>
				<table>
				<thead>
				<tr>
				<th class="service">Producto</th>
				<th class="desc">SKU</th>
				<th class="unit">Precio</th>
				<th class="qty">Cantidad</th>
				<th class="total" >Total</th>

				</tr>
				</thead> 
				<tbody>

				' . $tablerow . '

				<tr>
				<td colspan="4">SUBTOTAL</td>
				<td class="total">' . key_text("portalcurreny_symbol") . ' ' . $invoice['payment_amount'] . '</td>
				</tr>
				<tr>
				<td colspan="4" class="grand total">GRAND TOTAL</td>
				<td class="grand total">' . key_text("portalcurreny_symbol") . ' ' . $invoice['payment_amount'] . '</td>
				</tr>
				</tbody>
				</table>
				</main>
				<footer>
				La factura se creó en una computadora y es válida sin la firma y el sello.
				</footer>
				</body>	
				');

            if ($this->email->send()) {
                $this->session->userdata['ts_success'] = 'Invoice Sent successfully.';
                redirect(base_url('admin/orders'));
            }
             echo $this->email->print_debugger();


        }
    }


    public function exportcontact()
    {
        if ($this->DatabaseModel->contactexport()) {
            redirect(base_url('admin/contact_us'));
        }
    }

    function compliamce_pages()
    {
        $data['compliamce_pages'] = 1;
        $data['page_title'] = 'Compliance Pages';
        $setting_data = $this->DatabaseModel->access_database('rt_genral_setting', 'select', array('id' => '1'));
        $this->load->view('common/header', $data);
        $this->load->view('compliamce_pages', ['setting_data' => $setting_data]);
        $this->load->view('common/footer');

    }

    function compliamce_info()
    {
        $editor_data = $this->input->post('tc');
        $field = $this->input->post('field');
        $date_type = $this->input->post('date_type');
        $arr = array();
        if ($field == 'terms_condition' || $field == 'privacy_policy') {
            $arr = array($field => $editor_data, $date_type => date("jS  F Y "));
        } else {
            $arr = array($field => $editor_data);
        }
        $res = $this->DatabaseModel->access_database('rt_genral_setting', 'update', $arr, array('id' => '1'));
        if ($res) {
            echo "Update";
        } else {
            echo "Not Update";
        }

    }

    public function discount_coupon()
    {


        $data['all_offers'] = $this->DatabaseModel->access_database('rt_discount_coupons', 'select', '');
        $data['all_products'] = $this->DatabaseModel->select_data('*', 'rt_items');

        $data['dicount_active'] = 1;
        $data['page_title'] = 'Cupón de descuento';
        $this->load->view('common/header', $data);
        $this->load->view('discount_coupon/discount_coupons', $data);
        $this->load->view('common/footer', $data);
    }

    public function edit_offer()
    {
        if (isset($_POST['offer_id']) && $_POST['offer_id'] != '') {
            $where_data = array('offer_id' => $_POST['offer_id'],);
            $query = $this->DatabaseModel->select_data('*', 'rt_discount_coupons', $where_data, '');

            if ($query) {
                $query[0]['discounted_product'] = json_decode($query[0]['discounted_product']);


                $resp = array('status' => 1, 'edit_offer' => $query);
            } else {
                $resp = array('status' => 0, 'error' => 'algo salió mal. ¡Inténtalo de nuevo!');
            }
        } else {
            $resp = array('status' => 0, 'error' => 'algo salió mal. ¡Inténtalo de nuevo!');
        }
        echo json_encode($resp);
    }

    public function add_offer()
    {

        $this->form_validation->set_rules('offer_heading', 'Heading', 'required');
        $this->form_validation->set_rules('offer_title', 'Title', 'required');
        $this->form_validation->set_rules('offer_descr', 'Description', 'required');
        $this->form_validation->set_rules('coupen_code', 'Coupen Code', 'required');
        $this->form_validation->set_rules('coupen_uses_limit', 'Uses Limit', 'required');
        $this->form_validation->set_rules('discount_type', 'Discount Type', 'required');
        $this->form_validation->set_rules('discount_amount', 'Discount Amount', 'required');
        //$this->form_validation->set_rules('apply_discount_to','Apply Discount To', 'required');
        $this->form_validation->set_rules('coupen_expire_date', 'Expire Date', 'required');
        if ($this->form_validation->run() == FALSE) {
            echo 0;

        } else {
            $check_coupencode = 0;
            $where_data = array('coupen_code' => $this->input->post('coupen_code'));
            $query = $this->DatabaseModel->select_data('*', 'rt_discount_coupons', $where_data, '');
            if ($query) {

                if ($query[0]['coupen_code'] == $this->input->post('coupen_code')) {
                    $check_coupencode++;
                }
            }

            if ($check_coupencode == 0) {
                $discounted_prod = 0;
                if (isset($_POST['discounted_product'])) {
                    $discounted_prod = json_encode($_POST['discounted_product']);
                }
                if (isset($_FILES['offer_image']) && $_FILES['offer_image']['name'] !== "") {
                    $config['upload_path'] = 'assets/admin/images/coupon/';
                    $config['allowed_types'] = 'gif|jpg|png|jpeg';
                    $this->load->library('upload');
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('offer_image')) {

                        echo 0;
                    } else {
                        $imgdata = array('upload_data' => $this->upload->data());
                        foreach ($imgdata as $path) {
                            $imagename = $path['file_name'];
                        }
                        $imagedata = 'assets/admin/images/coupon/' . $imagename;

                        $config['width'] = '467';
                        $config['height'] = '279';
                        $image = 'assets/admin/images/coupon/' . $imagename;
                        $upload_path = $image;
                        $target_path = 'assets/admin/images/coupon/';
                        $this->ts_functions->resizeImage($config['width'], $config['height'], $upload_path, $target_path);
                    }
                    $data = array(
                        'offer_heading' => $this->input->post('offer_heading'),
                        'offer_title' => $this->input->post('offer_title'),
                        'offer_description' => $this->input->post('offer_descr'),
                        'create_at' => date('d M Y h:i:s'),
                        'offer_image' => $imagedata,
                        'coupen_code' => $this->input->post('coupen_code'),
                        'coupen_uses_limt' => $this->input->post('coupen_uses_limit'),
                        'discount_type' => $this->input->post('discount_type'),
                        'discount_amount' => $this->input->post('discount_amount'),
                        'apply_discount_to' => 'Total Amount', //$this->input->post('apply_discount_to'),
                        //'discounted_product'=>$discounted_prod,
                        'coupen_expire_date' => $this->input->post('coupen_expire_date'),
                        'offer_featured' => 0,
                        'user_used_limit' => $this->input->post('user_usedlimit'),
                    );
                } else {

                    $data = array(
                        'offer_heading' => $this->input->post('offer_heading'),
                        'offer_title' => $this->input->post('offer_title'),
                        'offer_description' => $this->input->post('offer_descr'),
                        'create_at' => date('d M Y h:i:s'),
                        'coupen_code' => $this->input->post('coupen_code'),
                        'coupen_uses_limt' => $this->input->post('coupen_uses_limit'),
                        'discount_type' => $this->input->post('discount_type'),
                        'discount_amount' => $this->input->post('discount_amount'),
                        'apply_discount_to' => 'Total Amount', //$this->input->post('apply_discount_to'),
                        //'discounted_product'=>$discounted_prod,
                        'coupen_expire_date' => $this->input->post('coupen_expire_date'),
                        'offer_featured' => 0,
                        'user_used_limit' => $this->input->post('user_usedlimit'),
                    );

                }

                $this->DatabaseModel->access_database('rt_discount_coupons', 'insert', $data, '');
                if ($this->db->affected_rows() > 0) {
                    echo 1;
                } else {
                    echo 0;
                }
            } else {
                echo 2;
            }

        }
    }

    public function update_offer()
    {
        //print_R($_POST);DIE;
        $this->form_validation->set_rules('up_offer_heading', 'Heading', 'required');
        $this->form_validation->set_rules('up_offer_title', 'Title', 'required');
        $this->form_validation->set_rules('up_offer_descr', 'Description', 'required');
        $this->form_validation->set_rules('up_coupen_code', 'Coupen Code', 'required');
        $this->form_validation->set_rules('up_coupen_uses_limit', 'Uses Limit', 'required');
        $this->form_validation->set_rules('up_discount_type', 'Discount Type', 'required');
        $this->form_validation->set_rules('up_discount_amount', 'Discount Amount', 'required');
        //$this->form_validation->set_rules('up_apply_discount_to','Apply Discount To', 'required');
        $this->form_validation->set_rules('up_coupen_expire_date', 'Expire Date', 'required');
        if ($this->form_validation->run() == FALSE) {
            echo 0;

        } else {

            $discounted_prod = 0;
            if (isset($_POST['up_discounted_product'])) {
                $discounted_prod = json_encode($_POST['up_discounted_product']);
            }

            if (isset($_FILES['up_offer_image']) && $_FILES['up_offer_image']['name'] !== "") {
                $config['upload_path'] = 'assets/admin/images/coupon/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $this->load->library('upload');
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('up_offer_image')) {

                    echo 0;
                } else {
                    $imgdata = array('upload_data' => $this->upload->data());
                    foreach ($imgdata as $path) {
                        $imagename = $path['file_name'];
                    }
                    $imagedata = 'assets/admin/images/coupon/' . $imagename;

                    $config['width'] = '467';
                    $config['height'] = '279';
                    $image = 'assets/admin/images/coupon/' . $imagename;
                    $upload_path = $image;
                    $target_path = 'assets/admin/images/coupon/';
                    $this->ts_functions->resizeImage($config['width'], $config['height'], $upload_path, $target_path);
                }
                $data = array(
                    'offer_heading' => $this->input->post('up_offer_heading'),
                    'offer_title' => $this->input->post('up_offer_title'),
                    'offer_description' => $this->input->post('up_offer_descr'),
                    'offer_image' => $imagedata,
                    'coupen_code' => $this->input->post('up_coupen_code'),
                    'coupen_uses_limt' => $this->input->post('up_coupen_uses_limit'),
                    'discount_type' => $this->input->post('up_discount_type'),
                    'discount_amount' => $this->input->post('up_discount_amount'),
                    'apply_discount_to' => 'Total Amount', //$this->input->post('up_apply_discount_to'),
                    //'discounted_product'=>$discounted_prod,
                    'coupen_expire_date' => $this->input->post('up_coupen_expire_date'),
                    'user_used_limit' => $this->input->post('up_user_usedlimit'),
                );
            } else {

                $data = array(
                    'offer_heading' => $this->input->post('up_offer_heading'),
                    'offer_title' => $this->input->post('up_offer_title'),
                    'offer_description' => $this->input->post('up_offer_descr'),
                    'coupen_code' => $this->input->post('up_coupen_code'),
                    'coupen_uses_limt' => $this->input->post('up_coupen_uses_limit'),
                    'discount_type' => $this->input->post('up_discount_type'),
                    'discount_amount' => $this->input->post('up_discount_amount'),
                    'apply_discount_to' => 'Total Amount', //$this->input->post('up_apply_discount_to'),
                    //'discounted_product'=>$discounted_prod,
                    'coupen_expire_date' => $this->input->post('up_coupen_expire_date'),
                    'user_used_limit' => $this->input->post('up_user_usedlimit'),
                );
            }

            $this->DatabaseModel->access_database('rt_discount_coupons', 'update', $data, array('offer_id' => $this->input->post('old_offer_id')));
            echo 1;
        }
    }

    public function update_offer_status()
    {
        $data = array('offer_status' => $this->input->post('offer_value'));
        $this->DatabaseModel->access_database('rt_discount_coupons', 'update', $data, array('offer_id' => $this->input->post('offer_id')));
        if ($this->db->affected_rows() > 0) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function update_offer_featured()
    {

        $data = array('offer_featured' => $this->input->post('offer_value'));
        $this->DatabaseModel->access_database('rt_discount_coupons', 'update', $data, array('offer_id' => $this->input->post('offer_id')));
        if ($this->db->affected_rows() > 0) {
            echo 1;
        } else {
            echo 0;
        }
    }


    public function delete_offer()
    {

        if ($this->uri->segment(3)) {
            $old_offer_id = $this->uri->segment(3);
            $where_data = array('offer_id' => $old_offer_id);
            $check_image = $this->DatabaseModel->select_data('*', 'rt_discount_coupons', $where_data, '');

            if ($check_image) {
                $image_path = $check_image[0]['offer_image'];
                if (file_exists($image_path)) {
                    unlink($image_path);
                }
            }
            $this->DatabaseModel->access_database('rt_discount_coupons', 'delete', '', array('offer_id' => $old_offer_id));

            redirect(base_url("admin/discount_coupon"));
        }
    }

    function get_itemoptionsList()
    {
        $item_str = '';
        if (isset($_POST['item_id'])) {
            $item_options = $this->DatabaseModel->select_data('*', 'rt_item_options', array('main_item_id' => $_POST['item_id']), '');
            if (!empty($item_options)) {

                foreach ($item_options as $options) {
                    $item_str .= '<tr><td>' . $options['item_size'] . '</td><td>' . key_text('portalcurreny_symbol') . ' ' . $options['item_price'] . '</td></tr>';
                }
                $resp = array('status' => 1, 'option_list' => $item_str);

            } else {

                $resp = array('status' => 2, 'option_list' => $item_str);
            }

        } else {
            $resp = array('status' => 0, 'option_list' => $item_str);

        }
        echo json_encode($resp);
    }
}
