<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		//$this->user_info1=$this->is_login();
		$this->load->model(array('Front_model'));
		$this->load->library(array('ts_functions','form_validation','session'));
		$this->load->helper(array('info_helper'));
		$this->load->library(array('encryption','image_lib','cart','email'));
		$this->user_info1	= $this->is_login();
	}
	public function index(){
	
		$data['menus']  		= $this->Front_model->getDataResult('rt_header_menu',array()); ;
		$data['infos']  		= $this->Front_model->getDataResult('rt_info',array('status'=>1)); ;
		$data['times']  		= $this->Front_model->getDataResult('rt_time_schedule',array('status'=>1)); ;
		$data['reating']  		= $this->Front_model->getDataResult('rt_rating',array()); ;
		$data['testimonials']   = $this->Front_model->getDataResult('rt_testimonial',array('status'=>1)); 
		$data['Item_cat']   	= $this->Front_model->getDataResult('rt_categories',array('cat_status'=>1));
		$data['general']  		= $this->Front_model->getData('rt_genral_setting',array('id'=>1));
		$data['blogs']  		= $this->Front_model->getblog(); 
		$data['blog_cat']  		= $this->Front_model->getblogcat(); 
		$data['discount_coupon'] = $this->DatabaseModel->select_data('*', 'rt_discount_coupons', array('offer_status'=>1,'offer_featured'=>1),1); 
		$data['user_info']		= $this->is_login();
		
		$data['img']  = $this->DatabaseModel->select_data('*','rt_images',array('id'=>1),'');
		$data['img'] =$data['img'][0];  
		if(!empty($data['user_info'])){
			$data['wallet']		= $this->Front_model->getData('rt_wallet',array('wallet_uid' => $data['user_info']['user_id']));
		}
		
		
		$i=0;
		foreach($data['blogs']  as $blog){
		     $comments = $this->Front_model->getDataResult('rt_blog_comment',array('blog_id'=>$blog['id'] ));	
			 $data['blogs'][$i]['comment'] =  count($comments);
		$i++;
		}
		
		$this->session->unset_tempdata('item_id');
		$data['items']  = $this->Front_model->getItems();
		 
		$prod = [];	
		foreach($data['items']  as $item){
			$prod[] = $item['item_id'];
		}
		if(!empty($prod)){
			$this->session->set_tempdata('item_id',$prod);
		}
	 
		$data['page']  = 'main';
		$this->load->view('front/include/header',$data);
		$this->load->view('front/index',$data);
		$this->load->view('front/include/footer');
	}
	public function auth($action=null)
	{
		if(!empty($_POST)){
			
			if($action == 'signup' ){
				
				   $this->form_validation->set_rules('user_name','Name ','trim|required');
				   $this->form_validation->set_rules('user_email','Email','trim|required|valid_email|is_unique[rt_users.user_email]');
					if(isset($_POST['user_mobile'])){
						$this->form_validation->set_rules('user_mobile','Mobile','trim|required|numeric|is_unique[rt_users.user_mobile]');
						$data['user_mobile'] = $_POST['user_mobile'];
				   }
				   $this->form_validation->set_rules('user_pass','Password','required|min_length[6]');
				   if($this->form_validation->run()==false)
				   {
					 		$error['error']=strip_tags(validation_errors());	
							$error['error']= explode("\n",$error['error']);
							$error['error'] = $error['error'][0];		
							$this->response('failed',$error);
				   }else{	
							$data['user_email'] =  $_POST['user_email'];
							$data['user_name'] = $_POST['user_name'];
							$data['user_pass'] 		= md5($_POST['user_pass']);
							$data['user_accesslevel'] 	= 2;
							$random  =   rand() ;
							$data['user_otp']			= $random;
					  if($this->Front_model->insertData('rt_users',$data)){
							 
							  $body = '<a href="'.site_url('api/verify_account/' . $random ) .'"> click here to verify account </a>' ;
							  $Msg_signup =site_url('api/verify_account/'. $random ).' click here to verify account';
							  
							  //$this->ts_functions->sendUserEmailCI( $_POST['user_email'] ,null,null,'Account Varify',$body);
							  $this->ts_functions->sendnotificationemails('emailveri',$_POST['user_email'] ,'E-Mail Verification','',$body);
							  
							  
							  
							  if(isset($data['user_mobile']) && $data['user_mobile'] !='' ){
									$this->ts_functions->send_msg($data['user_mobile'],$Msg_signup);
							  }
							  $this->response('success',['success'=>['msg'=>"Signup successfully! Varification mail has sent to your mail or mobile"]]);
							  
							  
							 
					   }else{
							$this->response('failed',['failed'=>['msg'=>"Something went wrong!Try Again"]]);
						 
					   }					   
				   }
				
			}else if($action == 'login' ){
						
						$this->form_validation->set_rules('user_email','Email','trim|required');
						$this->form_validation->set_rules('user_pass','Password','trim|required');
					   
						
						if($this->form_validation->run()==false)
						{
							$error['error']=strip_tags(validation_errors());	
							$error['error']= explode("\n",$error['error']);
							$error['error'] = $error['error'][0];		
							$this->response('failed',$error);
						}else{	
							$res = $this->valid_email($_POST['user_email']);
							if($res =='email' || $res =='mobile'){
									
									if(isset($_POST['remember']) && $_POST['remember'] == 1){
											setcookie('email', $_POST['user_email'], time() + (86400 * 30), "/"); setcookie('pass', $_POST['user_pass'], time() + (86400 * 30), "/");
									}
									unset($_POST['remember']);
									$pwd = md5($_POST['user_pass']);
									$email_mobile = $_POST['user_email'];
									$where_condition ="user_pass='$pwd' AND (user_email='$email_mobile' OR user_mobile='$email_mobile')";
									if( $row = $this->Front_model->getData('rt_users',$where_condition)){
										if($row['user_accesslevel'] != 1){
										if($row['user_status'] == 3 ){	
										 $this->response('failed',['failed'=>['msg'=>"Account blocked by admin"]]);
										}elseif($row['user_status'] == 0){
										 $this->response('failed',['failed'=>['msg'=>"Verify your account email address"]]);
										}else{
										
   											$array = array(
															'id' 		=> $row['user_id'],
															'is_login'	=>	true
															);
											$this->session->set_userdata($array);
										
										    $this->response('success',['success'=>['msg'=>"Login successfully"]]);										
										}		
									}else{
										$this->response('failed',['failed'=>['msg'=>"Please login as Admin "]]);										
									}
									}else{
										
										$this->response('failed',['failed'=>['msg'=>"email / mobile or password may be wrong ! Try Again"]]);
									}
										
							}else{
								$this->response('failed',['failed'=>['msg'=>"Please enter valid email or mobile"]]);
							}
						}
			
			
			}else if($action == 'forgot' ){
				
			   $this->form_validation->set_rules('user_email','Email','trim|required');
			   if($this->form_validation->run()==false)
			   {
				    $error['error']=strip_tags(validation_errors());	
					$error['error']= explode("\n",$error['error']);
					$error['error'] = $error['error'][0];		
					$this->response('failed',$error);
			   
			   }else{
				   
				   $res = $this->valid_email($_POST['user_email']);
			       if($res =='email' || $res =='mobile')
			       { 
			           $email_mobile = $_POST['user_email'];
		           $where_condition ="user_email='$email_mobile' OR user_mobile='$email_mobile'";
				   if($this->Front_model->getData('rt_users',$where_condition))
					{
						   $token =rand();  // $token = random_string('alnum', 8);
						   $row = $this->Front_model->getData('rt_users',$where_condition);
						        
								if($this->Front_model->updateData('rt_users', array('user_otp'=>$token) ,$where_condition )){
								    
								    $body	 ='<a href=" '. base_url("front/reset_password/1/").$token. '/'. $row['user_id'] . ' "> Reset Password </a>';
									$forgot_Msg	 = base_url("front/reset_password/1/").$token. '/'. $row['user_id'] . ' Reset Password ';
									if($res =='email'){
									$setting = $this->Front_model->getData('rt_settings',array('key_text'=>'email_fromemail'));
									$from	 =  $setting['value_text'];
									$to		 =	$email_mobile;
									$subject =  'Reset Password';		
									$this->ts_functions->sendnotificationemails('forgotps',$to ,'Reset Password','',$body);
									
									}else if($res =='mobile'){
										$this->ts_functions->send_msg($email_mobile,$forgot_Msg);
									}
									
									$this->response('success',['success'=>['msg'=>"Check your email or mobile ! Reset Link sent"]]);
							   }else{
								  echo json_encode(['failed'=>['msg'=>"Something went wrong!Try Again"]]);
							   }
					
					
					}else{
						$this->response('failed',['failed'=>['msg'=>"this email or mobile not available in record ! Try Again"]]);
					}
					
			       }else{
			           $this->response('failed',['failed'=>['msg'=>"Please enter valid email or mobile"]]);
			       }
					
				}
			} 
		}else{
			redirect(base_url());
		}
	}
	
	function valid_email($str) {
		if(preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)){
			return 'email';
		}else if(!preg_match ("/[^0-9]/", $str)){
			return 'mobile';
		}else{
			return false;
		}
	}
	
	function response($param = null,$response= null){
		
	if($param != null && $response != null)	
		if($param == 'success'){
			
			$this->output
					->set_status_header(200)
					->set_content_type('application/json', 'utf-8')
					->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
					->_display();
			exit;
		}else{
			
			$this->output
					->set_status_header(403)
					->set_content_type('application/json', 'utf-8')
					->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
					->_display();
			exit;		
		}
	
	}
	
	function is_login(){
		if($this->session->userdata('is_login')){
			$id = $this->session->userdata('id');
			if( $row = $this->Front_model->getData('rt_users',array('user_id'=> $id ))){
				return $row ;
			}
			
		}else{
				return array() ;
		}
	}
	function logout()
	{
		$this->session->unset_userdata('id','is_login');
		//$this->session->sess_destroy();
		redirect(base_url());
	}
	
	function contact_us(){
		
				
				   $this->form_validation->set_rules('name','Name ','trim|required|min_length[4]|max_length[30]');
				   $this->form_validation->set_rules('email','Email','trim|required|valid_email');
				   $this->form_validation->set_rules('contact','Contact','trim|required|numeric');
				   $this->form_validation->set_rules('subject','Subject','trim|required');
				   $this->form_validation->set_rules('description','Description','trim|required|max_length[350]');
				   if($this->form_validation->run()==false)
				   {
					 		$error['error']=strip_tags(validation_errors());	
							$error['error']= explode("\n",$error['error']);
							$error['error'] = $error['error'][0];		
							$this->response('failed',$error);
				   }else{
		
							$_POST['created_at']			= date('Y-m-d h:i:s') ;
					
					  if($this->Front_model->insertData('rt_contact_us',$_POST)){

							  $body   = $_POST['description']  ;
							  $subject = $_POST['subject']  ;
							  $name = $_POST['name']  ;
							  $email = $_POST['email']  ;
							  
							  $row = $this->Front_model->getData('rt_settings',array('key_text'=>'email_contactemail'));
							  
							  $this->ts_functions->sendUserEmailCI( $row['value_text'] ,$name,$email,$subject,$body);
						 
							 
							  $this->response('success',['success'=>['msg'=>"Recibimos tu mensaje, nos pondremos en contacto contigo lo antes posible"]]);
					   }else{
							$this->response('failed',['failed'=>['msg'=>"Something went wrong!Try Again"]]);
						 
					   }					   
				   }
				
	}
	 
	public function reset_password($id = null,$accessToken = null,$user_id = null){
		
		
			if(empty($_POST)){
				if(!empty($id) OR !empty($accessToken) OR !empty($user_id) ){
					  
					 
					if($this->Front_model->getData('rt_users',array('user_otp' => $accessToken ))){
						 
						redirect('front/index?reset=pw&at='. $accessToken .'&ui='. $user_id );
					}else{
						echo "Something Went Wrong OR Link Expired <a href='". base_url() ."'>Back</a>" ;die;
					}
				}
		}else{
		
			  $this->form_validation->set_rules('password','New Password ','trim|required|min_length[3]');
			  $this->form_validation->set_rules('cpassword','confirm password','trim|required|matches[password]');
			  $this->form_validation->set_rules('user_otp','acc token','required');
			  $this->form_validation->set_rules('user_id','User id','required');
			   
				if($this->form_validation->run()==false)
				{
						$error['error']=strip_tags(validation_errors());	
						$error['error']= explode("\n",$error['error']);
						$error['error'] = $error['error'][0];		
						$this->response('failed',$error);

				}else{
						$password = md5($_POST['password']);
					  if($this->Front_model->updateData('rt_users',array('user_pass'=>$password,'user_otp'=> 0),array('user_id'=>$_POST['user_id'], 'user_otp'=>$_POST['user_otp'] )) > 0){
							  $this->response('success',['success'=>['msg'=>"new password has updated"]]);
					  }
					else{
							$this->response('failed',['failed'=>['msg'=>"Something went wrong!Try Again"]]);
						}
				}
		}
	
	}
	
	function booking(){
				   $this->form_validation->set_rules('booking_date','Booking Date ','trim|required');
				   $this->form_validation->set_rules('booking_time','Booking Time','trim|required');
				   $this->form_validation->set_rules('name','Full Name','trim|required|min_length[4]|max_length[30]');
				   $this->form_validation->set_rules('mobile','Mobile','trim|required|numeric');
				   //$this->form_validation->set_rules('email','Email','trim|required|valid_email');
				   $this->form_validation->set_rules('description','Message','trim|required|max_length[350]');
				   if($this->form_validation->run()==false)
				   {
					 		$error['error']=strip_tags(validation_errors());	
							$error['error']= explode("\n",$error['error']);
							$error['error'] = $error['error'][0];		
							$this->response('failed',$error);
				   }else{
							
							$_POST['booking_date']  = date('Y-m-d' ,strtotime($_POST['booking_date']));
							$_POST['created_at']	= date('Y-m-d h:i:s') ;
					
					  if($this->Front_model->insertData('rt_booking',$_POST)){
							$this->response('success',['success'=>['msg'=>"Your booking request has received"]]);
					   }else{
							$this->response('failed',['failed'=>['msg'=>"Something went wrong!Try Again"]]);
						 
					   }					   
				   }
	}
	
	public function encrypt(){
		echo random_string('alnum', 8);
	
		// echo $encrypt = $this->encryption->encrypt('123');
		// echo "<br>";
		
		// echo $this->encryption->decrypt($encrypt);
	}
	public function uniq_value($holder){
		$pids = array();
		foreach ($holder as $h) {
			$pids[] = $h['cat_slug'];
		}
		return array_unique($pids);
	}
	function get_pro(){
		if(!empty($_POST)){
			
		if(isset($_POST['cat_id'])){
				$items  = $this->Front_model->getItems('',$_POST['cat_id']); 
			}else{
				$items  = $this->Front_model->getItems(); 
		}
			
		
		$reating  = $this->Front_model->getDataResult('rt_rating',array());
		$prod = [];
		foreach($items as $item){
			if($item['item_price'] =='' && $item['item_options'] == 'YES')
			{ 	$item_options = $this->DatabaseModel->select_data('*','rt_item_options',array('main_item_id'=>$item['item_id']),'');
				$item_price = key_text('portalcurreny_symbol').$item_options[0]['item_price'];
				$addToCart_btn='<a href="javascript:void(0);" onclick="showItemOption('.$item['item_id'].'); void(0); return false;"><span class="fd_transition"><i class="flaticon-online-shopping-cart"></i></span></a>';
			}else{  
				$item_price = key_text('portalcurreny_symbol').$item['item_price'];
				$addToCart_btn ='<a href="javascript:void(0);" onclick="addCart('.$item['item_id'].' , 1); void(0); return false;"><span class="fd_transition"><i class="flaticon-online-shopping-cart"></i></span></a>'; 
			}
			?>
				<div class="col-md-4 col-sm-6 col-xs-12 element-item <?php echo $item['cat_slug'] ?>" >
					<div class="fd_shop_box">
						<div class="fd_shop_img">
							<img src="<?php echo base_url('assets/admin/images/menus/thumb/' .$item['image_thumb']) ?>" alt="" class="img-fluid">
							<div class="fd_shop_detail fd_transition">
								<h2><?php echo $item['item_name'] ?></h2>
								<h3><?php echo $item_price; ?></h3>
							</div>
						</div>
						<div class="fd_shop_overlay fd_transition">
						<?php echo $addToCart_btn; ?>
							<!--<a href="javascript:void(0);" onclick="addCart(<?php //echo  $item['item_id'] ?> , 1); void(0); return false;"><span class="fd_transition"><i class="flaticon-online-shopping-cart"></i></span></a>-->
							<ul class="fd_rating fd_transition">
								<li><a href="javascript:;"><i <?php if(round(rating($reating,$item['item_id'])) >0) { ?> class="fa fa-star" <?php } else { ?> class="fa fa-star-half-o"  <?php  }  ?>  aria-hidden="true"></i></a></li>
								<li><a href="javascript:;"><i <?php if(round(rating($reating,$item['item_id'])) >= 2) { ?> class="fa fa-star" <?php } else { ?> class="fa fa-star-half-o"  <?php  }  ?> aria-hidden="true"></i></a></li>
								<li><a href="javascript:;"><i <?php if(round(rating($reating,$item['item_id'])) >=3) { ?> class="fa fa-star" <?php } else { ?> class="fa fa-star-half-o"  <?php  }  ?> aria-hidden="true"></i></a></li>
								<li><a href="javascript:;"><i <?php if(round(rating($reating,$item['item_id'])) >= 4) { ?> class="fa fa-star" <?php } else { ?> class="fa fa-star-half-o"  <?php  }  ?> aria-hidden="true"></i></a></li>
								<li><a href="javascript:;"><i <?php if(round(rating($reating,$item['item_id'])) >= 5) { ?> class="fa fa-star" <?php } else { ?> class="fa fa-star-half-o"  <?php  }  ?> aria-hidden="true"></i></a></li>
							</ul>
							<h2 class="fd_transition"><?php echo $item['item_name'] ?></h2>
							<h3 class="fd_transition"><?php echo $item_price; ?></h3>
						</div>
					</div>
				</div>
			<?php
			
			$prod[] =  $item['item_id'];
			
		}
			
				$prod1 = $this->session->tempdata('item_id');
				if(!empty($prod1)){
					$merge = array_merge($prod,$prod1);
					$this->session->set_tempdata('item_id',$merge,90000);	
				}
				
			
	}
	
	}
	
	function blogSearch(){
		if(!empty($_POST['search']))
		$results = $this->Front_model->getSearchBlog($_POST['search']);
		if(!empty($results)){
		foreach($results as $result){
		?>	
			<a href="#" onclick="singleBlock(<?php echo $result['id'] ?>); void(0); return false;"><li > <?php echo $result['title'] ?></li></a>
		<?php
		
		}
	  }else{
		  echo "<a><li> Records Not Available</li></a>";
		  
	  }
	}
	
	
	function singleBlock(){
		if(!empty($_POST['blog_id'])){
		$blog = $this->Front_model->getData('rt_blogs',array('id'=>$_POST['blog_id']));
		$comments = $this->Front_model->getDataResult('rt_blog_comment',array('blog_id'=>$_POST['blog_id'] ));
		
		$email = ''; $name  = '';
		$user_info	= $this->is_login();
		if(!empty($user_info)){
			$email = $user_info['user_email'];
			$name = $user_info['user_name'];
		}
		$count_comment = count($comments ); 
		?>
				<div class="fd_blog_box clear_shadow">
							<div class="fd_blog_img">
								<a href="javascript:;"><img width="850px" height="450px" src="<?php echo base_url(); ?>assets/admin/images/blog/<?php echo $blog['blog_img'] ?>" alt="" class="img-fluid"></a>
							</div>
						
							<div class="fd_blog_detail clear_space">
								<div class="fd_blog_meta fd_padder_bottom20">
									<ul>
										<li><span><i class="flaticon-calendar"></i>
										<?php    $time=strtotime($blog['created_at']);
												 echo $month=substr(date("F",$time),0,3);
												 echo $date=date("d",$time).',';
												 echo $year=date("y",$time);               ?>
			 							</span></li>
										<?php if(!$this->session->userdata('is_login')){ ?>
										<li>
											<a href="" onclick="addlike(); void(0); return false;">
											<i class="flaticon-thumbs-up-hand-symbol"></i><?php echo $blog['like_count'] ?>
											</a>
										</li>
										<?php }else{ ?>
										 <li>
											 <a href="#"  id="likebutton<?php echo $blog['id'] ?>" onclick="addlike(<?php echo $blog['id'] ?>,<?php echo  (get_like($blog['id']) == 1)?0:1; ?>); void(0); return false;">
											
											<?php if(get_like($blog['id']) == 1){ ?>
											  <i class="fa fa-thumbs-down" aria-hidden="true"></i><?php echo $blog['like_count'] ?>	
											<?php }else{ ?>
											  <i class="flaticon-thumbs-up-hand-symbol"></i><?php echo $blog['like_count'] ?>
											<?php } ?>
											</a>
										</li>
										<?php } ?>
										<li><a href="#atcomment"><i class="flaticon-speech-bubbles-comment-option"></i><?php echo $count_comment ?></a></li>
									</ul>
									<div class="fd_share_wrapper">
										<span class="flaticon-share"></span>
										
										<ul class="fd_social_icon fd_transition">
											<li><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url(); ?>front/oneblog/<?php echo $blog['slug']; ?>&title=&summary=&source="   class="linkedin fd_transition"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
											<li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url(); ?>front/oneblog/<?php echo $blog['slug']; ?>" class="facebook fd_transition"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
											<li><a href="https://twitter.com/home?status=<?php echo base_url(); ?>front/oneblog/<?php echo $blog['slug']; ?>" class="twitter fd_transition"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
										</ul>
									</div>
								</div>
								<h2 class="fd_subheading"><?php echo $blog['title'] ?></h2>
								<?php 
									$contents = unserialize($blog['content']);
									for($i=0; $i < count($contents) ; $i++){
								?>	
									<p><?php echo $contents[$i]  ?></p>
								<?php }?>
								<div class="fd_tags_wrapper">
									<ul>
										<li><span>tags -</span></li>
									<?php 
										$tags =  explode(',',$blog['tags']);
									$k=1;	
									foreach($tags as $tag){ ?>
										<li><a href="javascript:;"><?php echo $tag ?> <?php if(count($tags) != $k ){echo ',';} ?> </a></li>
									<?php $k++;  } ?>	
									</ul>
								</div>
							</div>
						</div>
									
						<br>			
						
						<div class="fd_author_section fd_padder_bottom60">
							<h1 class="fd_heading_with_line fd_subheading">About Author</h1>
							
							<ul>
								<li>
									<div class="fd_author">
										<div class="fd_author_img">
											<img width="120px" height="140px" src="<?php echo base_url(); ?>assets/admin/images/blog/<?php echo $blog['author_img'] ?>" alt="" class="img-fluid">
										</div>
										<div class="fd_author_detail">
											<div class="fd_author_share fd_padder_bottom20">
												<h3 class="fd_author_heading fd_subheading">about Author</h3>
												<ul class="fd_social_icon fd_transition">
													<li><a href="<?php echo $blog['facebook_url'] ?>" class="fd_transition"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
													<li><a href="<?php echo $blog['twitter_url'] ?>" class="fd_transition"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
													<li><a href="<?php echo $blog['linkedin_url'] ?>" class="fd_transition"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
													<li><a href="#" class="fd_transition"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
												</ul>
											</div>
											<p><?php echo $blog['author_quote'] ?>.</p>
											<h4 class="fd_subheading"><?php echo $blog['author_name'] ?> - <span><?php echo $blog['author_profile'] ?></span></h4>
										</div>
									</div>
								</li>
							</ul>
						</div>
						<?php if(set_blog('comment_section') == 1){ ?>
						<div class="fd_comment_section fd_padder_bottom60">
							<h1 class="fd_heading_with_line fd_subheading">comments</h1>
							
							<ul id="RecentComment">
							<?php foreach($comments as $comment){ ?>
								<li>
									<div class="fd_cooment_box fd_transition">
										<div class="fd_comment_img">
											<?php echo substr($comment['name'],0,1) ?>
										</div>
										<div class="fd_comment_detail">
											<h2 class="fd_subheading"><?php echo $comment['name'] ?></h2>
											<ul>
												<li><a href="#">
												<?php
														 $time=strtotime($comment['created_date']);
														 echo $date	=	date("d",$time). ' ';	
														 echo $month=	date("F",$time). '' ;
														 echo $year=date("Y",$time);              ?>
												
												</a></li>
											</ul>
											<p><?php echo $comment['comment'] ?></p>
										</div>
									</div>
								</li>
							<?php } ?>	
							
							</ul>
						</div>
					
						<div class="fd_comment_form_section" id="atcomment">
							<h1 class="fd_heading_with_line fd_subheading">leave a comments</h1>
							
							<div class="fd_comment_form">
								<div class="row">
									<div class="col-md-6 col-sm-12 col-xs-12">
									<input type="hidden" value="<?php echo $blog['id'] ?>" id="Bl_id" >
										<div class="form-group">
											<input type="text"  id="Bl_name" class="form-control" placeholder="Enter Your Name" value="<?php echo $name ?>" readonly>
											
										</div>
									</div>
									<div class="col-md-6 col-sm-12 col-xs-12">
										<div class="form-group">
											<input type="text" id="Bl_email" class="form-control" placeholder="Enter Your Email" value="<?php echo $email ?>" readonly>
										</div>
									</div>
									<div class="col-md-12 col-sm-12 col-xs-12">
										<div class="form-group">
											<textarea id="Bl_message" class="form-control" placeholder="Message"></textarea>
										</div>
									</div>
									<div class="col-md-12 col-sm-12 col-xs-12">
									<?php if($this->session->userdata('is_login')){ ?>
										<a href="#"  onclick="submitComment(); void(0); return false;" class="fd_btn">post comment</a>
									<?php }else{  ?>
										<button class="fd_btn" onclick="toastr.error('Login for post a comment...', '');" >post comment </button>
									<?php } ?>	
										
									</div>
								</div>
							</div>
						</div>
					<?php } ?>		
						
		<?php
		
	}
	}
	
	function submitComment(){
				   $this->form_validation->set_rules('name','Name','required');	
				   $this->form_validation->set_rules('email','Email','trim|required|valid_email');
				   $this->form_validation->set_rules('comment','Comment','trim|required|max_length[200]');
				
				   if($this->form_validation->run()==false)
				   {
					 		$error['error']=strip_tags(validation_errors());	
							$error['error']= explode("\n",$error['error']);
							$error['error'] = $error['error'][0];		
							$this->response('failed',$error);
				   }else{
				
							$_POST['created_date'] = date('Y-m-d h:i:s') ;
					  if($this->Front_model->insertData('rt_blog_comment',$_POST)){
							 ?>
							  <li>
								<div class="fd_cooment_box fd_transition">
										<div class="fd_comment_img">
											<?php echo substr($_POST['name'],0,1) ?>
										</div>
										<div class="fd_comment_detail">
											<h2 class="fd_subheading"><?php echo $_POST['name'] ?></h2>
											<ul>
												<li><a href="#">
												<?php
														 $time=strtotime($_POST['created_date']);
														 echo $date	=	date("d",$time). ' ';	
														 echo $month=	date("F",$time). '' ;
														 echo $year=date("Y",$time);              ?>
												
												</a></li>
											</ul>
											<p><?php echo $_POST['comment'] ?></p>
										</div>
									</div>
								</li>
							 
							 
							 <?php
					   }else{
							$this->response('failed',['failed'=>['msg'=>"Something went wrong!Try Again"]]);
						 
					   }					   
				   }
	}
	
	
	function getBlogOfCat(){
		if(!empty($_POST)){
		$blogs = $this->Front_model->getDataResult('rt_blogs',$_POST);
			$i=0;
			foreach($blogs as $blog){ 
			$comments = $this->Front_model->getDataResult('rt_blog_comment',array('blog_id'=>$blog['id'] ));	
			$comments =  count($comments);
			?> 
				<div class="col-md-6">
					<div class="fd_blog_box">
						<div class="fd_blog_img">
							<a class="blog_popup_open" onclick="singleBlock(<?php echo $blog['id'] ?>); void(0); return false;"><img src="<?php echo base_url(); ?>assets/admin/images/blog/thumb/<?php echo $blog['blog_img'] ?>" alt="blog" class="img-fluid"></a>
						</div>
						<div class="fd_blog_detail">
							<div class="fd_blog_meta fd_padder_bottom20">
								<ul>
									<li><span><i class="flaticon-calendar"></i>
									<?php
									 $time=strtotime($blog['created_at']);
									 echo $month=substr(date("F",$time),0,3);
									 echo $date=date("d",$time).',';
									 echo $year=date("y",$time);
									 
									?>
									</span></li>
									<li><a><i class="flaticon-thumbs-up-hand-symbol"></i><?php echo $blog['like_count']?></a></li>
									<li><a><i class="flaticon-speech-bubbles-comment-option"></i><?php echo $comments ?></a></li>
								</ul>
								<div class="fd_share_wrapper">
									<span class="flaticon-share"></span>
									
									<ul class="fd_social_icon fd_transition">
										 <li><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url(); ?>front/oneblog/<?php echo $blog['id'] ?>&title=&summary=&source="   class="linkedin fd_transition"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
											<li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url(); ?>front/oneblog/<?php echo $blog['id'] ?>" class="facebook fd_transition"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
											<li><a href="https://twitter.com/home?status=<?php echo base_url(); ?>front/oneblog/<?php echo $blog['id'] ?>" class="twitter fd_transition"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
										</ul>
								</div>
							</div>
							<h2 class="fd_subheading"><a class="blog_popup_open" onclick="singleBlock(<?php echo $blog['id'] ?>); void(0); return false;"><?php echo $blog['title'] ?></a></h2>
							<?php 
								$contents = unserialize($blog['content']);
								for($i=0; $i < 1 ; $i++){
							?>	
								<p><?php echo substr ($contents[$i] ,0,80) ?></p>
							<?php }?>
							<a class="fd_read_more blog_popup_open" onclick="singleBlock(<?php echo $blog['id'] ?>); void(0); return false;">read more</a>
						</div>
					</div>
				</div>
			
		<?php $i++; } 
					
	}
	}
	
	public function likebutton(){
		if(!empty($_POST)){
		$uid = 	$this->session->userdata('id');
			if($count = $this->Front_model->addLikeCount($_POST['blog_id'],$_POST['status'],$uid)){
				if($_POST['status'] == 1){
					echo '<i class="fa fa-thumbs-down" aria-hidden="true"></i>'. $count;
				}else{
					echo '<i class="flaticon-thumbs-up-hand-symbol"></i>'. $count;
				}
				
			}
		}
	}
	
	
	
	public function add_ajax_cart()
		{	$cart=$this->cart->contents(); 
			
			$product_id=$this->input->post('productid');
			$product=$this->Front_model->getData('rt_items',array('item_id'=>$product_id));
			$product['item_size'] = 'Single';
			
			/* IF Item Size Available */  
			if(isset($_POST['item_variation']) && $_POST['item_variation'] !=''){
				
				$item_options = $this->DatabaseModel->select_data('item_size,item_price','rt_item_options',array('main_item_id'=>$product_id,'item_size'=>$_POST['item_variation']),'1'); 
				if(!empty($item_options)){
					
					$product['item_price'] =$item_options[0]['item_price']; 
					$product['item_size'] = $item_options[0]['item_size']; 
				}
			}
			
			
			
				$cart=$this->cart->contents();
				
				$qty = 0;	
				foreach($cart as $item)
				{
				
					if($item['id'] === $product_id && $item['options']['size'] === $product['item_size']){
						$qty=$item['qty'];
					}
				}
				
				if($product['item_quantity'] > $qty)
				{
					$res = preg_replace("/[^a-zA-Z0-9\s]/", "", $product['item_name']);	
					$data = array(
									'id'      => $product['item_id'],
									'qty'     => 1,
									'price'   => $product['item_price'],
									'name'    => $res,
									'sku_no'  => $product['item_sku'],
									'image'   => $product['image_thumb'],
									'options'  => array('size'=>$product['item_size']),
								  );
						 $this->cart->insert($data);
				}else{
					echo "false";die;
				}
				$this->frontcart();
		}
	
	
	
	
	public function remove_ajax_cartitem()
	{
		$product_rowid=$this->input->post('proRowid');
		 
		$cart=$this->cart->contents();
		$rowid="all";
		if(!empty($cart))
		{
					foreach($cart as $item)
							{
								if($item['rowid'] === $product_rowid){
									$rowid=$item['rowid'];
								}
								
							}
		
		
					if ($rowid==="all")
					{                      
						$this->cart->destroy();   
					}else
					{
						$data = array(
							'rowid'   => $rowid,
							'qty'     => 0
						   );
											 
						$this->cart->update($data);
					}
	    }
					
		       
		
				$this->frontcart();
		
	}
	
	public function minus_ajax_cart()
	{
				$product_id= $this->input->post('proid');
				$qty=$this->input->post('qty') - 1;
				
				$data = array(
							  'rowid'      => $product_id,
							  'qty'     => $qty,
							  );
							
				$this->cart->update($data);
				$this->frontcart();
	}
		
	public function frontcart(){
		$cart = $this->cart->contents();
		if(empty($cart))
		{  echo " <table>
					  <tr class='red_color'>
						   <td colspan='3' style='text-align:center;'>
								Your Cart is Empty
						   </td>
					  </tr>
				  </table>"; 
		  if(isset($_SESSION['Pcode']))
		  {
			  unset($_SESSION['Pcode']);
		  }
		}
			 
		if ($cart = $this->cart->contents())
		{ 
			$grand_total = 0;
			$i = 1;
			foreach ($cart as $item)
			{ ?>
			
			<tr>
				<td class="fd_Product" data-th="Product">
				
					<img width="" src="<?php echo  base_url("assets/admin/images/menus/".$item['image']);  ?>" alt="..." height="50px" class="img-responsive">
				</td>
				<td data-th="title" class="fd_title">
					<?php echo $item['name'] ?>				</td>
				<td data-th="Price" class="fd_price"><?php echo key_text('portalcurreny_symbol') ?><?php echo $item['price'] ?></td>
				<td data-th="size" class="fd_size"><?php echo $item['options']['size']; ?></td>
				<td class="fd_quantity" data-th="Quantity">
					<a class="btn  btn-sm" href="javascript:void(0);" onclick="plus(<?php echo $item["id"] ?>,1,'<?php echo $item['options']['size'] ?>');"><i class="fa fa-plus"></i></a>
					<input class="form-control text-center" value="<?php echo $item['qty'] ?>" type="text" readonly="readonly">
					<a class="btn  btn-sm" href="javascript:void(0);" onclick="minus('<?php echo $item["rowid"] ?>', <?php echo $item['qty'] ?>, '<?php echo $item['id'] ?>');  "><i class="fa fa-minus"></i>	</a>	
				</td>
				<td data-th="Subtotal" class="text-center fd_subtotal"><?php echo key_text('portalcurreny_symbol') ?><?php echo $item['subtotal'];?></td>
				<td class="actions text-center fd_remove" data-th="">			
					<a class="btn  btn-danger btn-sm" href="javascript:void(0);" onclick="remove('<?php echo $item['rowid'] ?>',<?php echo $item['id'] ?>);" ><i class="fa fa-trash-o"></i></a>						
				</td>
			</tr> 
			
		
			
		<?php }
		
	}
	}
	
	function CartTotalItem(){
		$carts = $this->cart->contents();
		$dicoun_amount = 0;
		 
		if(isset($_SESSION['Pcode']))
		{
			$price_arr = explode('@$#',$_SESSION['Pcode']);
			$dicoun_amount  = $price_arr[0];
        }
		 $data['item'] = $this->cart->total_items();
		 $data['amount'] = $this->cart->total();  
		 $data['finalamount'] = ($this->cart->total() - $dicoun_amount);
		 $data['discount'] = $dicoun_amount;
		 echo json_encode($data);
	}
	function Save_Shipping_info(){
		$this->session->set_tempdata('user_address',$_POST,90000); 
		$address =  $this->session->tempdata('user_address');
		//$check_preorder = $this->check_preorder();
		//echo json_encode($check_preorder); 
		
	}
	
	function confirm_pre_order(){
		if(isset($_SESSION['preorder_time'])){
			unset($_SESSION['preorder_time']);	 
		}
		if(!empty($_POST)){
			$this->session->set_tempdata('preorder_time',$_POST,90000);
			if(isset($_SESSION['preorder_time'])){
				echo 1;
			}else{
				echo 0;
			}
		}else{
			echo 0;
		}
	}
	
	function confirmation(){
		if(isset($_POST['pay_type']) && !empty($_POST['pay_type'])){
			$this->session->set_tempdata('pay_type',$_POST['pay_type'],90000);
			$_SESSION['user_address']['special_instructions'] = $_POST["spe_instruction"];
			$user = $this->session->tempdata('user_address');
			$infos = $this->Front_model->getDataResult('rt_info',array('status'=>1)); 	
			/* Wallet Payment Confirmation start*/	
				if($_POST['pay_type'] == 'Wallet'){
					$wallet = $this->Front_model->getData('rt_wallet',array('wallet_uid'=>$this->session->userdata('id')));
					if(!empty($wallet) && isset($wallet)){
						if($wallet['wallet_amount'] < $this->cart->total() ){
							echo "You have unsufficient Wallet Balance ,Try with other Payment Method";die;
						}else{
							$formMaker = $this->CheckpaymentByWalletOrCod($_POST['pay_type'],1);
						}
						
					}else{
						echo "You have unsufficient Wallet Balance ,Try with other Payment Method";die;
					}
					
				}
			/* Wallet Payment Confirmation end*/
				/* Wallet Payment Confirmation start*/	
				if($_POST['pay_type'] == 'COD'){
					
					$formMaker = $this->CheckpaymentByWalletOrCod($_POST['pay_type'],0);
				
				}
			/* Wallet Payment Confirmation end*/
			
			
			
			/* Wallet Payment Confirmation start*/	
				if($_POST['pay_type'] == 'Payumoney'){
					
					$formMaker = $this->CheckpaymentByPayumoney();
				
				}
			/* Wallet Payment Confirmation end*/
			
			/* Wallet Payment Confirmation start*/	
				if($_POST['pay_type'] == 'Paypal'){
					
					$formMaker = $this->CheckpaymentByPaypal();
				
				}
			/* Wallet Payment Confirmation end*/
			
				if(!empty($user))
			?>
				<div class="col-md-12 col-sm-12 col-xs-12">
					<h2 class="fd_subheading">confirmation information</h2>
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12">
					 <div class="fd_padder_bottom30">
						<p class="gray">Hello <?php echo $user['name'] ?>,</p>
						<p><?php echo get_description('payment_confirmation',$infos); ?></p>
					</div>
				</div>
				<div class="col-md-6 col-sm-12 col-xs-12">
					<p class="gray text-capitalize fd_border_heading">shipping address</p>
					
					<p><?php echo $user['address'] ?>,<br><?php echo $user['country'] ?>,<br><?php echo $user['state'] ?><br><?php echo $user['city'] ?>, <?php echo $user['pincode'] ?> </p>
				</div>
				
				
				<div class="col-md-6 col-sm-12 col-xs-12">
					<p class="gray text-capitalize fd_border_heading">payment</p> 
					
					<p class="text-capitalize fd_padder_bottom30">
					<?php echo $_POST['pay_type'] ?>
						
					</p>
						
				</div>
				<?php if($_POST['pay_type'] == "Wallet"){ $formtype = site_url('front/paymentByWallet') ;}elseif($_POST['pay_type'] == "COD"){ $formtype = site_url('front/paymentByCod') ;}elseif($_POST['pay_type'] == "Payumoney"){$formtype = 'https://sandboxsecure.payu.in/_payment' ;}else{ $formtype = 'https://www.sandbox.paypal.com/cgi-bin/webscr';} ?>
				<form method="POST" action="<?php echo  $formtype ?>" id="submitpayment">
					<?php 
						if($_POST['pay_type'] == 'Wallet' || $_POST['pay_type'] == 'COD' || $_POST['pay_type'] == 'Payumoney' || $_POST['pay_type'] == 'Paypal'){
							foreach($formMaker as $key => $value){  ?>
								<textarea style="display:none;" name="<?php echo $key ?>"><?php echo $value ?></textarea>
					<?php } } ?>
				</form>	
			
			<?php
		}
	}
	public function CheckpaymentByWalletOrCod($paytype= null,$pay_status = null){
		
			$carts = $this->cart->contents();
			$dicoun_amount =0;
			$coupon_code = '';
			foreach($carts as $cart){
				
				$payment_pid[]  = array('item'=>$cart['id'], 'size'=>$cart['options']['size'], 'quantity'=>$cart['qty'],'amount'=>$cart['subtotal']);
			}
			
			if(isset($_SESSION['Pcode'])){
												 
					$price_arr = explode('@$#',$_SESSION['Pcode']);
												 
					$dicoun_amount =$price_arr[0];
					$coupon_code = $price_arr[1];
			}
			if($dicoun_amount !=0){
				
				$cart_total = ($this->cart->total() - $dicoun_amount);
			}else{
				$cart_total = $this->cart->total();
				
			}
			
			
			$payment_pid = json_encode($payment_pid);
			
			$user = $this->session->tempdata('user_address');
				$user_id         = $this->session->userdata('id');
				$payment_pid     = $payment_pid;
				$payment_mode    = $paytype;
				$payment_amount  = $cart_total;
				$payment_address = $user['address']. ' | ' . $user['country']. ' | ' . $user['city']. ' | ' . $user['state']. ' | ' .$user['pincode'];
				$payUniqid       = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 8);
				$payment_status  = $pay_status;
				$special_instructions = $user['special_instructions'];
				$delivery_type = $user['delivery_type'];
				 //if(isset($_SESSION['preorder_time'])){ $pre_order_status = 1; }else{ $pre_order_status= 0; }
				
				
				$paymentdetails  = array(
					'payment_uid' 		=> $user_id,
					'payment_uniqid' 	=> $payUniqid,
					'payment_pid' 		=> $payment_pid,
					'payment_status' 	=> $payment_status,
					'payment_mode' 		=> $payment_mode,
					'payment_amount' 	=> $payment_amount,
					'payment_date' 		=> date('Y-m-d H:i:s'),
					'payment_address' 	=> $payment_address,
					'used_coupen_code'  => $coupon_code,
					'discount_amount'   =>$dicoun_amount,
					'special_instructions'=>$special_instructions,
					'delivery_type'=>$delivery_type,
					//'pre_order_status' =>$pre_order_status,
				);
				
				return $paymentdetails;
    	
	
	}
	
	public function paymentByWallet(){
	if(!empty($_POST)){	
		$wallet = $this->Front_model->getData('rt_wallet',array('wallet_uid'=>$this->session->userdata('id')));
			if(!empty($wallet) && isset($wallet) && !empty($_POST)){
				if($wallet['wallet_amount'] > $_POST['payment_amount'] ){
					if($payment_id = $this->Front_model->insertData('rt_paymentdetails',$_POST)){
						$remain_amount = $wallet['wallet_amount'] - $_POST['payment_amount'];
						$this->Front_model->updateData('rt_wallet',array('wallet_amount'=>$remain_amount),array('wallet_uid'=>$this->session->userdata('id')));
						
						$this->Front_model->deductInventory(json_decode($_POST['payment_pid']));
						//$this->save_pre_order($payment_id);
						$this->sendbookingemails($payment_id);
						 
						$this->destroy_coupon();						
						$this->cart->destroy();
						$this->payresponse('success',$_POST['payment_uniqid']);
					}
				}else{
					$this->payresponse('failed','','Something Went Wrong');
				}	
			}else{
				$mes = 'You have unsufficient Wallet Balance ,Try with other Payment Method';
				$this->payresponse('failed','',$mes);
				
			}
		}	
	 }
	public function paymentByCod(){
		if(!empty($_POST)){
			 
		if(!empty($_POST) && !empty($_POST['payment_amount'])){
			if($payment_id = $this->Front_model->insertData('rt_paymentdetails',$_POST)){
				
				$this->Front_model->deductInventory(json_decode($_POST['payment_pid']));
				//$this->save_pre_order($payment_id);
				$this->sendbookingemails($payment_id);
				$this->destroy_coupon();				
				$this->cart->destroy();
				$this->payresponse('success',$_POST['payment_uniqid']);
				
			}
		}else{
				$this->payresponse('failed','','Something Went Wrong');
			}
		}
	}
	public function CheckpaymentByPayumoney(){
		$MERCHANT_KEY = key_text('payu_merchantKey');
		$SALT		  = key_text('payu_merchantSalt');
		// $MERCHANT_KEY = "77uoPzxV";
		// $SALT = "HKyD1g4aU9";// Merchant Key and Salt as provided by Payu.
		// $PAYU_BASE_URL = "https://sandboxsecure.payu.in/_payment";		// For Sandbox Mode
		//$PAYU_BASE_URL = "https://secure.payu.in";			// For Production Mode
		$hash = '';
		// Hash Sequence
		$carts = $this->cart->contents();
			$dicoun_amount = 0;
			$coupon_code = '';
			foreach($carts as $cart){
				$payment_pid[]  = array('name'=>$cart['name'],'description'=>'','value'=>$cart['subtotal'],'isRequired'=>'false');
			}
			
			if(isset($_SESSION['Pcode'])){
												 
				$price_arr = explode('@$#',$_SESSION['Pcode']);
											 
				$dicoun_amount =$price_arr[0];
				$coupon_code = $price_arr[1];
			}
			
			
			
			
			$productinfo = json_encode($payment_pid);
			
			foreach($carts as $cart){
			 $payment_pids[]  = array('item'=>$cart['id'], 'size'=>$cart['options']['size'],'quantity'=>$cart['qty'],'amount'=>$cart['subtotal']);
			}
			$pro_pid = json_encode($payment_pids);
			$this->session->set_tempdata('pro_pid',$pro_pid,90000); 
			
			$user = $this->session->tempdata('user_address');
				$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
				$posted  = array(
						'key' 				=>  $MERCHANT_KEY,
						'txnid' 			=>  substr(hash('sha256', mt_rand() . microtime()), 0, 20),
						'amount'			=>  $this->amountUSDTOINR(),
						'productinfo' 		=>  $productinfo,
						'firstname' 		=>  trim($user['name']),
						'email' 			=>  trim($user['email']),
						'phone' 			=>  trim($user['contact']),
						'surl' 				=>  base_url('front/payusuccess'),
						'furl' 				=>  base_url('front/payufailture') ,
						'service_provider' 	=>  'payu_paisa',
						'udf1'				=>  $this->session->userdata('id'), //payment_uid
						'udf2'				=>	substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 8), //payment_uniqid
						'udf3'				=>	'1',//payment_pid
						'udf4' 				=>  $user['address']. ' | ' . $user['country']. ' | ' . $user['city']. ' | ' . $user['state']. ' | ' .$user['pincode'], 
						'udf5'  =>$coupon_code.'@$#'.$dicoun_amount.'@$#'.$user['delivery_type'].'@$#'.$user['special_instructions'],
						 
				); 
				$hashVarsSeq = explode('|', $hashSequence);
				$hash_string = '';	
				foreach($hashVarsSeq as $hash_var) {
				  $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
				  $hash_string .= '|';
				}
				$hash_string .= $SALT;
				$hash = strtolower(hash('sha512', $hash_string));
				$posted['hash'] = $hash;
				return $posted;
	}
	
	
public function payusuccess(){
	
	if(!empty($_POST)){
		
		$status			=		$_POST["status"]; 
		$firstname		=		$_POST["firstname"];
		$amount			=		$_POST["amount"];
		$txnid			=		$_POST["txnid"];
		$posted_hash	=		$_POST["hash"];
		$key			=		$_POST["key"];
		$productinfo	=		$_POST["productinfo"];
		$email			=		$_POST["email"];
		$udf1 			= 		$_POST['udf1'];
		$udf2 			= 		$_POST['udf2'];
		$udf3 			= 		$_POST['udf3'];
		$udf4 			= 		$_POST['udf4'];
		$udf5           =		$_POST['udf5'];
		$salt			=		key_text('payu_merchantSalt');
		$discount_arr = explode('@$#',$udf5);
		//if(isset($_SESSION['preorder_time'])){ $pre_order_status = 1; }else{ $pre_order_status= 0; }
		
		If (isset($_POST["additionalCharges"])) {
				$additionalCharges=$_POST["additionalCharges"];
				$retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
		  } else {
				$retHashSeq = $salt.'|'.$status.'||||||'.$udf5.'|'.$udf4.'|'.$udf3.'|'.$udf2.'|'.$udf1.'|'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
         }
		 $hash = hash("sha512", $retHashSeq);
		
		if ($hash != $posted_hash) {
			  echo "Invalid Transaction. Please try again";
		   } else {
			    
			    if(key_text('portal_curreny') == 'USD'){
					$amount = (int) $this->currencyConverter_new('INR','USD',$_POST['amount']) ;
					$_POST['amount'] =  1 + (int) round($amount,2);
				}
				$pro_pid = $this->session->tempdata('pro_pid');
				// html_entity_decode($_POST['udf3']),
			$payment_txnId	= (isset($_POST['payuMoneyId']) ? $_POST['payuMoneyId'] : '') ;
				  $paymentdetails  = array(
						'payment_uid' 		=> $_POST['udf1'],
						'payment_uniqid' 	=> $_POST['udf2'],
						'payment_pid' 		=> $pro_pid,
						'payment_status' 	=> 1,
						'payment_mode' 		=> 'Payumoney',
						'payment_amount' 	=> $_POST['amount'],
						'payment_date' 		=> $_POST['addedon'],
						'payment_address' 	=> $_POST['udf4'], 
						'payment_txnId'		=> $payment_txnId,
						'used_coupen_code'=>htmlspecialchars_decode($discount_arr[0]),
						'discount_amount'=>$discount_arr[1],
						'special_instructions'=>$discount_arr[3],
						'delivery_type'=>$discount_arr[2],
						//'pre_order_status'=>$pre_order_status,
					);
					// echo "<pre>";
					// print_r($paymentdetails );die;
					if($payment_id = $this->Front_model->insertData('rt_paymentdetails',$paymentdetails)){
						$this->Front_model->deductInventory(json_decode($pro_pid));
						//$this->save_pre_order($payment_id);
						$this->sendbookingemails($payment_id);
						$this->destroy_coupon();		
						$this->cart->destroy();
						$this->payresponse('success',$_POST['udf2']);
						// echo "<h3>Thank You. Your order status is ". $status .".</h3>";
						// echo "<h4>Your Transaction ID for this transaction is ".$txnid.".</h4>";
						// echo "<h4>We have received a payment of Rs. " . $amount . ". Your order will soon be shipped.</h4>";
		   
					}	
			  }
		}
	}
	public function payufailture(){
		$status		=	$_POST["status"];
		$firstname	=	$_POST["firstname"];
		$amount		=	$_POST["amount"];
		$txnid		=	$_POST["txnid"];
		$posted_hash=	$_POST["hash"];
		$key		=	$_POST["key"];
		$productinfo=	$_POST["productinfo"];
		$email		=	$_POST["email"];
		$udf1 		= 	$_POST['udf1'];
		$udf2 		= 	$_POST['udf2'];
		$udf3 		= 	$_POST['udf3'];
		$udf4 		= 	$_POST['udf4'];
		$udf5       = 	$_POST['udf5'];
		$salt		=	key_text('payu_merchantSalt');

// Salt should be same Post Request 

		If (isset($_POST["additionalCharges"])) {
			$additionalCharges=$_POST["additionalCharges"];
			$retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
		} else {
			$retHashSeq = $salt.'|'.$status.'||||||'.$udf5.'|'.$udf4.'|'.$udf3.'|'.$udf2.'|'.$udf1.'|'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
        }
		 $hash = hash("sha512", $retHashSeq);
  
		if ($hash != $posted_hash) {
	      
		    $this->payresponse('failed','','Invalid Transaction. Please try again');
		   } else {
			 // echo "<h3>Your order status is ". $status .".</h3>";
			 // echo "<h4>Your transaction id for this transaction is ".$txnid.". You may try making the payment by clicking the link below.</h4>";
			 $this->payresponse('failed','','You may try making the payment again');
		 } 
	}
	
	public function CheckpaymentByPaypal(){ 
		$carts = $this->cart->contents();
			$paypal = [];
			$payinfo = [];
			$i=1;
			$dicoun_amount = 0;
			$coupon_code = '';
			if(isset($_SESSION['Pcode'])){
												 
				$price_arr = explode('@$#',$_SESSION['Pcode']);
											 
				$dicoun_amount =$price_arr[0];
				$coupon_code = $price_arr[1];
			}
			
			
			foreach($carts as $cart){
			 $payment_pid[]  = array('item'=>$cart['id'],'size'=>$cart['options']['size'],'quantity'=>$cart['qty'],'amount'=>$cart['subtotal']);
			 $payinfo['item_name_'.$i] = $cart['name'];
			 
				$payinfo['amount_'.$i]	   = $this->amountINRTOUSD($cart['subtotal']);
				$i++;
			 }
			 
			
			$user = $this->session->tempdata('user_address');
			$pro_pid['payment_amount']		= $this->amountINRTOUSD();
			$item_info['item_name'] ="Shoping Cart Total";
			$item_info['amount'] = $pro_pid['payment_amount'];
			 
			if(key_text('portal_curreny') == 'INR'){
					$amount = (int) $this->currencyConverter_new('USD','INR',$pro_pid['payment_amount']) ;
					$pro_pid['payment_amount'] =  (int) round($amount,2) + 1 ;  
			}
			
			
				
			$pro_pid['payment_uid']			= $this->session->userdata('id');
			$pro_pid['payment_uniqid']		= substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 8);
			$pro_pid['payment_pid'] 		= json_encode($payment_pid);
			$pro_pid['payment_status'] 		= 0;
			$pro_pid['payment_address']		= $user['address']. ' | ' . $user['country']. ' | ' . $user['city']. ' | ' . $user['state']. ' | ' .$user['pincode']  ;
			
			$pro_pid['payment_mode']		= 'Paypal';
			$pro_pid['payment_date']		= date('Y-m-d h:i:s');
			$pro_pid['used_coupen_code'] =$coupon_code;
			$pro_pid['discount_amount'] = $dicoun_amount;
			$pro_pid['special_instructions'] = $user['special_instructions'];
			$pro_pid['delivery_type'] = $user['delivery_type'];
			//if(isset($_SESSION['preorder_time'])){ $pre_order_status = 1; }else{ $pre_order_status= 0; }
			//$pro_pid['pre_order_status'] = $pre_order_status; 
			if($payment_id = $this->Front_model->insertData('rt_paymentdetails',$pro_pid)){
					//$this->save_pre_order($payment_id);
					$paypal = array(
						'cmd'			=>  '_xclick',
						'upload'		=>	'1',
						'business'		=>	key_text('paypal_business_mail'),
						'currency_code' =>	'USD',
						'return'		=>	base_url('front/paypalsuccess'),
						'cancel_return'	=>	base_url('front/paypalfailture'),
						'notify_url'	=>	base_url('front/paypalnotify_url'),
						'custom'		=>	$payment_id
						);
					
					$pay = array_merge($paypal,$item_info);
					
					return $pay;	
			}	
		
		
				
			
				 
	}
	public function paypalfailture(){
		 $this->payresponse('failed','','You may try making the payment again');
	}
	
	public function paypalsuccess(){
		$this->destroy_coupon();
		$this->cart->destroy();
		$this->payresponse('success','','your order has booked successfully, please check your mail for more detail');
	}
	public function paypalnotify_url(){ 
		 $paypalInfo = $this->input->post();
		
		 $payinfo = $paypalInfo['custom'];
		 $status = array(
			'payment_id' 	=>  $payinfo,
			);
			
		 // if(key_text('portal_curreny') == 'INR'){
					// $amount = (int) $this->currencyConverter_new('USD','INR',$payinfo->amount) ;
					// $payinfo->amount =  (int) round($amount,2) + 1 ;
				// }
		$payment_txnId = (isset($_POST['txn_id']) ? $_POST['txn_id'] : '');	 	
		 $paymentdetails  = array( 
				'payment_status' 	=>  1,
				'payment_txnId'		=>	$payment_txnId, 
		);	
			
			 
					if($this->Front_model->updateData('rt_paymentdetails',$paymentdetails,$status) > 0){
						
						$data = $this->Front_model->getData('rt_paymentdetails',$status);
						$this->Front_model->deductInventory(json_decode($data['payment_pid']));
						$this->sendbookingemails($data['payment_id']);	
						echo "true";
					}	
            
	} 
	public function payresponse($type=null,$orderid=null,$message = null){
		if($type != null){
			$data['type'] = $type;
			$data['orderid'] = $orderid;
			$data['message'] = $message;
			$data['menus'] = $this->Front_model->getDataResult('rt_header_menu',array()); ;
			$data['infos'] = $this->Front_model->getDataResult('rt_info',array('status'=>1)); 
			$data['img']  = $this->DatabaseModel->select_data('*','rt_images',array('id'=>1),'');
			$data['general']  		= $this->Front_model->getData('rt_genral_setting',array('id'=>1));
			$data['img'] =$data['img'][0];
			$data['page']  = 'success';
			$this->load->view('front/include/header',$data);
			$this->load->view('front/payresponse'); 
			$this->load->view('front/include/footer');
		}
	}
	public function product_rating($user_id,$star,$product_id)
	{
		$data1=$this->DatabaseModel->access_database('rt_rating','select','',array('user_id'=>$user_id));
		if($data1)
		{
			$this->DatabaseModel->access_database('rt_rating','update',array('user_id'=>$user_id,'star'=>$star,'product_id'=>$product_id),array('user_id'=>$user_id));
		}
		else
		{
			$this->DatabaseModel->access_database('rt_rating','insert',array('user_id'=>$user_id,'star'=>$star,'product_id'=>$product_id));
		}
		redirect(base_url('front/payresponse/reat_success'));
	} 
	public function send_invoice($order_no = null){
		if(!empty($order_no)){
		$invoice = $this->Front_model->GenerateInvoice('6gHv3qFG',$this->session->userdata('id'));
		$p_info = $invoice['invoice'];
		$pay_address = explode('|',$invoice['payment_address']);
		
		$config['charset'] = 'utf-8';
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';
		//$config['protocol'] = 'sendmail';

		$this->email->initialize($config);

		$this->email->from('ajay.parmar@himanshusofttech.com', 'Foodies');
		$this->email->to('ajay.parmar@himanshusofttech.com' );
		$this->email->subject('Foodies Order Invoice');

		$tablerow="";
		foreach($p_info as $order)
		{ $tablerow .='
			<tr>
			<td class="service">'. 	$order['item_name'] .'</td>
			<td class="desc">'. 	$order['item_sku'] .'</td>
			<td class="unit">₹ '. 	$order['item_price'] .'</td>
			<td class="qty">'. 		$order['qty'].'</td>
			<td class="total">₹ '. 	$order['subtotal'] .'</td>
			</tr>';


		}
$this->email->message('

				<head>

				<style>
				.clearfix:after {
				content: "";
				display: table;
				clear: both;
				}

				a {
				color: #5D6975;
				text-decoration: underline;
				}

				body {
				position: relative;
				width: 21cm; 
				height: 29.7cm; 
				margin: 0 auto; 
				color: #001028;
				background: #FFFFFF; 
				font-family: Arial, sans-serif; 
				font-size: 12px; 
				font-family: Arial;
				}

				header {
				padding: 10px 0;
				margin-bottom: 30px;
				}

				#logo {
				text-align: center;
				margin-bottom: 10px;
				}

				#logo img {
				width: 90px;
				}

				h1 {
				border-top: 1px solid #5D6975;
				border-bottom: 1px solid #5D6975;
				color: #5D6975;
				font-size: 2.4em;
				line-height: 1.4em;
				font-weight: normal;
				text-align: center;
				margin: 0 0 20px 0;
				background: url(dimension.png);
				}

				#project {
				float: left;
				}

				#project span {
				color: #5D6975;
				text-align: right;
				width: 52px;
				margin-right: 10px;
				display: inline-block;
				font-size: 0.8em;
				}

				#company {
				float: right;
				text-align: right;
				}

				#project div,
				#company div {
				white-space: nowrap; 
				}

				table {
				width: 100%;
				border-collapse: collapse;
				border-spacing: 0;
				margin-bottom: 20px;
				}

				table tr:nth-child(2n-1) td {
				background: #F5F5F5;
				}

				table th,
				table td {
				text-align: center;
				}

				table th {
				padding: 5px 20px;
				color: #5D6975;
				border-bottom: 1px solid #C1CED9;
				white-space: nowrap; 
				font-weight: normal;
				}

				table .service,
				table .desc {
				text-align: left;
				}

				table td {
				padding: 20px;
				text-align: right;
				}

				table td.service,
				table td.desc {
				vertical-align: top;
				}

				table td.unit,
				table td.qty,
				table td.total {
				font-size: 1.2em;
				}

				table td.grand {
				border-top: 1px solid #5D6975;;
				}

				#notices .notice {
				color: #5D6975;
				font-size: 1.2em;
				}

				footer {
				color: #5D6975;
				width: 100%;
				height: 30px;
				position: absolute;
				bottom: 0;
				border-top: 1px solid #C1CED9;
				padding: 8px 0;
				text-align: center;
				}
				</style>
				</head>


				<body>
				<header class="clearfix">
				<div id="logo">
				<img src="'. base_url("assets/front/images/logo.png") .'">
				</div>
				<h1>INVOICE ORDER NO: '. $invoice['payment_uniqid'] .'</h1>

				<div id="company" class="clearfix">
				<b>Billed To:</b>
				<div>'.$invoice['user_name'].'</div>
				<div>'.$pay_address[0].'</div>
				<div>'.$pay_address[2].'</div>
				<div>'.$pay_address[3].'</div>
				<div>'.$pay_address[1].' - '.$pay_address[4].'</div>
				<br>
				<br>
				<b>Order Date:</b>
				<div> '. date_format(date_create($invoice['payment_date']),"Y/m/d").'</div>
				</div>

				<div id="project">
				<b>Shipped to:</b>
				<div>'.$invoice['user_name'].'</div>
				<div>'.$pay_address[0].'</div>
				<div>'.$pay_address[2].'</div>
				<div>'.$pay_address[3].'</div>
				<div>'.$pay_address[1].' - '.$pay_address[4].'</div>
				<br>
				<br>
				<b>Payment Method:</b>
				<div> '.$invoice['payment_mode'].'</div>
				<div> '.$invoice['user_email'] .'</div>
				<div> '.$invoice['user_mobile'] .'</div>
				</div>
				</header>

				<main>
				<table>
				<thead>
				<tr>
				<th class="service">Item</th>
				<th class="desc">SKU</th>
				<th class="unit">Price</th>
				<th class="qty">Quantity</th>
				<th class="total" >Total</th>

				</tr>
				</thead> 
				<tbody>

				'.$tablerow.'

				<tr>
				<td colspan="4">SUBTOTAL</td>
				<td class="total">₹ '. $invoice['payment_amount'].'</td>
				</tr>
				<tr>
				<td colspan="4" class="grand total">GRAND TOTAL</td>
				<td class="grand total">₹ '. $invoice['payment_amount'].'</td>
				</tr>
				</tbody>
				</table>
				</main>
				<footer>
				Invoice was created on a computer and is valid without the signature and seal.
				</footer>
				</body>	
				');

	$this->email->send();
	echo $this->email->print_debugger();

}
	
}

	public function currencyConverter_new($from_Currency,$to_Currency,$amount)
	{
		$from_Currency = urlencode($from_Currency);
			$to_Currency = urlencode($to_Currency);
		/*	$get = file_get_contents('https://free.currencyconverterapi.com/api/v5/convert?q='.$from_Currency.'_'.$to_Currency.'&compact=y');
			$rate=json_decode($get,true);
		return $converted_amount = $amount*$rate[$from_Currency.'_'.$to_Currency]['val'];
	
	    */
		$apikey = key_text('currency_api');
		 
		$curl = curl_init();
		$url = 	'https://free.currencyconverterapi.com/api/v5/convert?q='.$from_Currency.'_'.$to_Currency.'&compact=y&apiKey='.$apikey.'';
	   // OPTIONS:
	   curl_setopt($curl, CURLOPT_URL, $url);
	   curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	   curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	   // EXECUTE:
	   $result = curl_exec($curl);
	  
	   if(!$result){die("Connection Failure");}
	   curl_close($curl);
	    $rate = json_decode($result , true);
	     
	    return $converted_amount = $amount*$rate[$from_Currency.'_'.$to_Currency]['val'];
	}
	public function amountUSDTOINR(){
		 
		$dicoun_amount = 0;
		 
		if(isset($_SESSION['Pcode'])){
											 
			$price_arr = explode('@$#',$_SESSION['Pcode']);
			$dicoun_amount = $price_arr[0];
	   }
		 
		
		if($dicoun_amount !=0){
			
			$cart_total = ($this->cart->total()- $dicoun_amount);
			
		}else{
			$cart_total =  $this->cart->total();
			
		}
		
		if(key_text('portal_curreny') == 'USD'){
			$amount = (int) $this->currencyConverter_new('USD','INR',$cart_total) ;
			 return (int) round($amount,2);
		}else{
			 return $cart_total;
		}
	}
	public function amountINRTOUSD($subtotal = null){
		$dicoun_amount = 0;
		 
		if(isset($_SESSION['Pcode'])){
											 
			$price_arr = explode('@$#',$_SESSION['Pcode']);
			$dicoun_amount = $price_arr[0];
	   }
		
		if($dicoun_amount !=0){
			$cart_total = ($this->cart->total()- $dicoun_amount);
			 
		}else{
			$cart_total = $this->cart->total();
			
		}
		
		if($subtotal == null){
			if(key_text('portal_curreny') == 'INR'){
				$amount = $this->currencyConverter_new('INR','USD',$cart_total);
				 return  round($amount,2);
			}else{
				 return $cart_total;
			}
		}else{
			if(key_text('portal_curreny') == 'INR'){
				$amount = $this->currencyConverter_new('INR','USD',$subtotal ) ;
				return  round($amount,2);
			}else{
				return $subtotal;
			}
		}	
	}
	
	public function sendbookingemails($payment_id = '', $type = '') {
        $email_bodyUser = $email_bodyDoctor = '';
        $this->load->library('parser');
        $paymentDetails = $this->DatabaseModel->select_data('*', 'rt_paymentdetails', array(
            'payment_id' => $payment_id
        ), 1);
		
        if (!empty($paymentDetails)) {
            $order_status = $paymentDetails[0]['payment_order_status'];
            $user_id      = $paymentDetails[0]['payment_uid'];
            $userDetails  = $this->DatabaseModel->select_data('*', 'rt_users', array(
                'user_id' => $user_id
            ), 1);
			
            $username     = $userDetails[0]['user_name'];
            $user_mobile  = $userDetails[0]['user_mobile'];
            //$user_device_token	=$userDetails[0]['user_device_token'];
            $subject      = $this->ts_functions->getsettings('bookingemail', 'subject');
			
            // Booking start
            if ($order_status == 0) {
                $emContent       = $this->ts_functions->getsettings('bookorder', 'text');
                $email_bodyAdmin = $this->ts_functions->parseBotemp($paymentDetails, $userDetails, $emContent);
				 $email_bodyUser   = $this->ts_functions->parseBotemp($paymentDetails, $userDetails, $emContent);
            }
			// Booking End
			// Intransist start
            if ($order_status == 1) {
                $emContent    = $this->ts_functions->getsettings('intransits', 'text');
				  $email_bodyAdmin = $this->ts_functions->parseBotemp($paymentDetails, $userDetails, $emContent);
                $email_bodyUser   = $this->ts_functions->parseBotemp($paymentDetails, $userDetails, $emContent);
                
            }
			// Intransist End
			// cancel order by admin start
            if ($order_status == 2) {
                $emContent  = $this->ts_functions->getsettings('delivered', 'text');
				$email_bodyAdmin = $this->ts_functions->parseBotemp($paymentDetails, $userDetails, $emContent);
                $email_bodyUser = $this->ts_functions->parseBotemp($paymentDetails, $userDetails,  $emContent);
            }
			if ($order_status == 3) {
                $emContent  = $this->ts_functions->getsettings('canceled', 'text');
				$email_bodyAdmin = $this->ts_functions->parseBotemp($paymentDetails, $userDetails, $emContent);
                $email_bodyUser = $this->ts_functions->parseBotemp($paymentDetails, $userDetails,  $emContent);
            }
            // cancel order by admin end
			//echo $email_bodyUser;die;  
			
            if ($email_bodyUser != '') {
					
                $bodyUser = $this->parser->parse('email_template', array(
                    'email_body' => $email_bodyUser
                ), true);
				
                $this->ts_functions->sendUserEmailCI($userDetails[0]['user_email'], '', '', $subject, $bodyUser);
               
            }
            if ($email_bodyAdmin != '') {
				
                $bodyAdmin = $this->parser->parse('email_template', array(
                    'email_body' => $email_bodyAdmin
                ), true);
				
                $this->ts_functions->sendUserEmailCI($this->ts_functions->getsettings('email', 'fromemail'), '', '', $subject, $bodyAdmin);
               
            }
        }
		
	
    }
	function filter_front(){
		 $data=$_POST['data'];
		 if($data!='')
		 {
			 //$item_info=$this->DatabaseModel->access_database('rt_items','select','',array('item_name'=>$data));
			 $q=$this->db->select('*')->from('rt_items')->like(array('item_name'=>$data))->limit(5,0)->get();
			 $item_info=$q->result();
			 if($item_info)
			 {
				 foreach($item_info as $item)
				 {
					 ?>
						<li><div class="fd_product_data"><img src="<?php echo  base_url()."assets/admin/images/menus/thumb/".$item->image_thumb; ?>" width="50px" alt="" class="img-fluid"><span><?php echo $item->item_name;  ?></span></div><div class="fd_price">$<?php echo $item->item_price;  ?></div><div><a href="#" class="fd_btn" onclick="addCart('<?php echo $item->item_id ?>' , 1);">add to cart</a></div></li>
					 <?php
				 }
			 }
			 else
			 {
				 ?><li>Item Not Found</li><?php
			 }
		 }
		 else
		 {
			  echo "empty";
		 }
		 
	}
	
	
	public function oneblog($slug = null){
		if($slug != null){
		
		$data['blog'] 	  = $this->Front_model->getData('rt_blogs',array('slug'=>$slug));
		
		$data['comments'] = $this->Front_model->getDataResult('rt_blog_comment',array('blog_id'=>$data['blog']['id'] ));
		
		$email = ''; $name  = '';
		$user_info	= $this->is_login();
		$data['email'] ='';
		$data['name'] ='';
		if(!empty($user_info)){
			$data['email']  		= $user_info['user_email']; 
			$data['name']  			= $user_info['user_name']; 
		}
		
		$data['count_comment']  = count($data['comments'] ); 
		$data['menus']  		= $this->Front_model->getDataResult('rt_header_menu',array()); ;
		$data['infos']  		= $this->Front_model->getDataResult('rt_info',array('status'=>1)); ;
		$data['blogs']  		= $this->Front_model->getblog(); 
		$data['blog_cat']  		= $this->Front_model->getblogcat(); 
		$data['user_info']		= $this->is_login();
		
		$data['img']  		= 	$this->DatabaseModel->select_data('*','rt_images',array('id'=>1),'');
		$data['img'] 		=	$data['img'][0];  
		$data['general']  	= 	$this->Front_model->getData('rt_genral_setting',array('id'=>1));
		$data['title'] 		= 	get_title('blog_single',$data['infos']) ;
		$data['page']  		= 	'blog_single';
		$this->load->view('front/include/header',$data);
		$this->load->view('front/blog_single');
		$this->load->view('front/include/footer');
			
		}
	}
	
	public function accept_coockies(){
		setcookie('accept_coockies', 1, time() + 31556926 , "/");
		redirect(base_url());		
	}
	function view_more()
	{
		$this->load->view('view_more');
	}
	function view_more2()
	{
		$this->load->view('view_more2');
	}
	function user_profile()
	{
		$name=$this->input->post('name');
		$cno=$this->input->post('cno');
		$ps=$this->input->post('ps');
		$id=$this->input->post('id');
		$add=$this->input->post('add');
		$ps2='';
		if(preg_match('/^[a-f0-9]{32}$/', $ps))
			{
				$ps2=$ps;
			}
			else
			{
				$ps2=md5($ps);
			}
		$arr=array('user_name'=>$name,'user_mobile'=>$cno,'user_pass'=>$ps2,'user_address'=>$add);
		$res=$this->DatabaseModel->access_database('rt_users','update',$arr,array('user_id'=>$id));
		if($res)
			echo "1";
		else
			echo "0";
	}
	function terms()
	{
		$data['menus']  		= $this->Front_model->getDataResult('rt_header_menu',array()); ;
		$data['infos']  		= $this->Front_model->getDataResult('rt_info',array('status'=>1)); ;
		$data['times']  		= $this->Front_model->getDataResult('rt_time_schedule',array()); ;
		$data['reating']  		= $this->Front_model->getDataResult('rt_rating',array()); ;
		$data['testimonials']   = $this->Front_model->getDataResult('rt_testimonial',array('status'=>1)); 
		$data['Item_cat']   	= $this->Front_model->getDataResult('rt_categories',array('cat_status'=>1));
		$data['general']  		= $this->Front_model->getData('rt_genral_setting',array('id'=>1));
		$data['blogs']  		= $this->Front_model->getblog(); 
		$data['blog_cat']  		= $this->Front_model->getblogcat(); 
		$data['user_info']		= $this->is_login();
		
		$data['img']  = $this->DatabaseModel->select_data('*','rt_images',array('id'=>1),'');
		$data['img'] =$data['img'][0];  
		$data['page']  = 'terms';
		$data['title']  = 'Terms & Conditions';
		$this->load->view('front/include/header',$data);
		$this->load->view('front/terms',$data);
		$this->load->view('front/include/footer');
	}
	
	function policy()
	{
		$data['menus']  		= $this->Front_model->getDataResult('rt_header_menu',array()); ;
		$data['infos']  		= $this->Front_model->getDataResult('rt_info',array('status'=>1)); ;
		$data['times']  		= $this->Front_model->getDataResult('rt_time_schedule',array()); ;
		$data['reating']  		= $this->Front_model->getDataResult('rt_rating',array()); ;
		$data['testimonials']   = $this->Front_model->getDataResult('rt_testimonial',array('status'=>1)); 
		$data['Item_cat']   	= $this->Front_model->getDataResult('rt_categories',array('cat_status'=>1));
		$data['general']  		= $this->Front_model->getData('rt_genral_setting',array('id'=>1));
		$data['blogs']  		= $this->Front_model->getblog(); 
		$data['blog_cat']  		= $this->Front_model->getblogcat(); 
		$data['user_info']		= $this->is_login();
		
		$data['img']  = $this->DatabaseModel->select_data('*','rt_images',array('id'=>1),'');
		$data['img'] =$data['img'][0];  
		$data['page']  = 'policy';
		$data['title']  = 'Privacy Policy';
		$this->load->view('front/include/header',$data);
		$this->load->view('front/policy',$data);
		$this->load->view('front/include/footer');
	}
	
	public function varify_coupon(){
		$carts = $this->cart->contents();
	  
		if(isset($_POST['coupon_code']) && !empty($carts))
		{   
			$coupen_code = $_POST['coupon_code'];
			$offer_info = $this->DatabaseModel->select_data('*','rt_discount_coupons',array('coupen_code'=>$coupen_code,'offer_status'=>1),'');
			
			
			if($offer_info)
			{ 	 
				/* check user limit */
				if($offer_info[0]['user_used_limit'] ==2)
				{
					 $pay_info = $this->DatabaseModel->select_data('used_coupen_code','rt_paymentdetails',array('payment_uid'=>$_SESSION['id']),'');	
					 if($pay_info){
						foreach($pay_info as $pinfo){
							$used_couponArr = $pinfo['used_coupen_code'];
							if( $used_couponArr == $_POST['coupon_code']){
									$resp = array('status' => 0 , 'error' =>'This coupon code already used');
									echo  json_encode($resp);
									die;
							}
							 						
						}
					 }  				
				}
				
				/* Match coupon code */
				if($offer_info[0]['coupen_code'] != $coupen_code ){
					  $resp = array('status' => 0 , 'error' =>'Coupon code does not matched');
					  echo  json_encode($resp);
					die;
				}
				
				/* Check Expire date */
				if($offer_info[0]['coupen_expire_date'] < date('Y-m-d'))
				{
					 $resp = array('status' => 0 , 'error' =>'This Coupon Code is Expired.');
					 echo  json_encode($resp);
					die;
					
				}
				/* check coupon limit */
				if($offer_info[0]['coupen_uses_limt'] <1)
				{
					  $resp = array('status' => 0 , 'error' =>'This Coupon Limit is Expired.');
					  echo  json_encode($resp);
					die;
				}
				 
				$cart_total = $this->cart->total();
							
				// 1 for percantage and 2 for flate amount
				if($offer_info[0]['discount_type'] == 1)
				{ 
					$disc_amount = $offer_info[0]['discount_amount'];
					$dicount_amount = ($disc_amount / 100) * $cart_total;
					$dicount_amount = round($dicount_amount);
					$session_str = $dicount_amount.'@$#'.$coupen_code; 
					$this->session->set_userdata('Pcode',$session_str);
					 
					$resp = array('status' => 1 ,'error' =>'Coupon apply Successfully', 'dicount_amount'=>$dicount_amount ,'type'=>'percantage');
				}else if($offer_info[0]['discount_type'] == 2)
				{
					$dicount_amount = $offer_info[0]['discount_amount'];
					$session_str = $dicount_amount.'@$#'.$coupen_code; 
					$this->session->set_userdata('Pcode',$session_str);
					 
					$resp = array('status' => 1 ,'error' =>'Coupon apply Successfully', 'dicount_amount' =>$dicount_amount, 'type'=>'flat');
				} 
										 
			}else{
				$resp = array('status' => 0 , 'error' =>'Coupon Code Not Exist');
			}
		}else{
			$resp = array('status' => 0 , 'error' =>'Coupon Code Not Valid');
		}		
		 
		echo  json_encode($resp);
	}
	
	/*  this function call for remove applied coupon */
	public function remove_coupon(){
		 if(isset($_SESSION['Pcode'])){
			 $price_arr = explode('@$#',$_SESSION['Pcode']);
			 $discount = $price_arr[0];
			 unset($_SESSION['Pcode']);
			 $resp = array('status' => 1 ,'error' =>'Coupon Remove Successfully', 'dicount_amount' =>$discount, 'type'=>'flat');
			 
		}else{
			$resp = array('status' => 0 ,'error' =>'Something went wrong!', );
		}
		echo  json_encode($resp);	
		 
	}
	
	/* this function call after order & payment success */
	public function destroy_coupon(){
		 
		if(isset($_SESSION['Pcode']))
		{
			$price_arr = explode('@$#',$_SESSION['Pcode']);
			  
			$offer_info = $this->DatabaseModel->select_data('*','rt_discount_coupons',array('coupen_code'=>$price_arr[1]),''); 
			$coupon_limit =  $offer_info[0]['coupen_uses_limt'] - 1;
			$updatelimit = array('coupen_uses_limt'=>$coupon_limit);
			$this->DatabaseModel->access_database('rt_discount_coupons','update',$updatelimit,array('coupen_code'=>$price_arr[1]));
			unset($_SESSION['Pcode']);
	   }
	}
	
	public function check_preorder(){
		
		$time_sheduale = $this->Front_model->getDataResult('rt_time_schedule',array('status'=>1));
		$open_time ='';
		$close_time ='';	
		if(!empty($time_sheduale)){
			foreach($time_sheduale as $times){
				 if($times['title'] == date('l')){
					 $time_shedual = json_decode($times['description'],true);
					 $open_time = $time_shedual['open_time'];
					 $close_time = $time_shedual['close_time'];
				}
			}
		}
		$TimeArr = array(
					  'open_time'=>$open_time,
					  'close_time'=>$close_time
					  );
		if(strtotime($TimeArr['open_time']) < strtotime(date('g:i A')) && strtotime($TimeArr['close_time']) > strtotime(date('g:i A')))
		{
			$resp = array('status' => 1 , 'error' =>'Open');
		}else{
			$resp = array('status' => 0 , 'error' =>'Closed');
		}
		return $resp;
	}
	
	 /*function save_pre_order($order_id){
		
		if($order_id !='' && isset($_SESSION['preorder_time'])){
			$time = $_SESSION['preorder_time'];
			 
			$Pre_orderArr =  array('payment_order_id'=>$order_id,
								   'pre_order_date'=>$time['pre_order_date'],
								   'pre_order_time'=>$time['pre_order_time'],
								   'status'=>0); 
			if($preorder_id = $this->Front_model->insertData('rt_pre_orders',$Pre_orderArr)){
				unset($_SESSION['preorder_time']);	
				return 1;
			}
		
			
		}else{
			return 0;
		}
	} */
	
	function get_itemoptions(){
		$item_str = '';
		if(isset($_POST['item_id'])){
			$item_options = $this->DatabaseModel->select_data('*','rt_item_options',array('main_item_id'=>$_POST['item_id']),'');
			if(!empty($item_options)){
				$i=1;
				foreach($item_options as $options)
				{	$checked='';
					if($i==1){ $checked = 'checked'; } 
					$item_str .='<lable>'.$options['item_size'].'</lable> <input type="radio" name="item_size" id="item_size" value="'.$options['item_size'].'" '.$checked.'> 
					 <span style="float:right">'.key_text('portalcurreny_symbol').' '.$options['item_price'].'</span><br>';
					$i++;
				}
				$item_str .='<br><button type="submit" class="fd_btn" id="addToCartBtn" onclick="addWithOpt('.$_POST['item_id'].' , 1,); void(0); return false;">Add To Cart</button>';
				
				$resp = array('status'=>1, 'option_list'=>$item_str);
				
			}else{
				
				$resp = array('status'=>2, 'option_list'=>$item_str);
			}
			
		}else{
			$resp = array('status'=>0,'option_list'=>$item_str);
			
		}
		echo json_encode($resp);
	}
	
	
	
}

