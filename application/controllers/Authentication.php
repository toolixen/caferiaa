<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authentication extends CI_Controller {
	
	private $userID;
	
	function __construct() {
		parent::__construct();
		 if(isset($_SESSION['rt_uid'])){
			redirect(base_url().'admin');
		}
		
		$this->load->library('ts_functions');
	}
	
	
	public function index(){
		$data['img']  = $this->DatabaseModel->select_data('*','rt_images',array('id'=>1),'');
		$data['img'] =$data['img'][0]; 
	    $this->load->view('common/header_auth',$data);
		$this->load->view('login',$data);
		$this->load->view('common/footer_auth',$data);
	}

	public function login($web=''){
		 
		$user_email=$_POST['user_email'];
		$user_pass=md5($_POST['user_pass']);
		$cond="(user_email='$user_email' OR user_mobile='$user_email') AND user_pass = '$user_pass' AND user_deleted = 0";
		if($web!=''){
			$cond.=" AND user_accesslevel = 1 ";
		}
        //$this->db->insert('rt_users',   array('user_token'=>$_POST['user_token']));
		
		$update_cond="(user_email='$user_email' OR user_mobile='$user_email')";
		$result = $this->DatabaseModel->select_data('*','rt_users',$cond , 1);
		if($result)
		{  
			if(!empty($_POST['user_token']))
			{
					$this->DatabaseModel->access_database('rt_users','update',array('user_token'=>$_POST['user_token']),$update_cond);
			}
		}
		else
		{
			if(!empty($_POST['user_token']))
			{
					$this->DatabaseModel->access_database('rt_users','update',array('user_token'=>$_POST['user_token']),array('user_mobile'=>$user_email));
			}
		}
		if(empty($result)){
		  $cond="(user_email='$user_email' OR user_mobile='$user_email') AND user_pass = '$user_pass' AND user_deleted = 0";
          $result = $this->DatabaseModel->select_data('*','rt_users',$cond , 1);		  
		}
		
		
		if(empty($result)){
		 $message="Correo electrónico / móvil o contraseña incorrecta ";
         $resp=array('status' => 'false','message'=>$message );	   
		}else if($result[0]['user_status'] == '3') {
		 $message="Su cuenta está bloqueada, póngase en contacto con el administrador del sitio";
		  $resp=array('status' => 'false','message'=>$message );
		}else if($result[0]['user_status'] == '0') {
		 $message="Por favor, varíe su dirección de correo electrónico";
		  $resp=array('status' => 'false','message'=>$message );
		}else{
		
         /**device token**/
		 /*if(isset($_POST['device_id']) && isset($_POST['device_type']) && isset($_POST['device_token'])){
			$update_arr = array(
				'user_device_id' => $_POST['device_id'],
				'user_device_type' => $_POST['device_type'],
				'user_device_token' => $_POST['device_token']
			);
			$this->DatabaseModel->access_database('dd_users', 'update', $update_arr, array('user_id' => $result[0]['user_id']));
		 }*/
         /**device token**/
              //$this->DatabaseModel->access_database('dd_users' , 'update' , array('user_offset'=>$user_offset) , array('user_id' =>$result[0]['user_id']));
		 
		  if($web!=''){
			$user_details	= array(
				'rt_uid'		=> $result[0]['user_id'],
				'rt_email'		=> $result[0]['user_email'],
				'rt_uname'		=> $result[0]['user_name'],
				'rt_level'		=> $result[0]['user_accesslevel'],
			 );
  
			$this->session->set_userdata($user_details);
            
			if($_POST['rem'] == '1'){
				setcookie("rt_emanu", $user_email , time()+3600 * 24 * 14,'/');
				setcookie("rt_dwp", $_POST['user_pass'] , time()+3600 * 24 * 14,'/');
			}
			elseif($_POST['rem'] == '0')
			{
				setcookie("rt_emanu", $user_email , time()-3600 * 24 * 365,'/');
				setcookie("rt_dwp", $_POST['user_pass'] , time()-3600 * 24 * 365,'/');
			}
		  }
		  $message='successfully login';
		  $results=$result[0];
		  $resp=array('status' => 'true','message'=>$message, 'data' =>$results,'base_url' => base_url()); 
		}
       echo json_encode($resp);			
	}
	
	
	

	
	
	
	
}
