<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//Librerias Culqi
require_once getcwd() . '/vendor/autoload.php';

class Api extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('ts_functions');
		$this->load->model('Front_model');
		$this->load->helper(array('info_helper'));
    }
	
    public function index() {
        $this->load->view('api_url');
    }
	// array(
            // 'cat_status' => 1
        // ));
    public function category() {
        $user_id = (isset($_POST['user_id']) ? $_POST['user_id'] : 0);
        $result  = $this->DatabaseModel->select_data('*', 'rt_categories' , array('cat_status'=>1));
        if ($result) {
            echo json_encode(array(
                'status' => 'true',
                'message' => 'Aquí está la lista de categorías',
                'base_url' => base_url(),
                'data' => $result,
                'cart_count' => $this->cart_count($user_id)
            ));
        } else {
            echo json_encode(array(
                'status' => 'false',
                'message' => 'No se ha encontrado ninguna categoría.',
                'data' => array()
            ));
        }
    }
	
	public function policy()
	{
		
		$general= $this->Front_model->getData('rt_genral_setting',array('id'=>1));
		
		echo json_encode(array(
                'status' => 'true',
                'message' => 'succes',
                'data' => strip_tags($general['privacy_policy']),
            ));
	}
	
	function notification()
	{
		if(!empty($_POST['user_id']))
		{
		$user_id=$_POST['user_id'];
		$data=$this->DatabaseModel->select_data(['message','item_pic','date'],'rt_notification',array('userid'=>$user_id));
		if($data)
		{
			echo json_encode(array(
                'status' => 'true',
                'message' => 'success',
                'base_url' => base_url(),
				'data' => $data,
				
			));
		}
		else
		{
			echo json_encode(array(
                'status' => 'false',
                'message' => 'Datos no encontrados',
                'data' => array()
			));
		}
		}
	}
	
    public function items() {
        $success = 'Aquí está la lista de artículos';
        $failure = 'Ningún artículo encontrado en esta categoría';
        $user_id = (isset($_POST['user_id']) ? $_POST['user_id'] : 0);
        if (isset($_POST['start']) && isset($_POST['length'])) {
            $cond            = "item_status = '1' ";
            $order_by        = array(
                'item_name',
                'asc'
            );
            $currentPosition = $_POST['start'];
            $limit           = $_POST['length'];
            if (isset($_POST['cat_id']) && $_POST['cat_id'] != '') {
                $item_cat = $_POST['cat_id'];
                $cond .= " AND item_cat= '$item_cat' ";
            }
            if (isset($_POST['keyword']) && $_POST['keyword'] != '') {
                $keyword = $_POST['keyword'];
                $cond .= "AND ((item_name LIKE '%$keyword%') OR (item_desc LIKE '%$keyword%'))";
            }
            //$join_array = array('dd_user_meta','dd_user_meta.user_id = dd_users.user_id');  
            $itemDetail = $this->DatabaseModel->select_data('*', 'rt_items', $cond, array(
                $limit,
                $currentPosition
            ), '', $order_by);
			
			if(!empty($itemDetail)){
				$i=0;
				foreach($itemDetail as $item){
					
					 $itemDetail[$i]['item_options'] =array();  
					 if($item['item_price'] =='' && $item['item_options'] == 'YES'){ 
						
						$item_options = $this->DatabaseModel->select_data('item_size,item_price','rt_item_options',array('main_item_id'=>$item['item_id']),''); 
						
						if(!empty($item_options)){
							$itemDetail[$i]['item_options'] = $item_options; 
						}
						
					}
					$i++;			   
				}
			}
            $result     = $itemDetail;
            if (count($result)) {
                echo json_encode(array(
                    'status' => 'true',
                    'message' => $success,
                    'symbol' => $this->ts_functions->getsettings('portalcurreny', 'symbol'),
                    'curreny' => $this->ts_functions->getsettings('portal', 'curreny'),
                    'data' => $result,
                    'cart_count' => $this->cart_count($user_id)
                ));
            } else {
                echo json_encode(array(
                    'status' => 'false',
                    'message' => $failure,
                    'data' => array()
                ));
            }
        }
    }
	
	
    public function check_out() {
        $user_id     = $_POST['user_id'];
        $result      = array();
        $cond        = "user_id = '$user_id' AND user_status = 1 AND user_deleted = 0";
        $user_detail = $this->DatabaseModel->select_data('*', 'rt_users', $cond, 1);
        if ($user_detail) {
            $result['user_status'] = 1;
        } else {
            $result['user_status'] = 0;
        }
        $result['wallet_amount']        = $this->check_wallet($user_id);
        $result['portal_curreny']       = $this->ts_functions->getsettings('portal', 'curreny');
        $result['portalcurreny_symbol'] = $this->ts_functions->getsettings('portalcurreny', 'symbol');
        $result['payu_status']          = $this->ts_functions->getsettings('payu', 'status');
        $result['payu_merchantKey']     = $this->ts_functions->getsettings('payu', 'merchantKey');
        $result['payu_merchantSalt']    = $this->ts_functions->getsettings('payu', 'merchantSalt');
        $result['wallet_status']        = $this->ts_functions->getsettings('wallet', 'status');
        $result['cod_status']           = $this->ts_functions->getsettings('cod', 'status');
        echo json_encode(array(
            'status' => 'true',
            'data' => $result,
            'cart_count' => $this->cart_count($user_id)
        ));
    }
	
	
    function success_payment() {
        // $this->DatabaseModel->access_database('rt_test', 'insert', array(
            // 'data' => json_encode($_POST)
        // ), '');
        // die; 
        // $payUresp = '{"mihpayid":"7333388295","mode":"DC","status":"success","unmappedstatus":"captured","key":"86Dss3U0","txnid":"b7631755ea7bd8f9e32d","amount":"4.0","addedon":"2018-08-28 14:21:37","productinfo":"Order Food","firstname":"Akash","lastname":"","address1":"","address2":"","city":"","state":"","country":"","zipcode":"","email":"aakash@gmail.com","phone":"8770573379","udf1":"6","udf2":"Order","udf3":"[{&quot;item&quot;:&quot;76&quot;,&quot;quantity&quot;:&quot;1&quot;,&quot;amount&quot;:&quot;4&quot;}]","udf4":"","udf5":"","udf6":"","udf7":"","udf8":"","udf9":"","udf10":"","hash":"7ad0674e429060a9d5a7b05cb0dac129f10e9e9366cc594d1c2be065e076f4e569dcb3b4b8bce78c8156242baf5832d68c046600840214b14147f2c24823e4f7","field1":"5354463413826160304021","field2":"981516","field3":"4.00","field4":"7333388295","field5":"100","field6":"02","field7":"7333388295","field8":"","field9":"Transaction is Successful","PG_TYPE":"HdfcCYBER","encryptedPaymentId":"6EFCC621ECAEEA4F63855CDF361C3A40","bank_ref_num":"5354463413826160304021","bankcode":"MAST","error":"E000","error_Message":"No Error","name_on_card":"Shubham","cardnum":"538103XXXXXX1462","cardhash":"This field is no longer supported in postback params.","amount_split":"{\"PAYU\":\"4.0\"}","payuMoneyId":"205820351","discount":"0.00","net_amount_debit":"4"}';
        // $_POST    = json_decode($payUresp, true);
        if (isset($_POST['txnid']) && $_POST['status'] == 'success') {
            $user_id        = $_POST['udf1'];
            
            $udf2_Arr = explode('|',$_POST['udf2']);
            $type = $udf2_Arr[0];
            
            
            $payment_amount = $_POST['amount'];
			$copon_Arr = explode('|', $_POST['udf3']);
            if ($type == 'Order') {
                $ReqData                    = array();
                $ReqData['user_id']         = $user_id;
                $ReqData['items_array']     = html_entity_decode($_POST['udf5']);
                $ReqData['payment_address'] = $_POST['udf4'];
                $ReqData['payment_amount']  = $_POST['amount'];
                $ReqData['payment_mode']    = 'PayU';
                $ReqData['payment_txnId']   = $_POST['txnid'];
				$ReqData['discount_amount'] = $copon_Arr[1];
				$ReqData['coupon_code']     = $copon_Arr[0];
				$ReqData['delivery_type']     = $udf2_Arr[1];
		        $ReqData['special_instructions']  = $udf2_Arr[2];
		        
                $this->processed_order($ReqData);
            }
        }
    }
	
	function payu_fail()
	{
		echo json_encode(array(
            'status' => 'flase',
			'message'=>'Process failed',
            'data' => array(),
        ));
	}
	
	function success_payment_culqi()
    {
        // $this->DatabaseModel->access_database('rt_test', 'insert', array(
        // 'data' => json_encode($_POST)
        // ), '');
        // die;
        // $payUresp = '{"mihpayid":"7333388295","mode":"DC","status":"success","unmappedstatus":"captured","key":"86Dss3U0","txnid":"b7631755ea7bd8f9e32d","amount":"4.0","addedon":"2018-08-28 14:21:37","productinfo":"Order Food","firstname":"Akash","lastname":"","address1":"","address2":"","city":"","state":"","country":"","zipcode":"","email":"aakash@gmail.com","phone":"8770573379","udf1":"6","udf2":"Order","udf3":"[{&quot;item&quot;:&quot;76&quot;,&quot;quantity&quot;:&quot;1&quot;,&quot;amount&quot;:&quot;4&quot;}]","udf4":"","udf5":"","udf6":"","udf7":"","udf8":"","udf9":"","udf10":"","hash":"7ad0674e429060a9d5a7b05cb0dac129f10e9e9366cc594d1c2be065e076f4e569dcb3b4b8bce78c8156242baf5832d68c046600840214b14147f2c24823e4f7","field1":"5354463413826160304021","field2":"981516","field3":"4.00","field4":"7333388295","field5":"100","field6":"02","field7":"7333388295","field8":"","field9":"Transaction is Successful","PG_TYPE":"HdfcCYBER","encryptedPaymentId":"6EFCC621ECAEEA4F63855CDF361C3A40","bank_ref_num":"5354463413826160304021","bankcode":"MAST","error":"E000","error_Message":"No Error","name_on_card":"Shubham","cardnum":"538103XXXXXX1462","cardhash":"This field is no longer supported in postback params.","amount_split":"{\"PAYU\":\"4.0\"}","payuMoneyId":"205820351","discount":"0.00","net_amount_debit":"4"}';
        // $_POST    = json_decode($payUresp, true);
        if (isset($_POST['txnid'])){ //&& $_POST['status'] == 'success') {
            $user_id = $_POST['udf1'];

            $udf2_Arr = explode('|', $_POST['udf2']);
            $type = $udf2_Arr[0];


            $payment_amount = str_replace(".", "", $_POST['amount']);
            $copon_Arr = explode('|', $_POST['udf3']);
            if ($type == 'Order') {
                $ReqData = array();
                $ReqData['user_id'] = $user_id;
                $ReqData['items_array'] = html_entity_decode($_POST['udf5']);
                $ReqData['payment_address'] = $_POST['udf4'];
                $ReqData['payment_amount'] = $_POST['amount'];
                $ReqData['payment_mode'] = 'Culqi';
                $ReqData['payment_txnId'] = $_POST['txnid'];
                $ReqData['discount_amount'] = $copon_Arr[1];
                $ReqData['coupon_code'] = $copon_Arr[0];
                $ReqData['delivery_type'] = $udf2_Arr[1];
                $ReqData['special_instructions'] = $udf2_Arr[2];

                //$this->load->model('Front_model');
        		$setting = $this->Front_model->getData('rt_settings',array('key_text'=>'sk_secreta_culqi'));
        		$sk_secreta	 =  $setting['value_text'];

                $SECRET_API_KEY = $sk_secreta;
                $culqi = new Culqi\Culqi(array('api_key' => $SECRET_API_KEY));

                $token = (isset($_POST['payment_txnId']) ? $_POST['payment_txnId'] : '');
                $result = array();
                //"description" => html_entity_decode($_POST['udf5']),
                try {
                    // Creando Cargo a una tarjeta
                    $charge = $culqi->Charges->create(
                        array(
                            "amount" => $payment_amount * 1.0,
                            "capture" => true,
                            "currency_code" => "PEN",
                            "description" => "user_id: ".$user_id."| Amount: ".$payment_amount,
                            "email" => $_POST['email'],
                            "installments" => 0,
                            "source_id" => $_POST["token"]
                        )
                    );
                    
                    $this->processed_order($ReqData);
                    //$result['order_number'] = $charge;

                } catch (Exception $e) {

//                    echo json_encode($e->getMessage());
                    $result['order_number'] = 0;
                    
                    $error = json_decode($e->getMessage(), true);
                    
                    $resp = array(
                        'status' => 0,
                        'message' => $error['user_message'],
                        //'message' => $e->getMessage(),
                        'data' => $result,
                        'cart_count' => $this->cart_count($user_id)
                    );
                    echo json_encode($resp);
                }
            }
        }
    }
	
    function processed_order($GET = "") {
        if (isset($_POST['payment_mode']) || isset($GET['payment_mode'])) {
            if (isset($GET['payment_mode'])) {
                $_POST = $GET;
            }
            $continue        = $status = 0;
            $result          = array();
            $user_id         = $_POST['user_id'];
            $payment_pid     = $_POST['items_array'];
            
           $couponCode     = $_POST['coupon_code'];
            $discountAmount     = $_POST['discount_amount'];
            
            
            $item=json_decode($payment_pid, true);
			$item_id=$item[0]['item'];
			$idata=$this->DatabaseModel->access_database('rt_items','select','',array('item_id'=>$item_id));
			$newq=$idata[0]['item_quantity']-$item[0]['quantity'];;
			$payment_mode    = $_POST['payment_mode'];
            $payment_amount  = $_POST['payment_amount'];
			$special_instruct = $_POST['special_instructions'];
			$delivery_type = $_POST['delivery_type'];
			
			$user_info       = $this->DatabaseModel->select_data('*','rt_users',array('user_id'=>$user_id));
			// $payment_address = (isset($_POST['payment_address']) ? $_POST['payment_address'] : $user_info[0]['user_address']); 
		     $payment_address = $user_info[0]['user_address'];  
			// $payment_address = $_POST['payment_address']; 
			
            $payment_txnId   = (isset($_POST['payment_txnId']) ? $_POST['payment_txnId'] : '');
            $payUniqid       = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 8);
            $payment_status  = ($payment_mode == 'cod' ? 0 : 1);
            $paymentdetails  = array(
                'payment_uid' => $user_id,
                'payment_uniqid' => $payUniqid,
                'payment_pid' => $payment_pid,
                'payment_status' => $payment_status,
                'payment_mode' => $payment_mode,
                'payment_amount' => $payment_amount,
                'payment_date' => date('Y-m-d H:i:s'),
                'payment_address' => $payment_address,
                'payment_txnId' => $payment_txnId,
                'used_coupen_code' => $couponCode,
                'discount_amount' => $discountAmount,
				'special_instructions'=>$special_instruct,
				'delivery_type'=>$delivery_type,
            );
			
            if ($payment_mode == 'wallet') {
                $wallet_amount = $this->check_wallet($user_id);
                if ($wallet_amount < $payment_amount) {
                    $message = "Cantidad de billetera insuficiente!";
                } else {
                    $new_wallet_amount = $wallet_amount - $payment_amount;
                    $this->DatabaseModel->access_database('rt_wallet', 'update', array(
                        'wallet_amount' => $new_wallet_amount
                    ), array(
                        'wallet_uid' => $user_id
                    ));
                    $continue++;
                }
            } else {
                //echo "<pre>";print_r($paymentdetails);die;
                $continue++;
            }
            if ($continue != 0) {
                $status  = 1;
                $message = "Pago realizado con éxito!";
               $payment_id = $this->Front_model->insertData('rt_paymentdetails',$paymentdetails);
			   //$this->DatabaseModel->access_database('rt_paymentdetails', 'insert', $paymentdetails, '');
			    $this->DatabaseModel->access_database('rt_items', 'update', array('item_quantity'=>$newq),array('item_id'=>$item_id));
			  /*  if(isset($_POST['time']) && $_POST['ispreorder'] ==1 ){
					$time_Arr = explode('@#',$_POST['time']);
					$pre_orderArr  = array('payment_order_id'=>$payment_id,
											'pre_order_date'=>$time_Arr[0],
											'pre_order_time'=>$time_Arr[1],
											'status'=>0);
					$this->DatabaseModel->access_database('rt_pre_orders', 'insert',$pre_orderArr, '');
				} */
			   
                $result['order_number'] = $payUniqid;
                $this->ts_functions->sendbookingemails($payUniqid);
                $this->DatabaseModel->access_database('rt_cart', 'delete', '', array(
                    'cart_uid' => $user_id
                ));
            }
            $resp = array(
                'status' => $status,
                'message' => $message,
                'data' => $result,
                'cart_count' => $this->cart_count($user_id)
            );
            echo json_encode($resp);
        }
    }
	
    function check_wallet($user_id) {
        $wallet_amount = 0;
        $wallet_detail = $this->DatabaseModel->select_data('*', 'rt_wallet', array(
            'wallet_uid' => $user_id
        ), 1);
        if ($wallet_detail) {
            $wallet_amount = $wallet_detail[0]['wallet_amount'];
        }
        return $wallet_amount;
    }
	
    function my_orders() {
        if (isset($_POST['start']) && isset($_POST['length'])) {
            $user_id          = $_POST['user_id'];
            $user_accesslevel = $_POST['user_accesslevel'];
            if ($user_accesslevel == 3) {
                $cond = "payment_duid = '$user_id' ";
            } else {
                $cond = "payment_uid = '$user_id' ";
            }
            $order_by        = array(
                'payment_id',
                'desc'
            );
            $currentPosition = $_POST['start'];
            $limit           = $_POST['length'];
            $join            = array(
                'rt_users',
                'rt_users.user_id=rt_paymentdetails.payment_uid'
            );
            $orderDetail     = $this->DatabaseModel->select_data('*', 'rt_paymentdetails', $cond, array(
                $limit,
                $currentPosition
            ), $join, $order_by);
            $results         = array();
            if (!empty($orderDetail)) {
                $total_order = count($orderDetail);
                for ($i = 0; $i < $total_order; $i++) {
                    $results[$i]['order_id']         = $orderDetail[$i]['payment_id'];
                    $results[$i]['order_number']     = $orderDetail[$i]['payment_uniqid'];
                    $results[$i]['order_amount']     = $orderDetail[$i]['payment_amount'];
                    $results[$i]['order_date']       = $orderDetail[$i]['payment_date'];
                    $results[$i]['order_note']       = $orderDetail[$i]['payment_note'];
                    $results[$i]['order_mode']       = $orderDetail[$i]['payment_mode'];
                    $ordSta                          = $orderDetail[$i]['payment_order_status'];
                    $order_status                    = ($ordSta == '0' ? 'Pending' : ($ordSta == '1' ? ' In-Transits' : ($ordSta == '2' ? 'Delivered' : ($ordSta == '3' ? 'Canceled' : 'Other'))));
                    $results[$i]['order_status']     = $order_status;
                    $results[$i]['order_status_num'] = $ordSta;
                    $results[$i]['user_name']        = $orderDetail[$i]['user_name'];
                    $results[$i]['user_mobile']      = $orderDetail[$i]['user_mobile'];
                    $results[$i]['user_address']     = $orderDetail[$i]['user_address'];
                }
                echo json_encode(array(
                    'status' => 'true',
                    'message' => 'Aquí está la lista de pedidos',
                    'data' => $results,
                    'cart_count' => $this->cart_count($user_id),
					'portal_currency'=>$this->ts_functions->getsettings('portal','curreny'),
					'portalcurreny_symbol'=>$this->ts_functions->getsettings('portalcurreny','symbol'),
                ));
            } else {
                echo json_encode(array(
                    'status' => 'false',
                    'message' => 'No se ha encontrado ningún pedido.',
                    'data' => array()
                ));
            }
        }
    }
	
	
	
	
	function my_last_orders() {
        if (isset($_POST['user_id']) && isset($_POST['user_accesslevel'])) {
            $user_id          = $_POST['user_id'];
            $user_accesslevel = $_POST['user_accesslevel'];
            if ($user_accesslevel == 3) {
                $cond = "payment_duid = '$user_id' ";
            } else {
                $cond = "payment_uid = '$user_id' ";
            }
            $order_by        = array(
                'payment_id',
                'desc'
            );
           // $currentPosition = $_POST['start'];
            $limit           = 1; //$_POST['length'];
            $join            = array(
                'rt_users',
                'rt_users.user_id=rt_paymentdetails.payment_uid'
            );
            $orderDetail     = $this->DatabaseModel->select_data('*', 'rt_paymentdetails', $cond, $limit, $join, $order_by);
            $results         = array();
            if (!empty($orderDetail)) {
                $total_order = count($orderDetail);
                for ($i = 0; $i < $total_order; $i++) {
                    $results[$i]['order_id']         = $orderDetail[$i]['payment_id'];
                    $results[$i]['order_number']     = $orderDetail[$i]['payment_uniqid'];
                    $results[$i]['order_amount']     = $orderDetail[$i]['payment_amount'];
                    $results[$i]['order_date']       = $orderDetail[$i]['payment_date'];
                    $results[$i]['order_note']       = $orderDetail[$i]['payment_note'];
                    $results[$i]['order_mode']       = $orderDetail[$i]['payment_mode'];
                    $ordSta                          = $orderDetail[$i]['payment_order_status'];
                    $order_status                    = ($ordSta == '0' ? 'Pending' : ($ordSta == '1' ? ' In-Transits' : ($ordSta == '2' ? 'Delivered' : ($ordSta == '3' ? 'Canceled' : 'Other'))));
                    $results[$i]['order_status']     = $order_status;
                    $results[$i]['order_status_num'] = $ordSta;
                    $results[$i]['user_name']        = $orderDetail[$i]['user_name'];
                    $results[$i]['user_mobile']      = $orderDetail[$i]['user_mobile'];
                    $results[$i]['user_address']     = $orderDetail[$i]['user_address'];
                }
                echo json_encode(array(
                    'status' => 'true',
                    'message' => 'Aquí está la lista de pedidos',
                    'data' => $results,
                    'cart_count' => $this->cart_count($user_id),
					'portal_currency'=>$this->ts_functions->getsettings('portal','curreny'),
					'portalcurreny_symbol'=>$this->ts_functions->getsettings('portalcurreny','symbol'),
                ));
            } else {
                echo json_encode(array(
                    'status' => 'false',
                    'message' => 'No se ha encontrado ningún pedido.',
                    'data' => array()
                ));
            }
        }
    }
	
	
	
	
    
    
    function get_orderid(){
        if($_POST['payment_mode'] !=''  && $_POST['payment_txnid'] !='')
        {
            $cond  = array(
                     'payment_mode' =>trim($_POST['payment_mode']),
                     'payment_txnId' =>trim($_POST['payment_txnid'])
                    );
            $orderDetail = $this->DatabaseModel->select_data('*', 'rt_paymentdetails', $cond, 1);
            if ($orderDetail) 
            {
                $payment_orderid = $orderDetail[0]['payment_uniqid'];
                $resp = array('status' =>'true','order_id' =>$payment_orderid);
            }else{
                $resp = array('status' =>'false','order_id' =>'');
            }
        }else{
             $resp = array('status' =>'false','order_id' =>'');
            
        }
        
         echo json_encode($resp);  
    }
        
    
	
    function order_details() {
        $payment_id  = $_POST['order_id'];
        $payment_uid = $_POST['user_id'];
        $cond        = array(
            'payment_id' => $payment_id,
            'payment_uid' => $payment_uid
        );
        $orderDetail = $this->DatabaseModel->select_data('*', 'rt_paymentdetails', $cond, 1);
        $result      = array();
        if ($orderDetail) {
            $items    = json_decode($orderDetail[0]['payment_pid'], true);
            $item_arr = array();
            foreach ($items as $item) {
                $temp        = array();
                $item_pic    = base_url() . 'assets/admin/images/menus/default.jpg';
                $item_name   = 'Ya no está disponible';
                $itemsDeatil = $this->DatabaseModel->select_data('*', 'rt_items', array(
                    'item_id' => $item['item']
                ), 1);
               
                $item_price=0;
                if ($itemsDeatil) {
                    if ($itemsDeatil[0]['item_pic'] != '') {
                        $item_pic = base_url() . $itemsDeatil[0]['item_pic'];
                    }
                    $item_name = $itemsDeatil[0]['item_name'];
                    
                    if($itemsDeatil[0]['item_price'] =='' && $itemsDeatil[0]['item_options'] == 'YES'){ 
						
						$item_options = $this->DatabaseModel->select_data('item_size,item_price','rt_item_options',array('main_item_id'=>$itemsDeatil[0]['item_id'],'item_size'=>$item['size']),''); 
						  
						if(!empty($item_options)){
							$itemsDeatil[0]['item_price'] = $item_options[0]['item_price']; 
						}
					}
                    
                    $item_price= $itemsDeatil[0]['item_price'];
                    
                }
                
                $temp = array(
                    'item_id'=>$item['item'],
                    'item_name' => $item_name,
                    'item_pic' => $item_pic,
                    'quantity' => $item['quantity'],
                    'amount' => $item_price,
                    'size' =>$item['size'],
                );
                array_push($item_arr, $temp);
            }
            $result['payment_amount'] = $orderDetail[0]['payment_amount'];
            $result['items_array']    = $item_arr;
            $resp                     = array(
                'status' => 'true',
                'message' => 'Aquí están los detalles del pedido',
                'data' => $result,
                'cart_count' => $this->cart_count($payment_uid),
				'portal_currency'=>$this->ts_functions->getsettings('portal','curreny'),
				'portalcurreny_symbol'=>$this->ts_functions->getsettings('portalcurreny','symbol'),
            );
        } else {
            $resp = array(
                'status' => 'false',
                'message' => 'ID de pedido no válido',
                'data' => array()
            );
        }
        echo json_encode($resp);
    }
	
	
	
	
	public function gettodayoffers()
	{
		$today = date('Y-m-d H:i:s');
		$where = array('offer_status' => 1, 'offer_date >=' => $today);
		$offerList = $this->DatabaseModel->get_data('rt_offers', $where);
		
		$resultdata = array();
		$tot_amount = array();
		
		if($offerList){
			foreach($offerList as $offer){
				$itemdata = array();
				
				$offer_pic=base_url().'assets/admin/images/menus/default.jpg';
				if($offer['offer_pic']!=''){
					$offer_pic=base_url().'assets/admin/images/offers/thumb/'.$offer['image_thumb']; 
				}
				
				if($offer['offer_products'] != ''){
					$items = json_decode($offer['offer_products'], true);
					
					foreach($items as $item){
						if( $data = get_itemdata_by_id($item['item_id']) ){
						    $data[0]['item_quantity'] = $item['quantity'];
							$itemdata[] = $data[0];
						}
					}
				}
				
				$amt = $offer['offer_price'];
				//$min_orderamount = $this->ts_functions->getsettings('min','orderamount');
				
				$resultdata[] = array('offer_id' => $offer['id'], 'title' => $offer['offer_name'], 'image' => $offer_pic, 'total_amount' => $amt, 'items' => $itemdata);
			}
			
			echo json_encode(array(
                'status' => 'true',
                'message' => 'success',
				'data' => $resultdata,
				
			));
		}else{
			echo json_encode(array(
                'status' => 'false',
                'message' => 'Datos no encontrados',
                'data' => array()
			));
		}
		
		exit();
	}
	
	
	
    function add_to_cart() {
        $user_id       = $_POST['user_id'];
        $item_id       = $_POST['item_id'];
		$item_size       = $_POST['size']; 
        $cart_items    = array();
        $item_quantity = $_POST['item_quantity'];
        $cartDetail    = $this->DatabaseModel->select_data('cart_id,cart_items', 'rt_cart', array(
            'cart_uid' => $user_id
        ), 1);
        $temp_cart     = array();
        if ($cartDetail) {
            $cart_items  = json_decode($cartDetail[0]['cart_items'], true);
            $update_cart = 0;
            foreach ($cart_items as $soloItem) {
                $item     = $soloItem['item']; 
                $quantity = $soloItem['quantity'];
				$size = $soloItem['size'];
                if ($item == $item_id &&  $size == $item_size) {
                    $update_cart = 1;
                    $quantity    = $quantity + $item_quantity;
                }
                array_push($temp_cart, array(
                    'item' => $item,
                    'quantity' => $quantity,
					'size'=>$size,
                ));
            }
            if ($update_cart == 0) {
                array_push($temp_cart, array(
                    'item' => $item_id,
                    'quantity' => $item_quantity,
					'size'=>$item_size,
                ));
            }
            $cart_items = $temp_cart;
        } else {
            array_push($cart_items, array(
                'item' => $item_id,
                'quantity' => $item_quantity,
				'size'=>$item_size,
            ));
        }
        $dataIns = array(
            'cart_uid' => $user_id,
            'cart_items' => json_encode($cart_items)
        );
        if ($cartDetail) {
            $this->DatabaseModel->access_database('rt_cart', 'update', $dataIns, array(
                'cart_id' => $cartDetail[0]['cart_id']
            ));
        } else {
            $this->DatabaseModel->access_database('rt_cart', 'insert', $dataIns, '');
        }
        $resp = array(
            'status' => 'true',
            'message' => 'Artículo agregado al carrito con éxito',
            'data' => array(),
            'cart_count' => $this->cart_count($user_id)
        );
        echo json_encode($resp);
    }
	
    function remove_to_cart() {
        if (isset($_POST['item_quantity']) && isset($_POST['item_id'])) {
            $item       = $_POST['item_id'];
            $quantity   = $_POST['item_quantity'];
            $user_id    = $_POST['user_id'];
            $cartDetail = $this->DatabaseModel->select_data('cart_id,cart_items', 'rt_cart', array(
                'cart_uid' => $user_id
            ), 1);
            if ($cartDetail) {
                $cart_arr = json_decode($cartDetail[0]['cart_items'], true);
            }
            if ($cart_arr) {
                foreach ($cart_arr as $key => $val) {
                    if ($val["item"] == $item && $val['quantity'] == $quantity) {
                        unset($cart_arr[$key]);
                    }
                }
            }
            if ($cart_arr) {
                $cart_arr   = array_values($cart_arr);
                $cart_items = json_encode($cart_arr);
                $dataIns    = array(
                    'cart_uid' => $user_id,
                    'cart_items' => $cart_items
                );
                $this->DatabaseModel->access_database('rt_cart', 'update', $dataIns, array(
                    'cart_uid' => $user_id
                ));
            } else {
                $this->DatabaseModel->access_database('rt_cart', 'delete', '', array(
                    'cart_uid' => $user_id
                ));
            }
            $resp = array(
                'status' => 'true',
                'message' => 'Artículo eliminado del carrito con éxito',
                'data' => array(),
                'cart_count' => $this->cart_count($user_id)
            );
            echo json_encode($resp);
        }
    }
	
	
    function my_cart() {
        $user_id    = $_POST['user_id'];
        $cartDetail = $this->DatabaseModel->select_data('*', 'rt_cart', array(
            'cart_uid' => $user_id
        ), 1);
        $cart_arr   = array();
        if ($cartDetail) {
            $cart_items = json_decode($cartDetail[0]['cart_items'], true);
            foreach ($cart_items as $soloItem) {
                $itemDetail = $this->DatabaseModel->select_data('*', 'rt_items', array(
                    'item_id' => $soloItem['item']
                ), 1);
                if ($itemDetail) {
					
					
					if($itemDetail[0]['item_price'] =='' && $itemDetail[0]['item_options'] == 'YES'){ 
						
						$item_options = $this->DatabaseModel->select_data('item_size,item_price','rt_item_options',array('main_item_id'=>$itemDetail[0]['item_id'],'item_size'=>$soloItem['size']),''); 
						
						if(!empty($item_options)){
							$itemDetail[0]['item_price'] = $item_options[0]['item_price']; 
						}
					}
					
					
					
                    $temp                  = array();
                    $temp['item_id']       = $itemDetail[0]['item_id'];
                    $temp['item_name']     = $itemDetail[0]['item_name'];
                    $temp['item_pic']      = $itemDetail[0]['item_pic'];
                    $temp['item_price']    = $itemDetail[0]['item_price'];
                    $temp['item_quantity'] = $soloItem['quantity'];
					$temp['item_size'] = $soloItem['size'];
					$temp['max_quantity'] = $itemDetail[0]['item_quantity'];
                    array_push($cart_arr, $temp);
                }
            }
        }
        $result          = array();
        $result['count'] = count($cart_arr);
        $result['Items'] = $cart_arr;
        if ($cart_arr) {
            $resp = array(
                'status' => 'true',
                'message' => 'Aqui esta tu carrito',
                'data' => $result,
                'cart_count' => $this->cart_count($user_id),
				'portal_currency'=>$this->ts_functions->getsettings('portal','curreny'),
				'portalcurreny_symbol'=>$this->ts_functions->getsettings('portalcurreny','symbol'),
            );
        } else {
            $resp = array(
                'status' => 'false',
                'message' => 'Sin datos en su carrito',
                'data' => $result
            );
        }
        echo json_encode($resp, JSON_UNESCAPED_SLASHES);
    }
	
    function cart_count($user_id) {
        $cartDetail = $this->DatabaseModel->select_data('*', 'rt_cart', array(
            'cart_uid' => $user_id
        ), 1);
        $cart_arr   = array();
        if ($cartDetail) {
            $cart_items = json_decode($cartDetail[0]['cart_items'], true);
            foreach ($cart_items as $soloItem) {
                $temp            = array();
                $temp['item_id'] = $soloItem['item'];
                array_push($cart_arr, $temp);
            }
        }
        return count($cart_arr);
    }
	
    function add_request() {
        if (isset($_POST['mobile']) && isset($_POST['email']) && isset($_POST['message'])) {
            $user_email  = $_POST['email'];
            $user_mobile = $_POST['mobile'];
            $message     = $_POST['message'];
            $cond        = "user_email = '$user_email' || user_mobile = '$user_mobile' ";
            $userDetail  = $this->DatabaseModel->select_data('user_id', 'rt_users', $cond, 1);
            if (empty($userDetail)) {
                $cond       = "req_mobile = '$user_email' || req_mobile = '$user_mobile' ";
                $reqDetails = $this->DatabaseModel->select_data('req_id', 'rt_request', $cond, 1);
                if (empty($reqDetails)) {
                    $dataIns = array(
                        'req_mobile' => $user_mobile,
                        'req_email' => $user_email,
                        'req_message' => $message,
                        'req_date' => date('Y-m-d H:i:s')
                    );
                    $this->DatabaseModel->access_database('rt_request', 'insert', $dataIns, '');
                    $status  = 'true';
                    $message = "Su solicitud es aceptada y en proceso, recibirá una notificación cuando la aprobemos.";
                } else {
                    $status  = 'false';
                    $message = "Espere que su solicitud ya esté en proceso, recibirá una notificación cuando la aprobemos.";
                }
            } else {
                $status  = 'false';
                $message = "Ya existe un correo electrónico o móvil con nosotros, pruebe con otros";
            }
            $resp = array(
                'status' => $status,
                'message' => $message,
                'data' => array()
            );
            echo json_encode($resp, JSON_UNESCAPED_SLASHES);
        }
    }
	
    function test() { 
        $this->ts_functions->sendbookingemails(44);
    }
	
    function canceled_payment() {
        echo 'Oops your payment is canceled please try again';
    }
	
    /*function update_user() {
		if (isset($_POST['user_id'])) {
            $user_id      = $_POST['user_id'];
            $user_address = $_POST['user_address'];
            $user_mobile  = $_POST['user_mobile'];
            $check_mobile = $this->DatabaseModel->select_data('user_id', 'rt_users', array(
                'user_mobile' => $user_mobile,
                'user_id !=' => $user_id
            ), 1);
            if (empty($check_mobile)) {
                $dataArr = array(
                    'user_name' => trim($_POST['user_name']),
                    'user_address' => trim($_POST['user_address'])
                );
				if($_FILES)
				{
			   if ($_FILES['user_pic']['name'] != '') {
                    $imagename = $this->ts_functions->upload_image('assets/images/users/', 'user_pic');
                    if ($imagename != '') {
                        $user_detail = $this->DatabaseModel->select_data('user_pic', 'rt_users', array(
                            'user_id' => $user_id
                        ), 1);
                        if ($user_detail[0]['user_pic'] != '') {
                            $path     = dirname(__FILE__);
                            $abs_path = explode('application', $path);
                            unlink($abs_path[0] . $user_detail[0]['user_pic']);
                        }
                        $dataArr['user_pic'] = 'assets/images/users/' . $imagename;
                    }
                }
				}
                $this->DatabaseModel->access_database('rt_users', 'update', $dataArr, array(
                    'user_id' => $user_id
                ));
                $status  = 'true';
				$info=$this->DatabaseModel->access_database('rt_users','select','',array('user_id' => $user_id));
				//$info=array('img_url'=>$dataArr['user_pic'],'name'=>$_POST['user_name'],'mobile_no'=>$_POST['user_mobile']);
                $message = "profile updated successfully";
            } else {
                $status  = 'false';
                $message = 'This mobile number is already taken by someone , Please try other mobile number';
            }
        } else {
            $status  = 'false';
            $message = 'Something missing';
        }
        $resp = array(
            'status' => $status,
            'message' => $message,
            'data' => $info[0]
        );
        echo json_encode($resp);
    }*/
	function update_user() 
	{
			 if(isset($_POST['user_id']))
			 {

				 $user_id      = $_POST['user_id'];
				 $user_address = $_POST['user_address'];
				 $user_mobile  = $_POST['user_mobile'];
				 $user_mobile  = $_POST['user_name'];
				 $data=$this->input->post();
				 unset($data['user_id']);
				 if(!empty($_FILES))
				 {
					 $imagename = $this->ts_functions->upload_image('assets/admin/images/users/', 'user_pic');
					 $img="assets/admin/images/users/".$imagename;
					 $data['user_pic']=$img;
				 }
				 $res=$this->DatabaseModel->access_database('rt_users','update',$data,array('user_id'=>$user_id)); 
				if($res)
				{
					$user_data = $this->DatabaseModel->select_data(array('user_name','user_address','user_mobile','user_pic'),'rt_users', array(
                     'user_id' => $user_id));
					 $resp = array(
						'status' => 'true',
						'message' => 'Actualización exitosa',
						'data' => $user_data[0]
					);
				}
				else
				{
					  $resp = array(
						'status' => 'false',
						'message' => 'Perfil no actualizado',
						'data' => ''
					);
				}
				echo json_encode($resp);
			 }
	}
	
    function forget_password() {
        if (isset($_POST['user_email'])) {
			$email_mobile = $_POST['user_email'];
            $data=$this->input->post();
			$this->load->model('Front_model');
			$where_condition ="user_email='$email_mobile' OR user_mobile='$email_mobile'";
			if($this->Front_model->getData('rt_users',$where_condition))
					{
						   $token =rand();  // $token = random_string('alnum', 8);
						   $row = $this->Front_model->getData('rt_users',$where_condition);
						        $res = $this->valid_email($_POST['user_email']);
								if($this->Front_model->updateData('rt_users', array('user_otp'=>$token) ,array('user_email' => $_POST['user_email'] ) )){
									$forgot_Msg	 = base_url("front/reset_password/1/").$token. '/'. $row['user_id'] . ' Reset Password ';
									if($res =='email'){
									$setting = $this->Front_model->getData('rt_settings',array('key_text'=>'email_fromemail'));
									$from	 =  $setting['value_text'];
									$to		 =	$_POST['user_email'];
									$subject =  'Reset Password';		
									$body	 = 	'<a href=" '. base_url("front/reset_password/1/").$token. '/'. $row['user_id'] . ' "> Reset Password </a>';
									$this->ts_functions->sendnotificationemails('forgotps',$to ,'Reset Password','',$body);
									
									}else if($res =='mobile'){
										$this->ts_functions->send_msg($email_mobile,$forgot_Msg);
									}
									$this->response('success',['success'=>['msg'=>"¡Revisa tu correo o móvil! Se envió link para reestablecer cuenta"]]);
								  
							   }else{
								  echo json_encode(['failed'=>['msg'=>"Algo salió mal, intenta de nuevo"]]);
							   }
					
					
					}else{
						$this->response('failed',['failed'=>['msg'=>"¡Este correo electrónico no está disponible en el registro! Inténtalo de nuevo"]]);
					}
           
		   
		   /* $user_otp    = substr(str_shuffle("0123456789zABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 6);
            $this->DatabaseModel->access_database('rt_users', 'update', array(
                'user_otp' => $user_otp
            ), array(
                'user_mobile' => $user_mobile
            ));
            $status  = 'true';
            $message = 'Now enter Otp';
        } else {
            $status  = 'false';
            $message = 'Something missing';
        }
        $resp = array(
            'status' => $status,
            'message' => $message,
            'data' => array()
        );
        echo json_encode($resp);*/
	}
    }
	
	
	function valid_email($str) {
		if(preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)){
			return 'email';
		}else if(!preg_match ("/[^0-9]/", $str)){
			return 'mobile';
		}else{
			return false;
		}
	}
	
	
	
	function response($param = null,$response= null){
		
	if($param != null && $response != null)	
		if($param == 'success'){
			
			$this->output
					->set_status_header(200)
					->set_content_type('application/json', 'utf-8')
					->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
					->_display();
			exit;
		}else{
			
			$this->output
					->set_status_header(403)
					->set_content_type('application/json', 'utf-8')
					->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
					->_display();
			exit;		
		}
	
	}
	
    function update_password() {
        if (isset($_POST['user_otp'])) {
            $user_otp  = $_POST['user_otp'];
            $user_pass = $_POST['user_pass'];
            $chekc_otp = $this->DatabaseModel->select_data('user_id', 'rt_users', array(
                'user_otp' => $user_otp
            ));
            if ($chekc_otp) {
                $this->DatabaseModel->access_database('rt_users', 'update', array(
                    'user_pass' => md5($user_pass),
                    'user_otp' => ''
                ), array(
                    'user_otp' => $user_otp
                ));
                $status  = 'true';
                $message = 'Cambio de contraseña exitosamente';
            } else {
                $status  = 'true';
                $message = 'Invalid Otp';
            }
        } else {
            $status  = 'false';
            $message = 'Algo falta';
        }
        $resp = array(
            'status' => $status,
            'message' => $message,
            'data' => array()
        );
        echo json_encode($resp);
    }
	
    function update_order() {
        if (isset($_POST['order_id'])) {
            $payment_update_by    = $_POST['user_id'];
            $payment_id           = $_POST['order_id'];
            $payment_order_status = $_POST['order_status'];
            $payment_note         = $_POST['order_note'];
            $dataArr              = array(
                'payment_order_status' => $_POST['order_status'],
                'payment_note' => trim($_POST['order_note']),
                'payment_update_by' => trim($_POST['user_id']),
                'payment_delivery_date' => date('Y-m-d H:i:s')
            );
            if ($_POST['order_status'] == 2) {
                $dataArr['payment_status'] = 1;
            }
            $this->DatabaseModel->access_database('rt_paymentdetails', 'update', $dataArr, array(
                'payment_id' => $payment_id
            ));
            $status  = 'true';
            $message = 'Orden de actualización con éxito';
        } else {
            $status  = 'false';
            $message = 'Algo falta';
        }
        $resp = array(
            'status' => $status,
            'message' => $message,
            'data' => array()
        );
        echo json_encode($resp);
    }
	
    function delete_user() {
		 if (isset($_POST['user_id'])) {
			$user_id     = $_POST['user_id'];
			$user_email  = $_POST['user_email'];
			$user_pass   = md5($_POST['user_pass']);
			$cond        = "user_id = '$user_id' AND user_email = '$user_email' AND user_pass = '$user_pass' AND user_accesslevel!=1";
			$user_detail = $this->DatabaseModel->select_data('*', 'rt_users', $cond, 1);
			if (empty($user_detail)) {
				$cond        = "user_id = '$user_id' AND user_mobile = '$user_email' AND user_pass = '$user_pass' AND user_accesslevel!=1";
				$user_detail = $this->DatabaseModel->select_data('*', 'rt_users', $cond, 1);
			}
			if ($user_detail) {
				if ($user_detail[0]['user_pic'] != '') {
					$path     = dirname(__FILE__);
					$abs_path = explode('application', $path);
					unlink($abs_path[0] . $user_detail[0]['user_pic']);
				}
				$this->DatabaseModel->access_database('rt_users', 'delete','', array(
					'user_id' => $user_id
				));
				$status  = 'true';
				$message = 'Eliminado con éxito';
			} else {
				$status  = 'false'; 
				$message = 'La credencial es incorrecta';
			}
			$resp = array(
				'status' => $status,
				'message' => $message,
				
			);
			echo json_encode($resp);
		 }
	}
	
	function signup() {
        if (isset($_POST['mobile']) && isset($_POST['email']) && isset($_POST['pass']) && isset($_POST['name']) ) {
            $user_email  = $_POST['email'];
            $user_mobile = $_POST['mobile'];
            $user_pass     = $_POST['pass'];
            $user_name     = $_POST['name'];
            $cond        = "user_email = '$user_email' || user_mobile = '$user_mobile' ";
            $userDetail  = $this->DatabaseModel->select_data('user_id', 'rt_users', $cond, 1);
            
                if (empty($userDetail)) {
					$random  =   rand() ;
                    $dataIns = array(
                        'user_mobile' 		=> $user_mobile,
                        'user_email'  		=> $user_email,
                        'user_pass' 		=> md5($user_pass),
                        'user_name' 		=> $user_name,
                        'user_registerdate' => date('Y-m-d H:i:s'),
						'user_otp'			=> $random,
						'user_accesslevel'  => 2
                    );
					 $body = '<a href="'.base_url('api/verify_account/' . $random ) .'"> click here to verify account </a>'  ;
					 $Msg_signup =site_url('api/verify_account/'. $random ).' click here to verify account';
                    if($this->DatabaseModel->access_database('rt_users', 'insert', $dataIns, '')){
						$this->ts_functions->sendnotificationemails('emailveri',$user_email ,'E-Mail Verification','',$body);
						//$this->ts_functions->sendUserEmailCI( $user_email,null,null,'Account Varify',$body);
						if(isset($user_mobile) && $user_mobile !='' ){
							$this->ts_functions->send_msg($user_mobile,$Msg_signup);
								  
					   }
					}
                    $status  = 'true';
                    $message = "Te has registrado correctamente, un enlace de verificación se ha enviado a tu correo o móvil ";
					
                } else {
                     $status  = 'false';
					 $message = "Ya existe un correo electrónico o celular con nosotros, pruebe con otros";
                }
            
            $resp = array(
                'status' => $status,
                'message' => $message,
                'data' => array()
            );
			
            echo json_encode($resp, JSON_UNESCAPED_SLASHES);
        }
    }
	
	function verify_account($otp){
		if(!empty($otp))
        $result  = $this->DatabaseModel->select_data('user_otp', 'rt_users', array(
            'user_otp' => $otp
        ));
		
		
        if ($result) {
            if($this->DatabaseModel->access_database('rt_users','update',array('user_otp'=>null,'user_status'=>1),array('user_otp'=>$otp))){
				$this->load->library('session');

				$this->session->set_flashdata('item', 'success');
				redirect(base_url());	
			 }
        } else {
				$this->load->library('session');

				$this->session->set_flashdata('item', 'failed');
				redirect(base_url());	
        }
	}
	
	function feedback() {
		
        if (isset($_POST['title']) && isset($_POST['description']) && isset($_POST['user_id']) ) {
            $title  		= $_POST['title'];
            $description 	= $_POST['description'];
            $user_id     	= $_POST['user_id'];
            $created_at     = date('Y-m-d h:i:s');
                $dataIns = array(
                        'title' 		=> $title,
                        'description'  		=> $description,
                        'user_id' 		=> $user_id,
                        'created_at' 		=> $created_at,
                    );
				
					 
                    if($this->DatabaseModel->access_database('rt_feedback', 'insert', $dataIns, '')){
						 $status  = 'true';
                         $message = "Gracias por sus comentarios";
					
					}else{
						$status  = 'false';
                         $message = "Something went wrong";
					}
                   
                
                   
            
            $resp = array(
                'status' => $status,
                'message' => $message,
                'data' => array()
            );
			
            echo json_encode($resp, JSON_UNESCAPED_SLASHES);
        }
    }
	
	public function currencyConverter_new()
	{	
		 if (isset($_POST['from_Currency']) && isset($_POST['to_Currency']) && isset($_POST['amount']) ) {
			$from_Currency = urlencode($_POST['from_Currency']);
			$to_Currency = urlencode($_POST['to_Currency']);
			/* $get = file_get_contents('https://free.currencyconverterapi.com/api/v5/convert?q='.$from_Currency.'_'.$to_Currency.'&compact=y');
			 $rate=json_decode($get,true);	*/
			$apikey = key_text('currency_api');
			$curl = curl_init();			
			$url = 	'https://free.currencyconverterapi.com/api/v5/convert?q='.$from_Currency.'_'.$to_Currency.'&compact=y&apiKey='.$apikey.'';		   
			
			
			curl_setopt($curl, CURLOPT_URL, $url);		   
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);		  
			curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);		  
			$result = curl_exec($curl);		  		
			if(!$result){die("Connection Failure");}	
			curl_close($curl);		
			$rate = json_decode($result , true);	
			 $converted_amount = $_POST['amount']*$rate[$from_Currency.'_'.$to_Currency]['val'];
			 $resp = array(
                'message' => $from_Currency .' ' . 'To' . ' ' . $to_Currency,
                'portal_curreny' => $this->ts_functions->getsettings('portal', 'curreny'),
                'data' => array('amount' => $converted_amount)
            );
			echo json_encode($resp, JSON_UNESCAPED_SLASHES);
			
		}
	}
	 
	public function get_offer(){ 
		  
		$discount_coupon = $this->DatabaseModel->select_data('*', 'rt_discount_coupons', array('offer_status'=>1,'offer_featured'=>1),1);
		//$data['infos'] = $this->Front_model->getDataResult('rt_info',array('status'=>1));
		$time_sheduale = $this->Front_model->getDataResult('rt_time_schedule',array('status'=>1));
		$open_time ='';
		$close_time ='';	
		if(!empty($time_sheduale)){
			foreach($time_sheduale as $times){
				 if($times['title'] == date('l')){
					 $time_shedual = json_decode($times['description'],true);
					 $open_time = $time_shedual['open_time'];
					 $close_time = $time_shedual['close_time'];
				}
			}
		}
		$TimeArr = array(
					  'open_time'=>$open_time,
					  'close_time'=>$close_time
					  );
		if(!empty($discount_coupon)){
		$title = $discount_coupon[0]['offer_heading'];
		$subtitle = $discount_coupon[0]['offer_title'];
		$desc = $discount_coupon[0]['offer_description'];
		//$data['img']  = $this->DatabaseModel->select_data('*','rt_images',array('id'=>1),'');
		$offerimg = base_url().$discount_coupon[0]['offer_image'];	
		}else{
			$title = '';
			$subtitle = '';
			$desc = '';
			//$data['img']  = $this->DatabaseModel->select_data('*','rt_images',array('id'=>1),'');
			$offerimg ='';
		}
		if($title !='' && $subtitle !='' && $desc !='' && $offerimg !=''){
		$status = 'true';
		$dataArr = array('title'=>$title,
					  'sub_title'=>$subtitle,
					  'description'=>$desc,
					  'offer_img'=>$offerimg
					 );
		}else{
		 $status = 'false';  
		 $dataArr =array();   
		}			 
		$resp = array('status' => $status,
                      'data' => array_merge($TimeArr,$dataArr)
                      );
		echo json_encode($resp, JSON_UNESCAPED_SLASHES);
	}
	
	
	function booking(){
	    if(isset($_POST['booking_date']) && isset($_POST['booking_time']) && isset($_POST['name']) && isset($_POST['mobile']) && isset($_POST['description']))
		 {
				$_POST['booking_date']  = date('Y-m-d' ,strtotime($_POST['booking_date']));
                $_POST['created_at'] = date('Y-m-d h:i:s') ;
					
				    if($this->Front_model->insertData('rt_booking',$_POST)){
						 
					 $status='true';	 
					 $message = "Su solicitud de reserva ha recibido";
						
				   }else{
					 $status='false';	 
					 $message = "Algo salió mal, intenta de nuevo";
				   }					   
			   }else{
			       $status='false';	 
				   $message = "Algo salió mal, intenta de nuevos";
			   }
			   
	    $resp = array(
                'status' => $status,
                'message' => $message,
               );
             
        echo json_encode($resp, JSON_UNESCAPED_SLASHES);
	}
	
	
	
	public function varify_coupon(){
		
	 
		if(isset($_POST['coupon_code']) && !empty($_POST['coupon_code']) && !empty($_POST['user_id']) && !empty($_POST['total_amount']))
		{   
			$coupen_code = $_POST['coupon_code'];
			$offer_info = $this->DatabaseModel->select_data('*','rt_discount_coupons',array('coupen_code'=>$coupen_code,'offer_status'=>1),'');
			 
			if($offer_info)
			{ 
				/* check user limit */
				if($offer_info[0]['user_used_limit'] ==2)
				{
					 $pay_info = $this->DatabaseModel->select_data('used_coupen_code','rt_paymentdetails',array('payment_uid'=>$_POST['user_id']),'');	
					 if($pay_info){
						foreach($pay_info as $pinfo){
							$used_couponArr = $pinfo['used_coupen_code'];
							if( $used_couponArr == $_POST['coupon_code']){
									$resp = array('status' => 'false' , 'message' =>'Este código de cupón ya se usó');
									echo  json_encode($resp);
									die;
							}
							 						
						}
					 }  				
				}
				
				/* Match coupon code */
				if($offer_info[0]['coupen_code'] != $coupen_code ){
					  $resp = array('status' => 'false' , 'message' =>'El código de cupón no coincide');
					  echo  json_encode($resp);
					die;
				}
				
				/* Check Expire date */
				if($offer_info[0]['coupen_expire_date'] < date('Y-m-d'))
				{
					 $resp = array('status' => 'false', 'message' =>'Este código de cupón ha caducado.');
					 echo  json_encode($resp);
					die;
					
				}
				
				/* check coupon limit */
				if($offer_info[0]['coupen_uses_limt'] <1)
				{
					  $resp = array('status' => 'false' , 'message' =>'El límite de cupón ha caducado.');
					  echo  json_encode($resp);
					die;
				}
				
				
					$total_amount = $_POST['total_amount'];			
					// 1 for percantage and 2 for flate amount
					if($offer_info[0]['discount_type'] == 1)
					{ 
						$disc_amount = $offer_info[0]['discount_amount'];
						$dicount_amount = ($disc_amount / 100) * $total_amount;
						$dicount_amount = round($dicount_amount);
						
						$resp = array('status' =>'true','message' =>'El cupón se ha aplicado con éxito', 'dicount_amount'=>$dicount_amount ,'type'=>'percantage');
					}else if($offer_info[0]['discount_type'] == 2)
					{
						$dicount_amount = $offer_info[0]['discount_amount'];
						
						$resp = array('status' =>'true' ,'message' =>'El cupón se ha aplicado con éxito', 'dicount_amount'=>$dicount_amount, 'type'=>'flat');
					} 
									 
			}else{
				$resp = array('status' =>'false' , 'message' =>'El código de cupón no existe');
				
			}
		}else{
			$resp = array('status' =>'false', 'message' =>'Código de cupón no válido');
		}		
		 
		echo  json_encode($resp);
	}
	
	public function check_preorder(){
		
		$time_sheduale = $this->Front_model->getDataResult('rt_time_schedule',array('status'=>1));
		$open_time ='';
		$close_time ='';	
		if(!empty($time_sheduale)){
			foreach($time_sheduale as $times){
				 if($times['title'] == date('l')){
					 $time_shedual = json_decode($times['description'],true);
					 $open_time = $time_shedual['open_time'];
					 $close_time = $time_shedual['close_time'];
				}
			}
		}
		$TimeArr = array(
					  'open_time'=>$open_time,
					  'close_time'=>$close_time
					  );
		if(strtotime($TimeArr['open_time']) < strtotime(date('g:i A')) && strtotime($TimeArr['close_time']) > strtotime(date('g:i A')))
		{
			$resp = array('status' => 1 , 'error' =>'Open');
		}else{
			$resp = array('status' => 0 , 'error' =>'Closed');
		}
		return $resp;
	}
	
	
	public function UserWalletRecharge_Request(){
		if(isset($_POST['user_id']) && !empty($_POST['user_id'])){
			$Req_Data = array('user_id'=>$_POST['user_id'],
							  'user_mobile'=>$_POST['user_mobile'],
							  'amount'=>$_POST['amount'],
							  'remarks'=>$_POST['remarks'],
							);
			  if($this->Front_model->insertData('rt_userwallet_recharge_request',$Req_Data)){
			 
					$status='true';	 
					$message = "Su solicitud se envió con éxito";
			
			  }else{
				  
				   $status='false';	 
				   $message = "Algo salió mal, intenta de nuevo";
			  }
			
		}else{
			$status='false';	 
			$message = "Algo salió mal, intenta de nuevo";
		}
		  $resp = array(
                'status' => $status,
                'message' => $message,
               );
             
        echo json_encode($resp, JSON_UNESCAPED_SLASHES);
	}
	
	
	public function Discount_CouponList(){
	
		$all_offers = $this->DatabaseModel->select_data('*','rt_discount_coupons',array('offer_status'=>1,'coupen_expire_date >= '=>date('Y-m-d')));	 
	 
		if(!empty($all_offers)){
			$status='true';	 
			$message = "Lista de cupones de descuento";
			$data = $all_offers;
		}else{
			
			$status='false';	 
			$message = "Cupón de descuento no disponible";
			$data =array();
		}
	 
		$resp = array(
			'status' => $status,
			'message' => $message,
			'data'=>$data,
		   );
		echo json_encode($resp, JSON_UNESCAPED_SLASHES);
	} 	
	
	
	public function userWalletHistory()
	{
		 if(isset($_POST['user_id']))
		 {
			 $uid = $_POST['user_id'];
			 
			 $wallet_credit = $this->DatabaseModel->select_data('*','rt_wallet_credit',array('wc_uid'=>$uid));
			 
			 if(!empty($wallet_credit))
			 {
				$status='true';	 
				$message = "Historial de billetera";
				$data = $wallet_credit;
			 }else{
				$status='false';	 
				$message = "Historial de billetera no encontrado";
				$data =array(); 
				 
			 }
			 
		 }else{
			$status='false';	 
			$message = "Algo salió mal ! Inténtalo de nuevo";
			$data =array();
			 
		 }
		 
		 $resp = array(
			'status' => $status,
			'message' => $message,
			'data'=>$data,
		   );
		echo json_encode($resp, JSON_UNESCAPED_SLASHES);
		
	}
	
	
	public function get_search_items()
	{
	    if(isset($_POST['searchterm']) && trim($_POST['searchterm'] !='') && isset($_POST['user_id']))
	    {   $user_id = $_POST['user_id'];
	       $keyword= trim($_POST['searchterm']);
	           
            $cond            = "item_name LIKE '%$keyword%' AND item_status = '1' ";
            $order_by        = array(
                'item_name',
                'asc'
            );
             
                
            $itemDetail = $this->DatabaseModel->select_data('*', 'rt_items', $cond,'', '', $order_by);
			
			if(!empty($itemDetail)){
				$i=0;
				foreach($itemDetail as $item){
					
					 $itemDetail[$i]['item_options'] =array();  
					 if($item['item_price'] =='' && $item['item_options'] == 'YES'){ 
						
						$item_options = $this->DatabaseModel->select_data('item_size,item_price','rt_item_options',array('main_item_id'=>$item['item_id']),''); 
						
						if(!empty($item_options)){
							$itemDetail[$i]['item_options'] = $item_options; 
						}
						
					}
					$i++;			   
				}
			}
            $result     = $itemDetail;
            if (count($result)) {
                echo json_encode(array(
                    'status' => 'true',
                    'message' => 'Lista de articulos',
                    'symbol' => $this->ts_functions->getsettings('portalcurreny', 'symbol'),
                    'curreny' => $this->ts_functions->getsettings('portal', 'curreny'),
                    'data' => $result,
                    'cart_count' => $this->cart_count($user_id)
                    
                ));
            } else {
                echo json_encode(array(
                    'status' => 'false',
                    'message' => 'Producto no encontrado',
                    'data' => array()
                ));
            }
        }
	}
	
	public function get_pk_public_culqi(){ 
	    
	    $data=$this->input->post();
	    $this->load->model('Front_model');
		$setting = $this->Front_model->getData('rt_settings',array('key_text'=>'pk_public_culqi'));
		$pk_public	 =  $setting['value_text'];
		$TimeArr = array(
					  'pk_public'=>$pk_public
					  );
		$status = "true";
		$resp = array('status' => $status,
                      'data' => array_merge($TimeArr)
                      );
		echo json_encode($resp, JSON_UNESCAPED_SLASHES);
	}
	
	
	
	
	
}
