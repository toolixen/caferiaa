<?php // echo substr(strip_tags(),0,1000) . "..."; ?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if( !isset($this->session->userdata['rt_uid']) ) {
		    redirect(base_url());
		}
		if( isset($this->session->userdata['ts_uid']) ) {
    		if($this->session->userdata['rt_level'] != 1) {
			    redirect(base_url());
			}
		}
		if(isset($_POST) && !empty($_POST)) {
	        if(!isset($_SERVER['HTTP_REFERER'])) {
                die('Direct Access Not Allowed!!');
	        }
	    }
		$this->load->library('ts_functions');
	    
	}

	public function index(){
	    redirect(base_url());
	}
	public function site_setting()
	{
		$site_title=$this->input->post('site_title');
		$site_keyword=$this->input->post('site_keyword');
		$author_name=$this->input->post('author_name');
		$site_description=$this->input->post('site_description');
		$arr=array('site_title'=>$site_title,
				   'site_keyword'=>$site_keyword,
				   'author_name'=>$author_name,
				   'site_description'=>$site_description,
		);
		$res=$this->DatabaseModel->access_database('rt_genral_setting','update',$arr,array('id'=>'1'));
		if($res)
		{
			echo '1';
		}
		else
		{
			echo '0';
		}
		
	}
	function ads_integration()
 	{
		$add_s=$this->input->post('add_s');
		$add_mob=$this->input->post('add_mob');
		$arr=array('add_s'=>$add_s,'add_mob'=>$add_mob);
		$res=$this->DatabaseModel->access_database('rt_genral_setting','update',$arr,array('id'=>'1'));
		if($res)
		{
			echo "1";
		}
		else
		{
			echo "0";
		}
			
	}
	function google_analytics()
 	{
		$google_a=$this->input->post('google_a');
		$arr=array('google_a'=>$google_a);
		$res=$this->DatabaseModel->access_database('rt_genral_setting','update',$arr,array('id'=>'1'));
		if($res)
		{
			echo "1";
		}
		else
		{
			echo "0";
		}
			
	}





	/**** Function to update values of tables ****/

	function updatethevalue() {
		
	    if(isset($_POST['id'])) {
			$dArr1 = explode('_',$_POST['id']);
			$info=$this->DatabaseModel->select_data('*','rt_paymentdetails',array('payment_id'=>$dArr1[0]));
	        if( $_POST['type'] == 'item' ) {
	            $dArr = explode('_',$_POST['id']);
	            $k = 'item_'.$dArr[1];
	            $this->DatabaseModel->access_database('rt_items','update',array($k=>$_POST['vlu']),array('item_id'=>$dArr[0]));
				
	        }
	        
			if( $_POST['type'] == 'order' ) {
	            $dArr = explode('_',$_POST['id']);
	            $k = 'payment_'.$dArr[1].'_'.$dArr[2];
				$date=explode(' ',$info[0]['payment_date']);
				//push notification
				
				$data=$this->DatabaseModel->access_database('rt_paymentdetails','select','',array('payment_id'=>$dArr[0]));
				$user_data=$this->DatabaseModel->access_database('rt_users','select','',array('user_id'=>$data[0]['payment_uid']));
				
				$obj = json_decode($data[0]['payment_pid'],true); 
				$item_info=$this->DatabaseModel->access_database('rt_items','select','',array('item_id'=>$obj[0]['item']));
			     if($_POST['vlu']==0)
				 {
				$msg=str_replace("[order_number]",$info[0]['payment_uniqid'],str_replace("[username]",$user_data[0]['user_name'],str_replace("[break]","<br />",$this->ts_functions->getsettings('bookorder','text'))));
				 $msg=str_replace("[order_date]",$date[0],$msg);
				
				 $this->ts_functions->email2('bookorder',$user_data[0]['user_email'],$msg);
				 }
				 else if($_POST['vlu']==1)
				 {
					 $msg=str_replace("[order_number]",$info[0]['payment_uniqid'],str_replace("[username]",$user_data[0]['user_name'],str_replace("[break]","<br />",$this->ts_functions->getsettings('intransits','text'))));
					$msg=str_replace("[order_date]",$date[0],$msg);
					$this->ts_functions->email2('intransits',$user_data[0]['user_email'],$msg);
				 }
				 else if($_POST['vlu']==2)
				 {
					$dArr = explode('_',$_POST['id']);
					
					$this->DatabaseModel->access_database('rt_paymentdetails','update',array('payment_delivery_date'=>date("d M,y")),array('payment_id'=>$dArr[0]));
					$msg=str_replace("[order_number]",$info[0]['payment_uniqid'],str_replace("[username]",$user_data[0]['user_name'],str_replace("[break]","<br />",$this->ts_functions->getsettings('delivered','text'))));	
					$msg=str_replace("[order_date]",$date[0],$msg);
					$this->ts_functions->email2('delivered',$user_data[0]['user_email'],$msg);
					//$this->ts_functions->sendnotificationemails('prating', $user_data[0]['user_email'],'Order delivered', $user_data[0]['user_name'],$user_data[0]['user_id']);
				 
				 }
				else if($_POST['vlu']==3)
				 {
						$msg=str_replace("[order_number]",$info[0]['payment_uniqid'],str_replace("[username]",$user_data[0]['user_name'],str_replace("[break]","<br />",$this->ts_functions->getsettings('canceled','text'))));
						$msg=str_replace("[order_date]",$date[0],$msg);
						$this->ts_functions->email2('canceled',$user_data[0]['user_email'],$msg);
				 }
				 $insert_arr=array(
				 'message'=>$msg,
				 'userid'=>$data[0]['payment_uid'],
				 'product_id'=>$data[0]['payment_pid'],
				 'item_pic'=>$item_info[0]['item_pic'],
				 'date'=>date('d/m/y'),
				 );

				 $this->push($this->ts_functions->getsettings('msg91','sender'),$user_data[0]['user_token'], strip_tags($msg));
				//push Notification end
				$this->DatabaseModel->access_database('rt_notification','insert',$insert_arr);
				$this->DatabaseModel->access_database('rt_paymentdetails','update',array($k=>$_POST['vlu']),array('payment_id'=>$dArr[0]));
	        }
	        elseif( $_POST['type'] == 'cat' ) {
	            $dArr = explode('_',$_POST['id']);
	            $k = 'cat_'.$dArr[1];
	            $this->DatabaseModel->access_database('rt_categories','update',array($k=>$_POST['vlu']),array('cat_id'=>$dArr[0]));

	        }
	        elseif( $_POST['type'] == 'user' ) {
	            $dArr = explode('_',$_POST['id']);
	            $k = 'user_'.$dArr[1];
	            $data_ins=array($k=>$_POST['vlu']);
				$this->DatabaseModel->access_database('rt_users','update',$data_ins,array('user_id'=>$dArr[0]));  
	        }
			elseif( $_POST['type'] == 'deliveryboy' ) {
	            $dArr = explode('_',$_POST['id']);
	            $k = 'payment_'.$dArr[1];
	            $data_ins=array($k=>$_POST['vlu']);
	           	$data1=$this->DatabaseModel->access_database('rt_users','select','',array('user_id'=>$_POST['vlu']));
	           if($_POST['vlu']!=0)
	           {
	               $msg="New Order ID :".$info[0]['payment_uniqid'];
				   $this->push($this->ts_functions->getsettings('msg91','sender'),$data1[0]['user_token'],$msg);
	         
	           }
	  
				$this->DatabaseModel->access_database('rt_paymentdetails','update',$data_ins,array('payment_id'=>$dArr[0]));  
	        }
	        echo '1';
	    }
	    else {
	        echo '0';
	    }
	    die();
	}
    function send_notification ($tokens, $message)
	{
			$url = 'https://fcm.googleapis.com/fcm/send';
			$fields = array(
				 'registration_ids' => $tokens,
				 'notification' => $message,
				);

			$headers = array(
				'Authorization:key ='.$this->ts_functions->getsettings('fire','base') .'',
				'Content-Type: application/json'
				);

		   $ch = curl_init();
		   curl_setopt($ch, CURLOPT_URL, $url);
		   curl_setopt($ch, CURLOPT_POST, true);
		   curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		   curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
		   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		   curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		   $result = curl_exec($ch);           
		   if ($result === FALSE) {
			   die('Curl failed: ' . curl_error($ch));
		   }
		   curl_close($ch);
		   return $result;
	}
	function push($header,$token,$message)
	{
		$tokens=array();
        $tokens[0]=$token;
		$message = array("body" => $message,
					"title"=>$header,
		);
		$message_status = $this->send_notification($tokens, $message);
	}
	
	public function blog_setting()
	{
			
		
		$res=$this->DatabaseModel->access_database('rt_blog_setting','update',array('status'=>$_POST['open_blog']) ,array('id'=>'1'));
		$res1=$this->DatabaseModel->access_database('rt_blog_setting','update',array('status'=>$_POST['comment_section']),array('id'=>'2'));
		if($res && $res1)
		{
			echo '1';
		}
		else
		{
			echo '0';
		}
		
	}
	function credentials()
	{
		$email=$this->input->post('email');
		$ps=$this->input->post('ps');
		$data=array('user_email'=>$email,'user_pass'=>md5($ps));
		$res=$this->DatabaseModel->access_database('rt_users','update',$data,array('user_id'=>'1'));
		if($res)
		{
			echo "1";
		}
		else 
		{
			echo "0";
		}
		
	}
	
	function saveCookies()
	{
		$res=$this->DatabaseModel->access_database('rt_genral_setting','update',$_POST,array('id'=>'1'));
		if($res)
		{
			echo "1";
		}
		else 
		{
			echo "0";
		}
		
	}


	
	

	

	
	

	

}
?>
