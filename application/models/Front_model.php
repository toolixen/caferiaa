<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('session'));
	}
	public function insertData($table,$data){
		 $this->db->insert($table,$data);
		return $this->db->insert_id();
	}
	public function getData($table,$where){
		return $this->db->get_where($table,$where)->row_array();
	}
	public function getDataResult($table,$where){
		return $this->db->get_where($table,$where)->result_array();
	}
	public function updateData($table, $data, $where){
		$this->db->update($table,$data,$where);
		return $this->db->affected_rows();
	}
	
	public function getItems($limit=NULL,$cat_id=NULL){
		$this->db->select('*');
		$this->db->from('rt_items');
		$this->db->join('rt_categories', 'rt_categories.cat_id = rt_items.item_cat','inner');
		
		if(!empty($cat_id)){
			
			$this->db->where('rt_items.item_cat',$cat_id);
		}
		$this->db->where('rt_categories.cat_status',1);
		$this->db->where('rt_items.item_status',1);
		
		$itm = $this->session->tempdata('item_id');
		if(!empty($itm)){
			$this->db->where_not_in('item_id', $itm);
		}
		
		$this->db->order_by('rt_items.item_date', 'DESC');
		if(!empty($limit)){
		
			$this->db->limit(3, $limit);
		}else{
			$this->db->limit(3, 0);
		}
		
		return $this->db->get()->result_array();
		return $this->db->last_query();
		
	}
	
	public function getblog($limit=null){
		$this->db->select('rt_blogs.slug,rt_blogs.blog_img,rt_blogs.created_at,rt_blogs.id,rt_blogs.content,rt_blogs.blog_img,rt_blogs.author_img,rt_blogs.title,rt_blogs.facebook_url,rt_blogs.twitter_url,rt_blogs.linkedin_url,rt_blogs.like_count');
		$this->db->from('rt_blogs');
		$this->db->join(' rt_blog_category', ' rt_blog_category.id = rt_blogs.category','inner');
		$this->db->limit(10);
		$this->db->where(' rt_blog_category.status',1);
		$this->db->where('rt_blogs.status',1);
		$this->db->order_by('rt_blogs.created_at', 'DESC');
		
		return $this->db->get()->result_array();
	}
	 public function getblogcat(){
		$this->db->select('rt_blog_category.title,rt_blog_category.id,COUNT(rt_blogs.category) as total');
		$this->db->from('rt_blog_category');
		$this->db->join(' rt_blogs', ' rt_blog_category.id = rt_blogs.category','inner');
		$this->db->where(' rt_blog_category.status',1);
		$this->db->where('rt_blogs.status',1);
		$this->db->group_by('rt_blogs.category'); 
	    $this->db->order_by('total', 'desc'); 
		
		return $this->db->get()->result_array();
	}
	public function getSearchBlog($search){
		$this->db->like('title', $search);
		$this->db->limit(8); 
		return $this->db->get('rt_blogs')->result_array();
	}
	public function addLikeCount($bid,$status,$uid){
			$this->db->delete('rt_likes',array('blog'=>$bid,'user'=>$uid));
			$this->db->insert('rt_likes',array('blog'=>$bid,'user'=>$uid,'status'=>$status));
			 
			$this->db->where('id',$bid);
			if($status == 1){
			$this->db->set('like_count', '`like_count`+ 1', FALSE);
			}else{
			$this->db->set('like_count', '`like_count`- 1', FALSE);	
			}
			$this->db->update('rt_blogs');
			
			$row = $this->getData('rt_blogs',array('id'=>$bid));
			return $row['like_count'];
		
	}
	public function deductInventory($payment_pid){
		foreach($payment_pid as $pay){
			$this->db->where('item_id',$pay->item);
			$this->db->set('item_quantity', '`item_quantity`- '. $pay->quantity .'', FALSE);
			$this->db->update('rt_items');
		}

	}
	public function GenerateInvoice($order_id,$user_id=null){
		$rowdata = [];
		$this->db->select('rt_paymentdetails.*,rt_users.*');
		$this->db->from('rt_paymentdetails');
		$this->db->join(' rt_users', 'rt_paymentdetails.payment_uid = rt_users.user_id','inner');
		// $this->db->where(' rt_paymentdetails.payment_uid',$user_id);
		$this->db->where('rt_paymentdetails.payment_uniqid',$order_id);
		$rowdata = $this->db->get()->row_array();
		$payment_pid = json_decode($rowdata['payment_pid']);
		
		$pro_info = [];
		foreach($payment_pid as $pay){
			$data = $this->getData('rt_items',array('item_id'=>$pay->item));
			$pro_info[	] = array('item_sku'=>$data['item_sku'],'item_name'=>$data['item_name'],'item_price'=>$data['item_price'],'qty'=>$pay->quantity,'subtotal'=>$pay->amount);
		}
		$rowdata['invoice'] = $pro_info;
		return $rowdata;
	}
	
	
	
	
	
	 
    
}
