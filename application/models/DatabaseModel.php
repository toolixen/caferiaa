<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class DatabaseModel extends CI_Model {

        public function __construct()
        {
                $this->load->database();
        }

		#common function of DML commands
		function access_database($tablename, $mode, $data_array, $where_array='', $join_array=""){
		if($mode == 'select')
		{
			$this->db->select('*');
			$this->db->from($tablename);
			if($where_array!='')
				$this->db->where($where_array);

			if($join_array != '' && $join_array[0] == 'limit'){
				$this->db->limit($join_array[1], $join_array[2]);
			}

			$rs=$this->db->get();
			return $rs->result_array();
		}
		elseif($mode=='wherein'){
		    $this->db->select("*");
		    $this->db->from($tablename);
            $this->db->where_in($join_array , $data_array);
            if($where_array!='')
				$this->db->where($where_array);

            $rs=$this->db->get();
			return $rs->result_array();
		}
		elseif($mode=='insert'){
			$this->db->insert($tablename,$data_array);
			return $this->db->insert_id();
		}
		elseif($mode=='update'){
			$this->db->where($where_array);
			return $this->db->update($tablename,$data_array);
		}
		elseif($mode=='delete'){
			return $this->db->delete($tablename,$where_array);
		}
		elseif($mode == 'like')
		{
			$this->db->select('*');
			$this->db->from($tablename);
			$this->db->like($where_array);
			if($join_array != ''){
			    $this->db->or_like($join_array);
			}
			if($data_array != '') {
			    $this->db->having($data_array);
			}
			$rs=$this->db->get();
			return $rs->result_array();
		}
		elseif($mode=='orderby'){
		    $this->db->select('*');
			$this->db->from($tablename);
			if($where_array!='')
				$this->db->where($where_array);

			$this->db->order_by($data_array[0], $data_array[1]);
			$rs=$this->db->get();
			return $rs->result_array();

		}
		elseif($mode=='totalvalue'){
		    $this->db->select("SUM(".$data_array[0].") AS ".$data_array[1]."");
            $this->db->from($tablename);
            if($where_array!='')
				$this->db->where($where_array);

            $rs=$this->db->get();
			return $rs->result_array();

		}
		elseif($mode=='groupby'){
		    $this->db->select('*');
			$this->db->from($tablename);
			if($where_array!='')
				$this->db->where($where_array);

			$this->db->group_by($data_array);
			$rs=$this->db->get();
			return $rs->result_array();

		}
		elseif($mode=='join_order_limit'){
		    $this->db->select('*');
			$this->db->from($tablename);
			$this->db->join($join_array[0], $join_array[1]);
			if($where_array!=''){
				$this->db->where($where_array);
			}
			$this->db->order_by($data_array[0], $data_array[1]);
			$this->db->limit($join_array[2], $join_array[3]);
			$rs=$this->db->get();
			return $rs->result_array();

		}
		elseif($mode == 'select_like')
		{
			$this->db->select('*');
			$this->db->from($tablename);
			if($where_array!='')
				$this->db->where($where_array);

            if($data_array!='')
                $this->db->like($data_array);

            if($join_array!='')
                $this->db->where_in($join_array[0] , json_decode($join_array[1]));

			$rs=$this->db->get();
			return $rs->result_array();
		}
		elseif($join_array != ''){
			$this->db->select('*');
			$this->db->from($tablename);
			$this->db->join($join_array[0], $join_array[1]);

			if($where_array!='')
				$this->db->where($where_array);

			$rs=$this->db->get();
			return $rs->result_array();
		}
	}


	function select_data($field , $table , $where = '' , $limit = '' , $join_array = '',$order_array=''){
		$this->db->select($field);
		$this->db->from($table);
		if($where != ""){
			$this->db->where($where);
		}

		if($join_array != ''){
			if(in_array('multiple',$join_array)){
				foreach($join_array['1'] as $joinArray){
					$this->db->join($joinArray[0], $joinArray[1]);
				}
			}else{
				$this->db->join($join_array[0], $join_array[1]);
			}
		}


		if($limit != ""){
			if(is_array($limit)){
				$this->db->limit($limit['0'] , $limit['1']);
			}else{
				$this->db->limit($limit);
			}
		}
		if($order_array != ''){
		   $this->db->order_by($order_array[0], $order_array[1]);
		}
		$result=$this->db->get()->result_array();
		return $result;
	
	}
	function aggregate_data($table , $field_nm , $function , $where = NULL,$like=""){
		$this->db->select("$function($field_nm) AS MyFun");
        $this->db->from($table);
		if($where != ''){ $this->db->where($where); }
		if($like != ""){ $this->db->like($like); }
        $query1 = $this->db->get();
        if($query1->num_rows() > 0){ 
			$res = $query1->row_array();
			return $res['MyFun'];													
        }else{
			return array();
		}  
		die();  
	}
	public function contactexport(){
	
			$this->load->dbutil();
			$this->load->helper('file');
			$this->load->helper('download');

			$delimiter = ",";
			$newline = "\r\n";
			$this->db->select('*')
			->from('rt_contact_us');
			$query = $this->db->get();
			$data = $this->dbutil->csv_from_result($query, $delimiter, $newline);
			force_download('contact.csv',$data);
			return TRUE;
			
	}
	
	
	public function get_data($table, $where = array(), $limit = 0, $orderby = array(), $whereIn = array(), $whereInCol = '', $likeCol = '', $keyword = '', $orlikeCol = '', $orkeyword = '') {

		$this->db->select('*');
		$this->db->from($table);
		if($where){
			$this->db->where($where);
		}
		
		if($whereIn && $whereInCol){
			$this->db->where_in($whereInCol, $whereIn);
		}

		if($likeCol && $keyword){
			$this->db->like($likeCol, $keyword);
		}
		
		if($orlikeCol && $orkeyword){
			$this->db->or_like($orlikeCol, $orkeyword);
		}
		
		if($orderby){
			foreach ($orderby as $key => $value) {
				$this->db->order_by($key, $value);
			}
		}
		
		if($limit){
			$this->db->limit($limit);
		}

		$query = $this->db->get();

		if ($query->num_rows()) {
			return $query->result_array();
		} else {
			return false;
		}

	}
}
?>
