<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function get_description($var ,array $infos)
    {
        foreach($infos as $info){
			if($info['field'] == $var){
				return $info['description'];
			}
		}
    } 
function get_title($var ,array $infos)
    {
        foreach($infos as $info){
			if($info['field'] == $var){
				return $info['title'];
			}
		}
    } 
function key_text($key_text)
    {
		$CI =& get_instance();
		$data = $CI->db->get_where('rt_settings',array('key_text'=>$key_text))->row_array();
        return $data['value_text'];
  } 
function get_like($blog_id)
    {
		$CI =& get_instance();
		$id = $CI->session->userdata('id');
		$data = $CI->db->get_where('rt_likes',array('blog'=>$blog_id,'user'=>$id))->row_array();
		if($data){
		   return $data['status'];	
		}else{
		   return '';
		}
       
  }
function set_blog($key_text)
    {
		$CI =& get_instance();
		$data = $CI->db->get_where('rt_blog_setting',array('key_text'=>$key_text))->row_array();
		if($data){
		   return $data['status'];	
		}else{
		   return '';
		}
       
  }  
 function rating($arr,$product_id)
 {
	 $star=0;
	 $count=0;
	 foreach($arr as $rating)
	 {
		 
		 if($rating['product_id']==$product_id)
		 {
			 $star=$star+$rating['star'];
			 $count++;
		 }
	 }
	 if($count==0)
		return 0;
	 else
	    return $star/$count;
 } 
  
function get_itemdata_by_id($item_id)
 {
	$ci = &get_instance();
	$where = array('item_id' => $item_id);
	$itemData = $ci->DatabaseModel->get_data('rt_items',$where,1);
	
	return $itemData;
 }	
