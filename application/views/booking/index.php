<div class="row">
   <div class="col-md-12">
      <div class="hs_heading medium">
         <h3>Customer Booking (<?php if(!empty($bookings)) {echo count($bookings);}else{ echo 0 ; } ?>) </h3>
      </div>
      <div class="hs_datatable_wrapper table-responsive">
         <table class="hs_datatable table table-bordered">
            <thead>
               <tr>
                  <th>#</th>
                  <th>Nombre</th>
                  <!--<th>Email</th>-->
				  <th>Celular</th>
                  <th>Booking Hora</th>
                  <th>Mensaje</th>
                  <th>Estado</th>
               </tr>
            </thead>
            <tbody>
               <?php if(!empty($bookings)) {
                  $count = 1;
                  foreach($bookings as $booking) {
					 ?>
						<tr>		
							<td><?= $count++ ?></td>
							<td><?= $booking['name'] ?></td>
							<!--<td><?= $booking['email'] ?></td>-->
							<td><?= $booking['mobile'] ?></td>
							<td>
							<?php   			 $time=strtotime($booking['booking_date']);
												 echo $date=date("d",$time).' ';
												 echo $month=substr(date("F",$time),0,3).' ';
												 echo $year=date("y",$time).','; 
												 echo $booking['booking_time'] ;
													?>
							
							</td>
							<td><?php echo substr($booking['description'], 0, 25);   if(str_word_count($booking['description'])>3) { ?>
							<br><a onclick="read_more('<?php echo $booking['description'] ?>','<?php echo  $booking['name'] ?>');">Read More</a> <?php } ?></td>
							
							<td>
								<select class="form-control" onchange="updatecallstatus(this,<?= $booking['id'] ?>);"  id="<?= $booking['id'] ?>_status">
									<option value="3"  <?= ($booking['status'] == '3' )? 'selected' : '' ; ?>>Pending</option>
									<option value="1"  <?= ($booking['status'] == '1' )? 'selected' : '' ; ?>>Accept</option>
									<option value="0"  <?= ($booking['status'] == '0' )? 'selected' : '' ; ?>>Cancel</option>
								</select>
							</td>
						</tr>		
						
					 <?php
					}} ?>
            </tbody>
         </table>
      </div>
   </div>
</div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
	  <h4 class="modal-title" id="rname"></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <p id="read_more_text">Some text in the modal.</p>
      </div>
    </div>

  </div>
</div>
<script>
   function updatecallstatus(e,id){
   	 $.ajax({
   		  type		: "POST",
   		  url		: "<?php echo site_url('admin/booking'); ?>",
   		  data		: {	'status' :	 e.value,
   						'id'	 :   id			
   					},
   		  success: function(response){
   			  if(response==1){
   				  toastr.success("booking status updated successfully");
   			  }
   		  }
   	});
   	
   }
</script>
<!-- page body end -->