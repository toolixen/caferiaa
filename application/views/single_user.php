<?php
    if(empty($userdetails)) {
        redirect(base_url());
    }
?>
<div class="row">
    <div class="col-md-12">
        <div class="hs_heading medium">
            <h3><?php echo $userdetails[0]['user_name']?></h3>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <!-- tab start -->
        <div class="hs_tabs">
            <ul class="nav nav-pills">
                <li class="active"><a data-toggle="pill" href="#user_setting_tab">Informacion </a></li>
				<?php if($userdetails[0]['user_accesslevel']==2){ ?>
				<li><a data-toggle="pill" href="#wallet_credit_tab">Wallet Credit  </a></li>
				<?php } ?>
                <li class="hide"><a data-toggle="pill" href="#creditial_setting_tab">Credenciales  </a></li>
            </ul>
                
            <div class="tab-content">
				<!-- User setting tab start -->
                <div id="user_setting_tab" class="tab-pane fade in active">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="hs_input">
                                <label>Nombre</label>
                                <input class="form-control update_user" type="text" placeholder="Full Name"  value="<?php echo $userdetails[0]['user_name']?>"  id="user_name">
                            </div>
                        </div>
						<div class="col-md-8">
                            <div class="hs_input">
                                <label>Email</label>
                               <input class="form-control update_user" type="text" placeholder="Full Name"  value="<?php echo $userdetails[0]['user_email'];?>"  id="user_email" readonly>
                            </div>
                        </div>
						<div class="col-md-8">
                            <div class="hs_input">
                                <label>Celular</label>
                               <input class="form-control update_user" type="text" placeholder="Full Name"  value="<?php echo $userdetails[0]['user_mobile'];?>"  id="user_mobile" >
                            </div>
                        </div> 
						<div class="col-md-8">
                            <div class="hs_input">
                                <label>Dirección</label><textarea rows="4" class="form-control update_user" placeholder="Address" id="user_address"><?php echo $userdetails[0]['user_address'];?></textarea>
                            </div>
                        </div>
						<div class="col-md-8">
                            <div class="hs_input">
                                <label>Estado</label>
                                <select class="form-control update_user" id="user_status">
									<option value="1" <?php echo ($userdetails[0]['user_status'] == '1' ? 'selected' : '' ); ?>>Active</option>
									<option value="3" <?php echo ($userdetails[0]['user_status'] == '3' ? 'selected' : '' ); ?>>Blocked</option>
								</select>
                            </div>
                        </div>
						<div class="col-md-12">
							<input class="update_user" type="hidden"  value="<?php echo $userdetails[0]['user_id'];?>" id="user_id">
							<a class="btn" onclick="updateuser()" >Actualizar</a>
                        </div>
                    </div>
                </div>
                <!-- User setting tab end -->
				
				<?php if($userdetails[0]['user_accesslevel']==2){ ?>
			   <!-- Wallet Credit start -->
                <div id="wallet_credit_tab" class="tab-pane fade">
                    <div class="row"> 
					<form method="post">
						<div class="col-md-12">
                           <div class="hs_datatable_wrapper table-responsive">
							<table class="hs_datatable table table-bordered">
							   <thead>
									<tr>
										<th>#</th>
										<th>Cantidad</th>
										<th>Notas</th>
										<th>Fecha</th>
									</tr>
								<thead>
                                <tbody>
				                 <?php if(!empty($wallet_credit)) {
									   $count = 0;
									   foreach($wallet_credit as $soloCredit) {
									   $count++;
									?>
									<tr>
										<td><?php echo $count;?></td>
										<td><?php echo $soloCredit['wc_amount'];?></td>
										<td><?php echo $soloCredit['wc_note'];?></td>
										<td><?php echo date_format(date_create ( $soloCredit['wc_date'] ) , 'M d, Y');?>
									</tr>
													
									<?php } } else { ?>
										<tr>
											<td colspan="4" align="center"> No wallet credit.</td>
										</tr>
									<?php } ?>
                                 </tbody>
                               </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Wallet Credit  end -->
				<?php } ?>
			 
              	<!-- Credintials setting tab start -->
                <div id="creditial_setting_tab" class="tab-pane fade">
                    <div class="row">
					<form method="post">
						<div class="col-md-8">
                            <div class="hs_input">
                                <label>Email</label>
                                 <input type="email" class="form-control" name="user_email"  value="" >
								
                            </div>
                        </div>
						<div class="col-md-8">
							  <div class="hs_input">
								<label>Password</label>
								<input type="text" class="form-control" name="user_pass"  pattern=".{8,}"   required title="8 characters minimum"  >
								
							</div>
					   </div>
                       <div class="col-md-12">
                           <button type="button" class="btn"  disabled="true">Actualizar</button>
						  <p>Esta característica no se admite en la demostración. Gracias por su comprensión.</p>
                       </div>
					  </form> 
                    </div>
                </div>
                <!-- Credintials setting tab end -->		  
            </div>
        </div>
        <!-- tab end -->
    </div>
</div>   



   