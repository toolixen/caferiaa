<div class="row">
   <div class="col-md-12">
      <div class="hs_heading medium">
         <h3>Customer Queries (<?php if(!empty($contacts)) { echo count($contacts); }else{ echo 0 ; } ?>)</h3>
		 <p><a href="<?php echo base_url('admin/exportcontact') ;?>" >Export</a></p>
      </div>
      <div class="hs_datatable_wrapper table-responsive">
         <table class="hs_datatable table table-bordered">
            <thead>
               <tr>
                  <th>#</th>
                  <th>Nombre</th>
                  <th>Email</th>
                  <th>Contacto</th>
                  <th>Asunto</th>
                  <th>Mensaje</th>
                  <th>Estado</th>
               </tr>
            </thead>
            <tbody>
               <?php if(!empty($contacts)) {
                  $count = 1;
                  foreach($contacts as $contact) {
					 ?>
						<tr>		
							<td><?php echo  $count++ ?></td>
							<td><?php echo  $contact['name'] ?></td>
							<td><?php echo  $contact['email'] ?></td>
							<td><?php echo  $contact['contact'] ?></td>
							<td><?php echo  $contact['subject'] ?> </td>
							<td><?php echo substr($contact['description'], 0, 25);   if(str_word_count($contact['description'])>3) { ?>
							<br><a onclick="read_more('<?php echo $contact['description'] ?>','<?php echo  $contact['name'] ?>');">Read More</a> <?php } ?></td>
							<td>
								<select class="form-control" onchange="updatecallstatus(this,<?= $contact['id'] ?>);"  id="<?= $contact['id'] ?>_status">
									<option value="1"  <?= ($contact['status'] == '1' )? 'selected' : '' ; ?>>Done</option>
									<option value="0"  <?= ($contact['status'] == '0' )? 'selected' : '' ; ?>>Not Done</option>
								</select>
							</td>
						</tr>		
						
					 <?php
					}} ?>
            </tbody>
         </table>
      </div>
   </div>
</div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
	  <h4 class="modal-title" id="rname"></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <p id="read_more_text">Some text in the modal.</p>
      </div>
    </div>

  </div>
</div>
<script>
   function updatecallstatus(e,id){
   	 $.ajax({
   		  type		: "POST",
   		  url		: "<?php echo site_url('admin/contact_us'); ?>",
   		  data		: {	'status' :	 e.value,
   						'id'	 :   id			
   					},
   		  success: function(response){
   			  if(response==1){
   				  toastr.success("contact status updated successfully");
   			  }
   		  }
   	});
   	
   }
</script>
<!-- page body end -->