<div class="row">
   <div class="col-md-12">
      <div class="hs_heading medium">
         <h3>Customer Feedback (<?php if(!empty($customer_feedback)) { echo count($customer_feedback); }else{ echo 0 ; } ?>)</h3>
		  
      </div>
      <div class="hs_datatable_wrapper table-responsive">
         <table class="hs_datatable table table-bordered">
            <thead>
               <tr>
                  <th>#</th>
                  <th>Nombre</th>
                  <th>Titulo</th>
                  <th>Mensaje</th>
                </tr>
            </thead>
            <tbody>
               <?php if(!empty($customer_feedback)) {
                  $count = 1;
                  foreach($customer_feedback as $feedback) {
					 ?>
						<tr>		
							<td><?php echo  $count++ ?></td>
							<td><?php echo  $feedback['user_name'] ?></td>
							<td><?php echo  $feedback['title'] ?></td>
						    <td><?php echo substr($feedback['description'], 0, 25);   if(str_word_count($feedback['description'])>3) { ?>
							<br><a onclick="read_more('<?php echo $feedback['description'] ?>','<?php echo  $feedback['user_name'] ?>');">Leer mas...</a> <?php } ?></td>
							 
						</tr>		
						
					 <?php
					}} ?>
            </tbody>
         </table>
      </div>
   </div>
</div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
	  <h4 class="modal-title" id="rname"></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <p id="read_more_text">Some text in the modal.</p>
      </div>
    </div>

  </div>
</div>
 
<!-- page body end -->