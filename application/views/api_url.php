
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Cup and Puff</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 1500px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height: auto;} 
    }
  </style>
</head>
<body>

<div class="container-fluid">
  <div class="row content">
   <div class="text-center"> <h3>Api Url for Cup and Puff</h3></div>

    <div class="col-sm-10">
    
	<div>		 
	<b>Currency Converter</b> =<a href="<?php echo base_url();?>api/currencyConverter_new"><?php echo base_url();?>api/currencyConverter_new</a>
	<br>		
	This is a post api,		 parameters are
	<br>		
		<table class="table">		 
			<tr>			
				<th>parameter</th>			
				<th>Description</th>	
			</tr>		 
			<tr>			
				<td>from_Currency</td>	
				<td>Currency from you want to convert like USD Or INR</td> 
			</tr>		
			<tr>		
				<td>to_Currency</td>		
				<td>Currency into you want to convert like USD Or INR</td>  	
			</tr>		
			<tr>		
				<td>amount</td>	
				<td>Amount you want to convert</td>  	
			</tr>		  
		</table>		
	</div>
	
	<br>
	
	<div>		 
	<b>Feedback</b> =<a href="<?php echo base_url();?>api/feedback"><?php echo base_url();?>api/feedback</a>
	<br>		
	This is a post api,		 parameters are
	<br>		
		<table class="table">		 
			<tr>			
				<th>parameter</th>			
				<th>Description</th>	
			</tr>		 
			<tr>			
				<td>user_id</td>	
				<td>User Id of the user</td> 
			</tr>		
			<tr>		
				<td>title</td>		
				<td>Subject of feedback</td>  	
			</tr>		
			<tr>		
				<td>description</td>	
				<td>Message of the feedback</td>  	
			</tr>		  
		</table>		
	</div>
	
	<br>
	
	<div>		 
	<b>Signup</b> =<a href="<?php echo base_url();?>api/signup"><?php echo base_url();?>api/signup</a>
	<br>		
	This is a post api,		 parameters are
	<br>		
		<table class="table">		 
			<tr>			
				<th>parameter</th>			
				<th>Description</th>	
			</tr>		 
			<tr>			
				<td>email</td>	
				<td>Email of the user</td> 
			</tr>		
			<tr>		
				<td>pass</td>		
				<td>Password of the user</td>  	
			</tr>		
			<tr>		
				<td>name</td>	
				<td>Name of the user</td>  	
			</tr>		  
			<tr>		
				<td>mobile</td>		
				<td>Mobile of the user</td>  	
			</tr>	
		</table>		
	</div>
	
	<br>
     
	 
	  <div>
		 <b>Login</b> =<a href="<?php echo base_url();?>api/login"><?php echo base_url();?>api/login</a><br>
		 This is a post api,
		 parameters are<br>
		 <table class="table">
		  <tr>
			<th>parameter</th>
			<th>Description</th>
		  </tr>
		  <tr>
			<td>user_email</td>
			<td>Email of the user</td>  
		  </tr>
		  <tr>
			<td>user_pass</td>
			<td>Password of the user</td>  
		  </tr>
		</table>
		<p>after this when you get success call category api</p>
	  </div><br>
	  
	  <div>
		 <b>Category</b> =<a href="<?php echo base_url();?>api/category"><?php echo base_url();?>api/category</a><br>
		 This is a POST api
	  </div><br>
	  
	   <div>
		 <b>Items</b> =<a href="<?php echo base_url();?>api/items"><?php echo base_url();?>api/items</a><br>
		 This is a post api,
		 parameters are<br>
		 <table class="table">
		  <tr> 	
			<th>parameter</th>
			<th>Description</th>
		  </tr>
		  <tr>
			<td>start</td>
			<td>Start from starting with 0</td>  
		  </tr>
		  <tr>
			<td>length</td>
			<td>how many product you want to fetch</td>  
		  </tr>
		  <tr>
			<td>cat_id</td>
			<td>category Id</td>  
		  </tr>
		  <tr>
			<td>keyword</td>
			<td>Keyword</td>  
		  </tr>
		   <tr>
			<td>user_id</td>
			<td>Current user id</td>  
		  </tr>
		</table>
		
	  </div><br>
	  
	   <div>
		 <b>Checkout</b> =<a href="<?php echo base_url();?>api/check_out"><?php echo base_url();?>api/check_out</a><br>
		 This is a post api,
		 parameters are<br>
		 <table class="table">
		  
		  <tr>
			<td>user_id</td>
			<td>Current user id</td>  
		  </tr>
		</table>
		
	  </div><br>
	  
	   <div>
		 <b>Processed payment</b> =<a href="<?php echo base_url();?>api/processed_order"><?php echo base_url();?>api/processed_order</a><br>
		 This is a post api,
		 parameters are<br>
		 <table class="table">
		  
		  <tr>
			<td>user_id</td>
			<td>Current user id</td>  
		  </tr>
		  <tr>
			<td>items_array</td>
			<td>Array Of items              [{"item":1,"quantity":2,"amount":5},{"item":12,"quantity":5,"amount":5}].</td>  
		  </tr>
		  <tr>
			<td>payment_mode</td>
			<td>wallet,cod,payu,paypal</td>   
		  </tr>
		  <tr>
			<td>payment_amount</td>
			<td>Total amount</td>  
		  </tr>
		  <tr>
			<td>payment_address</td>
			<td>Address on which order will delivered</td>  
		  </tr> 
		  <tr>
			<td>payment_txnId</td>
			<td>Payment Transaction detail</td>  
		  </tr>
		  
		</table>
		
	  </div><br>
       
	   <div>
		 <b>Orders</b> =<a href="<?php echo base_url();?>api/my_orders"><?php echo base_url();?>api/my_orders</a><br>
		 This is a post api,
		 parameters are<br>
		 <table class="table">
		  <tr> 	
			<th>parameter</th>
			<th>Description</th>
		  </tr>
		  <tr>
			<td>start</td>
			<td>Start from starting with 0</td>  
		  </tr>
		  <tr>
			<td>length</td>
			<td>how many order you want to fetch</td>  
		  </tr>
		  <tr>
			<td>user_id</td>
			<td>Current user id</td>  
		  </tr>
		  <tr>
			<td>user_accesslevel</td>
			<td>Current user_accesslevel</td>  
		  </tr>
		</table>
		
	  </div><br>
	  
	  <div>
		 <b>Order Details</b> =<a href="<?php echo base_url();?>api/order_details"><?php echo base_url();?>api/order_details</a><br>
		 This is a post api,
		 parameters are<br>
		 <table class="table">
		  <tr> 	
			<th>parameter</th>
			<th>Description</th>
		  </tr>
		 
		  <tr>
			<td>order_id</td>
			<td>order_id</td>  
		  </tr>
		  <tr>
			<td>user_id</td>
			<td>Current user id</td>  
		  </tr>
		</table>
		
	  </div><br>
	  
	   <div>
		 <b>Add to cart</b> =<a href="<?php echo base_url();?>api/add_to_cart"><?php echo base_url();?>api/add_to_cart</a><br>
		 This is a post api,
		 parameters are<br>
		 <table class="table">
		  <tr> 	
			<th>parameter</th>
			<th>Description</th>
		  </tr>
		 
		  <tr>
			<td>user_id</td>
			<td>user_id</td>  
		  </tr>
		  <tr>
			<td>item_id</td>
			<td>item_id</td>  
		  </tr>
		  <tr>
			<td>item_quantity</td>
			<td>item_quantity</td>  
		  </tr>
		</table>
		
	  </div><br>
	  
	  <div>
		 <b>Remove to cart</b> =<a href="<?php echo base_url();?>api/remove_to_cart"><?php echo base_url();?>api/remove_to_cart</a><br>
		 This is a post api,
		 parameters are<br>
		 <table class="table">
		  <tr> 	
			<th>parameter</th>
			<th>Description</th>
		  </tr>
		 
		  <tr>
			<td>user_id</td>
			<td>user_id</td>  
		  </tr>
		  <tr>
			<td>item_id</td>
			<td>item_id</td>  
		  </tr>
		  <tr>
			<td>item_quantity</td>
			<td>item_quantity</td>  
		  </tr>
		</table>
		
	  </div><br>
	  
	   <div>
		 <b>My Cart</b> =<a href="<?php echo base_url();?>api/my_cart"><?php echo base_url();?>api/my_cart</a><br>
		 This is a post api,
		 parameters are<br>
		 <table class="table">
		  <tr> 	
			<th>parameter</th>
			<th>Description</th>
		  </tr>
		 
		  <tr>
			<td>user_id</td>
			<td>user_id</td>  
		  </tr>
		  
		</table>
		
	  </div><br>

      <div>
		 <b>Add request</b> =<a href="<?php echo base_url();?>api/add_request"><?php echo base_url();?>api/add_request</a><br>
		 This is a post api,
		 parameters are<br>
		 <table class="table">
		  <tr> 	
			<th>parameter</th>
			<th>Description</th>
		  </tr>
		 
		  <tr>
			<td>email</td>
			<td>Email for requester</td>  
		  </tr>
		  <tr>
			<td>mobile</td>
			<td>Mobile for requester</td>  
		  </tr>
		  <tr>
			<td>message</td>
			<td>Message from requester</td>  
		  </tr>
		</table>
		
	  </div><br>


     <div>
		 <b>Update User</b> =<a href="<?php echo base_url();?>api/update_user"><?php echo base_url();?>api/update_user</a><br>
		 This is a post api,
		 parameters are<br>
		 <table class="table">
		  
		  <tr>
			<td>user_id</td>
			<td>Current user id</td>  
		  </tr>
		  <tr>
			<td>user_name</td>
			<td>Current user name</td>  
		  </tr>
		  <tr>
			<td>user_address</td>
			<td>Current user address</td>  
		  </tr>
		  <tr>
			<td>user_mobile</td>
			<td>Current user mobile</td>  
		  </tr>
		  <tr>
			<td>user_pic</td>
			<td>Current user picture</td>  
		  </tr>
		  
		</table>
		
	  </div><br>

      <div>
		 <b>Forget password</b> =<a href="<?php echo base_url();?>api/forget_password"><?php echo base_url();?>api/forget_password</a><br> 
		 This is a post api,
		 parameters are<br>
		 <table class="table">
		  <tr>
			<td>user_mobile</td>
			<td>user Mobile which otp will be send</td>  
		  </tr> 
		</table>
		
	  </div><br>
       <div>
		 <b>Update Order</b> =<a href="<?php echo base_url();?>api/update_order"><?php echo base_url();?>api/update_order</a><br> 
		 This is a post api,
		 parameters are<br>
		 <table class="table">
		  <tr>
			<td>user_id</td>
			<td>Current user id</td>  
		  </tr> 
		  <tr>
			<td>order_id</td>
			<td>Order Id which you want to update</td>  
		  </tr> 
		  <tr>
			<td>order_status</td>
			<td>numeric value 0,1,2,3,4</td>  
		  </tr>
          <tr>
			<td>order_note</td>
			<td>message </td>  
		  </tr> 		  
		</table>
		
	  </div><br>

       <div>
		 <b>Update password</b> =<a href="<?php echo base_url();?>api/update_password"><?php echo base_url();?>api/update_password</a><br> 
		 This is a post api,
		 parameters are<br>
		 <table class="table">
		  <tr>
			<td>user_otp</td>
			<td>Latest Otp recieed by user</td>  
		  </tr> 
		  <tr>
			<td>user_pass</td>
			<td>New password</td>  
		  </tr> 
		</table>
		
	  </div><br>

        <div>
		 <b>Delete User</b> =<a href="<?php echo base_url();?>api/delete_user"><?php echo base_url();?>api/delete_user</a><br> 
		 This is a post api,
		 parameters are<br>
		 <table class="table">
		  <tr>
			<td>user_id</td>
			<td>current user id</td>  
		  </tr> 
		   <tr>
			<td>user_email</td>
			<td>Email or mobile of the user</td>  
		  </tr>
		  <tr>
			<td>user_pass</td>
			<td>Password of the user</td>  
		  </tr>
		</table>
		
	  </div><br>
	  
       <div>
		 <b>Notification</b> =<a href="<?php echo base_url();?>api/notification"><?php echo base_url();?>api/notification</a><br> 
		 This is a post api,
		 parameters are<br>
		 <table class="table">
		  <tr>
			<td>user_id</td>
			<td>current user id</td>  
		  </tr> 
		</table>
		
	  </div><br>	  

    <div>
		 <b>Get Offer</b> =<a href="<?php echo base_url();?>api/get_offer"><?php echo base_url();?>api/get_offer</a><br>
		 This is a POST api
	  </div><br>
	  
	  <div>
		 <b>Table Booking</b> =<a href="<?php echo base_url();?>api/booking"><?php echo base_url();?>api/booking</a><br> 
		 This is a post api,
		 parameters are<br>
		 <table class="table">
		  <tr>
			<td>booking_date</td>
			<td>Table Booking date</td>  
		  </tr> 
		   <tr>
			<td>booking_time</td>
			<td>Table Booking Time</td>  
		  </tr>
		  <tr>
			<td>name</td>
			<td>Customer Name</td>  
		  </tr>
		  <tr>
			<td>mobile</td>
			<td>Customer Mobile No.</td>  
		  </tr>
		  <tr>
			<td>email</td>
			<td>Customer Email Id</td>  
		  </tr>
		  <tr>
			<td>description</td>
			<td>Description / Message</td>  
		  </tr>
		</table>
		
	  </div><br>

	  <div>
		 <b>Apply Discount Coupon</b> =<a href="<?php echo base_url();?>api/varify_coupon"><?php echo base_url();?>api/varify_coupon</a><br> 
		 This is a post api,
		 parameters are<br>
		 <table class="table">
		  <tr>
			<td>coupon_code</td>
			<td>Discount Coupon Code</td>  
		  </tr> 
		   <tr>
			<td>item_id</td>
			<td>Item Id for apply coupon</td>  
		  </tr>
		</table>
		
	  </div><br>
	  
	  
	  
	  
  </div>
  </div>
</div>

</body>
</html>
