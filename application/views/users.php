<div class="row">
    <div class="col-md-12">
        <div class="hs_heading medium">
            <h3>Users (<?php echo count($userList); ?>) <a data-toggle="modal" data-target="#addnewuser" class="btn">Add User</a></h3>
        </div>
        <div class="hs_datatable_wrapper table-responsive">
            <table class="hs_datatable table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Celular</th>
                        <th>Estado</th>
						<th>Tipo</th>
                        <th>Accion</th>
                    </tr>
                </thead>
                <tbody>
				 <?php if($userList){$cnt=0; ?>
				   <?php foreach($userList as $soloUser){
					   
					  $cnt++;
					  $user_id=$soloUser['user_id'];
                      $user_name=$soloUser['user_name'];
					  $username_arr=explode(' ',$user_name);
					  $name_initiails=substr($username_arr[0] , 0, 1); 
					  if(isset($username_arr[1])){
						$name_initiails.=substr($username_arr[1] , 0, 1); 
					  }
					  $useractiveselected = ($soloUser['user_status'] == '1' ? 'selected' : '' );
					  $userinactiveselected = ($soloUser['user_status'] == '3' ? 'selected' : '' );
					 ?>
				    <tr>
                        <td><?php echo $cnt;?></td>
                        <td width="200">
                            <div class="hs_user">
                                <div class="user_img">
                                    <span class="hs_user_initials"><?php echo $name_initiails; ?></span>
                                </div>
                                <div class="user_name">
                                    <p><?php echo $user_name; ?></p>
                                    <span>Created at - <?php echo date("d-m-Y", strtotime($soloUser['user_registerdate'])); ?></span>
                                </div>
                            </div>
                        </td>
                        <td><?php echo  $soloUser['user_email']?></td>
                        <td><?php echo $soloUser['user_mobile']?></td>
						<td>
                            <select class="form-control" onchange="updatethevalue(this,'user');"  id="<?php echo $user_id;  ?>_status">
                                <option value="1" <?php echo $useractiveselected; ?>>Active</option>
                                <option value="3"  <?php echo $userinactiveselected; ?>>Inactive</option>
                            </select>
                        </td>
						<td>
							<?php echo ($soloUser['user_accesslevel']==2?'User':'Delivery Boy') ;?>
                        </td>
						
                       <td width="200">
                            <a href="<?php echo base_url() ?>admin/single_user/<?php echo $user_id; ?>" class="btn" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            <a  class="btn" title="Delete" onclick="delete_user(<?php echo  $user_id?>)"><i class="fa fa-trash" aria-hidden="true"></i></a>
							<?php if($soloUser['user_accesslevel']==2){ ?>
							<a class="btn" title="Add Wallet" onclick="openWalletcreditPopup(<?php echo $user_id; ?> , '<?php echo $user_name; ?>')"><i class="fa fa-money" aria-hidden="true"></i></a>
							<?php } ?>
                        </td>
                     </tr> 
				   <?php } ?>	 
				 <?php } ?> 
                </tbody>
            </table>
        </div>
    </div>
</div>












<!-- Add New Category popup start -->
<div id="addnewuser" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><span>Add New</span> User</h4>
            </div>
            <div class="modal-body">
			<form method="post" enctype="multipart/form-data" id="">
                <div class="hs_input">
                    <label>Usuario</label>
					<input type="text" class="form-control addnewuser user_name" id="user_name"/>
                </div>
				 <div class="hs_input">
                    <label>Email</label>
                    <input type="email" class="form-control addnewuser user_email" id="user_email"/>
					
                </div>
                <div class="hs_input">
                    <label>Celular</label>
                    <input type="text" class="form-control addnewuser user_mobile" id="user_mobile"/>
                </div>
				 <div class="hs_input">
                    <label>Password</label>
                    <input type="password" class="form-control addnewuser user_pass" id="user_pass"/>
                </div>
				 <div class="hs_input">
                    <label>Tipo Usuario</label>
                    <select  class="form-control addnewuser user_accesslevel" id="user_accesslevel">
						<option value="2" >User</option>
						<option value="3" >Delivery Boy</option>
                    </select>
                </div>
                <div class="hs_input">
				  <a onclick="addnewuser();" class="btn">Registrar  <i class="fa fa-spinner fa-spin addnew_wait hide" aria-hidden="true"></i></a>
                </div>
            </div>
			</form>
        </div>
    </div>
</div>
<!-- Add New Category popup end -->


<!-- Wallet Credit start -->
<div id="wallet_credit" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Credit to <span id="user_unameTxt"> </span>'s Wallet</h4>
		        <p>Wallet Balance <span id="wallet_amountTxt">0</span></p>
            </div>
            <div class="modal-body">
                <div class="hs_input">
                    <label>Amount</label>
					 <input type="text" class="form-control" id="wc_amount" />
                </div>
				 <div class="hs_input">
                    <label>Note(Description)</label>
                    <textarea class="form-control" id="wc_note"></textarea>
					
                </div>
                
				
                <div class="hs_input">
				 <input type="hidden" id="wallet_user_id" value="0">
                   <button type="button" class="btn" onclick="update_wallet_fun()">Add</button>
				  
                </div>
            </div>
			
        </div>
    </div>
</div>
<!-- Wallet Credit popup end -->



 </div>
    <!-- page body end -->
