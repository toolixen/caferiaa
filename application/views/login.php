<?php $img  = $this->DatabaseModel->select_data('*','rt_images',array('id'=>1),''); ?>
<div class="hs_auth_wrapper" id="login_form">
    <div class="hs_auth_logo">
        <img src="<?php echo base_url($img[0]['img_logo']); ?>" alt="cafe">
    </div>
    <div class="hs_auth_inner">
        <div class="hs_input">
            <label>Email</label>
            <input type="email" class="form-control" placeholder="Email" id="user_email" value="<?php if(isset($_COOKIE['rt_emanu'])){ echo $_COOKIE['rt_emanu'];} ?>">
        </div>
        <div class="hs_input">
            <label>Password</label>
            <input type="password" class="form-control" placeholder="Password" id="user_pass" value="<?php if(isset($_COOKIE['rt_dwp'])){ echo $_COOKIE['rt_dwp'];} ?>">
        </div>
        <div class="hs_checkbox">
            <input type="checkbox" id="user_remember" <?php if(isset($_COOKIE['rt_dwp'])){ echo "checked";} ?>>
            <label for="user_remember">Recordar contraseña</label>
        </div>
        <a class="btn target-submit" button-type="login">Ingresar</a>
    </div>
</div>