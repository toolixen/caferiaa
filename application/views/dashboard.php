<div class="row">
	<div class="col-md-3">
		<a href="<?php echo base_url().'admin/users'; ?>" class="hs_analytics_data">
			<div class="pull-left">
				<span class="icon"><i class="fa fa-user-o" aria-hidden="true"></i></span>
				<p>Usuarios</p>
			</div>
			<h3 class="pull-right"><?php echo $userCount; ?></h3>
		</a>
	</div>
	<div class="col-md-3">
		<a href="<?php echo base_url().'admin/items'; ?>" class="hs_analytics_data">
			<div class="pull-left">
				<span class="icon"><i class="fa fa-cutlery" aria-hidden="true"></i></span>
				<p>Productos</p>
			</div>
			<h3 class="pull-right"><?php echo $itemCount; ?></h3>
		</a>
	</div>
	<div class="col-md-3">
		<a href="<?php echo base_url().'admin/orders'; ?>" class="hs_analytics_data">
			<div class="pull-left">
				<span class="icon"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span>
				<p>Pedidos</p>
			</div>
			<h3 class="pull-right"><?php echo $orderCount; ?></h3>
		</a>
	</div>
	<div class="col-md-3">
		<a href="<?php echo base_url().'admin/orders'; ?>" class="hs_analytics_data">
			<div class="pull-left">
				<span class="icon"><i class="fa fa-credit-card" aria-hidden="true"></i></span>
				<p>Ingresos</p>
			</div>
			<h3 class="pull-right"><?php echo $this->ts_functions->getsettings('portalcurreny','symbol'); echo $revenueCount;  ?></h3>
		</a>
	</div>
	<!--div class="col-md-4 ">
		<a href="<?php echo base_url().'admin/orders'; ?>" class="hs_analytics_data">
			<div class="pull-left">
				<span class="icon"><i class="fa fa-credit-card" aria-hidden="true"></i></span>
				<p>Account Request</p>
			</div>
			<h3 class="pull-right"><?php echo $accountrequestCount; ?></h3>
		</a>
	</div-->
	
</div>

<div class="row hide">
    <div class="col-md-12">
        <div class="hs_heading medium">
            <h3>Latest Orders  </h3>
        </div>
        <div class="hs_datatable_wrapper table-responsive">
            <table class="hs_datatable table table-bordered table-responsive">
                <thead>
                    <tr>
                        <!--  <th>#</th> -->
						<th>Order Id</th>
                        <th>Name</th>
                        <th>Mobile</th>
						<th>Items</th>
                        <th>Order Status</th>
						<th>Delivery Boy</th>
						<th>Payment Status</th>
						<th>Payment Mode</th>
                        <th>Amount ($)</th>
                        <th>Date</th> 
                    </tr>
                </thead>
                <tbody>
				 <?php if($ordersList){$cnt=0; ?>
				   <?php foreach($ordersList as $soloOrd){
					  $cnt++;
					  $payment_id=$soloOrd['payment_id'];
                      $user_name=$soloOrd['user_name'];
					  $username_arr=explode(' ',$user_name);
					  $name_initiails=substr($username_arr[0] , 0, 1); 
					  if(isset($username_arr[1])){
						$name_initiails.=substr($username_arr[1] , 0, 1); 
					  }
					  $useractiveselected = ($soloOrd['user_status'] == '1' ? 'selected' : '' );
					  $userinactiveselected = ($soloOrd['user_status'] == '0' ? 'selected' : '' );
					  
					  $items=json_decode($soloOrd['payment_pid'],true);
					  $itemHtml='';
					  foreach($items as $item){
						 $itemsDeatil=$this->DatabaseModel->select_data('item_name','rt_items' , array('item_id'=>$item['item']), 1);
						  $itemName=($itemsDeatil ? $itemsDeatil[0]['item_name']:'No longer available');
						  
						 $itemHtml.='<p>'.$item['quantity'].'X - '.$itemName.' <p>';
					  }
					  
					  
					  
					 ?>
				    <tr >
                        <!-- <td><?php echo $cnt;?></td> -->
                        <td><?php echo $soloOrd['payment_uniqid'];?></td>
                        <td><?php echo  $user_name?></td>
                        <td><?php echo $soloOrd['user_mobile']?></td>
						<td><?php echo $itemHtml?></td>
						<td>
                            <select class="form-control" onchange="updatethevalue(this,'order');"  id="<?php echo $payment_id;  ?>_order_status">
                                <option value="0" <?php echo ($soloOrd['payment_order_status'] == '0' ? 'selected' : '' ); ?>>Pending</option>
                                <option value="1"  <?php echo ($soloOrd['payment_order_status'] == '1' ? 'selected' : '' ); ?>>InTransist</option>
                                <option value="2"  <?php echo ($soloOrd['payment_order_status'] == '2' ? 'selected' : '' ); ?>>Delivered</option>
								<option value="3"  <?php echo ($soloOrd['payment_order_status'] == '3' ? 'selected' : '' ); ?>>Canceled</option>
                            </select>
                        </td>
						<td>
						  <select class="form-control" onchange="updatethevalue(this,'deliveryboy');"  id="<?php echo $payment_id;  ?>_duid">
                                <option value="0">Not Assign</option>
								<?php 
								if($deliveryBoy){
									foreach($deliveryBoy as $soloBoy){
										$selected=($soloBoy['user_id']==$soloOrd['payment_duid'] ? 'selected':'');
										echo '<option value="'.$soloBoy['user_id'].'" '.$selected.'>'.$soloBoy['user_name'].'</option>';
									}
								}
								?>
                               
                            </select>
						</td>
						<td><?php echo ($soloOrd['payment_status'] == '1' ? 'Paid' : 'UnPaid' )?></td>
						<td><?php echo $soloOrd['payment_mode'];?></td>
						<td><?php echo $soloOrd['payment_amount'];?></td>
						 <td><?php echo date('d M y',strtotime($soloOrd['payment_date']));?></td> 
						
                      
                     </tr> 
				   <?php } ?>	 
				 <?php } ?> 
                </tbody>
            </table>
        </div>
    </div>
</div>

 

