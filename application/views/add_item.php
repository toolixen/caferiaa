<?php
if (isset($item_details)) {
    $item_id = $item_details[0]['item_id'];
    $item_name = $item_details[0]['item_name'];
    $item_price = $item_details[0]['item_price'];
    $item_desc = $item_details[0]['item_desc'];
    $item_quantity = $item_details[0]['item_quantity'];
    $item_cat = $item_details[0]['item_cat'];
    $item_sku = $item_details[0]['item_sku'];

} else {
    $item_id = 0;
    $item_name = '';
    $item_price = '';
    $item_desc = '';
    $item_cat = 0;
    $item_quantity = 0;
    $item_sku = '';
    $item_optiondetails = '';
}

?>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12 margin-bottom-30">
        <div class="hs_heading medium">
            <h3><?php echo(isset($item_details) ? 'Update' : 'Add'); ?> Producto</h3>
        </div>
        <form action="<?php echo base_url() . 'admin/update_item'; ?>" method="post" id="add_item_form"
              enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="hs_input">
                        <label>Categoria</label>
                        <select class="form-control required" id="item_cat" name="item_cat">
                            <option value="">Selecciona Categoria</option>
                            <?php
                            if ($catlist) {
                                foreach ($catlist as $soloCat) {
                                    $selected = ($soloCat['cat_id'] == $item_cat) ? 'selected' : '';
                                    echo '<option value="' . $soloCat['cat_id'] . '" ' . $selected . '>' . $soloCat['cat_name'] . '</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="hs_input">
                        <label>Nombre Producto</label>
                        <input type="text" class="form-control required" id="item_name" name="item_name"
                               value="<?php echo $item_name; ?>" maxlength="40">
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="hs_input"><label>Producto SKU </label>
                        <input type="text" class="form-control required"
                                                                          id="item_sku" name="item_sku"
                                                                          value="<?php echo $item_sku; ?>"></div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12" id="mainprod_price">
                    <?php if (empty($item_optiondetails)) { ?>
                        <div class="hs_input">
                            <label>Precio </label>
                            <input type="text" class="form-control required" id="main_item_price" name="main_item_price"
                                   value="<?php echo $item_price; ?>" data-valid="number"
                                   data-error="Please enter valid price">
                        </div>
                    <?php } ?>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="hs_input">
                        <label for="exampleInputFile">Imagen</label>
                        <input type="file" id="item_pic" name="item_pic" class="form-control">
                        <p class="help-block">Preferred size 370px*393px.</p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="hs_input">
                        <label for="exampleInputFile">Cantidad</label>
                        <input type="text" class="form-control required" id="item_quantity" name="item_quantity"
                               value="<?php echo $item_quantity; ?>" data-valid="number"
                               data-error="Please enter valid quantity">
                        <p class="help-block"></p>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="hs_input">
                        <label>Descripcion</label>
                        <textarea rows="4" class="form-control" placeholder="Item Description" id="item_desc"
                                  name="item_desc"><?php echo $item_desc; ?></textarea>
                    </div>
                </div>
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <div class="hs_checkbox">
                        <?php if (!empty($item_optiondetails)) { ?>
                            <input type="checkbox" id="prod_optionscheckbox"
                                   name="prod_optionscheckbox" <?php echo !empty($item_optiondetails) ? 'checked' . ' ' . 'disabled' : ''; ?>
                                   value="on">
                            <input type="hidden" id="prod_optionscheckbox" name="prod_optionscheckbox" value="on">
                            <label>Does this product have different sizes like (Full , Half) </label>
                        <?php } else { ?>
                            <input type="checkbox" id="prod_optionscheckbox" name="prod_optionscheckbox"
                                   onclick="prodoptions_checkbox(this);" value="on">
                            <label for="prod_optionscheckbox">¿Este producto tiene diferentes tamaños como Pequeño, Completo, Medio, etc? </label>
                        <?php } ?>

                    </div>
                </div>

                <div id="prod_option_div" class="col-md-12 col-sm-12 col-xs-12">
                    <?php if (!empty($item_optiondetails)) {
                        $id = 0;
                        foreach ($item_optiondetails as $item_option) {
                            $function_name = 'add_optionfileds';
                            $symbol = 'plus';
                            if ($id > 0) {
                                $function_name = 'remove_optionfileds';
                                $symbol = 'minus';
                            }

                            ?>
                            <div id="prodoption_<?php echo $id; ?>" class="row counter flex_row">

                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="hs_input">
                                        <input type="hidden" class="form-control" id="olditem_optionid"
                                               name="olditem_optionid[]"
                                               value="<?php echo $item_option['item_option_id']; ?>">
                                        <label>Item Sizes </label>
                                        <input type="text" class="form-control required" id="item_size"
                                               name="item_size[]" value="<?php echo $item_option['item_size']; ?>">
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="hs_input">
                                        <label>Item Price </label>
                                        <input type="text" class="form-control required" id="item_price"
                                               name="item_price[]" value="<?php echo $item_option['item_price']; ?>"
                                               data-valid="number" data-error="Please enter valid price">
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="hs_input subprod_btn">
                                        <a onclick="<?php echo $function_name; ?>(<?php echo $id; ?>,<?php echo $item_option['item_option_id']; ?>);"
                                           id="prouductopt_btn" class="btn"><i class="fa fa-<?php echo $symbol; ?>"></i></a>
                                    </div>
                                </div>

                            </div>
                            <?php $id++;
                        }
                    } ?>
                </div>


            </div>
            <input id="item_id" type="hidden" name="item_id" value="<?php echo $item_id; ?>">
            <a class="btn " onclick="add_item(this)"><?php echo(isset($item_details) ? 'Update' : 'Add'); ?></a>
        </form>
    </div>
</div>


<script>

    /*function prodoptions_checkbox($this){
          if($($this).is(':checked')){
             $('#mainprod_price').html('');
             add_optionfileds();
         }else{
             $('#mainprod_price').html('<div class="hs_input"><label>Item Price </label><input type="text" class="form-control required" id="main_item_price" name="main_item_price" value="<?php echo $item_price; ?>" data-valid="number" data-error="Please enter valid number"></div>');
    	    $('#prod_option_div').html('');
    	}
   } 
    
   
	
	function add_optionfileds(id=0){

		var add_btn='';
		if($('#prodoption_'+id).length == 0 ){
			var id=0;
			var div_id = 'prodoption_'+id;
			add_btn += '<div class="col-md-3 col-sm-12 col-xs-12"><div class="hs_input"><a onclick="add_optionfileds('+id+');" id="prouductopt_btn" class="btn" ><i class="fa fa-plus"></i></a></div></div>';
		}else{
			id = 0 + $('.counter').length;
			var div_id = 'prodoption_'+id;
			add_btn += '<div class="col-md-3 col-sm-12 col-xs-12"><div class="hs_input"><a onclick="remove_optionfileds('+id+',0);" id="prouductopt_btn" class="btn" ><i class="fa fa-minus"></i></a></div></div>';
		}
	 
		var box_html = '<div id="'+div_id+'" class="row counter"><div class="col-md-3 col-sm-12 col-xs-12"><div class="hs_input"><label>Item Sizes </label><input type="text" class="form-control required" id="item_size" name="item_size[]" value=""></div></div><div class="col-md-3 col-sm-12 col-xs-12"><div class="hs_input"><label>Item Price </label><input type="text" class="form-control required" id="item_price" name="item_price[]" value="" data-valid="number" data-error="Please enter valid price"></div></div>'+add_btn+'</div>';
		$('#prod_option_div').append(box_html);
	}
	
	function remove_optionfileds(box_id, item_option_id =''){
		
		 if(item_option_id !='' && item_option_id !=0){
			 var cnf=confirm('Are you sure remove this option from database ?');
			 if(cnf)
			 {
				var basepath = $('#base_url').val();
				dataArr = {};
				dataArr['item_opt_id']= item_option_id;  
				$.post(basepath+"admin/delete_item_options",dataArr,function(data) {
					if(data == 1)
					{
						if(box_id > 0)
						{
							$('#prodoption_'+box_id).remove();
						}
						toastr.success('Data remove successfully'); 
					}
				});
			}
		}else{
			if(box_id > 0)
			{
				$('#prodoption_'+box_id).remove(); 
			}
		}		
	}*/
</script>








