
<script src="https://www.gstatic.com/firebasejs/5.5.8/firebase.js"></script>
<script>
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyBM4lfvr_sxywgon0R4dV-3Xq1nXYV29tA",
    authDomain: "dstest-f89ad.firebaseapp.com",
    databaseURL: "https://dstest-f89ad.firebaseio.com",
    projectId: "dstest-f89ad",
    storageBucket: "dstest-f89ad.appspot.com",
    messagingSenderId: "201261070061"
  };
  firebase.initializeApp(config);
  
  // Retrieve Firebase Messaging object.
const messaging = firebase.messaging();

messaging.requestPermission().then(function() {
  console.log('Notification permission granted.');
  getRegToken();
  // TODO(developer): Retrieve an Instance ID token for use with FCM.
  // ...
}).catch(function(err) {
  console.log('Unable to get permission to notify.', err);
});
function getRegToken()
{
	  messaging.getToken().then(function(currentToken) {
	  if (currentToken) {
		//sendTokenToServer(currentToken);
		console.log(currentToken);
	  } else {
		// Show permission request.
		console.log('No Instance ID token available. Request permission to generate one.');
		// Show permission UI.
		updateUIForPushPermissionRequired();
		setTokenSentToServer(false);
	  }
	}).catch(function(err) {
	  console.log('An error occurred while retrieving token. ', err);
	  showToken('Error retrieving Instance ID token. ', err);
	  setTokenSentToServer(false);
	});
}
function setTokenSentToServer(sent) {
    window.localStorage.setItem('sentToServer', sent ? '1' : '0');
  }
</script>