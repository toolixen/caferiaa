<!--blog single start--> 


<div class="fd_section fd_blog_single_wrapper fd_padder_bottom80">
			<div class="container">
				
 
	
				<div class="row">
					<div class="col-md-8 col-sm-12" >
						<div  id="blogContent">
							<div class="fd_blog_box clear_shadow">
							<div class="fd_blog_img">
								<a href="javascript:;"><img width="850px" height="450px" src="<?php echo base_url(); ?>assets/admin/images/blog/<?php echo $blog['blog_img'] ?>" alt="" class="img-fluid"></a>
							</div>
						
							<div class="fd_blog_detail clear_space">
								<div class="fd_blog_meta fd_padder_bottom20">
									<ul>
										<li><span><i class="flaticon-calendar"></i>
										<?php    $time=strtotime($blog['created_at']);
												 echo $month=substr(date("F",$time),0,3);
												 echo $date=date("d",$time).',';
												 echo $year=date("y",$time);               ?>
										</span></li>
										<?php if(!$this->session->userdata('is_login')){ ?>
										<li>
											<a href="" onclick="addlike(); void(0); return false;">
											<i class="flaticon-thumbs-up-hand-symbol"></i><?php echo $blog['like_count'] ?>
											</a>
										</li>
										<?php }else{ ?>
										 <li>
											 <a href="javascript:;"  id="likebutton<?php echo $blog['id'] ?>" onclick="addlike(<?php echo $blog['id'] ?>,<?php echo  (get_like($blog['id']) == 1)?0:1; ?>); void(0); return false;">
											
											<?php if(get_like($blog['id']) == 1){ ?>
											  <i class="fa fa-thumbs-down" aria-hidden="true"></i><?php echo $blog['like_count'] ?>	
											<?php }else{ ?>
											  <i class="flaticon-thumbs-up-hand-symbol"></i><?php echo $blog['like_count'] ?>
											<?php } ?>
											</a>
										</li>
										<?php } ?>
										<li><a href="#atcomment"><i class="flaticon-speech-bubbles-comment-option"></i><?php echo $count_comment ?></a></li>
									</ul>
									<div class="fd_share_wrapper">
				 						<span class="flaticon-share"></span>
										
										<ul class="fd_social_icon fd_transition">
											<li><a class="linkedin fd_transition" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url(); ?>front/oneblog/<?php echo $this->uri->segment(3); ?>&title=&summary=&source="><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
											<li ><a class="facebook fd_transition"  href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url(); ?>front/oneblog/<?php echo $this->uri->segment(3); ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
											<li> <a class="twitter fd_transition" href="https://twitter.com/home?status=<?php echo base_url(); ?>front/oneblog/<?php echo $this->uri->segment(3); ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
										</ul>
									</div>
								</div>
								<h2 class="fd_subheading"><?php echo $blog['title'] ?></h2>
								<?php 
									$contents = unserialize($blog['content']);
									for($i=0; $i < count($contents) ; $i++){
								?>	
									<p><?php echo $contents[$i]  ?></p>
								<?php }?>
								<div class="fd_tags_wrapper">
									<ul>
										<li><span>tags -</span></li>
									<?php 
										$tags =  explode(',',$blog['tags']);
									$k=1;	
									foreach($tags as $tag){ ?>
										<li><a href="javascript:;"><?php echo $tag ?> <?php if(count($tags) != $k ){echo ',';} ?> </a></li>
									<?php $k++;  } ?>	
									</ul>
								</div>
							</div>
						</div>
									
						<br>			
						
						<div class="fd_author_section fd_padder_bottom60">
							<h1 class="fd_heading_with_line fd_subheading">About Author</h1>
							
							<ul>
								<li>
									<div class="fd_author">
										<div class="fd_author_img">
											<img width="120px" height="140px" src="<?php echo base_url(); ?>assets/admin/images/blog/<?php echo $blog['author_img'] ?>" alt="" class="img-fluid">
										</div>
										<div class="fd_author_detail">
											<div class="fd_author_share fd_padder_bottom20">
												<h3 class="fd_author_heading fd_subheading">Sobre el autor</h3>
												<ul class="fd_social_icon fd_transition">
													<li><a href="<?php echo $blog['facebook_url'] ?>" class="fd_transition"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
													<li><a href="<?php echo $blog['twitter_url'] ?>" class="fd_transition"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
													<li><a href="<?php echo $blog['linkedin_url'] ?>" class="fd_transition"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
													<li><a href="#" class="fd_transition"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
												</ul>
											</div>
											<p><?php echo $blog['author_quote'] ?>.</p>
											<h4 class="fd_subheading"><?php echo $blog['author_name'] ?> - <span><?php echo $blog['author_profile'] ?></span></h4>
										</div>
									</div>
								</li>
							</ul>
						</div>
						<?php if(set_blog('comment_section') == 1){ ?>
						<div class="fd_comment_section fd_padder_bottom60">
							<h1 class="fd_heading_with_line fd_subheading">comments</h1>
							
							<ul id="RecentComment">
							<?php foreach($comments as $comment){ ?>
								<li>
									<div class="fd_cooment_box fd_transition">
										<div class="fd_comment_img">
											<?php echo substr($comment['name'],0,1) ?>
										</div>
										<div class="fd_comment_detail">
											<h2 class="fd_subheading"><?php echo $comment['name'] ?></h2>
											<ul>
												<li><a href="#">
												<?php
														 $time=strtotime($comment['created_date']);
														 echo $date	=	date("d",$time). ' ';	
														 echo $month=	date("F",$time). '' ;
														 echo $year=date("Y",$time);              ?>
												
												</a></li>
											</ul>
											<p><?php echo $comment['comment'] ?></p>
										</div>
									</div>
								</li>
							<?php } ?>	
							
							</ul>
						</div>
					
						<div class="fd_comment_form_section" id="atcomment">
							<h1 class="fd_heading_with_line fd_subheading">leave a comments</h1>
							
							<div class="fd_comment_form">
								<div class="row">
									<div class="col-md-6 col-sm-12 col-xs-12">
									<input type="hidden" value="<?php echo $blog['id'] ?>" id="Bl_id" >
										<div class="form-group">
											<input type="text"  id="Bl_name" class="form-control" placeholder="Enter Your Name" value="<?php echo $name ?>" readonly>
											
										</div>
									</div>
									<div class="col-md-6 col-sm-12 col-xs-12">
										<div class="form-group">
											<input type="text" id="Bl_email" class="form-control" placeholder="Enter Your Email" value="<?php echo $email ?>" readonly>
										</div>
									</div>
									<div class="col-md-12 col-sm-12 col-xs-12">
										<div class="form-group">
											<textarea id="Bl_message" class="form-control" placeholder="Message"></textarea>
										</div>
									</div>
									<div class="col-md-12 col-sm-12 col-xs-12">
									<?php if($this->session->userdata('is_login')){ ?>
										<a href="#"  onclick="submitComment(); void(0); return false;" class="fd_btn">post comment</a>
									<?php }else{  ?>
										<button class="fd_btn" onclick="toastr.error('Login for post a comment...', '');" >post comment </button>
									<?php } ?>	
									 	
									</div>
								</div>
							</div>
						</div>
						<?php } ?>	
						</div>
					</div>
					<div class="col-md-4 col-sm-12">
						<div class="fd_sidebar_wrapper">
							<div class="widget widget_search">
								<h1 class="fd_heading_with_line fd_subheading">search</h1>
								
								<div class="fd_search_wrapper">
									<div class="fd_search">
										<input type="text" id="searchBlog" value="" class="form-control" placeholder="Search Here">
										<a href="javascript:;" class="fd_search_btn"><i class="fa fa-search" aria-hidden="true"></i></a>
									</div>
									<ul class='list-group' id="searchResult"> </ul>
								</div>
							</div>
							
							<div class="widget widget_categories">
								<h1 class="fd_heading_with_line fd_subheading">categories</h1>
								<ul>
								<?php foreach($blog_cat as $cat){ ?>	
									<li><a href="#" onclick="getBlogOfCate(<?php echo $cat['id'] ?>); void(0); return false;" ><span><?php echo $cat['title'] ?></span><span>(<?php echo $cat['total'] ?>)</span></a></li>
								<?php } ?>	
								</ul>
							</div>
							
							<div class="widget widget_post">
								<h1 class="fd_heading_with_line fd_subheading">recent posts</h1>
								
								<ul>
								<?php foreach($blogs as $blog){ ?>
									<li><div class="fd_rec_post">
											<a href="#" onclick="singleBlock(<?php echo $blog['id'] ?>); void(0); return false;">
											<img width="60px" height="60px" src="<?php echo base_url(); ?>assets/admin/images/blog/<?php echo $blog['author_img'] ?>" alt=""></a>
											<span>
												<?php  	$contents = unserialize($blog['content']);
														for($i=0; $i < 1 ; $i++){ 			?>
													
												<p>	<?php echo substr ($contents[$i] ,0,45) ?>....	</p>
												<?php } ?>
												<p><a href="#" onclick="singleBlock(<?php echo $blog['id'] ?>); void(0); return false;">
												<?php 	$time=strtotime($blog['created_at']);
														 echo $date=date("d",$time).' ';
														 echo $month=date("F",$time) .' ';
														 echo $year=date("Y",$time);
												?>
												
												</a></p>
											</span>
										</div>
									</li>
								<?php } ?>	
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div> 
		</div>
	<!--blog single end--> 
	