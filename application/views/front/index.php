<?php if($menus[1]['status'] == 1){ ?>
	<!--product start--> 
	<div class="fd_section" id="<?php echo $menus[1]['field'] ?>">
		<div class="fd_shap_wrapper fd_shap_top">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="fd_heading_wrapper text-center">
							<img src="<?php echo base_url();?>assets/front/images/default_image/cup.svg" alt="">
							<h1>Nuestro Menú</h1>
						</div>
					</div>
					
					<div class="fd_filter_wrapper">
						<!-- menu-for-isotope -->
						<ul class="fd_filter_menu">
						<li data-filter="*" class="active fd_transition" id="allMenu">all</li>
						<?php foreach($Item_cat as $cat){ ?>
						 <li onclick="getcatallitem(<?php echo $cat['cat_id'] ?>)" data-filter=".<?php echo $cat['cat_slug'] ?>" class="fd_transition"> <?php echo $cat['cat_name'] ?> </li>	
						<?php } ?>
						  
						 <input type="hidden" value="" id="tempcat"> 
						  
						</ul>
						<!-- filter-for-isotope -->
						
						<div class="grid">
						<?php foreach($items as $item){ ?>
							<?php if($item['item_price'] =='' && $item['item_options'] == 'YES')
								  { 
							  
									$item_options = $this->DatabaseModel->select_data('item_size,item_price','rt_item_options',array('main_item_id'=>$item['item_id']),'1'); 
									$item_price = !empty($item_options) ? key_text('portalcurreny_symbol').$item_options[0]['item_price'] : '' ;
									$addToCart_btn='<a href="javascript:void(0);" onclick="showItemOption('.$item['item_id'].'); void(0); return false;"><span class="fd_transition"><i class="flaticon-online-shopping-cart"></i></span></a>';
								  }else{  
									$item_price = key_text('portalcurreny_symbol').$item['item_price'];
									$addToCart_btn ='<a href="javascript:void(0);" onclick="addCart('.$item['item_id'].' , 1); void(0); return false;"><span class="fd_transition"><i class="flaticon-online-shopping-cart"></i></span></a>'; 
								  } ?> 
						
						
							<div class="col-md-4 col-sm-6 col-xs-12 element-item <?php echo $item['cat_slug'] ?>">
								<div class="fd_shop_box">
									<div class="fd_shop_img">
										<img src="<?php echo base_url('assets/admin/images/menus/thumb/' .$item['image_thumb']) ?>" alt="" class="img-fluid">
										<div class="fd_shop_detail fd_transition">
											<h2><?php echo $item['item_name']; ?></h2>
											<h3> <?php echo $item_price; ?></h3>
										</div>
									</div>
									<div class="fd_shop_overlay fd_transition">
									<?php echo $addToCart_btn; ?>
									<!--<a href="javascript:void(0);" onclick="addCart(<?php //echo  $item['item_id'] ?> , 1); void(0); return false;"><span class="fd_transition"><i class="flaticon-online-shopping-cart"></i></span></a>--> 
	
										<ul class="fd_rating fd_transition">
											<li><a href="javascript:;"><i <?php if(round(rating($reating,$item['item_id'])) > 0) { ?> class="fa fa-star" <?php } else { ?> class="fa fa-star-half-o"  <?php  }  ?>  aria-hidden="true"></i></a></li>
											<li><a href="javascript:;"><i <?php if(round(rating($reating,$item['item_id'])) >= 2) { ?> class="fa fa-star" <?php } else { ?> class="fa fa-star-half-o"  <?php  }  ?> aria-hidden="true"></i></a></li>
											<li><a href="javascript:;"><i <?php if(round(rating($reating,$item['item_id'])) >=3) { ?> class="fa fa-star" <?php } else { ?> class="fa fa-star-half-o"  <?php  }  ?> aria-hidden="true"></i></a></li>
											<li><a href="javascript:;"><i <?php if(round(rating($reating,$item['item_id'])) >= 4) { ?> class="fa fa-star" <?php } else { ?> class="fa fa-star-half-o"  <?php  }  ?> aria-hidden="true"></i></a></li>
											<li><a href="javascript:;"><i <?php if(round(rating($reating,$item['item_id'])) >= 5) { ?> class="fa fa-star" <?php } else { ?> class="fa fa-star-half-o"  <?php  }  ?> aria-hidden="true"></i></a></li>
										</ul>
										<h2 class="fd_transition"><?php echo $item['item_name']; ?></h2>
																				
										<h3 class="fd_transition"><?php echo $item_price; ?></h3>
									</div>
								</div>
							</div>
							
							<?php } ?>
						</div>
						
						<div class="text-center">
							<span id="changeMore">
							<a id="isoitem" href="javascript:void(0);" class="fd_down_button fd_viewmore_wrapper fd_transition ">
								<span><span><span class="fd_viewmore_btn">Ver más</span></span></span>
							</a>
							</span> 
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--product end--> 
	<?php } ?>
	
	
	<?php if($menus[2]['status'] == 1 || !empty($times) ) { ?>
	<!--reservation start--> 
	<div class="fd_section fd_section_overlay fd_padder_top80" id="<?php echo $menus[2]['field'] ?>" style="background-image:url('<?php echo base_url() ?>assets/front/images/default_image/reservation_bg.jpg')">
		<div class="fd_shap_wrapper">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="fd_heading_wrapper text-center">
							<img src="<?php echo base_url(); ?>assets/front/images/default_image/cup_white.svg" alt="">
							<h1 class="fd_white_color"><?php if($menus[2]['status'] == 1){ echo $menus[2]['sub_heading']; }else{ echo 'Times'; } ?></h1>
						</div>
					</div>
                     <?php if(!empty($times)){ ?>
					<div class="col-md-<?php if($menus[2]['status'] == 1){ echo 4; }else{ echo 4; } ?>">
						<div class="fd_opening_time_wrapper text-center">
							<div class="fd_heading">
								<div class="fd_shap_wrapper">
									<h1><?php echo get_title('opening_time',$infos) ?></h1>
								</div>
							</div>
							<div class="fd_opentime">
								<img src="<?php echo base_url(); ?>assets/front/images/default_image/cup1.svg" alt="">
								<ul>
								<?php foreach($times as $time){ ?>
									<li><span><?php echo $time['title'] ?></span><span><?php
									$time_arr = json_decode($time['description'],true);

									echo $time_arr['open_time'].' - '.$time_arr['close_time'] ; ?></span></li>
								<?php } ?>	
								</ul>
							</div>
							<div class="fd_forcall_section">
								<h2 class="fd_subheading"><?php echo get_title('for_call_booking',$infos) ?></h2>
								<h1><?php echo get_description('contact',$infos) ?></h1>
							</div>
						</div>
					</div>
					<?php } ?>
					<?php if($menus[2]['status'] == 1){ ?>
					<div class="col-md-<?php if(empty($times)){ echo 12; }else{ echo 8; } ?>">
						<div class="fd_reservation_wrapper">
							<div class="fd_heading_wrapper text-center">
								<h1 class="fd_white_color"><?php echo get_title('booking_now',$infos) ?></h1>
							</div>
							<form action="" method="POST" id="bookingform">
							<div class="row">	
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" id="booking_date" value="" placeholder="Fecha" class="form-control fd_date" autocomplete="off">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" id="booking_time" value="" placeholder="Hora" class="form-control fd_time"  autocomplete="off">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" id="bname" value="<?php echo  (!empty($user_info))? $user_info['user_name'] : '' ?>" placeholder="Nombre" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" id="bmobile" value="<?php echo  (!empty($user_info))? $user_info['user_mobile'] : '' ?>" placeholder="Celular" class="form-control">
									</div>
								</div>
								<!--<div class="col-md-12">
									<div class="form-group">
										<input type="text" id="bemail" value="<?php //echo  (!empty($user_info))? $user_info['user_email'] : '' ?>" placeholder="Your Email" class="form-control">
									</div>
								</div>-->
								
								<div class="col-md-12">
									<textarea id="bmessage" placeholder="Message" class="form-control"></textarea>
								</div>
								<div class="col-md-12">
									<button type="submit" class="fd_btn">Reservar ahora</button>
								</div>
							</div>
							</form>
							
						</div>
					</div>
					<?php } ?>
					
					
					<?php //if(!empty($times)){ ?>
					<!--<div class="col-md-<?php if($menus[2]['status'] == 1){ echo 4; }else{ echo 4; } ?>">
						<div class="fd_opening_time_wrapper text-center">
							<div class="fd_heading">
								<div class="fd_shap_wrapper">
									<h1><?php echo get_title('delivery_time',$infos) ?></h1>
								</div>
							</div>
							<div class="fd_opentime">
								<img src="<?php echo base_url(); ?>assets/front/images/default_image/cup1.svg" alt="">
							<!--This Is a Static Delivery Time. You Can Change Day Name And Time From Here -->
								<!--<ul>
									<li><span >Monday</span><span>11:15 AM 7:20 PM</span></li>
									<li><span>Tuesday</span><span>7:00 PM 7:15 PM</span></li>
									<li><span>Wednesday</span><span>6:00 PM 6:30 AM</span></li>
									<li><span>Thursday</span><span>9:00 AM 5:15 PM</span></li>
									<li><span>Friday</span><span>10:00 AM 6:30 PM</span></li>
									<li><span>Saturday</span><span>6:00 PM 6:30 AM</span></li>
									<li><span>Sunday</span><span>6:00 AM 6:00 PM</span></li>
								</ul>
							<!--This Is a Static Delivery Time. You Can Change Day Name And Time From Here -->
							<!--</div>
							<div class="fd_forcall_section">
								<h2 class="fd_subheading"><?php //echo get_title('for_call_booking',$infos) ?></h2>
								<h1><?php //echo get_description('contact',$infos) ?></h1>
							</div>
						</div>
					</div>-->
					<?php //} ?>
					
				</div>
			</div>
		</div>
	</div>		
	<!--reservation end--> 
	 <?php } ?>
	<?php if(!empty($discount_coupon)){ ?>
	<!--offer start-->
	<div class="fd_section fd_padder_top60 fd_padder_bottom100 fd_offer_main_wrapper">
		<div class="fd_shap_wrapper fd_shap_top">	
			<div class="container">
				<div class="row">	
					<div class="col-md-7">
						<div class="fd_offer_detail">
							<h1><?php echo $discount_coupon[0]['offer_heading']; ?>      </h1>
							<h2><?php echo $discount_coupon[0]['offer_title']; ?>       </h2>
							 <p><?php echo $discount_coupon[0]['offer_description']; ?>.</p>
						</div>
					</div>
					<div class="col-md-5">
						<div class="fd_offer_img">
							<img src="<?php echo base_url().$discount_coupon[0]['offer_image']; ?>" alt="" class="img-fluid">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--offer end--> 
	<?php } ?>
	
	
	<?php if($menus[3]['status'] == 1){ ?>
	<!--blog start--> 
	<div class="fd_section fd_section_overlay fd_padder_top80" id="<?php echo $menus[3]['field'] ?>" style="background-image:url(assets/front/images/default_image/blog_bg.jpg')">	
		<div class="fd_shap_wrapper fd_padder_bottom100">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="fd_heading_wrapper text-center">
							<img src="<?php echo base_url(); ?>assets/front/images/default_image/cup_white.svg" alt="">
							<h1 class="fd_white_color"><?php echo $menus[3]['sub_heading'] ?></h1>
						</div>
					</div>
					
					<div class="col-md-12 fd_padder_bottom30">
						<div class="fd_blog_slider">
							<div class="swiper-container">
								<div class="swiper-wrapper">
									<?php foreach($blogs as $blog){ 
									
									?> 
									 <div class="swiper-slide">
										<div class="fd_blog_box">
											<div class="fd_blog_img">
												<a class="blog_popup_open" onclick="singleBlock(<?php echo $blog['id'] ?>); void(0); return false;"><img src="<?php echo base_url(); ?>assets/admin/images/blog/thumb/<?php echo $blog['blog_img'] ?>" alt="" class="img-fluid"></a>
											</div>
											<div class="fd_blog_detail">
												<div class="fd_blog_meta fd_padder_bottom20">
													<ul>
														<li><span><i class="flaticon-calendar"></i>
														<?php
														 $time=strtotime($blog['created_at']);
														 echo $month=substr(date("F",$time),0,3);
														 echo $date=date("d",$time).',';
														 echo $year=date("y",$time);
														 
														?>
														</span></li>
														 <li><a><i class="flaticon-thumbs-up-hand-symbol"></i><?php echo $blog['like_count'] ?></a></li>
														<li><a><i class="flaticon-speech-bubbles-comment-option"></i><?php echo $blog['comment'] ?></a></li>
					 								</ul>
													<div class="fd_share_wrapper">
														<span class="flaticon-share"></span>
														
														<ul class="fd_social_icon fd_transition">
															 <li><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url(); ?>front/oneblog/<?php echo $blog['slug'] ?>&title=&summary=&source="   class="linkedin fd_transition"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
															<li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url(); ?>front/oneblog/<?php echo $blog['slug'] ?>" class="facebook fd_transition"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
															<li><a href="https://twitter.com/home?status=<?php echo base_url(); ?>front/oneblog/<?php echo $blog['slug'] ?>" class="twitter fd_transition"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
														</ul>
													</div>
												</div>
												<h2 class="fd_subheading"><a href class="blog_popup_open" onclick="singleBlock(<?php echo $blog['id'] ?>); void(0); return false;"><?php echo $blog['title'] ?></a></h2>
												<?php 
													$contents = unserialize($blog['content']);
													for($i=0; $i < 1 ; $i++){
												?>	
													<p><?php echo substr ($contents[$i] ,0,80) ?></p>
												<?php }?>
												<?php if(set_blog('open_blog') == 1){ ?>
													<a class="fd_read_more blog_popup_open" onclick="singleBlock(<?php echo $blog['id'] ?>); void(0); return false;">read more</a>
												<?php }else{ ?>
													<a class="fd_read_more" href="<?php echo base_url('front/oneblog/'.$blog['slug']) ?>" >read more</a>
												<?php } ?>
											</div>
										</div>
									</div>
									<?php } ?>
									
								</div>
								<!-- Add Pagination -->
								<div class="swiper-pagination"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--blog end--> 
	
	<?php } ?>
	
	<?php if($menus[4]['status'] == 1){ ?>
	<!--testimonial start--> 
	<div class="fd_section fd_padder_bottom80 fd_testimonial_main_wrapper" id="<?php echo $menus[4]['field'] ?>">
		<div class="fd_shap_wrapper fd_shap_top">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="fd_heading_wrapper text-center">
							<img src="assets/front/images/default_image/cup.svg" alt="">
							<h1><?php echo $menus[4]['sub_heading'] ?></h1>
						</div>
						
						<div class="fd_testimonial_wrapper">
							<div class="swiper-container">
								<div class="swiper-wrapper">
									
									<?php foreach($testimonials as $testimonial){ ?>
										<div class="swiper-slide">
											<div class="fd_testimonial_detail text-center">
												<div class="fd_test_img fd_padder_bottom20">
													<img src="<?php echo base_url(); ?>assets/admin/images/testimonial/thumb/<?php echo $testimonial['image'] ?>" alt="">
												</div>
												<p><?php echo $testimonial['description'] ?>.</p>
												<h2 class="fd_subheading"><?php echo $testimonial['title'] ?> - <span><?php echo $testimonial['profile'] ?></span></h2>
												<span class="flaticon-left-quote"></span>
											</div>
										</div>
									<?php } ?>	
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--testimonial end-->
	<?php } ?>

	<?php if($menus[5]['status'] == 1){ ?>	
	<!--contact start--> 
	<div class="fd_section fd_section_overlay fd_padder_top80" id="<?php echo $menus[5]['field'] ?>" style="background-image:url('assets/front/images/default_image/blog_bg.jpg')">
		<div class="container">
			<div class="row fd_padder_top100">
				<div class="col-md-7">
					<div class="fd_contact_wrapper fd_reservation_wrapper">
						<div class="fd_heading_wrapper text-center">
							<h1 class="fd_white_color"><?php echo $menus[5]['sub_heading'] ?></h1>
						</div>
						<form action="" type="post" id="contactform">
						<div class="row">	
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" name="name" id="cname"  value="<?php echo  (!empty($user_info))? $user_info['user_name'] : '' ?>" placeholder="Tu Nombre" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" name="email" id="cemail" value="<?php echo  (!empty($user_info))? $user_info['user_email'] : '' ?>" placeholder="Tu Email" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" name="subject" id="csubject" value="" placeholder="Asunto class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" name="contact" id="ccontact" value="<?php echo  (!empty($user_info))? $user_info['user_mobile'] : '' ?>" placeholder="Tu Celular nro. " class="form-control">
								</div>
							</div>
							<div class="col-md-12">
								<textarea name="description" id="cdescription" placeholder="Mensaje" class="form-control"></textarea>
							</div>
							<div class="col-md-12">
								<button type="submit" class="fd_btn">Enviar Mensaje</button>
							</div>
						</div>
						</form>
					</div>
				</div>
				<div class="col-md-5">
					<div class="fd_contact_img">
						<img src="<?php echo base_url().$img['img_contact_us']; ?>" alt="" class="img-fluid">
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--contact end--> 
	
	<?php } ?>
	<!--contact detail start--> 
	<div class="fd_contact_detail_wrapper fd_section fd_padder_top80 fd_padder_bottom50">
		<div class="container">
			<div class="row">
				<div class="fd_contact_detail_section">
					<div class="col-md-4 col-sm-12">
						<div class="fd_contact_detail_box text-center">
							<span class="fd_transition"><i class="flaticon-telephone"></i></span>
							<h2 class="fd_subheading"><?php echo get_title('contact',$infos) ?> </h2>
							<p><?php echo get_description('contact',$infos) ?></p>
						</div>
					</div>
					<div class="col-md-4 col-sm-12">
						<div class="fd_contact_detail_box text-center">
							<span class="fd_transition"><i class="flaticon-close-envelope"></i></span>
							<h2 class="fd_subheading"><?php echo get_title('email',$infos) ?> </h2>
							<p><?php echo get_description('email',$infos) ?> </p>
						</div>
					</div>
					<div class="col-md-4 col-sm-12">
						<div class="fd_contact_detail_box text-center">
							<span class="fd_transition"><i class="flaticon-maps-and-flags"></i></span>
							<h2 class="fd_subheading"><?php echo get_title('location',$infos) ?> </h2>
							<p><?php echo get_description('location',$infos) ?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!--contact detail end--> 
	<div class="fd_blogsigle_popup fd_transition">
		<span class="fd_close"></span>
		<div class="pf_popup_inner">
	

	<!--banner start--> 
		<div class="fd_banner_wpapper" style="background-image:url('<?php echo base_url() ?>assets/front/images/default_image/banner_img.jpg');">
			<div class="fd_shap_wrapper">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="fd_breadcrum_wrappper text-center fd_padder_top50 fd_padder_bottom100">
								<h1 class="fd_padder_bottom20"><?php echo get_title('blog_single',$infos) ?></h1>
								 
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--banner end--> 
		
		<!--blog single start--> 
		<div class="fd_section fd_blog_single_wrapper fd_padder_bottom80">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-sm-12" >
						<div class="row" id="blogContent">
						
						</div>
					</div>
					<div class="col-md-4 col-sm-12">
						<div class="fd_sidebar_wrapper">
							<div class="widget widget_search">
								<h1 class="fd_heading_with_line fd_subheading">search</h1>
								
								<div class="fd_search_wrapper">
									<div class="fd_search">
										<input type="text" id="searchBlog" value="" class="form-control" placeholder="Search Here">
										<a href="#" class="fd_search_btn"><i class="fa fa-search" aria-hidden="true"></i></a>
									</div>
									<ul class='list-group' id="searchResult"> </ul>
								</div>
							</div>
							
							<div class="widget widget_categories">
								<h1 class="fd_heading_with_line fd_subheading">categorias</h1>
								<ul>
								<?php foreach($blog_cat as $cat){ ?>	
									<li><a href="#" onclick="getBlogOfCate(<?php echo $cat['id'] ?>); void(0); return false;" ><span><?php echo $cat['title'] ?></span><span>(<?php echo $cat['total'] ?>)</span></a></li>
								<?php } ?>	
								</ul>
							</div>
							
							<div class="widget widget_post">
								<h1 class="fd_heading_with_line fd_subheading">Mensajes recientes</h1>
								
								<ul>
								<?php foreach($blogs as $blog){ ?>
									<li><div class="fd_rec_post">
											<a href="#" onclick="singleBlock(<?php echo $blog['id'] ?>); void(0); return false;">
											<img width="60px" height="60px" src="<?php echo base_url(); ?>assets/admin/images/blog/<?php echo $blog['author_img'] ?>" alt=""></a>
											<span>
												<?php  	$contents = unserialize($blog['content']);
														for($i=0; $i < 1 ; $i++){ 			?>
													
												<p>	<?php echo substr ($contents[$i] ,0,45) ?>....	</p>
												<?php } ?>
												<p><a href="#" onclick="singleBlock(<?php echo $blog['id'] ?>); void(0); return false;">
												<?php 	$time=strtotime($blog['created_at']);
														 echo $date=date("d",$time).' ';
														 echo $month=date("F",$time) .' ';
														 echo $year=date("Y",$time);
												?>
												
												</a></p>
											</span>
										</div>
									</li>
								<?php } ?>	
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div> 
		</div>
		<!--blog single end--> 
		
		</div>
	</div>	
	
	<!--checkout start-->
	<div class="text-right">
		<div class="fd_cart_section text-left">
			<span class="fd_ring_effect"><span><span class="flaticon-shopping-bag"></span><span class="fd_shop_number" id="carttotal">0</span></span></span>
			
			<div class="fd_cart_wrapper fd_transition">
				<h2 class="fd_subheading">checkout steps</h2>
				<!-- To Change Breakfast Lunch Dinner Time -->
				<h2 class="oth_heading">Desayuno</h2>
				<h2 class="oth_heading">Almuerzo</h2>
				<h2 class="oth_heading">Cena</h2>
				<!-- And -->
				
				<h2 class="fd_subheading"></h2>
				<div id="msform" >
					<!-- progressbar -->
					<ul id="progressbar">
						<li class="active"><a>Carrito Compra</a></li>
						<li><a>Envío</a><span class="fd_line fd_transition"></span></li>
						<li><a>Pago</a><span class="fd_line fd_transition"></span></li>
						<li><a>Confirmación</a><span class="fd_line fd_transition"></span></li>
					</ul>
					<!-- fieldsets -->    
					<fieldset>
						<div class="row text-left ">
						<div class="table-responsive">
								<table class="table table-hover table-condensed" id="cart">
									<thead>
										<tr>
											<th>Producto</th>
											<th>Titulo</th>
											<th>Precio</th>
											<th>Tamaño</th>
											<th>Cantidad</th>
											<th>Subtotal</th>
											<th>Accion</th>
										</tr>
									</thead>
									<tbody>
										
									</tbody>
									<tfoot>
									<?php $carts = $this->cart->contents(); ?>
										<tr class="visible-xs">
											<td colspan="6"><strong>Total -     <?php echo key_text('portalcurreny_symbol') ?><b id="totalamount">0 </b></strong></td>
										</tr> 
										<!--<tr class="visible-xs">
											<td colspan="6"><strong>Coupon Discount -     <?php //echo key_text('portalcurreny_symbol') ?><b id="discount_amount"> 0</b></strong></td>
										</tr> 
										 <tr class="visible-xs">
											<td colspan="6"><strong>Final Amount -     <?php //echo key_text('portalcurreny_symbol') ?><b id="finalamount">0</b></strong></td>
										</tr> -->
									 
									</tfoot> 
								</table>
								</div>
							
							<div class="col-md-12 col-sm-12 col-xs-12">
							<?php if(empty($user_info)){ ?>
								<button class="fd_btn action-button" onclick="toastr.error('Login for continue...', '');" >continue </button>
							<?php }else{ ?>
								<button id="cartbutton" class="fd_btn next action-button">continue</button>
							<?php } ?>	
							</div>
						</div>
					</fieldset>
					<fieldset>
					<form id="Submit_info" method="POST">
						<div class="row text-left">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<h2 class="fd_subheading">Información de envío</h2>
							</div>
							
							<div class="col-md-12 col-sm-12 col-xs-12">
							     
								<input type="radio" name="delivery_type"  id="Delivery" value="1" checked>
										<label class="fd_netbanking" for="Delivery">
											<p class="gray text-capitalize">
											<b><a> Delivery</a></b></p>
										</label>
								<input type="radio" name="delivery_type"  id="Pickup" value="2">
										<label class="fd_netbanking" for="Pickup">
											<p class="gray text-capitalize"><b>
											<a>Pickup From Restaurant </a></b></p>
										</label>
							</div>
							<!--<div class="col-md-6">
								<div class="form-group">
									<input type="text" id="pickup_date" value="" placeholder="Pickup Date" class="form-control fd_date pickup_date_time requ" autocomplete="off" style="display:none;">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" id="pickup_time" name=" " placeholder="Time" class="form-control fd_time pickup_date_time requ" style="display:none;">
								</div>
							</div>-->
							
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="form-group">
									<input type="text" id="SDname"  placeholder="Full Name" class="form-control" value="<?php echo  (!empty($user_info))? $user_info['user_name'] : '' ?>" >
								</div>
							</div>
							
							<div class="col-md-6 col-sm-12 col-xs-12">
								<div class="form-group">
									<input type="text" id="SDemail"  placeholder="Email" class="form-control requ" value="<?php echo  (!empty($user_info))? $user_info['user_email'] : '' ?>">
								</div>
							</div>
							<?php
							$info=explode('|',$user_info['user_address'])
							?>
							<div class="col-md-6 col-sm-12 col-xs-12">
								<div class="form-group">
									<input type="text" id="SDcontact" value="<?php echo  (!empty($user_info))? $user_info['user_mobile'] : '' ?>" placeholder="contact" class="form-control requ" value="<?php echo  (!empty($user_info))? $user_info['user_mobile'] : '' ?>">
								</div>
							</div>
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="form-group">
									<input type="text" id="SDaddress" value="<?php echo  (isset($info[0]))? $info[0] : '' ?>" placeholder="Address" class="form-control requ">
								</div>
							</div>
							<div class="col-md-6 col-sm-12 col-xs-12">
								<div class="form-group">
									<input type="text" id="SDcountry" placeholder="Country"  value="<?php echo  (isset($info[1]))? $info[1] : '' ?>" class="form-control requ">
								</div>
							</div>
							<div class="col-md-6 col-sm-12 col-xs-12">
								<div class="form-group">
									<input type="text" id="SDstate" placeholder="State"  value="<?php echo  (isset($info[3]))? $info[3] : '' ?>" class="form-control requ">
								</div>
							</div>
							<div class="col-md-6 col-sm-12 col-xs-12">
								<div class="form-group">
									<input type="text" id="SDcity" placeholder="City"  value="<?php echo  (isset($info[2]))? $info[2] : '' ?>" class="form-control requ">
								</div>
							</div>
							<div class="col-md-6 col-sm-12 col-xs-12">
								<div class="form-group">
									<input type="text" id="SDpincode" placeholder="Pincode"  value="<?php echo  (isset($info[4]))? $info[4] : '' ?>" class="form-control requ">
								</div>
							</div>
							<div class="col-md-12 col-sm-12 col-xs-12">
								<button class="fd_btn previous action-button">previous</button>
								<button type="submit" id="Sub_info"  class="fd_btn action-button ">continue</button>
							</div>
						 </div>
					</form>	
					</fieldset>
					<fieldset>
					
					<form id="Submit_payment_info" method="POST">
					
						<div class="row text-left">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<h2 class="fd_subheading">payment option</h2>
								<ul class="fd_payment_wrapper">
									
										<div class="fd_netbanking">
											<p class="gray text-capitalize">Your Current Wallet Amount - <b><?php echo key_text('portalcurreny_symbol') ?> <?php if(isset($wallet)){echo $wallet['wallet_amount'] ;}else{ echo 0;} ?></b> </p>
											<p class="gray text-capitalize">Your Current Order Amount - <b><?php echo key_text('portalcurreny_symbol') ?> </b><b id="orderamount"></b> </p>
											<p class="gray text-capitalize">Discount Amount - <b><?php echo key_text('portalcurreny_symbol') ?> </b><b id="discountamount">0</b> </p>
											<?php if(isset($_SESSION['Pcode'])){ ?>
											<p> <a href="javascrip:;"  id="coupon_btn" onclick="remove_coupon();" style="float:right;">Remove Coupon <i class="fa fa-times"></i></a></p>
											
											<?php }else{ ?>
											<p> <a href="javascrip:;"  id="coupon_btn" onclick="apply_coupon_modal();" style="float:right;">Apply Coupon</a></p> 
											<?php } ?>
											<p class="gray text-capitalize">Your Final Order Amount - <b><?php echo key_text('portalcurreny_symbol') ?> </b><b id="finalamount"></b> </p>
										</div>
									<?php if(key_text('payu_status') == 1){ ?>
									<li>
										<input type="radio" name="pay_type" id="fd_payu" value="Payumoney">
										<label class="fd_netbanking" for="fd_payu">
											<p class="gray text-capitalize">
											<a>Payumoney</a></p>
										</label>
									</li>
									<?php } ?>
									<?php if(key_text('paypal_status') == 1){ ?>
									<li>
										<input type="radio" name="pay_type"  id="fd_paypal" value="Paypal">
										<label class="fd_netbanking" for="fd_paypal">
											<p class="gray text-capitalize">
											<a>Paypal</a></p>
										</label>
									</li>
									<?php } ?>
									<?php if(key_text('cod_status') == 1){ ?>
									<li id="cod_optlist">
										<input type="radio" name="pay_type" id="fd_cod" value="COD">
										<label class="fd_netbanking" for="fd_cod">
											<p class="gray text-capitalize">
											<a>Cash On Delivery</a></p>
										</label>
									</li>
									<?php } ?>
									
									
									<?php if(isset($wallet)){ ?>
									<?php if(key_text('wallet_status') == 1){ ?>
									<li>
										<input type="radio" name="pay_type" id="fd_wallet" value="Wallet">
										<label class="fd_netbanking" for="fd_wallet">
										<div class="fd_netbanking">
											<p class="gray text-capitalize">
											Wallets</a></p>
										</label>	
										</div>
									</li>	
									<?php } ?>
									<?php } ?>
									<li>
									<p class="gray text-capitalize">Instrucciones Especiales :- </p>
									 <textarea rows="4" cols="50" name="special_instructions"></textarea>
									</li>
								</ul>
								
								<button class="fd_btn previous action-button">previous</button>
								<button id="pymchse" type="submit" class="fd_btn action-button">Continuar</button>
							</div>
						</div>
					</form>
					</fieldset>
					<fieldset>
					
						<div class="row text-left"  id="confirmation">
						</div>
						<div class="row text-left">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<button class="fd_btn previous action-button">Previo</button>
								<button class="fd_btn submit action-button" id="submitpayment">Pagar</button>
							</div>	
						 </div>
					
					</fieldset>
				</div>
			</div>
		</div>
	</div>
	<!--checkout end-->