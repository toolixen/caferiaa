<div class="fd_terms_wrapper">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="fd_terms_box">
					<div class="fd_terms_head">
					<h4 class="fd_terms_title">Terms & Conditions</h4>
						<?php 
							echo "<div class='fd_date_update'><p><span>Last Update: </span>".$general['terms_condition_update_date']."</p></div>"; 
						?>
					</div>
					<?php 
					echo $general['terms_condition']; 
					?>
					</div>
			</div>
		</div>
	</div>
</div>