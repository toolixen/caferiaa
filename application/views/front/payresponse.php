<?php if($type == 'success'){ ?>
<div class="fd_sucess_wrapper fd_padder_top100 fd_padder_bottom100">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-10 col-sm-10 col-xs-12">	
				<div class="fd_sucess_detail text-center">
						<img src="https://lh3.googleusercontent.com/PNbgu5ZGtNyKj0c1fUTCuZH3lMeb0UsaDgjPft0tS5lKbBoh5zfyzm_FnqfImFbsWQ=w150" alt="" class="img-fluid">
						
						<h1>El pedido de su producto(s) fue exitoso!!!</h1>
						<h4><?php if(!empty($orderid)){ ?>
                                Tu número de orden es <b><?= $orderid ?></b>!!!
						<?php } ?>
						</h4>
						<p><?= $message ?></p><a href="<?php echo base_url(); ?>" class="fd_btn">Inicio</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<?php if($type == 'failed'){ ?>
<div class="fd_sucess_wrapper fd_padder_top100 fd_padder_bottom100">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-10 col-sm-10 col-xs-12">	
				<div class="fd_sucess_detail text-center">
						<img src="<?= base_url('assets/front/images/default_image/cancel.png'); ?>" width="150px" height="150px" alt="" class="img-fluid">
						
						<h1>Su pedido de productos Falló !!!</h1>
						
						<p><?= $message?></p>						<a href="<?php echo base_url(); ?>" class="fd_btn">Inicio</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>

<?php if($type == 'reat_success'){ ?>
<div class="fd_sucess_wrapper fd_padder_top100 fd_padder_bottom100">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-10 col-sm-10 col-xs-12">	
				<div class="fd_sucess_detail text-center">
						<img src="https://lh3.googleusercontent.com/PNbgu5ZGtNyKj0c1fUTCuZH3lMeb0UsaDgjPft0tS5lKbBoh5zfyzm_FnqfImFbsWQ=w150" width="150px" height="150px" alt="" class="img-fluid">
						
						<h1>Gracias por calificar nuestro producto ...</h1>
						
						<p><?= $message?></p>						<a href="<?php echo base_url(); ?>" class="fd_btn">Inicio</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>