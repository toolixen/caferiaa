
						<div class="fd_banner_content text-center">
							<img src="<?php echo base_url().$img['img_banner_up']; ?>" alt="" class="img-fluid">
							<div class="fd_styletext_box">
								<h1 class="fd_style_text"><?= get_description('center_head_line',$infos ) ?></h1>
							</div>
							<h1><?= get_description('sub_center_head_line',$infos ) ?></h1>
							<?php if(get_description('front_center_button',$infos ) !=''){ ?>
							<a href="javascript:void(0);" class="fd_btn" data-target=".fd_banner_wpapper"><?= get_description('front_center_button',$infos ) ?></a>
							<?php } ?>
						</div>
						<div class="text-center">
							<a href="javascript:void(0);" class="fd_down_button" data-target=".fd_banner_wpapper">
								<span class="fd_ring_effect"><span><span><span class="fd_icon"></span></span></span></span>
							</a>
						</div>