	<!--footer start-->
	<div class="fd_footer_wrapper fd_padder_top70">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-7">
					<div class="fd_footer_content text-center">
						<div class="fd_logo fd_padder_bottom20">
							<a href="javascript:;"><img src="<?= base_url($img['img_logo']); ?>" alt="" class="img-fluid"></a>
						</div>
						<p><?= get_description('footer_content',$infos) ?></p>
						
						<div class="fd_menu fd_padder_bottom60">
							<ul>
							 <li><a href="<?php echo base_url('front/terms'); ?>">Términos y condiciones</a></li>
							 <li><a href="<?php echo base_url('front/policy'); ?>">Política de privacidad</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--footer end--> 
	<!--copyright start--> 
	<div class="fd_copyright_wrapper text-center">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
				
					<ul class="fd_social_icon fd_transition">
						<?php if(get_description('facebook_url',$infos) !=''){ ?>
						<li><a href="<?php get_description('facebook_url',$infos) ?>" class="fd_transition"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<?php }  if(get_description('twitter_url',$infos) !=''){ ?>
						<li><a href="<?php get_description('twitter_url',$infos) ?>" class="fd_transition"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<?php }  if(get_description('linkedin_url',$infos) !=''){ ?>
						<li><a href="<?php get_description('linkedin_url',$infos) ?>" class="fd_transition"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
						<?php }  if(get_description('youtube_url',$infos) !=''){ ?>
						<li><a href="<?php get_description('youtube_url',$infos) ?>" class="fd_transition"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
						<?php }  ?>
					</ul>
					
					<!--p>Copyright &copy; 2018 <a href="#">Foodies 2</a> . All Right Reserved.</p-->
					<p><?= get_description('copyright',$infos) ?> </p>
					
				</div>
			</div>
		</div>
	</div>
	<!--copyright end--> 

	<!-- reset password start-->
	<div class="modal fd_modal" id="resetp">
		<div class="modal-dialog">
			  <div class="modal-content">
			  
				<!-- Modal Header -->
				<div class="modal-header">
				  <h4 class="modal-title fd_subheading">Restablecer la contraseña</h4>
				  <button type="button" class="close" data-dismiss="modal"><span class="fd_close"></span></button>
				</div>
				
				<!-- Modal body -->
				<div class="modal-body">
				
				<div class="fd_user_inner fd_signin">
				  <form id="resetform" action="">
					
					<div class="form-group">
						<input type="password" id="rpassword" placeholder="Password" class="form-control">
					</div>
					<div class="form-group">
						<input type="password" id="rcpassword"  placeholder="Confirm Password" class="form-control">
					</div>
					<input type="hidden" id="acctoken" value="<?php if(isset($_GET['at'])){echo $_GET['at'];} ?>">
					<input type="hidden" id="user_id" value="<?php if(isset($_GET['ui'])){echo $_GET['ui'];} ?>">
					
					<button type="submit" class="fd_btn">Reiniciar</button>
				</form>
				</div>
				</div>
				
			  </div>
		</div>
   </div>
 <!-- reset password end-->
<!-- apply coupon start-->
	<div class="modal fd_modal" id="apply_coupon">
		<div class="modal-dialog">
			  <div class="modal-content">
			  
				<!-- Modal Header -->
				<div class="modal-header">
				  <h4 class="modal-title fd_subheading">Aplicar cupón</h4>
				  <button type="button" class="close" data-dismiss="modal"><span class="fd_close"></span></button>
				</div>
				
				<!-- Modal body -->
				<div class="modal-body">
				
				<div class="fd_user_inner fd_signin">
				  <form id="Apply_coupon_code" action="">
					
					<div class="form-group">
						<input type="text" id="coupon_code" name="coupon_code" placeholder="Coupon Code" class="form-control">
					</div>
					<input type="hidden" id="citem_ids" name="citem_id" value="">
					<button type="submit" class="fd_btn" id="apply_btn" >Aplicar</button>
				</form>
				</div>
				</div>
				
			  </div>
		</div>
   </div>
 <!-- apply coupon end-->	
 <!-- preorder popup start-->
	<div class="modal fd_modal" id="pre_order_popup">
		<div class="modal-dialog">
			  <div class="modal-content">
			  
				<!-- Modal Header -->
				<div class="modal-header">
				  <h4 class="modal-title fd_subheading">Pre Orden</h4>
				  <button type="button" class="close" data-dismiss="modal"><span class="fd_close"></span></button>
				</div>
				
				<!-- Modal body -->
				<div class="modal-body">
				
				<div class="fd_user_inner fd_signin">
				  <form id="pre_order_form" action="">
					<p>En este movimiento, la Tienda está cerrada, haga su Pre-Orden ahora</p>
					<p>Por favor seleccione su fecha y hora de entrega</p>
					<div class="form-group">
						<input type="text" id="pre_order_date" name="pre_order_date" placeholder="Date" class="form-control fd_date">
					</div>
					<div class="form-group">
						<input type="text" id="pre_order_time" name="pre_order_time" placeholder="Time" class="form-control fd_time">
					</div>
					 
					<button type="submit" class="fd_btn" id="pre_order_btn" >Registrar</button>
				</form>
				</div>
				</div>
				
			  </div>
		</div>
   </div>
 <!-- preorder popup end-->	
 
  <!-- item option popup start-->
	<div class="modal fd_modal" id="item_option_modal">
		<div class="modal-dialog">
			  <div class="modal-content">
			  
				<!-- Modal Header -->
				<div class="modal-header">
				  <h4 class="modal-title fd_subheading">Elegir tamaño del artículo</h4>
				  <button type="button" class="close" data-dismiss="modal"><span class="fd_close"></span></button>
				</div>
				
				<!-- Modal body -->
				<div class="modal-body">
				
				<div class="fd_user_inner fd_signin">
				  <form id="item_option_form" action="">
					<p>Por favor, seleccione el tamaño del artículo</p>
					<div class="form-group" id="item_optlist">
						
					</div>
					<!--<button type="submit" class="fd_btn" id="addToCartBtn" >Submit</button>-->
				</form>
				</div>
				</div>
				
			  </div>
		</div>
   </div>
 <!-- item option  popup end-->	
 
 
	
	<!-- custom js link -->
	<script src="<?php echo base_url(); ?>assets/front/js/jquery.js"></script>
	<script src="<?php echo base_url(); ?>assets/front/js/jquery.easing.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/front/js/moment.js"></script>
	<script src="<?php echo base_url(); ?>assets/front/js/plugin/swiper/js/swiper.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/front/js/isotope.pkgd.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/front/js/imagesloaded.pkgd.js"></script>
	<script src="<?php echo base_url(); ?>assets/front/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/front/js/plugin/datepicker/bootstrap-datepicker.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/front/js/plugin/timepicker/bootstrap-timepicker.js"></script>
	<script src="<?php echo base_url(); ?>assets/front/js/plugin/scroll/jquery.mCustomScrollbar.js"></script>
	<script src="<?php echo base_url(); ?>assets/front/js/toastr.js"></script>
	<script src="<?php echo base_url(); ?>assets/front/js/custom.js?a="<?php echo date('his') ?>></script>
	<?php  if(isset($_GET['reset'])){ ?>
	<script>
		$('#resetp').modal('show');
	</script>
    <?php } ?>	
	
	<?php  if(!empty($this->session->flashdata('item'))){ 
	
		if($this->session->flashdata('item') == 'success'){
			?>
			<script>	
				toastr.success('Ha verificado su cuenta con éxito', '');
			</script>	
			<?php 
		}else{
			?>
			<script>
				toastr.error('Lo sentimos, la Varificación de correo falló', '');
			</script>	
			<?php 
		}
	?>
		
    <?php } ?>	
	</body>
</html>