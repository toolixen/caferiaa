<?php $meta_info=$this->DatabaseModel->access_database('rt_genral_setting','select','',array('id'=>'1'));
 ?> 
<!DOCTYPE html>
<html>
	<head>
		<title><?= $meta_info[0]['site_title'] ?></title>
		<meta charset="UTF-8">
		<meta name="description" content="<?= $meta_info[0]['site_description'] ?>">
		<meta name="keywords" content="<?= $meta_info[0]['site_keyword'] ?>">
		<meta name="author" content="<?= $meta_info[0]['author_name'] ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<script>	
			 var BASE_URL = '<?= base_url(); ?>';
			 
		</script>	
		<link rel="icon" type="image/ico" href="<?php echo base_url($img['fevicon']); ?>" />
		<!-- custom css links-->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/js/plugin/swiper/css/swiper.min.css" type="text/css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/bootstrap.min.css" type="text/css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/font-awesome.min.css" type="text/css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/fonts.css" type="text/css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/js/plugin/timepicker/timepicker.css" type="text/css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/js/plugin/datepicker/bootstrap-datepicker.min.css" type="text/css">
		
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/js/plugin/scroll/jquery.mCustomScrollbar.css" type="text/css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/flaticon.css" type="text/css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/style.min.css?a=<?= date('his'); ?>" type="text/css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/toastr.css" />
	
	</head>
	
	<body <?php  if($page == 'success'){echo 'class="fd_inner_page"'; } ?> 
	<?php if($this->session->userdata('is_login')){ echo 'class="fd_login"'; }?>  >
	
	<?php
	$obj = new Front_model();
	$f = '';	
	$f2='';
	$f1='';
	if($this->session->userdata('is_login')){
		$id = $this->session->userdata('id');
		if( $row = $this->Front_model->getData('rt_users',array('user_id'=> $id ))){
			//$f = strtoupper(substr($row['user_name'],0,1));
			$uname=explode(' ',$row['user_name']);
			if(isset($uname[0]))
			{
				$f1 = strtoupper(substr($uname[0],0,1));
			}
			if(isset($uname[1]))
			{
				$f2 = strtoupper(substr($uname[1],0,1));
			}
			$f=$f1.$f2;
		}
	}
	
	?>
	
	
	<?php  if(!isset($_COOKIE['accept_coockies'])){ ?>
	<form action="<?php echo base_url('front/accept_coockies'); ?>">
	<div class="fd_cookie_message">
		<div class="fd_container">
			<a href="javascript:;" onclick=" $('.fd_cookie_message').hide();" class="fd_close"></a>
			<div><h4 class="fd_subheading"><?php echo $general['cookie_title']; ?></h4>
			<p><?php echo $general['cookie_text']; ?> <a target="_blank" href="<?php echo $general['cookie_link']; ?>"><?php echo $general['cookie_link_title']; ?></a> </p></div>
			<button type="submit" class="fd_btn"><?php echo $general['cookie_button']; ?></button><p></p>	
		</div>
	</div>
	</form>
	<?php } ?>
	<div class="fd_header" id="header">
		<div class="fd_logo">
			<a href="<?= base_url()?>"><img src="<?= base_url($img['img_logo']) ?>" alt=""></a>
		</div>
		<div class="fd_right_section">
			<button data-toggle="collapse" data-target="#menu" class="fd_toggle"><i class="fa fa-bars" aria-hidden="true"></i></button>
			<?php
			$segment=$this->uri->segment(2);
			if($segment=="paypalfailture" || $segment=="payufailture" || $segment=='terms' || $segment=='policy' || $segment=='oneblog' || $segment=='paymentByCod' || $segment=='payusuccess' || $segment=='paypalsuccess' || $segment=='paymentByWallet')
			{ ?> 
			<a href="<?= base_url(); ?>" class="fd_home" ><i class="fa fa-home"></i></a>
			<?php }
			else
			{
				?>
			<div class="fd_menu collapse" id="menu"> 
				<ul>
					<?php $menus = $obj->getDataResult('rt_header_menu',array('status'=>1)); 
					 foreach($menus as $menu){ ?>
					 <li><a href="#<?= $menu['field'] ?>"><?= $menu['title'] ?></a></li>
					 <?php } ?>
				</ul>
				
			</div>
				<?php
			}
			?>
			
			<div class="fd_search">
				<a ><span class="flaticon-search"></span></a>
				<div class="fd_Seardh_box fd_transition">
					<div class="fd_search_field">
						<input type="text" name="search" onkeyup="filter_front()" id="fb_keyword1" value="" class="form-control" placeholder="Busca aquí.....">
						<button class="fd_btn">Buscar</button>
						
						<div class="fd_related_search" id="result">
							<ul id="data_disp">
							</ul>
						</div>
					</div>
					<span class="fd_close"></span>
				</div>
			</div>
			<div class="fd_user" id="fd_user">
				<?php 
					if($segment=="paypalfailture" || $segment=="payufailture" || $segment=='terms' || $segment=='policy' || $segment=='oneblog' || $segment=='paymentByCod' || $segment=='payusuccess' || $segment=='paypalsuccess' || $segment=='paymentByWallet')
					/* if($segment=='payufailture' || $segment=='paymentByCod' || $segment=='payusuccess' || $segment=='paypalsuccess') */
				{
				}
				else {
				if(!empty($f))
				{ 
				
					$order_info	= $this->Front_model->getDataResult('rt_paymentdetails',array('payment_uid'=>$this->user_info1['user_id']));
			  ?>
				<div class="fd_user_wrapper text-center fd_transition fd_user_profile">
					<span class="fd_user_close"><i class="fa fa-times"></i></span>
					<ul class="nav nav-tabs"> 
					   <li><a class="profile_user active" href="#user" >Ajustes de perfil</a></li>
					   <li><a class="order_history" href="#order_history">Historial de pedidos</a></li>
					</ul>
					<?php 
					$data=explode('|',$user_info['user_address']);
					?>
					<div class="tab-content">
					
						<div id="user" class="tab-pane in active">
							<div id="user" class="tab-pane in active">
							<div class="fd_user_inner fd_signin">
									<input type="text" placeholder="Nombre Completo" id="name" value="<?php echo  $user_info['user_name']; ?>" class="form-control">
									<input type="text" placeholder="Contacto Celular"  id="cno" value="<?php echo  $user_info['user_mobile']; ?>" class="form-control">
									<input type="text" placeholder="Ingresar Dirección" value="<?php if(isset($data[0])) { echo $data[0]; } ?>"  id="address" class="form-control">
									<input type="text" placeholder="Ingresar Ciudad" value="<?php if(isset($data[2])) { echo $data[2]; } ?>"  id="city" class="form-control">
									<input type="text" placeholder="Ingresar Estado" value="<?php if(isset($data[3])) { echo $data[3]; } ?>"   id="state" class="form-control">
									<input type="text" placeholder="Ingresar País" value="<?php if(isset($data[1])) { echo $data[1]; } ?>"   id="contry" class="form-control">
									<input type="text" placeholder="Ingresar pin" value="<?php if(isset($data[4])) { echo $data[4]; } ?>"   id="pin" class="form-control">
									<input type="hidden" id="ps1" value="<?php echo  $user_info['user_pass']; ?>">
									<input type="hidden" id="id" value="<?php echo  $user_info['user_id']; ?>">
									<input type="password" placeholder="Password" id="password" class="form-control">
									<button type="submit" onclick="user_profile();" class="fd_btn">Actualizar Perfil</button>
							</div>
						</div>
						</div>
						<div id="order_history" class="tab-pane">
							<div class="fd_order_status fd_signin">
								<table class="table table-hover table-condensed">
									<tr>
										<th>Pedido</th>
										<th>Cantidad</th>
										<th>Monto</th>
										<th>Fecha</th>
										<th>Estado</th>
									</tr>
									<?php 
									$qty = 0 ;
									$i=0;
									if($order_info)
									{
									foreach($order_info as $order)
									{
										$arr=$order['payment_pid'];
										$arr2=json_decode($arr, true);
										$date=explode(' ',$order['payment_date']);
										
										$j=0;
										foreach($arr2 as $a)
										{
											// echo 'Item:' . $a['item'] . '<br>';
											// echo 'quantity:' . $a['quantity'] . '<br>';
											// echo 'amount:' . $a['amount'] . '<br>';
											
											$qty = $qty + (int)$a['quantity'];
											// $a['totalQty'] = $qty;
											// echo 'totalQty	:' . $a['totalQty'] . '<br>'; 
											// echo '<br><br><br><br><br>';
											$order['totalQty'] = $qty;
											$j++;
											if($j ==  count($arr2)){
												$qty = 0;
											}
											
											
										}
										
										$i++;	
									?>
									<tr>
										<td><?php echo $order['payment_uniqid']; ?></td>
										<td><?php echo $order['totalQty'];  ?></td>
										<td><?php echo $this->ts_functions->getsettings('portalcurreny','symbol'); echo $order['payment_amount'];   ?></td>
										<td><?php 
										 $time=strtotime($order['payment_date']);
										 echo $date=date("d",$time);
										 echo $month=substr(date("F",$time),0,3).',';
										 echo $year=date("y",$time);
										?></td>
										<td><?php
										if($order['payment_status']=='0')
											echo "Pendiente";
										else if($order['payment_status']=='1')
											echo "En Camino";
										else if($order['payment_status']=='2')
											echo "Entregado";
										else if($order['payment_status']=='3')
											echo "Canceledo";
										?></td>
									</tr>
									<?php } } else { echo "<tr><td colspan='5'>Pedido no encontrado</td></tr>"; }?>
								</table>
							</div>
							
						</div>
					</div>
				</div>
			      
				<?php } } if(empty($f)){ ?>
						<a class="fd_login">
							<img src="<?php echo base_url('assets/front/images/default_image/user.svg'); ?>" id="user_login">
							<!--span><i class="fa fa-user"  id="user_login" aria-hidden="true"></i></span-->
							<i class="flaticon-down-arrow"></i>
						</a>
				<?php }else{ ?>
					
					<a href="javascript:void(0)"  >
						<span class="user_icon">
							<?= $f ?>	
						</span>
						<i title="Signout" onclick="javascript:window.location='<?= base_url('front/logout') ?>'"  class="fa fa-sign-out" aria-hidden="true"></i>
					</a>
					
				<?php } ?>
				
				<?php if(empty($f)){ ?>
				<div class="fd_user_wrapper text-center fd_transition">
					<ul class="nav nav-tabs">
					   <li><a class="s1 active" href="#signin" >Ingresar</a></li>
					   <li><a id="s2"  href="#signup">Registrarse</a></li>
					</ul>
					<div class="tab-content">
						<div id="signin" class="tab-pane in active">
							<div class="fd_user_inner fd_signin">
							<form id="loginform" action="<?= base_url('front/auth/login'); ?>">
								<input type="text" id="lemail"  placeholder="Email o Celular" class="form-control" value="<?php echo isset($_COOKIE['email'])? $_COOKIE['email']:'';?>">
								<input type="password" id="lpassword" placeholder="Password" class="form-control" value="<?php echo isset($_COOKIE['pass'])? $_COOKIE['pass']:'';?>">
								
								<input type="checkbox" name="check" value="" id="fd_check">	
								<label class="fd_check_box" for="fd_check">Mantenerme conectado</label>
								
								<button type="submit" class="fd_btn">Ingresar</button>
							</form>	
								<a  class="fd_forgot_password_link">Se te olvidó tu contraseña?</a>
							</div>
						</div>
						<div id="signup" class="tab-pane">
							<div class="fd_user_inner fd_signin">
							<form id="signupform">
								<input type="text" id="name" name="user_name" placeholder="Usuario" class="form-control">
								<input type="text" id="email" name="user_email" placeholder="Email" class="form-control">
								<?php  $msg91_status = $this->ts_functions->getsettings('msg91', 'status'); 
								 
								if($msg91_status ==1){ ?>
								<input type="text" id="contacts" name="user_mobile" placeholder="Celular" class="form-control">
								<?php } ?>
								<input type="password" id="password" name="user_pass" placeholder="Password" class="form-control">
								
								<button type="submit" class="fd_btn">submit</button>
							</form>	
								<a class="s1">¿Ya tienes una cuenta?</a>
							</div>
						</div>
					</div>
					
					<div class="fd_user_inner fd_forgot_password fd_transition">
						<form id="formsforget" action="<?= base_url('front/auth/forgot'); ?>"  method="POST">
						<a class="fd_close"></a>
						<h2 class="fd_subheading">Forgot Password?</h2>
					
							<p>Para restablecer su contraseña, ingrese su dirección de correo electrónico completa para solicitar un restablecimiento de contraseña</p>
							<input type="text" value="" id="femail" placeholder="Email o Celular" class="form-control">
							<button type="submit"  class="fd_btn">submit</button>
						</form>
					</div>
					
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
	<!--banner start--> 
	<?php if($page != 'success'){ ?>
	<div class="fd_banner_wpapper" style="background-image:url('<?= base_url($img['img_banner_down']); ?>');" id="home">
		<div class="fd_shap_wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						
						<?php 
						if(isset($page)){
							if($page == 'main'){
								$this->load->view('front/include/banner');
							}elseif($page != 'success'){
								$this->load->view('front/include/bredcumb');
							}
						}
						
						
						?>
						
						
					</div>
				</div>
			</div>
		</div>
	</div>		
						
	<?php } ?>		