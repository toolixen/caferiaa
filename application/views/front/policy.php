<div class="fd_policy_wrapper">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="fd_privacy_box">
					<div class="fd_privacy_head">
						<h4 class="fd_privacy_title">Privacy Policy</h4>
						<?php 
							echo "<div class='fd_date_update'><p><span>Last Update: </span>".$general['privacy_policy_update_date']."</p></div>"; 
						?>
					</div>
					<?php 
					echo $general['privacy_policy']; ?>
					</div>
			</div>
		</div>
	</div>
</div>