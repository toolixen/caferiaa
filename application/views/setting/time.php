<div class="row">

    <div class="col-md-12">

        <div class="hs_heading medium">

            <h3>Add Time </a></h3>

        </div>

        <div class="hs_datatable_wrapper table-responsive">

            <table class="hs_datatable table table-bordered">

                <thead>

                    <tr>
                        <th>#</th>
						<th>Title</th>
                        <th>Open Time </th>
						<th>Close Time </th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
               <tbody>
				<?php if(!empty($timess)) {
					 $count = 1;
					 foreach($timess as $times) {
					 ?>
					 	<tr>
							<td>
							<?= $count++; ?>
							</td>
							<td>
							<?= $times['title']; ?>
							</td>
							<td>
							<?php $timeArr = json_decode($times['description'],true);
								echo $timeArr['open_time'];
							?>
							</td>
							<td>
							<?php echo $timeArr['close_time']; ?>
							</td>
							<td>
							  <input type="checkbox" value="<?= $times['id']; ?>" class="custom-control-input" onchange="updatestatus(this);" <?= ($times['status'] == 1)?'checked':''; ?>>
							</td>
							<td>
							<a  class="btn" title="Edit" data-description="<?= $times['description']; ?>" data-title="<?= $times['title']; ?>" data-opentime="<?= $timeArr['open_time']; ?>" data-closetime="<?= $timeArr['close_time']; ?>" onclick="add_time(<?= $times['id']; ?> , this)"><i class="fa fa-pencil" aria-hidden="true"></i></a>
							<!--<a class="btn" title="Delete" href="<?= site_url('admin/Manage_time_schedule/' .$times['id']) ?>"><i class="fa fa-trash" aria-hidden="true"></i></a>-->	
							</td>
						</tr>
					 <?php }

					 }

				 ?>

					

                </tbody>

            </table>

        </div>

    </div>

</div>





   <!-- Add New Category popup start -->

<div id="timess" class="modal fade" role="dialog">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal">&times;</button>

                 <h4 class="modal-title"><span>Add </span> times</h4>

            </div>

            <div class="modal-body">

			<form action="<?php echo base_url();?>admin/Manage_time_schedule" method="post" enctype="multipart/form-data" >
				<div class="hs_input">
					<label>Select Day</label>
					<select class="form-control required" id="title" name="title" required>
						<option value="">Select Day</option>
						 <option value="Monday">Monday</option>
						 <option value="Tuesday">Tuesday</option>
						 <option value="Wednesday">Wednesday</option>
						 <option value="Thursday">Thursday</option>
						 <option value="Friday">Friday</option>
						 <option value="Saturday">Saturday</option>
						 <option value="Sunday">Sunday</option>
					</select>
				</div>
                <!--<div class="hs_input">

                    <label>Day</label>

					<input type="text" required class="form-control" placeholder="title"  name="title" id="title" required>

                </div>-->
				<div class="hs_input">

                    <label>From Times</label>

					<input type="text" required class="form-control fd_time" readonly placeholder="title"  name="opening_time" id="openTime" required>

                </div>
				<div class="hs_input">

                    <label>To Times</label>

					<input type="text" required class="form-control fd_time" readonly placeholder="title"  name="closing_time" id="closeTime" required>

                </div>
				
				<!--<div class="hs_input">

                    <label>Description</label>
					<textarea class="form-control" required name="description" id="description">
					</textarea>
                </div>-->

                <div class="hs_input">

				   <input type="hidden" value="0" name="id" id="id">

					<button type="submit" class="btn " ></button>

                </div>

            </div>

			</form>

        </div>

    </div>

</div>

<!-- Add New Category popup end -->



<script>
function add_time(id="", thiss=""){
		
	if(id==''){
		 $('.modal-title span').text('Add New') ;
		 $('.modal-body .btn').text('Add');
		 $('#title').val('');
		 $('#openTime').val('');
		 $('#closeTime').val('');
		 //$('#description').val('');
		 $('#id').val(0);
	 }else{
		$('.modal-title span').text('Update');
		$('.modal-body .btn').text('Update');
		$('#title').val($(thiss).attr('data-title'));
		$('#title').trigger('change');
		$('#openTime').val($(thiss).attr('data-opentime'));
		$('#closeTime').val($(thiss).attr('data-closetime'));
		//$('#description').val($(thiss).attr('data-description'));
		$('#id').val(id);
		 
	 }
		$('#timess').modal('show');
		
 }
 
 
</script>

	

<script>

function updatestatus(e){
	var status ;
	(e.checked == true)?status=1:status=0;
	 $.ajax({
		  type		: "POST",
		  url		: "<?php site_url('admin/Manage_time_schedule'); ?>",
		  data		: {	'status' :	 status,
						'tid'	 :   e.value			
						},
		  success: function(response){
			  if(response==1){
				  toastr.success("times updated successfully");
			  }
		  }
	});
	
}
</script>



   

    <!-- page body end -->

