
<style>
.btn-bs-file{
    position:relative;
	background-color: #ab845d !important;
    color: #ffffff !important;
}
.btn-bs-file input[type="file"]{
    position: absolute;
    top: -9999999;
    filter: alpha(opacity=0);
    opacity: 0;
    width:0;
    height:0;
    outline: none;
    cursor: inherit;
}
.myclass{
	background:white !important;
	border:none !important;
	font-weight:bold !important;
}
.btn-bs-file .btn{
	background-color:#fff;
	color:#ab845d;
	margin-left:20px;
}
.btn-bs-file:hover {background-color: #fff !important;color: #ab845d !important;}

.btn-bs-file:hover .btn {
    background-color: #ab845d;
    color: #ffffff;
}
</style>

<script>
function validateForm() {
	
    var x = document.forms["myForm"]["image"].value;
    if (x == "") {
       
		toastr.error("Please Browse Image ");
        return false;
    }
	var y = document.forms["myForm"]["document"].value;
    if (y == "") {
       
		toastr.error("Please checked image ");
        return false;
    }
}
</script>
<div class="row">
					
                        <div class="col-md-12">
                            <div class="hs_image_viewer">
                                <label>Upload Images</label>
                                <div class="iv_image">
									<form   name="myForm" method="POST" enctype="multipart/form-data" action="<?php echo site_url('admin/addImages'); ?>" >
	
										<div class="row" >
											<div class="col-md-6 col-sm-12 col-xs-12">
												<label class="btn-bs-file btn btn-lg btn-info">
													Browse
													<input type="file" name="image"   id="image">
													<input onclick="return validateForm()" type="submit" value="upload" class="btn  btn-success">
												</label>
											</div>
											<div class="col-md-6 col-sm-12 col-xs-12">
											<div class="hs_radio_list">
												<div class="col-md-12 col-sm-12 col-xs-12">		
													<div class="hs_radio">		
														<input type="radio" name="document" value="img_logo" required id="header_logo">	
														<label for="header_logo"> Logo Image(134x60px)</label>	
													</div>
												</div>
												<!--div class="col-md-12 col-sm-12 col-xs-12">		
													<div class="hs_radio">		
														<input type="radio" name="document" value="img_logo_footer" required id="footer_logo">	
														<label for="footer_logo">Footer Logo Image(114x63px)</label>	
													</div>
												</div-->
												<!--<div class="col-md-12 col-sm-12 col-xs-12">		
													<div class="hs_radio">		
														<input type="radio" name="document" value="img_special_offer" required id="offer">	
														<label for="offer">Special Offer(467x279px)</label>	
													</div>	
												</div>	-->
												<div class="col-md-12 col-sm-12 col-xs-12">		
													<div class="hs_radio">		
														<input type="radio" name="document" value="img_contact_us" required id="contact">	
														<label for="contact">Contact Us Image(440x717px) </label>	
													</div>	
												</div>	
												<div class="col-md-12 col-sm-12 col-xs-12">		
													<div class="hs_radio">		
														<input type="radio" name="document" value="img_banner_down" required id="banner_bg">	
														<label for="banner_bg">Banner Background Image (1920x950px)</label>	
													</div>	
												</div>	
												<div class="col-md-12 col-sm-12 col-xs-12">		
													<div class="hs_radio">		
														<input type="radio" name="document" value="img_banner_up" required id="banner_center_img">	
														<label for="banner_center_img"> Banner Front Center image (100x50px)</label>	
													</div>	
												</div>	
												<div class="col-md-12 col-sm-12 col-xs-12">		
													<div class="hs_radio">		
														<input type="radio" name="document" value="fevicon" required id="favicon">	
														<label for="favicon">Favicon (32x32px)</label>	
													</div>	
												</div>	
											
											</div>
											</div>
										</div>
									</form>
                                </div>
                               
                            </div>
                        </div>
						
                        <div class="col-md-12">
                            <div class="hs_image_viewer">
                                <label>Header Logo</label>
                                <div class="iv_image">
                                    <?php if(!empty($img['img_logo'])){ ?>
										<img style="height:50px;" src="<?php echo base_url($img['img_logo']); ?>"  >
									<?php } ?>	
                                </div>
                            </div>
                        </div>
						
						 <!--div class="col-md-12">
                            <div class="hs_image_viewer">
                                <label>Footer Logo</label>
                                <div class="iv_image">
                                   <?php if(!empty($img['img_logo_footer'])){ ?>
									<img style="height:25px;" src="<?php echo base_url('assets/front/images/front_imges/'.$img['img_logo_footer']); ?>"  >
									<?php } ?>
                                </div>
                            </div>
                        </div
						
						<div class="col-md-12">
                            <div class="hs_image_viewer">
                                <label>Offer Image</label>
                                <div class="iv_image">
                                   <?php if(!empty($img['img_special_offer'])){ ?>
									<img style="height:100px;" src="<?php echo base_url().$img['img_special_offer']; ?> " >
									<?php } ?>
                                </div>
                            </div>
                        </div>-->
						
						<div class="col-md-12">
                            <div class="hs_image_viewer">
                                <label>Contact Image</label>
                                <div class="iv_image">
                                  <?php if(!empty($img['img_contact_us'])){ ?>
									<img style="height:100px;" src="<?php echo base_url().$img['img_contact_us']; ?>">
								  <?php } ?>
                                </div>
                            </div>
                        </div>
						
						<div class="col-md-12">
                            <div class="hs_image_viewer">
                                <label>Banner Background Image</label>
                                <div class="iv_image">
                                  <?php if(!empty($img['img_banner_down'])){ ?>
									<img style="height:100px;" src="<?php echo base_url().$img['img_banner_down']; ?>">
									<?php } ?>
                                </div>
                            </div>
                        </div>
						
						<div class="col-md-12">
                            <div class="hs_image_viewer">
                                <label>Front Center Image</label>
                                <div class="iv_image">
                                  <?php if(!empty($img['img_banner_up'])){ ?>
										<img style="height:100px;" src="<?php echo base_url().$img['img_banner_up']; ?>">
									<?php } ?>
                                </div>
                            </div>
                        </div>
						
						<div class="col-md-12">
                            <div class="hs_image_viewer">
                                <label>Favicon icon</label>
                                <div class="iv_image">
                                  <?php if(!empty($img['fevicon'])){ ?>
										<img style="height:100px;" src="<?php echo base_url().$img['fevicon']; ?>">
									<?php } ?>
                                </div>
                            </div>
                        </div>
                        
                      
                    </div>