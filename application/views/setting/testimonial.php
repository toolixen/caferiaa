<div class="row">
   <div class="col-md-12">
      <div class="hs_heading medium">
         <h3>Testimonial (<?php if(!empty($testimonials)){ echo count($testimonials);}else{ echo 0 ; } ?>) <a  onclick="add_testimonial()" class="btn">Add Testimonial</a></h3>
      </div>
      <div class="hs_datatable_wrapper table-responsive">
         <table class="hs_datatable table table-bordered">
            <thead>
               <tr>
                  <th>#</th>
                  <th>Name</th>
				  <th>Other Info.</th>
                  <th>Description</th>
				  <th>Profile</th>
                  <th>Status</th>
                  <th>Action</th>
               </tr>
            </thead>
            <tbody>
               <?php if(!empty($testimonials)) {
                  $count = 0;
                  foreach($testimonials as $testimonial) {
					$count++;
					$id = $testimonial['id'];
					$title=$testimonial['title'];
					$description=$testimonial['description'];
					$profile=$testimonial['profile'];
					$image=base_url().'assets/admin/images/testimonial/default.jpg';
                  
                  if($testimonial['image']!=''){
                  	$image=base_url('assets/admin/images/testimonial/thumb/').$testimonial['image']; 
                  }
                  
					$activeselected = ($testimonial['status'] == '1' ? 'selected' : '' );
					$inactiveselected = ($testimonial['status'] == '0' ? 'selected' : '' );
					$checked = ($testimonial['status'] == '1' ? 'checked' : '' );
					$cat='cat';
					
					echo '<tr>
							<td>'.$count.'</td>
							<td>'.$title.'</td>
							<td>'.$profile.'</td>
							<td>'.$description.'</td>
							
							<td>
								<div class="hs_category_icon">
									<img src='.$image.' alt="icon">
								</div>                            
							</td>
							
							<td>
								<input type="checkbox" value="'. $id . '" class="custom-control-input" onchange="updatestatus(this);" '. $checked .'>
							</td>
							
							<td width="200">
								<a  class="btn" title="Edit" data-profile="'.$profile.'" data-description="'.$description.'" data-name="'.$title.'" onclick="add_testimonial('.$id.' , this)"><i class="fa fa-pencil" aria-hidden="true"></i></a>
								<a class="btn" title="Delete" href="'. site_url("admin/testimonial_setting/" .$id) . '"><i class="fa fa-trash" aria-hidden="true"></i></a>	
							</td>
						</tr>';
			  }} ?>
            </tbody>
         </table>
      </div>
   </div>
</div>
<!-- Add New Category popup start -->
<div id="testimonial" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><span>Add New</span> Testimonial</h4>
         </div>
         <div class="modal-body">
            <form action="<?php echo base_url();?>admin/testimonial_setting" method="post" enctype="multipart/form-data" >
               <div class="hs_input">
                  <label>Name</label>
                  <input type="text" class="form-control add_cate_form" placeholder="Name"  name="title" id="title">
               </div>
			   <div class="hs_input">
                  <label>Profile</label>
                  <input type="text" class="form-control add_cate_form" placeholder="Profile"  name="profile" id="Profile">
               </div>
               <div class="hs_input">
                  <label>Description</label>
                  <textarea class="form-control add_cate_form" name="description" id="description">
                  </textarea>
               </div>
               <div class="hs_input">
                  <label>Profile Image</label>
                  <input type="file" class="form-control " name="image" id="image">
				  preferred image size should be 200px * 200px;
               </div>
               <div class="hs_input">
                  <input type="hidden" value="0" name="id" id="id">
                  <button type="submit" class="btn ">Add</button>
               </div>
         </div>
         </form>
      </div>
   </div>
</div>
<!-- Add New Category popup end -->
<script>
   function add_testimonial(id='' , thiss=''){
   	 $(' #cateimage').val('');
   	 if(id==''){
   		 $('.modal-title span').text('Add New') ;
   		 $('.modal-body .btn').text('Add');
   		 $('#title').val('');
   		 $('#description').val('');
   		 $('#Profile').val('');
   		 $('#id').val(0);
   	 }else{
   		$('.modal-title span').text('Update');
   		$('.modal-body .btn').text('Update');
   		$('#title').val($(thiss).attr('data-name'));
   		$('#description').val($(thiss).attr('data-description'));
   		$('#Profile').val($(thiss).attr('data-profile'));
   		$('#id').val(id);
   		 
   	 }
   		$('#testimonial').modal('show');	   
    }
    
    
</script>
<script>
   function updatetestimonial(e,id){
   	 $.ajax({
   		  type		: "POST",
   		  url		: "<?php echo site_url('admin/delete_testimonial'); ?>",
   		  data		: {	'status' :	 e.value,
   						'id'	 :   id			
   					},
   		  success: function(response){
   			  if(response==1){
   				  toastr.success("testimonial status updated successfully");
   			  }
   		  }
   	});
   	
   }
  
function updatestatus(e){
	var status ;
	(e.checked == true)?status=1:status=0;
	 $.ajax({
		  type		: "POST",
		  url		: "<?php echo site_url('admin/delete_testimonial'); ?>",
		  data		: {	'status' :	 status,
						'id'	 :   e.value			
						},
		  success: function(response){
			  if(response==1){
				  toastr.success("testimonial status  updated successfully");
			  }
		  }
	});
	
} 
</script>
<!-- page body end -->