<div class="row">

    <div class="col-md-12">

        <div class="hs_heading medium">

            <h3>Admin Setting</h3>

        </div>

        <div class="hs_datatable_wrapper table-responsive">

            <table class="hs_datatable table table-bordered">

                <thead>

                    <tr>
                        <th>#</th>
						<th>Title</th>
                        <th>Information</th>
                        <th>Edit</th>
                    </tr>
                </thead>
               <tbody>
				<?php if(!empty($infos)) {
					 $count = 1;
					 foreach($infos as $info) {
					 ?>
					 	<tr>
							<td>
							<?= $count++; ?>
							</td>
							<td>
							<?= $info['title']; ?>
							</td>
							<td>
							<?= $info['value_text']; ?>
							<td>
							<a  class="btn" title="Edit" data-value_text="<?= $info['value_text']; ?>" onclick="update_setting(<?= $info['uniq_id']; ?> , this)"><i class="fa fa-pencil" aria-hidden="true"></i></a>
							</td>
						</tr>
					 <?php }

					 }

				 ?>

					

                </tbody>

            </table>

        </div>

    </div>

</div>





   <!-- Add New Category popup start -->

<div id="infos" class="modal fade" role="dialog">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal">&times;</button>

                 <h4 class="modal-title"><span>Update </span> info</h4>

            </div>

            <div class="modal-body">

			<form action="<?php echo base_url();?>admin/admin_setting" method="post" enctype="multipart/form-data" >

               <div class="hs_input">

                    <label>Description</label>
					<textarea  rows="6" class="form-control add_cate_form" name="value_text" id="title">
					</textarea>
                </div>

                <div class="hs_input">

				   <input type="hidden" value="0" name="uniq_id" id="id">

					<button type="submit" class="btn ">Update</button>

                </div>

            </div>

			</form>

        </div>

    </div>

</div>

<!-- Add New Category popup end -->



<script>
function update_setting(id, thiss){
		
		$('#title').val($(thiss).attr('data-value_text'));
		$('#id').val(id);
		$('#infos').modal('show');	   
 }
 
 
</script>







   

    <!-- page body end -->

