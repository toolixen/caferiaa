<div class="row">

    <div class="col-md-12">

        <div class="hs_heading medium">

            <h3>Information Setting</h3>

        </div>

        <div class="hs_datatable_wrapper table-responsive">

            <table class="hs_datatable table table-bordered">

                <thead>

                    <tr>
                        <th>#</th>
						
                        <th>Title</th>
                        <th>Information</th>
                        <th>Status</th>
                        <th>Edit</th>
                    </tr>
                </thead>
               <tbody>
				<?php if(!empty($infos)) {
					 $count = 1;
					 foreach($infos as $info) {
					 ?>
					 	<tr>
							<td>
							<?= $count++; ?>
							</td>
							
							<td>
							<?= $info['title']; ?>
							</td>
							<td>
							<?= $info['description']; ?>
							</td>
							<td>
							  <input type="checkbox" value="<?= $info['id']; ?>" class="custom-control-input" onchange="updatestatus(this);" <?= ($info['status'] == 1)?'checked':''; ?>>
							</td>
							<td>
							<a  class="btn" title="Edit" data-description="<?= $info['description']; ?>" data-title="<?= $info['title']; ?>"  data-title="<?= $info['title']; ?>"onclick="add_testimonial(<?= $info['id']; ?> , this)"><i class="fa fa-pencil" aria-hidden="true"></i></a>
							</td>
						</tr>
					 <?php }

					 }

				 ?>

					

                </tbody>

            </table>

        </div>

    </div>

</div>





   <!-- Add New Category popup start -->

<div id="infos" class="modal fade" role="dialog">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal">&times;</button>

                 <h4 class="modal-title"><span>Update </span> info</h4>

            </div>

            <div class="modal-body">

			<form action="<?php echo base_url();?>admin/info_setting" method="post" enctype="multipart/form-data" >

                <div class="hs_input">

                    <label>info</label>

					<input type="text" class="form-control add_cate_form" placeholder="title"  name="title" id="title">

                </div>
				<div class="hs_input">

                    <label>Description</label>
					<textarea class="form-control add_cate_form" name="description" id="description">
					</textarea>
                </div>

                <div class="hs_input">

				   <input type="hidden" value="0" name="id" id="id">

					<button type="submit" class="btn" >Update</button>

                </div>

            </div>

			</form>

        </div>

    </div>

</div>

<!-- Add New Category popup end -->



<script>
function add_testimonial(id, thiss){
		
		$('#title').val($(thiss).attr('data-title'));
		console.log($('#title').val($(thiss).attr('data-title')));
		$('#description').val($(thiss).attr('data-description'));
		$('#id').val(id);
		$('#infos').modal('show');	   
 }
 
 
</script>




<script>

function updatestatus(e){
	var status ;
	(e.checked == true)?status=1:status=0;
	 $.ajax({
		  type		: "POST",
		  url		: "<?php site_url('admin/Front_info_setting'); ?>",
		  data		: {	'status' :	 status,
						'id'	 :   e.value			
						},
		  success: function(response){
			  if(response==1){
				  toastr.success("info updated successfully");
			  }
		  }
	});
	
}
</script>



   

    <!-- page body end -->

