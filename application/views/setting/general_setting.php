<div class="row">
    <div class="col-md-12">
        <div class="hs_heading medium">
            
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- tab start -->
        <div class="hs_tabs">
            <ul class="nav nav-pills">
                <li class="active"><a data-toggle="pill" href="#setting">Setting</a></li>
                <li><a data-toggle="pill" href="#google_ads">Ads Integration</a></li> 	
				<li><a data-toggle="pill" href="#google_analytics">Google Analytics</a></li>
				<li><a data-toggle="pill" href="#blog_setting">Blog Setting</a></li>
				<li><a data-toggle="pill" href="#credentials">Credentials</a></li>								
				<li><a data-toggle="pill" href="#cookies">GDPR Cookies Message</a></li>
            </ul>
               
            <div class="tab-content">
				<!-- site setting tab start -->
                <div id="setting" class="tab-pane fade in active">
				    <div class="row">
						<div class="col-sm-12">
							<div class="row">
								<form >
								<div class="col-md-6 col-sm-12 col-xs-12">
									<div class="hs_input">
									   <label>Site Title</label>
									   <input type="text" value="<?= $meta_info[0]['site_title'] ?>" id="site_title" name="site_title" value="" class="form-control settingsfields"  placeholder="Site Title" >
									</div>
								</div>
								<div class="col-md-6 col-sm-12 col-xs-12">
									<div class="hs_input">
									   <label>Site Keyword</label>
									   <input type="text" value="<?= $meta_info[0]['site_keyword'] ?>" id="site_keyword" name="site_keyword" value="" class="form-control settingsfields" placeholder="Site Keyword" >
									</div>
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="hs_input">
									   <label>Author Name</label>
									   <input type="text" value="<?= $meta_info[0]['author_name'] ?>" id="author_name" name="author_name" value="" class="form-control settingsfields" placeholder="Author Name" >
									</div>
									<div class="hs_input">
									   <label>Description</label>
									   <textarea name="site_description" id="site_description" placeholder="Site Description" class="form-control"><?= $meta_info[0]['site_description'] ?></textarea>
									</div>
								</div>
								
								</form>
								<div class="col-md-12">
									<button onclick="meta_info_update();" class="btn">Update</button> 
								</div>
							</div>
						</div>
                    </div>
                </div>
			    <div id="google_ads" class="tab-pane fade">
				  <div class="row">
						<form >
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="hs_input">
							   <label>Google Adsense</label>
							   <textarea class="form-control" placeholder="Google Adsense Script" id="add_s" name="add_s"><?php echo $meta_info[0]['add_s'] ?></textarea>
							</div>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="hs_input">
							   <label>Google Admob id</label>
							   <input type="text" value="<?php echo $meta_info[0]['add_mob'] ?>" name="add_mob" id="add_mob" placeholder="Admob ID" class="form-control settingsfields" >
							</div>
						</div>
						</form>
						<div class="col-md-12">
							<button onclick="google_ads();" class="btn">Update</button> 
						</div>
					</div>
                </div>
			  <!-- Analytics setting tab end -->
              <!-- Analytics setting tab start -->
                <div id="google_analytics" class="tab-pane fade">
                   <div class="row">
						<form >
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="hs_input">
							   <label>Google Analytics Script</label>
							   <textarea class="form-control" placeholder="Google Analytics Script" id="google_a" name="google_a"><?php echo $meta_info[0]['google_a'] ?></textarea>
							</div>
						</div>
						</form>
						<div class="col-md-12">
							<button onclick="google_analytics();" class="btn">Update</button> 
						</div>
					</div>
                </div>
              <!-- Analytics setting tab end -->
              <!-- Manage blog setting tab start -->
			   <div id="blog_setting" class="tab-pane fade">
                   <div class="row">
						<form id="blog_setting_form">
						<div class="col-md-12 col-sm-12 col-xs-12">
						    <?php  $i=0; foreach($blog_set as $set){ ?>
						   	<div class="hs_input">
							   <label><?php echo $set['setting_title'] ?></label>
								<div class="hs_radio_list">
									<div class="col-md-12 col-sm-12 col-xs-12">		
										<div class="hs_radio">		
											<input type="radio" name="<?php echo $set['key_text'] ?>" value="1" required id="<?php echo $set['key_text'] ?>" <?php if($set['status']  == 1) {echo "checked='checked'";} ?>>	
											<label for="<?php echo $set['key_text'] ?>"> <?php echo $set['option1'] ?></label>	
										</div>
									</div>
									
									 <div class="col-md-12 col-sm-12 col-xs-12">		
										<div class="hs_radio">		
											<input type="radio" name="<?php echo $set['key_text'] ?>" value="0" required id="<?php echo $set['name'] ?>" <?php if($set['status']  == 0) {echo "checked='checked'";} ?>>	
											<label for="<?php echo $set['name'] ?>">  <?php echo $set['option2'] ?></label>	
										</div>
									</div>
								</div>
							</div>
							<?php $i++; } ?>
							
							
						</div>
						</form>
						<div class="col-md-12">
							<button onclick="blog_setting();" class="btn">Update</button> 
						</div>
					</div>
                </div>
				<div id="credentials" class="tab-pane fade">
				<?php $data=$this->DatabaseModel->access_database('rt_users','select','',array('user_id'=>'1'));
 
				?>
                   <div class="row">
				
						<form> 
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="hs_input">
							   <label>E-Mail</label>
							   <input type="text" value="<?php echo !empty($data) ? $data[0]['user_email'] : '' ; ?>" name="email" id="email" placeholder="E-Mail" class="form-control settingsfields" >
							</div>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="hs_input">
							   <label>Password</label>
							   <input type="hidden" value="123456789" id="ps1" />
							   <input type="password" value="" name="password" id="password" placeholder="Password" class="form-control settingsfields" >
							</div>
						</div>
						</form>
						<div class="col-md-12">
							<button onclick="credentials();" class="btn">Update</button> 
						</div>
					</div>
                </div>
				 <!-- Manage blog setting tab end -->				<div id="cookies" class="tab-pane fade">			                   <div class="row">										<form id="saveCookies_form">						<div class="col-md-12 col-sm-12 col-xs-12">							<div class="hs_input">							   <label>Cookies Title</label>							   <input type="text" value="<?= $meta_info[0]['cookie_title'] ?>" name="text" id="cookie_title" placeholder="Cookie Title" class="form-control settingsfields" >							</div>							<div class="hs_input">							   <label>Cookies Information</label>							   <textarea  id="cookie_text" class="form-control settingsfields" ><?= $meta_info[0]['cookie_text'] ?></textarea>							</div>							<div class="hs_input">							   <label>Cookies Link</label>							   <input type="text" value="<?= $meta_info[0]['cookie_link'] ?>" name="text" id="cookie_link" placeholder="Cookie Link" class="form-control settingsfields" >								<p>Note: Url must contain http:// or https:// </p>							</div>														<div class="hs_input">							   <label>Cookies Link Title</label>							   <input type="text" value="<?= $meta_info[0]['cookie_link_title'] ?>" name="text" id="cookie_link_title" placeholder="Cookie Link Title" class="form-control settingsfields" >							</div>														<div class="hs_input">							   <label>Cookies Button</label>							   <input type="text" value="<?= $meta_info[0]['cookie_button'] ?>" name="text" id="cookie_button" placeholder="Cookie Button" class="form-control settingsfields" >							</div>													</div>						</form>						<div class="col-md-12">							<button onclick="saveCookies();" class="btn">Update</button> 						</div>					</div>                </div>								
            </div>
        </div>
        <!-- tab end -->
    </div>
</div>    
