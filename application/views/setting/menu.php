<div class="row">

    <div class="col-md-12">

        <div class="hs_heading medium">

            <h3>Menu Setting</h3>

        </div>

        <div class="hs_datatable_wrapper table-responsive">

            <table class="hs_datatable table table-bordered">

                <thead>

                    <tr>

                        <th>#</th> 

                        <th>Menu</th>

                        <th>Sub Heading</th>

                        <th>Status</th>

                        
                        <th>Edit</th>

                    </tr>

                </thead>

               <tbody>

				<?php if(!empty($menus)) {

					 $count = 1;

					 foreach($menus as $menu) {
					 ?>
					 
						<tr>
							<td>
							<?= $count++; ?>
							</td>
							<td>
							<?= $menu['title']; ?>
							</td>
							<td>
							<?= $menu['sub_heading']; ?>
							</td>
							<td>
							  <input type="checkbox" value="<?= $menu['id']; ?>" class="custom-control-input" onchange="updatestatus(this);" <?= ($menu['status'] == 1)?'checked':''; ?>>
							</td>
							<td>
							<a  class="btn" title="Edit" data-sub_heading="<?= $menu['sub_heading']; ?>" data-title="<?= $menu['title']; ?>" onclick="add_testimonial(<?= $menu['id']; ?> , this)"><i class="fa fa-pencil" aria-hidden="true"></i></a>
							</td>
						</tr>
					 <?php }

					 }

				 ?>

					

                </tbody>

            </table>

        </div>

    </div>

</div>





   <!-- Add New Category popup start -->

<div id="menus" class="modal fade" role="dialog">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal">&times;</button>

                 <h4 class="modal-title"><span>Update </span> Menu</h4>

            </div>

            <div class="modal-body">

			<form action="<?php echo base_url();?>admin/Front_menu_setting" method="post" enctype="multipart/form-data" >

                <div class="hs_input">

                    <label>Menu</label>

					<input type="text" class="form-control add_cate_form" placeholder="title"  name="title" id="title">

                </div>
				<div class="hs_input">

                    <label>SubHeading</label>
					<textarea class="form-control add_cate_form" name="sub_heading" id="sub_heading">
					</textarea>
                </div>

                <div class="hs_input">

				   <input type="hidden" value="0" name="id" id="id">

					<button type="submit" class="btn">Update</button>

                </div>

            </div>

			</form>

        </div>

    </div>

</div>

<!-- Add New Category popup end -->



<script>
function add_testimonial(id, thiss){
		
		$('#title').val($(thiss).attr('data-title'));
		$('#sub_heading').val($(thiss).attr('data-sub_heading'));
		$('#id').val(id);
		$('#menus').modal('show');	   
 }
 
 
</script>




<script>

function updatestatus(e){
	var status ;
	(e.checked == true)?status=1:status=0;
	 $.ajax({
		  type		: "POST",
		  url		: "<?php site_url('admin/Front_menu_setting'); ?>",
		  data		: {	'status' :	 status,
						'id'	 :   e.value			
						},
		  success: function(response){
			  if(response==1){
				  toastr.success("Menu updated successfully");
			  }
		  }
	});
	
}
</script>



   

    <!-- page body end -->

