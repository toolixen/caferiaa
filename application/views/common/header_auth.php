 <!DOCTYPE html>
<?php $meta_info=$this->DatabaseModel->access_database('rt_genral_setting','select','',array('id'=>'1')); ?>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= $meta_info[0]['site_title'] ?> - Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

    <link rel="shortcut icon" type="image/ico" href="<?= base_url('assets/front/imges/front_images/'.$img['fevicon']) ?>" />
    <link rel="icon" type="image/ico" href="<?= base_url($img['fevicon'] ) ?>" />

    <link rel="stylesheet" type="text/css" media="screen" href="<?= base_url('assets/admin/css/preloader.css') ?>" />
	<link rel="stylesheet" type="text/css" media="screen" href="<?= base_url('assets/admin/css/toastr.css') ?>" />   
    <link rel="stylesheet" type="text/css" media="screen" href="<?= base_url('assets/admin/css/bootstrap.min.css') ?>" />
	<link rel="stylesheet" type="text/css" media="screen" href="<?= base_url('assets/admin/css/font-awesome.min.css') ?>" />
    <link rel="stylesheet" type="text/css" media="screen" href="<?= base_url('assets/admin/css/main.css') ?>" />    
 
</head>
<body class="auth_body">

<!-- preloader start -->
<div class="hs_preloader">
    <div class="hs_preloader_inner">
        <span></span><span></span><span></span>
    </div>
</div>
<!-- preloader end -->