
<!DOCTYPE html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<?php $meta_info=$this->DatabaseModel->access_database('rt_genral_setting','select','',array('id'=>'1'));
$img  = $this->DatabaseModel->select_data('*','rt_images',array('id'=>1),''); 
?>
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= $meta_info[0]['site_title'] ?> - Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

   <link rel="icon" type="image/ico" href="<?php echo base_url().$img[0]['fevicon']; ?>" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/admin/css/preloader.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/admin/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/admin/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/css/scrollbar.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/css/datatables.min.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/js/plugin/timepicker/timepicker.css" type="text/css">
	<link rel="stylesheet" type="text/css" media="screen" href="<?= base_url('assets/admin/css/toastr.css') ?>" />
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/admin/css/main.css" /> 
	
</head>

<body>
<!-- preloader start -->
<div class="hs_preloader">
    <div class="hs_preloader_inner">
        <span></span><span></span><span></span>
    </div>
</div>
<!-- preloader end -->

<!-- sidebar start -->
<?php $img  = $this->DatabaseModel->select_data('*','rt_images',array('id'=>1),''); ?>
<div class="hs_sidebar_wrapper">
    <div class="hs_logo">
        <a href="<?php echo base_url().'admin'; ?>"><img src="<?php echo base_url($img[0]['img_logo']); ?>" alt=""></a>
		
    </div>
    <div class="hs_sidebar_body hs_custom_scrollbar">
        <div class="hs_nav">
            <ul>
                <li><a class="<?php echo(isset($dashboard_active) 	? 'active' : '') ?>" href="<?php echo base_url().'admin'; 						?>"><i class="fa fa-home"  aria-hidden="true"></i> Dashboard</a></li>
                <li><a class="<?php echo(isset($category_active) 	? 'active' : '') ?>" href="<?php echo base_url().'admin/categories'; 			?>"><i class="fa fa-list-alt" aria-hidden="true"></i> Categoria</a></li>
                <li><a class="<?php echo(isset($item_active) 		? 'active' : '') ?>" href="<?php echo base_url().'admin/items'; 				?>"><i class="fa fa-cutlery" aria-hidden="true"></i> Productos</a></li>
				<li><a class="<?php echo(isset($order_active) 		? 'active' : '') ?>" href="<?php echo base_url().'admin/orders'; 				?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Pedidos</a></li>
				<li><a class="<?php echo(isset($order_active_latest)? 'active' : '') ?>" href="<?php echo base_url().'admin/orders/latest'; 		?>"><i class="fa fa-pencil-square" aria-hidden="true"></i> Últimos Pedidos</a></li>
				<li><a class="<?php echo(isset($user_active) 		? 'active' : '') ?>" href="<?php echo base_url().'admin/users'; 				?>"><i class="fa fa-users" aria-hidden="true"></i> Usuarios</a></li>
				<li><a class="<?php echo(isset($user_active) 		? 'active' : '') ?>" href="<?php echo base_url().'admin/users_wallet_recharge_request'; ?>"><i class="fa fa-bitcoin" aria-hidden="true"></i> Solicitud Recarga Wallet</a></li>
				<li><a class="<?php echo(isset($dicount_active) 		? 'active' : '') ?>" href="<?php echo base_url().'admin/discount_coupon'; 				?>"><i class="fa fa-gift" aria-hidden="true"></i> Cupones de Descuento</a></li>
				<li><a class="<?php echo(isset($offer_active) 		? 'active' : '') ?>" href="<?php echo base_url().'admin/offers'; 				?>"><i class="fa fa-gift" aria-hidden="true"></i> Combo Ofertas</a></li>
				<!--li><a class="<?php echo(isset($account_request) 	? 'active' : '') ?>" href="<?php echo base_url().'admin/account_request'; 		?>"><i class="fa fa-credit-card" aria-hidden="true"></i> Account Request</a></li-->	
				<li><a class="<?php echo(isset($blog_category) 		? 'active' : '') ?>" href="<?php echo base_url().'admin/blog_category'; 		?>"><i class="fa fa-th" aria-hidden="true"></i> Blog Categoria</a></li>
				<li><a class="<?php echo(isset($add_blog) 		? 'active' : '') ?>" href="<?php echo base_url().'admin/add_blog'; 				?>"><i class="fa fa-th-large" aria-hidden="true"></i> Admin Blog </a></li>
				<li><a class="<?php echo(isset($contactus) 			? 'active' : '') ?>" href="<?php echo base_url().'admin/contact_us'; 			?>"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Consultas de Clientes </a></li>
				<li><a class="<?php echo(isset($cust_booking) 		? 'active' : '') ?>" href="<?php echo base_url().'admin/booking'; 				?>"><i class="fa fa-book" aria-hidden="true"></i> Reserva de Cliente </a></li>
                <li><a class="<?php echo(isset($feedback) 		? 'active' : '') ?>" href="<?php echo base_url().'admin/feedback'; 				?>"><i class="fa fa-book" aria-hidden="true"></i> Comentarios del cliente </a></li>
				<li class="hs_nav_dropdown"><a><i class="fa fa-cog" aria-hidden="true"></i> Setting</a>
					<ul>
						<li><a class="<?php echo(isset($multiple_upload) 	? 'active' : '') ?>" href="<?php echo base_url().'admin/multiple_upload'; 		?>">Image setting</a></li>
						 <li><a class="<?php echo(isset($compliamce_pages) 		? 'active' : '') ?>" href="<?= base_url('admin/compliamce_pages') ?>">Compliance pages</a>
						 <li><a class="<?php echo(isset($general_setting) ? 'active' : '') ?>" href="<?php echo base_url().'admin/general_setting';	?>">General Setting</a></li>
						<li><a class="<?php echo(isset($Front_menu_setting) ? 'active' : '') ?>" href="<?php echo base_url().'admin/front_menu_setting';	?>">Menu Setting</a></li>
						<li><a class="<?php echo(isset($testimonial_setting)? 'active' : '') ?>" href="<?php echo base_url().'admin/testimonial_setting';	?>">Testimonial Setting</a></li>
						<li><a class="<?php echo(isset($info_setting) 		? 'active' : '') ?>" href="<?php echo base_url().'admin/info_setting'; 			?>">Info Setting</a></li>
						<li><a class="<?php echo(isset($time_setting) 		? 'active' : '') ?>" href="<?php echo base_url().'admin/manage_time_schedule'; 	?>">Time Setting</a></li>
						<li><a class="<?php echo(isset($admin_setting) 		? 'active' : '') ?>" href="<?php echo base_url().'admin/admin_setting'; 		?>">Admin Setting</a></li>
						
					</ul>
				</li>
			</ul>
        </div>
    </div>
    
</div>
<!-- sidebar end -->

<!-- page start -->
<div class="hs_page_wrapper">
    <!-- page title start -->
    <div class="hs_page_title">
        <h3><?php echo (isset($page_title) ? $page_title : ''); ?></h3>
		
        <div class="hs_logout">
            <a href="<?php echo base_url('admin/logout') ?>"><i class="fa fa-sign-out" aria-hidden="true"></i> Salir</a>
        </div>
		
		<div class="hs_logout">
            <a href="<?php echo base_url() ?>" target="_blank"><i class="fa fa fa-external-link" aria-hidden="true"></i> Ver Web</a>
        </div>
    </div>
    <!-- page title end -->

    <!-- page body start -->
    <div class="hs_page_body">
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/lib/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/plugins/Chart.min.js"></script>

    