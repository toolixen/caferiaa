    </div>
    <!-- page body end -->
    <div class="hs_footer">
        <p class='hs_copyright'><?= $this->ts_functions->getsettings('copyright','text'); ?></p>
    </div>
</div>
<!-- page end -->
<input type="hidden" value="<?php echo base_url();?>" id="base_url">
<!-- library javascript start -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/lib/jquery-3.2.1.min.js"></script><script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/lib/bootstrap.min.js"></script>
<!-- library javascript end -->

<!-- plugin javascript start -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/plugins/scrollbar.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/plugins/datatables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/plugins/select2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/toastr.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/plugins/printThis.js"></script>

<!-- plugin javascript end -->

<script src="<?php echo base_url(); ?>assets/front/js/plugin/timepicker/bootstrap-timepicker.js"></script> 
<script type="text/javascript" src="<?= base_url('assets/admin/js/main.js?a='.date('his')); ?>"></script>
 
 
 <script src="<?php echo base_url(); ?>"></script>
<?php  
	if(isset($_SESSION['ts_error']) && $_SESSION['ts_error'] != ''){
		echo '<script>toastr.error("'.$_SESSION['ts_error'].'");</script>';
		$_SESSION['ts_error']='';
	}
	if(isset($_SESSION['ts_success']) && $_SESSION['ts_success'] != ''){
		echo '<script>toastr.success("'.$_SESSION['ts_success'].'");</script>';
		$_SESSION['ts_success']='';
	}
	?> 
</body>
</html>