<div class="row">
    <div class="col-md-12">
        <div class="hs_heading medium">
            <h3><?php echo $page_title; ?> (<?php echo count($ordersList); ?>) </h3>
<!--            <h2>--><?php //echo $_SESSION["idLastOrder"]; ?><!--</h2>-->
<!--            <h1>--><?php //echo $_SESSION["flagNewOrders"]; ?><!--</h1>-->
            <input type="hidden" id="latest_order_type" value="<?php echo $latest_order_type; ?>">
            <input type="hidden" id="flagNewOrders" value="<?php echo $_SESSION["flagNewOrders"]; ?>">
            <audio id="xyz" src="<?php echo base_url(); ?>assets/audio/alerta_nuevo_pedido.wav" preload="auto"></audio>
        </div>


        <!-- tab start -->

        <div class="hs_tabs">

            <ul class="nav nav-pills">

                <li class="<?php if ($pickuporder_tab == 0) {
                    echo 'active';
                } ?>" onclick="initilize_table_order();"><a data-toggle="pill" href="#delevery_orders">Delivery</a></li>

                <li class="<?php if ($pickuporder_tab == 1) {
                    echo 'active';
                } ?>" onclick="initilize_table();"><a data-toggle="pill" href="#pickup_orders">Recoger en Tienda</a>
                </li>

            </ul>


            <div class="tab-content">

                <!-- Delivery orders tab start -->
                <div id="delevery_orders" class="tab-pane fade <?php if ($pickuporder_tab == 0) {
                    echo 'in active';
                } ?>">

                    <div class="row">

                        <div class="col-md-12">
                            <div class="hs_datatable_wrapper table-responsive">

                                <table class="table table-bordered table-responsive" id="dtOrderDelivery">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Pedido Id</th>
                                        <th>Cliente</th>
                                        <th>Celular</th>
                                        <th>Productos</th>
                                        <th>Estado Pedido</th>
                                        <th>Dirección Delivery</th>
                                        <th>Delivery Boy</th>
                                        <th>Estado Pago</th>
                                        <th>Modo Pago</th>
                                        <th>Pago
                                            (<?php echo $this->ts_functions->getsettings('portalcurreny', 'symbol'); ?>)
                                        </th>
                                        <th>Descuento
                                            (<?php echo $this->ts_functions->getsettings('portalcurreny', 'symbol'); ?>)
                                        </th>
                                        <th>Fecha Pedido</th>
                                        <th>Fecha Delivery</th>
                                        <th>Txn Id</th>
                                        <th>Enviar Factura</th>
                                        <!--<th>Delevery Type</th> -->
                                        <th>Instrucciones Especiales</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Pedido Id</th>
                                        <th>Cliente</th>
                                        <th>Celular</th>
                                        <th>Productos</th>
                                        <th>Estado Pedido</th>
                                        <th>Dirección Delivery</th>
                                        <th>Delivery Boy</th>
                                        <th>Estado Pago</th>
                                        <th>Modo Pago</th>
                                        <th>Pago
                                            (<?php echo $this->ts_functions->getsettings('portalcurreny', 'symbol'); ?>)
                                        </th>
                                        <th>Descuento
                                            (<?php echo $this->ts_functions->getsettings('portalcurreny', 'symbol'); ?>)
                                        </th>
                                        <th>Fecha Pedido</th>
                                        <th>Fecha Delivery</th>
                                        <th>Txn Id</th>
                                        <th>Enviar Factura</th>
                                        <!--<th>Delevery Type</th> -->
                                        <th>Instrucciones Especiales</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    <?php if ($ordersList) {
                                        $cnt = 0; ?>
                                        <?php foreach ($ordersList as $soloOrd) {

                                            if ($soloOrd['delivery_type'] == '1') {
                                                $cnt++;
                                                $payment_id = $soloOrd['payment_id'];
                                                $user_name = $soloOrd['user_name'];
                                                $username_arr = explode(' ', $user_name);
                                                $name_initiails = substr($username_arr[0], 0, 1);
                                                if (isset($username_arr[1])) {
                                                    $name_initiails .= substr($username_arr[1], 0, 1);
                                                }
                                                $useractiveselected = ($soloOrd['user_status'] == '1' ? 'selected' : '');
                                                $userinactiveselected = ($soloOrd['user_status'] == '0' ? 'selected' : '');


                                                $items = json_decode($soloOrd['payment_pid'], true);
                                                $itemHtml = '';
                                                foreach ($items as $item) {
                                                    $itemsDeatil = $this->DatabaseModel->select_data('item_name', 'rt_items', array('item_id' => $item['item']), 1);
                                                    $itemName = ($itemsDeatil ? $itemsDeatil[0]['item_name'] : 'Ya no está disponible');
                                                    $item_size = '(' . $item['size'] . ')';
                                                    $itemHtml .= '<p>' . $item['quantity'] . 'X - ' . $itemName . ' ' . $item_size . ' <p>';
                                                    // $itemHtml.='<p>'.$item['quantity'].'X - '.$itemName.' <p>';
                                                }

                                                if ($soloOrd['pre_order_status'] == 1) {
                                                    $preorder_Deatil = $this->DatabaseModel->select_data('*', ' rt_pre_orders', array('pre_order_id' => $soloOrd['payment_id']), 1);
                                                    $pre_order = 'Pre-Ordered';
                                                } else {
                                                    $pre_order = '';
                                                };

                                                ?>
                                                <tr>
                                                    <td><?php echo '<b><span style="color:red">' . $pre_order . '</span></b><br>';
                                                        echo $cnt; ?></td>
                                                    <td><?php echo $soloOrd['payment_uniqid']; ?></td>
                                                    <td><?php echo $user_name ?></td>
                                                    <td>
                                                        <a href="https://api.whatsapp.com/send?phone=51<?php echo $soloOrd['user_mobile'] ?>&text=Hola%20<?php echo $user_name ?>.%20Te%20escribimos%20por%20tu%20pedido%20'<?php echo $soloOrd['payment_uniqid']; ?>'"
                                                           target="_blank"><?php echo $soloOrd['user_mobile'] ?></a>
                                                    </td>
                                                    <td><?php echo $itemHtml ?></td>
                                                    <td>
                                                        <select class="form-control delivery_boy"
                                                                onchange="updatethevalue(this,'order');"
                                                                id="<?php echo $payment_id; ?>_order_status">
                                                            <option value="0" <?php echo($soloOrd['payment_order_status'] == '0' ? 'selected' : ''); ?>>
                                                                Pendiente
                                                            </option>
                                                            <option value="1" <?php echo($soloOrd['payment_order_status'] == '1' ? 'selected' : ''); ?>>
                                                                En Camino
                                                            </option>
                                                            <option value="2" <?php echo($soloOrd['payment_order_status'] == '2' ? 'selected' : ''); ?>>
                                                                Entregado
                                                            </option>
                                                            <option value="3" <?php echo($soloOrd['payment_order_status'] == '3' ? 'selected' : ''); ?>>
                                                                Cancelado
                                                            </option>
                                                        </select>
                                                    </td>
                                                    <td><?php echo $soloOrd['payment_address'] ?></td>
                                                    <td>
                                                        <select class="form-control"
                                                                onchange="updatethevalue(this,'deliveryboy');"
                                                                id="<?php echo $payment_id; ?>_duid">
                                                            <option value="0">No Asignado</option>
                                                            <?php
                                                            if ($deliveryBoy) {
                                                                foreach ($deliveryBoy as $soloBoy) {
                                                                    $selected = ($soloBoy['user_id'] == $soloOrd['payment_duid'] ? 'selected' : '');
                                                                    echo '<option value="' . $soloBoy['user_id'] . '" ' . $selected . '>' . $soloBoy['user_name'] . '</option>';
                                                                }
                                                            }
                                                            ?>

                                                        </select>
                                                    </td>
                                                    <td><?php echo($soloOrd['payment_status'] == '1' ? 'Pagado' : 'No pagado') ?></td>
                                                    <td><?php echo $soloOrd['payment_mode']; ?></td>
                                                    <td><?php echo $soloOrd['payment_amount']; ?></td>
                                                    <td><?php echo $soloOrd['discount_amount']; ?>
                                                    <td><?php echo $soloOrd['payment_date']; ?></td>
                                                    <td><?php echo $soloOrd['payment_delivery_date']; ?></td>
                                                    <td><?php echo $soloOrd['payment_txnId']; ?></td>
                                                    <td>
                                                        <a href="<?= base_url('admin/send_invoice/' . $soloOrd['payment_uniqid']) ?>" target="_blank"><i
                                                                    class="fa fa-send-o"></i></a></td>
                                                    <!--<td><?php echo $soloOrd['delivery_type']; ?></td>-->
                                                    <td><?php echo $soloOrd['special_instructions']; ?></td>
                                                </tr>
                                            <?php }
                                        } ?>
                                    <?php } ?>
                                    </tbody>
                                </table>


                            </div>
                        </div>
                    </div>
                </div>


                <!-- Delivery orders tab start -->
                <div id="pickup_orders" class="tab-pane fade <?php if ($pickuporder_tab == 1) {
                    echo 'in active';
                } ?>">

                    <div class="row">

                        <div class="col-md-12">


                            <div class="hs_datatable_wrapper table-responsive">
                                <table class="hs_datatable table table-bordered table-responsive">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Pedido Id</th>
                                        <th>Cliente</th>
                                        <th>Celular</th>
                                        <th>Productos</th>
                                        <th>Estado Pedido</th>
                                        <th>Dirección Delivery</th>
                                        <th>Delivery Boy</th>
                                        <th>Estado Pago</th>
                                        <th>Modo Pago</th>
                                        <th>Pago
                                            (<?php echo $this->ts_functions->getsettings('portalcurreny', 'symbol'); ?>)
                                        </th>
                                        <th>Descuento
                                            (<?php echo $this->ts_functions->getsettings('portalcurreny', 'symbol'); ?>)
                                        </th>
                                        <th>Fecha Pedido</th>
                                        <th>Fecha Delivery</th>
                                        <th>Txn Id</th>
                                        <th>Enviar Factura</th>
                                        <!--<th>Delevery Type</th> -->
                                        <th>Instrucciones Especiales</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Pedido Id</th>
                                        <th>Cliente</th>
                                        <th>Celular</th>
                                        <th>Productos</th>
                                        <th>Estado Pedido</th>
                                        <th>Dirección Delivery</th>
                                        <th>Delivery Boy</th>
                                        <th>Estado Pago</th>
                                        <th>Modo Pago</th>
                                        <th>Pago
                                            (<?php echo $this->ts_functions->getsettings('portalcurreny', 'symbol'); ?>)
                                        </th>
                                        <th>Descuento
                                            (<?php echo $this->ts_functions->getsettings('portalcurreny', 'symbol'); ?>)
                                        </th>
                                        <th>Fecha Pedido</th>
                                        <th>Delivery Date</th>
                                        <th>Txn Id</th>
                                        <th>Enviar Factura</th>
                                        <!--<th>Delevery Type</th> -->
                                        <th>Instrucciones Especiales</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    <?php if ($ordersList) {
                                        $cnt = 0; ?>
                                        <?php foreach ($ordersList as $soloOrd) {

                                            if ($soloOrd['delivery_type'] == '2') {
                                                $cnt++;
                                                $payment_id = $soloOrd['payment_id'];
                                                $user_name = $soloOrd['user_name'];
                                                $username_arr = explode(' ', $user_name);
                                                $name_initiails = substr($username_arr[0], 0, 1);
                                                if (isset($username_arr[1])) {
                                                    $name_initiails .= substr($username_arr[1], 0, 1);
                                                }
                                                $useractiveselected = ($soloOrd['user_status'] == '1' ? 'selected' : '');
                                                $userinactiveselected = ($soloOrd['user_status'] == '0' ? 'selected' : '');


                                                $items = json_decode($soloOrd['payment_pid'], true);
                                                $itemHtml = '';
                                                foreach ($items as $item) {
                                                    $itemsDeatil = $this->DatabaseModel->select_data('item_name', 'rt_items', array('item_id' => $item['item']), 1);
                                                    $itemName = ($itemsDeatil ? $itemsDeatil[0]['item_name'] : 'No longer available');
                                                    $item_size = '(' . $item['size'] . ')';
                                                    $itemHtml .= '<p>' . $item['quantity'] . 'X - ' . $itemName . ' ' . $item_size . ' <p>';
                                                    // $itemHtml.='<p>'.$item['quantity'].'X - '.$itemName.' <p>';
                                                }

                                                if ($soloOrd['pre_order_status'] == 1) {
                                                    $preorder_Deatil = $this->DatabaseModel->select_data('*', ' rt_pre_orders', array('pre_order_id' => $soloOrd['payment_id']), 1);
                                                    $pre_order = 'Pre-Ordered';
                                                } else {
                                                    $pre_order = '';
                                                };

                                                ?>
                                                <tr>
                                                    <td><?php echo '<b><span style="color:red">' . $pre_order . '</span></b><br>';
                                                        echo $cnt; ?></td>
                                                    <td><?php echo $soloOrd['payment_uniqid']; ?></td>
                                                    <td><?php echo $user_name ?></td>
                                                    <!--											<td>-->
                                                    <?php //echo $soloOrd['user_mobile']
                                                    ?><!--</td>-->
                                                    <td>
                                                        <a href="https://api.whatsapp.com/send?phone=51<?php echo $soloOrd['user_mobile'] ?>&text=Hola%20<?php echo $user_name ?>"
                                                           target="_blank"><?php echo $soloOrd['user_mobile'] ?></a>
                                                    <td><?php echo $itemHtml ?></td>
                                                    <td>
                                                        <select class="form-control delivery_boy"
                                                                onchange="updatethevalue(this,'order');"
                                                                id="<?php echo $payment_id; ?>_order_status">
                                                            <option value="0" <?php echo($soloOrd['payment_order_status'] == '0' ? 'selected' : ''); ?>>
                                                                Pendiente
                                                            </option>
                                                            <option value="1" <?php echo($soloOrd['payment_order_status'] == '1' ? 'selected' : ''); ?>>
                                                                En Camino
                                                            </option>
                                                            <option value="2" <?php echo($soloOrd['payment_order_status'] == '2' ? 'selected' : ''); ?>>
                                                                Entregado
                                                            </option>
                                                            <option value="3" <?php echo($soloOrd['payment_order_status'] == '3' ? 'selected' : ''); ?>>
                                                                Cancelado
                                                            </option>
                                                        </select>
                                                    </td>
                                                    <td><?php echo $soloOrd['payment_address'] ?></td>
                                                    <td>
                                                        <select class="form-control"
                                                                onchange="updatethevalue(this,'deliveryboy');"
                                                                id="<?php echo $payment_id; ?>_duid">
                                                            <option value="0">No asignado</option>
                                                            <?php
                                                            if ($deliveryBoy) {
                                                                foreach ($deliveryBoy as $soloBoy) {
                                                                    $selected = ($soloBoy['user_id'] == $soloOrd['payment_duid'] ? 'selected' : '');
                                                                    echo '<option value="' . $soloBoy['user_id'] . '" ' . $selected . '>' . $soloBoy['user_name'] . '</option>';
                                                                }
                                                            }
                                                            ?>

                                                        </select>
                                                    </td>
                                                    <td><?php echo($soloOrd['payment_status'] == '1' ? 'Paid' : 'UnPaid') ?></td>
                                                    <td><?php echo $soloOrd['payment_mode']; ?></td>
                                                    <td><?php echo $soloOrd['payment_amount']; ?></td>
                                                    <td><?php echo $soloOrd['discount_amount']; ?>
                                                    <td><?php echo $soloOrd['payment_date']; ?></td>
                                                    <td><?php echo $soloOrd['payment_delivery_date']; ?></td>
                                                    <td><?php echo $soloOrd['payment_txnId']; ?></td>
                                                    <td>
                                                        <a href="<?= base_url('admin/send_invoice/' . $soloOrd['payment_uniqid']) ?>" target="_blank"><i
                                                                    class="fa fa-send-o"></i></a></td>
                                                    <!--<td><?php echo $soloOrd['delivery_type']; ?></td>-->
                                                    <td><?php echo $soloOrd['special_instructions']; ?></td>
                                                </tr>
                                            <?php }
                                        } ?>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>


<!-- Order ID popup start -->
<div id="popupOrderId" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><span>Detalle</span> Pedido  <input type='button' id='btnImprimirPedido' value='Imprimir' class="btn-info btn-xs"></h4>
            </div>
            <div class="modal-body" id="printPedido">
                <!--                <form action="-->
                <?php //echo base_url();?><!--admin/add_categories" method="post" enctype="multipart/form-data" id="add_cate_form">-->
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="hs_input">
                            <label style="color: blue;font-weight: bold;"><i class="fa fa-first-order"></i> Pedido Id:</label>
                            <div id="PedidoId"></div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="hs_input">
                            <label style="color: blue;font-weight: bold;"><i class="fa fa-calendar"></i> Fecha Pedido:</label>
                            <div id="FechaPedido"></div>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="hs_input">
                            <label style="color: blue;font-weight: bold;"><i class="fa fa-user"></i> Cliente:</label>
                            <div id="Cliente"></div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="hs_input">
                            <label style="color: blue;font-weight: bold;">Celular <i class="fa fa-whatsapp"></i>:</label>
                            <div id="Celular"></div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="hs_input">
                            <label style="color: blue;font-weight: bold;"><i class="fa fa-list"></i> Productos:</label>
                            <div id="Productos"></div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="hs_input">
                            <label style="color: blue;font-weight: bold;"><i class="fa fa-feed"></i> Estado Pedido:</label>
                            <div id="EstadoPedido"></div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="hs_input">
                            <label style="color: blue;font-weight: bold;"><i class="fa fa-map-marker"></i> Dirección Delivery:</label>
                            <div id="DirecciónDelivery"></div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="hs_input">
                            <label style="color: blue;font-weight: bold;"><i class="fa fa-motorcycle"></i> Delivery Boy:</label>
                            <div id="DeliveryBoy"></div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="hs_input">
                            <label style="color: blue;font-weight: bold;"><i class="fa fa-money"></i> Modo Pago:</label>
                            <div id="ModoPago"></div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="hs_input">
                            <label style="color: blue;font-weight: bold;"><i class="fa fa-money"></i> Monto Pago:</label>
                            <div id="MontoPago"></div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="hs_input">
                            <label style="color: blue;font-weight: bold;"><i class="fa fa-exchange"></i> Estado Pago:</label>
                            <div id="EstadoPago"></div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="hs_input">
                            <label style="color: blue;font-weight: bold;"><i class="fa fa-calendar"></i> Fecha Delivery:</label>
                            <div id="FechaDelivery"></div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="hs_input">
                            <label style="color: blue;font-weight: bold;"><i class="fa fa-angle-down"></i> Descuento:</label>
                            <div id="Descuento"></div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="hs_input">
                            <label style="color: blue;font-weight: bold;"><i class="fa fa-list-ul"></i> Instrucciones Especiales:</label>
                            <div id="InstruccionesEspeciales"></div>
                        </div>
                    </div>

                    <div class="hs_input">
                        <input type="hidden" value="0" name="old_cateid" id="old_cateid">
                        <!--                        <a onclick="updateSettings('add_cate_form')" class="btn ">ADD</a>-->
                    </div>
                </div>
            </div>
            <!--            </form>-->
        </div>
    </div>
</div>
<!-- Order ID popup end -->


</div>
<!-- page body end -->

