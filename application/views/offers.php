<div class="row">
    <div class="col-md-12">
        <div class="hs_heading medium">
            <h3>Ofertas (<?php echo count($offerList); ?>) <a href="<?php echo base_url(); ?>admin/add_item_offer/" class="btn">Agregar Nuevo</a></h3>
        </div>
        <div class="hs_datatable_wrapper table-responsive">
            <table class="hs_datatable table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Foto</th>
                        <th>Fecha</th>
						<th>Estado</th>
                        <th>Accion</th>
                    </tr>
                </thead>
                <tbody>
				 <?php if($offerList){$cnt=0; ?>
				   <?php foreach($offerList as $soloOffer){
					   
					  $cnt++;
					  $offer_id=$soloOffer['id'];
                      $offer_name=$soloOffer['offer_name'];
					  $offer_date=date('Y-m-d', strtotime($soloOffer['offer_date']));
                      $offer_desc=$soloOffer['offer_description'];
					  $offer_pic=base_url().'assets/admin/images/menus/default.jpg';
					  if($soloOffer['offer_pic']!=''){
						 $offer_pic=base_url().'assets/admin/images/offers/thumb/'.$soloOffer['image_thumb']; 
					  }
					  
					  $offeractiveselected = ($soloOffer['offer_status'] == '1' ? 'selected' : '' );
					  $offerinactiveselected = ($soloOffer['offer_status'] == '0' ? 'selected' : '' );
					 ?>
				    <tr>
                        <td><?php echo $cnt;?></td>
                       
                        <td><?php echo   $offer_name?></td>
						 <td>
                            <div class="hs_category_icon">
                                <img src="<?php echo $offer_pic; ?>" alt="icon">
                            </div>                            
                        </td>
                        <td><?php echo $offer_date; ?></td>
						<td>
                            <select class="form-control kf_update_offer_item" data-offerid="<?php echo $offer_id; ?>">
                                <option value="1" <?php echo $offeractiveselected; ?>>Active</option>
                                <option value="0"  <?php echo $offerinactiveselected; ?>>Inactive</option>
                            </select>
                        </td>
						
						<td width="200">
                            <a href="<?php echo base_url() ?>admin/add_item_offer/<?php echo $offer_id; ?>" class="btn" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            <a  class="btn" title="Delete" onclick="delete_item_offer(<?php echo  $offer_id?>)"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </td>
                     </tr> 
				   <?php } ?>	 
				 <?php } ?> 
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- page body end -->
