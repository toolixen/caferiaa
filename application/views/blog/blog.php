<div class="row">

    <div class="col-md-12">

        <div class="hs_heading medium">

            <h3>Blogs  (<?php if(!empty($blogs)) { echo count($blogs); }else{ echo 0 ; } ?>) <a  href="<?= site_url('admin/add_blog/add'); ?>" class="btn">Add Blog</a></h3>

        </div>

        <div class="hs_datatable_wrapper table-responsive">

            <table class="hs_datatable table table-bordered">

                <thead>

                    <tr>
                        <th>#</th> 
                        <th>Blog</th>
						<th>Status</th>
                        <th>Action</th>
                       
                    </tr>
                </thead>

               <tbody>

				<?php if(!empty($blogs)) {

					 $count = 0;

					 foreach($blogs as $blog) {
						 $count++;

					 $id = $blog['id'];
					 $title=$blog['title'];
					 $activeselected = ($blog['status'] == '1' ? 'selected' : '' );
					 $inactiveselected = ($blog['status'] == '0' ? 'selected' : '' );

				    echo '<tr>
							<td>'.$count.'</td>
							<td>'.$title.'</td>
							<td>
								<select class="form-control" onchange="update_cat_status(this,'."'".$id."'".');"  id="'.$id.'_status">
									<option value="1" '. $activeselected.'>Active</option>
									<option value="0"  '.$inactiveselected.'>Inactive</option>
								</select>
							</td>
							<td width="200">
								<a  class="btn" title="Edit"  href="'. base_url('admin/add_blog/' . $id) .'" ><i class="fa fa-pencil" aria-hidden="true"></i></a>
								<a class="btn" title="Delete" href="'. base_url('admin/delete_blog/' .$id) .'"><i class="fa fa-trash" aria-hidden="true"></i></a>	
							</td>
						  </tr>';
						 }
					}
					?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<script>

function update_cat_status(e,id){
	 $.ajax({
		  type		: "POST",
		  url		: "<?php echo site_url('admin/delete_blog'); ?>",
		  data		: {	'status' :	 e.value,
						'id'	 :   id			
					},
		  success: function(response){
			  if(response==1){
				  toastr.success("blog status updated successfully");
			  }
		  }
	});
	
}
</script>



<!-- Add New blog popup end -->




   

    <!-- page body end -->

