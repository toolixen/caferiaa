<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" />
<div class="row">
		   <div class="col-md-12 margin-bottom-30">
			  <div class="hs_heading medium">
				 <h3><?php echo (isset($blog) ? 'Update' : 'Add');?> Blog</h3>
			  </div>
			  <?php if(isset($blog)){ 
				$blog = $blog[0];
			  ?>
			  <form action="<?php echo base_url('admin/add_blog/' . $blog['id'] );?>"  method="post" id="add_item_form" enctype="multipart/form-data">
			  <?php }else{ ?>
			  <form action="<?php echo base_url('admin/add_blog');?>"  method="post" id="add_item_form" enctype="multipart/form-data">
			  <?php } ?>
				<div class="row">	
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="hs_input">
							<label>Blog Title</label>
							<input type="text" class="form-control required" id="title" name="title" value="<?php echo  (isset($blog))? $blog['title'] : set_value('title'); ?>" maxlength="50">
							<?= form_error('title') ;?>
						</div>
					</div>
					<div class="col-md-6 col-sm-12 col-xs-12">
					 <div class="hs_input">
						<label for="exampleInputFile">Blog Photo</label>
						<input type="file" id="blog_img" name="blog_img" class="form-control">
						<p class="help-block">Preferred size 850x450px</p>
						 <?php if(isset($blog)){ ?>
						<img style="width:90%" src="<?= base_url('assets/admin/images/blog/' . $blog['blog_img'] ) ?>" >
					  <?php } ?>
					 </div>
					 </div>
					<div class="col-md-6 col-sm-12 col-xs-12">
					  <div class="hs_input">
						<label for="exampleInputFile">Add Tag</label>
						  <input type="text" value="<?php echo  (isset($blog))? $blog['tags'] : set_value('tags'); ?>" data-role="tagsinput"  class="form-control " name="tags"  />
							
						<?= form_error('tags') ;?>
					  </div>
					</div> 
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="hs_input">
							<label>Categories</label>
								<select class="form-control required" id="category" name="category">
									<option value="">Select Category</option>
									<?php
														
											foreach($blog_cats as $cats){
												$selected = '';
												if(isset($blog)){
													
													
													$selected = ($cats['id'] == $blog['category'] )?'selected' : '';
												}
												;
												echo '<option value="'.$cats['id'].'" '.$selected.'>'.$cats['title'].'</option>';
											
											}
										
									?>
								</select>
								<?= form_error('category') ;?>
						</div>
					</div>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="hs_input">
							<label>Author Name</label>
							<input type="text" class="form-control required" id="author_name" name="author_name" value="<?php echo  (isset($blog))? $blog['author_name'] : set_value('author_name'); ?>">
							<?= form_error('author_name') ;?>
						</div>
					</div>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="hs_input">
							<label>Author Profile</label>
							<input type="text" class="form-control required" id="author_profile" name="author_profile" value="<?php echo  (isset($blog))? $blog['author_profile'] : set_value('author_profile'); ?>">
							<?= form_error('author_profile') ;?>
						</div>
					</div>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="hs_input">
							<label for="exampleInputFile">Profile Image</label>
							<input type="file" id="author_img" name="author_img" class="form-control">
							<p class="help-block">Preferred size 120px140px.</p>
						 
						 </div>
					  <?php if(isset($blog)){ ?>
						<img src="<?= base_url('assets/images/blog/' . $blog['author_img'] ) ?>" >
					  <?php } ?>
					</div>  
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="hs_input">
							<label>Facebook URL</label>
							<input type="text" class="form-control required " onchange="isUrlValid(this)" id="facebook_url" name="facebook_url" value="<?php echo  (isset($blog))? $blog['facebook_url'] : set_value('facebook_url'); ?>">
							<?= form_error('facebook_url') ;?>
						</div>
					</div>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="hs_input">
							<label>Twitter URL</label>
							<input type="text" class="form-control required" onchange="isUrlValid(this)"  id="twitter_url" name="twitter_url" value="<?php echo  (isset($blog))? $blog['twitter_url'] : set_value('twitter_url'); ?>">
							<?= form_error('twitter_url') ;?>
						</div>
					</div>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="hs_input">
							<label>LinkedIn URL</label>
							<input type="text" class="form-control required"  onchange="isUrlValid(this)" id="linkedin_url" name="linkedin_url" value="<?php echo  (isset($blog))? $blog['linkedin_url'] : set_value('linkedin_url'); ?>">
							<?= form_error('linkedin_url') ;?>
						</div>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="hs_input">
							<label>Author Quote</label>
							<textarea type="text" class="form-control required" id="author_quote" name="author_quote" ><?php echo  (isset($blog))? $blog['author_quote'] : set_value('author_quote'); ?></textarea>
						<?= form_error('author_quote') ;?>
						</div>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<a onclick="append(this)" class="btn  btn-md" style="margin-top: 25px;">
							<span class="glyphicon glyphicon-plus"></span>	
						</a>
						<br>
						<?php if(isset($blog)){
						$contents = unserialize($blog['content']);
						$i=0;
						foreach($contents as $content){
						?>
						<script>
							var count = <?= $i ?>;
						</script>
						<?php if($i== 0){ ?>
						<br>				
						<div class="hs_input">
							<label>content</label>
							<textarea rows="10" type="text" class="form-control" name="content[]"><?= $content ; ?></textarea>		
						</div>
					<?php }else{ ?>	
						<span id="remove<?= $i ?>" >
						<div class="hs_input">
							<label>content</label>
							<textarea type="text" rows="10" class="form-control" name="content[]" ><?= $content ; ?></textarea>
						</div>
						<br>
						<a class="btn btn-md" onclick="remove(<?= $i ?>)"><span class="glyphicon glyphicon-minus"></span></a>
						</span>
						
					<?php } ?>	
					<?php $i++; } ?>
						
						
						<div class="hs_input" id="append">
							
						</div>
					
					<?php 
						
					}else{ 
					
					?>
					<script>
						var count=0;
					</script>
					<br>
						<div class="hs_input">
							<label>content</label>
							<textarea type="text" rows="10" class="form-control" name="content[]"></textarea>
									
						</div>
						
						<div class="hs_input" id="append">
							
						</div>
					
					<?php } ?>
					
					</div>
					
					
				</div>
			<button type="submit"  class="btn">Submit</button>		 
		
		</div>
			
         
      </form>
   </div>
</div>


<script>
$(document).ready(function () {
    //@naresh action dynamic childs
    var next = 0;
    $("#add-more").click(function(e){
        e.preventDefault();
        var addto = "#field" + next;
        var addRemove = "#field" + (next);
        next = next + 1;
        var newIn = ' <div id="field'+ next +'" name="field'+ next +'"><div class="form-group"><label class="control-label" for="content"></label><textarea  name="content[]" type="text" placeholder="" rows="10" class="form-control input-md"></textarea><br></div>';
        var newInput = $(newIn);
        var removeBtn = '<br><button id="remove' + (next - 1) + '" class="btn btn-danger remove-me glyphicon glyphicon-minus" ></button></div></div><div id="field"><br>';
        var removeButton = $(removeBtn);
        $(addto).after(newInput);
        $(addRemove).after(removeButton);
        $("#field" + next).attr('data-source',$(addto).attr('data-source'));
        $("#count").val(next);  
        
            $('.remove-me').click(function(e){
                e.preventDefault();
                var fieldNum = this.id.charAt(this.id.length-1);
                var fieldID = "#field" + fieldNum;
                $(this).remove();
                $(fieldID).remove();
            });
    });

});
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js"></script>

<script>
	function append(that){

		$("#append").append('<span id="remove'+count+'"><div class="hs_input"><label>content</label><textarea type="text" rows="10" class="form-control" name="content[]"></textarea><br><a class="btn btn-md" onclick="remove('+count+')"><span class="glyphicon glyphicon-minus"></span></a></span>	');	
			count++;	
	}

	function remove(countid){
		$("#remove" + countid ).remove();	
	}
	
	function isUrlValid(url) {
		if( /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url.value) == false){
			var id = url.id;
			$('#'+id).val(' ');
		
			toastr.error("Please enter right URL");
			
		   $('#'+id).focus();
		}
		
	}

</script>

