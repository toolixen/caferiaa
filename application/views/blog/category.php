<div class="row">

    <div class="col-md-12">

        <div class="hs_heading medium">

            <h3>Blog Categoria (<?php if(!empty($categories)){ echo count($categories);}else{ echo 0 ; } ?>) <a  onclick="add_blog_cat()" class="btn">Add categoria</a></h3>
			<?php echo validation_errors(); ?>
        </div>

        <div class="hs_datatable_wrapper table-responsive">

            <table class="hs_datatable table table-bordered">

                <thead>

                    <tr>
                        <th>#</th> 
                        <th>Categoria</th>
						<th>Estado</th>
                        <th>Accion</th>
                       
                    </tr>
                </thead>

               <tbody>

				<?php if(!empty($categories)) {

					 $count = 0;

					 foreach($categories as $category) {
						 $count++;

					 $id = $category['id'];
					 $title=$category['title'];
					 $activeselected = ($category['status'] == '1' ? 'selected' : '' );
					$inactiveselected = ($category['status'] == '0' ? 'selected' : '' );

				    echo '<tr>
							<td>'.$count.'</td>
							<td>'.$title.'</td>
							<td>
								<select class="form-control" onchange="update_cat_status(this,'."'".$id."'".');"  id="'.$id.'_status">
									<option value="1" '. $activeselected.'>Active</option>
									<option value="0"  '.$inactiveselected.'>Inactive</option>
								</select>
							</td>
							<td width="200">
								<a  class="btn" title="Edit"  data-name="'.$title.'" onclick="add_blog_cat('.$id.' , this)"><i class="fa fa-pencil" aria-hidden="true"></i></a>
								<a class="btn" title="Delete" href="'. site_url("admin/blog_category/" .$id) . '"><i class="fa fa-trash" aria-hidden="true"></i></a>	
							</td>
						  </tr>';
						 }
					}
					?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
function add_blog_cat(id='' , thiss=''){
	 if(id==''){
		 $('.modal-title span').text('Add New') ;
		 $('.modal-body .btn').text('Add');
		 $('#title').val('');
		 $('#id').val(0);
	 }else{
		$('.modal-title span').text('Update');
		$('.modal-body .btn').text('Update');
		$('#title').val($(thiss).attr('data-name'));
		$('#id').val(id);
		 
	 }
		$('#category').modal('show');	   
 }
 
 
</script>


<script>

function update_cat_status(e,id){
	 $.ajax({
		  type		: "POST",
		  url		: "<?php echo site_url('admin/blog_category'); ?>",
		  data		: {	'status' :	 e.value,
						'id'	 :   id			
					},
		  success: function(response){
			  if(response==1){
				  toastr.success("category status updated successfully");
			  }
		  }
	});
	
}
</script>


<div id="category" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                 <h4 class="modal-title"><span>Add New</span> Category</h4>
            </div>
            <div class="modal-body">
			<form action="<?php echo base_url();?>admin/blog_category" method="post" enctype="multipart/form-data" >
                <div class="hs_input">
                    <label>Name</label>
					<input type="text" class="form-control" required placeholder="Name"  name="title" id="title">
                </div>
                <div class="hs_input">
				    <input type="hidden" value="0" name="id" id="id">
					<button type="submit" class="btn">ADD</button>
                </div>
            </div>
			</form>
        </div>
    </div>
</div>

<!-- Add New Category popup end -->




   

    <!-- page body end -->

