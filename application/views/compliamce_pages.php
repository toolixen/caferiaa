<div class="row">
    <div class="col-md-12">
        <div class="hs_heading medium">
            <h3>Compliance Pages</h3>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- tab start -->
        <div class="hs_tabs">
            <ul class="nav nav-pills">
                <li class="active"><a data-toggle="pill" href="#site_setting_tab">Terms & Condition</a></li>
                <li><a data-toggle="pill" href="#image_setting_tab">Privacy Policy</a></li>	 
            </ul>
               
            <div class="tab-content">
				<!-- site setting tab start -->
                <div id="site_setting_tab" class="tab-pane fade in active">
                    <div class="row">
						<div class="col-sm-12">
							<form id="tnc_form">
							<div class="hs_input">								
								<label>Terms & Condition</label>
								 <textarea id="terms_condition"  name="terms_condition" ><?php echo $setting_data[0]['terms_condition']; ?></textarea>
							</div>
							</form>
							<button type="button" onclick="terms_condation()" class="btn">update</button>
						</div>
                    </div>
                </div>
                <!-- site setting tab end -->
				
                <!-- image setting tab start -->
                <div id="image_setting_tab" class="tab-pane fade">
				   <div class="hs_input">								
						<label>Privacy Policy</label>
						 <textarea id="privacy_policy" name="privacy_policy" ><?php echo $setting_data[0]['privacy_policy']; ?></textarea>
					</div>
					<button type="button" onclick="privacy_policy();" class="btn">update</button>
                </div>
            </div>
        </div>
        <!-- tab end -->
    </div>
</div>    

<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/lib/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets/admin/js/ckeditor/ckeditor.js'); ?>"></script>	 
<script>
$('document').ready(function(){
	CKEDITOR.replace( 'terms_condition' );
	CKEDITOR.replace( 'privacy_policy' );
});
</script>