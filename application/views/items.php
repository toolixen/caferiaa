<div class="row">
    <div class="col-md-12">
        <div class="hs_heading medium">
            <h3>Productos (<?php echo count($itemList); ?>) <a href="<?php echo base_url(); ?>admin/add_item/" class="btn">Agregar</a></h3>
        </div>
        <div class="hs_datatable_wrapper table-responsive">
            <table class="hs_datatable table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Foto</th>
                        <th>Precio (S/)</th>
						<th>Estado</th>
                        <th>Accion</th>
                    </tr>
                </thead>
                <tbody>
				 <?php if($itemList){$cnt=0; ?>
				   <?php foreach($itemList as $soloItem){
					   
					  $cnt++;
					  $item_id=$soloItem['item_id'];
                      $item_name=$soloItem['item_name'];
                      $item_price=$soloItem['item_price'];
					  if($item_price =='' && $soloItem['item_options'] =='YES'){
						$item_price ='<a href="javascript:;" onclick="getItemOption_list('.$item_id.')">Option List</a>';
					  }
					  
					  $item_pic=base_url().'assets/admin/images/menus/default.jpg';
					  if($soloItem['item_pic']!=''){
						 $item_pic=base_url().'assets/admin/images/menus/thumb/'.$soloItem['image_thumb']; 
					  }
					  
					  $itemactiveselected = ($soloItem['item_status'] == '1' ? 'selected' : '' );
					  $iteminactiveselected = ($soloItem['item_status'] == '0' ? 'selected' : '' );
					 ?>
				    <tr>
                        <td><?php echo $cnt;?></td>
                       
                        <td><?php echo   $item_name?></td>
						 <td>
                            <div class="hs_category_icon">
                                <img src="<?php echo $item_pic; ?>" alt="icon">
                            </div>                            
                        </td>
                        <td><?php echo $item_price?></td>
						<td>
                            <select class="form-control" onchange="updatethevalue(this,'item');"  id="<?php echo $item_id;  ?>_status">
                                <option value="1" <?php echo $itemactiveselected; ?>>Active</option>
                                <option value="0"  <?php echo $iteminactiveselected; ?>>Inactive</option>
                            </select>
                        </td>
						
                       <td width="200">
                            <a href="<?php echo base_url() ?>admin/add_item/<?php echo $item_id; ?>" class="btn" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            <a  class="btn" title="Delete" onclick="delete_item(<?php echo  $item_id?>)"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </td>
                     </tr> 
				   <?php } ?>	 
				 <?php } ?> 
                </tbody>
            </table>
        </div>
    </div>
</div>
 
<!-- option list start -->
<div id="item_optionlist_modal" class="modal fade" role="dialog">    
	<div class="modal-dialog">       
		<div class="modal-content">  
			<div class="modal-header">    
				<button type="button" class="close" data-dismiss="modal">&times;</button>   
				<h4 class="modal-title" id="myModalLabel">Item Options<span id="user_unameTxt"> </span> List</h4>		  
			</div> 
			<div class="modal-body">	
				<table width="100%">					
					<thead>			
						<tr>						
							<th>Option Name</th>					
							<th>Price</th>					
						</tr>				
					</thead>				
					<tbody id="option_list">			
					</tbody>				
				</table>		
			</div>	
		</div>    
	</div>
</div>
 <!-- option list popup end -->





</div>
<!-- page body end -->

	
<script>	
	/*function getItemOption_list(mainitem_id){
		var basepath = $('#base_url').val();
		$('#option_list').html('');
		if(mainitem_id !='')
		{
			$.ajax({
				type: "POST",
				url: basepath + '/admin/get_itemoptionsList',
				data:{'item_id':mainitem_id},
				success: function(data)
				{ 	obj = JSON.parse(data);
					if(obj['status'] == 1){
						$('#option_list').html(obj['option_list']);
						$('#item_optionlist_modal').modal('show');
					}					
				}   
			});
		}
	}*/
</script>