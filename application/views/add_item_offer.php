<?php
if(isset($offer_details)){
	$offer_id     =  $offer_details[0]['id'];
	$offer_name    = $offer_details[0]['offer_name'];
	$offer_price    = $offer_details[0]['offer_price'];
	$offer_desc   = $offer_details[0]['offer_description'];
	$offer_products   = $offer_details[0]['offer_products'];
	$offer_date    = date('Y-m-d', strtotime($offer_details[0]['offer_date']));
}else{
	$offer_id     = 0;
	$offer_name    = '';
	$offer_desc   = '';	
	$offer_products   = '';
	$offer_date ='';
	$offer_price='';
}	
	
?>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12 margin-bottom-30">
		<div class="hs_heading medium">
			<h3><?php echo (isset($offer_details) ? 'Update' : 'Add');?> Offer</h3>
		</div>
	    <form action="<?php echo base_url().'admin/update_item_offer';?>"  method="post" id="add_combo_offer_form" enctype="multipart/form-data">
		<div class="row">				
			<div class="col-md-6 col-sm-12 col-xs-12">
				<div class="hs_input">
					<label>Offer Name</label>
					<input type="text" class="form-control required" id="offer_name" name="offer_name" value="<?php echo $offer_name; ?>" maxlength="40">
				</div>
			</div>
			<div class="col-md-6 col-sm-12 col-xs-12">
				<div class="hs_input">
					<label>Offer End Date </label>
						<input type="text" class="form-control required" id="offer_date" name="offer_date" value="<?php echo $offer_date; ?>" >
				</div>
			</div>
			
			<div class="col-md-6 col-sm-12 col-xs-12">
				<div class="hs_input">
					<label for="exampleInputFile">Offer Picture</label>
					<input type="file" id="offer_pic" name="offer_pic" class="form-control">
				</div>
			</div>
			
			<div class="col-md-8 col-sm-12 col-xs-12">
				<div class="hs_input">
					<input type="text" class="form-control" id="kf_search_offer_products" placeholder="Search for Products">
					<div id="kf_products_results"></div>
				</div>
			</div>
			
			<div class="col-md-4 col-sm-12 col-xs-12">
				<div class="hs_input">
					<a  class="btn " id="search-offer-product">Search</a>
				</div>
			</div>
			
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="hs_input">
					<label>Selected Items</label>
					<div id="kfp_selected_products">
						<?php if(empty($offer_products)){ ?>
							<div class="message information" id="no-products-message">There are currently no items selected</div>
						<?php } else{ 
							$items = json_decode($offer_products, true);
							$quant = array();
							foreach($items as $item){
								$itemdata = get_itemdata_by_id($item['item_id']);
								if($itemdata){
								    $quant[] = $item['quantity'] * $itemdata[0]['item_price'];
									?>
									<div id="product-added-<?= $itemdata[0]['item_id'] ?>">
										<input name="offer_products[]" value="<?= $itemdata[0]['item_id'] ?>" type="hidden">
										<div class="kf_item_box">
										    <div class="kf_item_title">
    											<a id="product-remove-<?= $itemdata[0]['item_id'] ?>" class="delete">
    												<i class="fa fa-minus-circle" aria-hidden="true"></i>
    											</a>
    											<span><?= $itemdata[0]['item_name'] ?></span>
											</div>
											<div class="kf_item_quantity">
											    <span>Quantity: </span>
											    <input type="number" class="kf_product_quantity" name="product_quantity[]" min="1" max="10" value="<?= $item['quantity'] ?>" data-price="<?= $itemdata[0]['item_price'] ?>">
											</div>
										</div>
									</div>
									<script type="text/javascript">
										$('#product-remove-<?= $itemdata[0]["item_id"] ?>').click(function()
										{
											$(this).parent().fadeOut('slow', function() { $(this).parent().remove();
											var numItems = $('.kf_product_quantity').length;
                        				    if(numItems){
                        				        $('.kf_product_quantity').trigger('change');   
                        				    }else{
                        				        $('.kf_tmt_price').html('');
                        				    }
											});
										});
									</script> 
									<?php
								}
							}
						?>
							
						<?php } ?>
					</div>
					
					<div class="kf_total_selected_price">
					    <p><strong>Total Amount: <span class="kf_tmt_price"><?= (!empty($quant)) ? array_sum($quant) : '' ?></span></strong></p>
					</div>
				</div>
			</div>
			
			<div class="col-md-4 col-sm-12 col-xs-12 kf_offer_amount_section">
				<div class="hs_input">
				    <label>Offer Price</label>
					<input type="text" class="form-control kf_offer_selected_price" name="offer_price" data-valid="number" value="<?= $offer_price ?>" data-error="Please enter valid number">
				</div>
			</div>
			
		</div>
		<input id="offer_id" type="hidden" name="offer_id" value="<?php echo $offer_id; ?>" > 
		<a  class="btn " onclick="add_offer(this)"><?php echo (isset($offer_details) ? 'Update' : 'Add');?></a>
	  </form>	
	</div>
</div>   