<input type="hidden" id="baseurl" value="<?php echo base_url(); ?>" >
 
<div class="row">
    <div class="col-md-12">
        <div class="hs_heading medium">
            <h3>Offers (<?php if(!empty($all_offers)){ echo count($all_offers); }else{ echo 0; } ?>) <a href="#add_offer" class="btn" data-toggle="modal">Add new</a></h3>
        </div>
        <div class="hs_datatable_wrapper">
            <table class="hs_datatable table table-bordered">
                <thead>
                    <tr>
                        <th>#</th> 
                        <th>Encabezado</th>
						<th>Titulo</th>
						<th>Descripcion</th>
						<th>Fecha Creación</th>
						<th>Fecha Vencimiento</th>
						<th>Imagen</th>
						<th>Codigo Cupón</th>
						<th>Límite de usos de Cupón</th>
						<th>Tipo de descuento</th>
						<th>Estado</th>
						<th>Destacado</th>
						<th>Importe de descuento</th>
						<!--<th>Apply discount To</th>-->
						<!--<th>Discounted Product</th>-->
						<th>Accion</th>
                    </tr>
                </thead>
                <tbody>
				<?php 
				  $i= 0;
				  if(!empty($all_offers)){
				foreach($all_offers  as $offer){ ?>
					<tr>
						<td><?php echo ++$i; ?></td>
						<td><?php echo $offer['offer_heading']; ?></td>
						<td><?php echo $offer['offer_title']; ?></td>
						<td><?php echo $offer['offer_description']; ?></td>
						<td><?php echo $offer['create_at']; ?></td>
						<td><?php $date = $offer['coupen_expire_date'];
								  echo date('d M Y', strtotime($date));
									//echo $offer->coupen_expire_date; ?></td>
						<td>
							<div class="product_simg hs_category_icon">
								 <img src="<?php echo base_url().$offer['offer_image']; ?>" alt="icon">
							</div> 
						</td>
						
						<td><?php echo $offer['coupen_code']; ?></td>
						<td><?php echo $offer['coupen_uses_limt']; ?></td>
						<td><?php if($offer['discount_type'] == 1){
									  echo 'Percentage';
								  }else{
									  echo 'Flat';
								  } 
						?></td>
						<td>
							<label class="switch">
								<input id="offerstatus_<?php echo $offer['offer_id']; ?>" onclick="update_offer_status(<?php echo $offer['offer_id']; ?>);"   type="checkbox" value="" <?php if($offer['offer_status'] == 1){ echo "checked"; } ?> >
								<span class="slider"></span>
							</label>
						</td>
						<td><input id="offerfeatured_<?php echo $offer['offer_id']; ?>" onclick="update_offer_featured(<?php echo $offer['offer_id']; ?>);"   type="checkbox" name="featured" value="" <?php if($offer['offer_featured'] == 1){ echo "checked"; } ?> ></td>
						<td><?php echo $offer['discount_amount']; ?></td>
						<!--<td><?php echo $offer['apply_discount_to']; ?></td>-->
						<!--<td>
						<?php
						/*if($offer['discounted_product'] !=''){
							if(!empty($all_products)){
								foreach($all_products as $prod){
									foreach(json_decode($offer['discounted_product']) as $offer_prod){
									  if($offer_prod == $prod['item_id']){
											echo  $prod['item_name'] .' | ';
									  }  
									}
								  }
							}else{
								echo 'None';
							}
						}else{ 
							echo 'None';

						}*/ ?></td>-->
						<td width="200">
							<a  class="btn" title="Edit" data-name="Business Statistics" onclick="edit_offers(<?php echo $offer['offer_id']; ?>)"><i class="fa fa-pencil" aria-hidden="true"></i></a>
							 <a  href="<?php echo base_url().'admin/delete_offer/'.$offer['offer_id']; ?>" class="btn" title="Delete" onclick="return delete_offer()"><i class="fa fa-trash" aria-hidden="true"></i></a>
						</td>
					</tr>
			    <?php
				 }
				} 
				?>	
				</tbody>
            </table>
        </div>
    </div>
</div>

<br>
<br>
<br>
<br>


<!-- add offer popup start -->
<div class="modal_box">
<div id="add_offer" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><span>Add</span> Coupon</h4>
            </div>
            <div class="modal-body">
			<input type="hidden" id="baseurl" value="<?php echo base_url(); ?>" >
			<form method="post" enctype="multipart/form-data" id="add_offer_form">
                <div class="hs_input">
                    <label>Encabezado</label>
                    <input type="text" class="form-control add_cate_form" name="offer_heading" id="offer_heading" value="">
                </div>
				<div class="hs_input">
                    <label>Titulo</label>
                    <input type="text" class="form-control add_cate_form" name="offer_title" id="offer_title" value="">
                </div>
				<div class="hs_input">
                    <label>Descripcion</label>
                    <textarea rows="4" class="form-control" name="offer_descr" id="offer_descr"></textarea>
                </div>
				<div class="hs_input">
                    <label>Imagen</label>
                    <input type="file" class="form-control add_cate_form" name="offer_image" id="offer_image" value="">
                </div>
				
				<div class="hs_input">
                    <label>Código Cupón</label>
                    <input type="text" class="form-control add_cate_form" name="coupen_code" id="coupen_code" value="">
                </div>
				<div class="hs_input">
                    <label>Límite de usos de Cupón</label>
                    <input type="Number" class="form-control add_cate_form" name="coupen_uses_limit" id="coupen_uses_limit" value="" min="1">
                </div>
				<div class="hs_input">
					<label>Tipo de descuento</label>
						<select class="form-control" id="discount_type" name="discount_type">
						<option value="0">Select Type</option>
						<option value="1">Percentage</option>	 
						<option value="2">Flat</option>
						</select>
				</div>
				<div class="hs_input">
                    <label>Importe de descuento</label>
                    <input type="text" class="form-control add_cate_form" name="discount_amount" id="discount_amount" value="">
                </div>				
				<div class="hs_input">                  
					<label>¿Puede el usuario usar el cupón por tiempo ilimitado?</label>
					<input type="radio"   name="user_usedlimit" id="user_usedlimit_yes" value="1"><span> Si </span>
					<input type="radio"   name="user_usedlimit" id="user_usedlimit_no" value="2" checked> <span> No </span>                </div>
				<!--<div class="hs_input">
					<label>Apply Discount To </label>
						<select class="form-control" id="apply_discount_to" name="apply_discount_to">
						<option value="Total_amount">Total Amount</option>	 
						<option value="single_product">Product</option>
						</select> 
				</div>
				<div class="hs_input">
					<label>Discounted Product</label>
						<select class="form-control" id="discounted_product" name="discounted_product[]" multiple>
						<option value="0">Select Product</option>
							<?php /*if(!empty($all_products)){
								  foreach($all_products as $prod){
									echo '<option value="'.$prod['item_id'].'">'.$prod['item_name'].'</option>';
								  }
								}*/ ?> 
						</select>
						<p>Note:- For Select Multiple Product Hold Ctrl key Then Select.</p>
				</div>-->
				<div class="hs_input">
                    <label>Fecha de vencimiento del cupón</label>
                    <input type="Date" class="form-control add_cate_form" name="coupen_expire_date" id="coupen_expire_date" value="">
                </div>
				<div class="hs_input">
				  <button type="submit" id="add"  class="btn">Agregar</button>
                </div>
            </div>
			</form>
        </div>
    </div>
</div>
</div>
<!-- add offer popup end -->
	
<!-- update offer popup start -->
<div class="modal_box">
<div id="update_offer" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><span>Actualizar</span> Oferta</h4>
            </div>
            <div class="modal-body">
			<input type="hidden" id="baseurl" value="<?php echo base_url(); ?>" >
			<form method="post" enctype="multipart/form-data" id="update_offer_form">
                <div class="hs_input">
                    <label>Encabezado</label>
                    <input type="text" class="form-control add_cate_form" name="up_offer_heading" id="up_offer_heading" value="">
                </div>
				<div class="hs_input">
                    <label>Titulo</label>
                    <input type="text" class="form-control add_cate_form" name="up_offer_title" id="up_offer_title" value="">
                </div>
				<div class="hs_input">
                    <label>Descripción</label>
                    <textarea rows="4" class="form-control" name="up_offer_descr" id="up_offer_descr"></textarea>
                </div>
				<div class="hs_input">
                    <label>Imagen</label>
                    <input type="file" class="form-control add_cate_form" name="up_offer_image" id="up_offer_image" value="">
                </div>
				<div class="hs_input">
                    <label>Codigo Cupón</label>
                    <input type="text" class="form-control add_cate_form" name="up_coupen_code" id="up_coupen_code" value="">
                </div>
				<div class="hs_input">
                    <label>Límite de usos de Cupón</label>
                    <input type="Number" class="form-control add_cate_form" name="up_coupen_uses_limit" id="up_coupen_uses_limit" value="" min="1">
                </div>
				<div class="hs_input">
					<label>Tipo de descuento</label>
						<select class="form-control" id="up_discount_type" name="up_discount_type">
						<option value="0">Select Type</option>
						<option value="1">Percentage</option>	 
						<option value="2">Flat</option>
						</select>
				</div>
				<div class="hs_input"> 
                    <label>Importe de descuento</label>
                    <input type="text" class="form-control add_cate_form" name="up_discount_amount" id="up_discount_amount" value="">
                </div>				
				<div class="hs_input"> 
					<label>¿Puede el usuario usar el cupón por tiempo ilimitado?</label>
					<input type="radio"   name="up_user_usedlimit" id="up_user_usedlimit_yes" value="1">	<span> Si </span>
					<input type="radio"   name="up_user_usedlimit" id="up_user_usedlimit_no" value="2" > <span> No </span> 
				</div> 
				<!--<div class="hs_input">
					<label>Apply Discount To </label>
						<select class="form-control" id="up_apply_discount_to" name="up_apply_discount_to">
						<option value="Total_amount">Total Amount</option>	 
						<option value="single_product">Product</option>
						</select> 
				</div>
				<div class="hs_input">
					<label>Dicounted Product</label> 
						<select class="form-control" id="up_discounted_product" name="up_discounted_product[]" multiple>
						<option value="0">Select Product</option>
							<?php/* if(!empty($all_products)){
								  foreach($all_products as $prod){
									echo '<option value="'.$prod['item_id'].'">'.$prod['item_name'].'</option>';
								  }
								}*/ ?> 
						</select>
						<p>Note:- For Select Multiple Product Hold Ctrl key Then Select.</p>
				</div>-->
				<div class="hs_input">
                    <label>Fecha de caducidad de Coupen</label>
                    <input type="Date" class="form-control add_cate_form" name="up_coupen_expire_date" id="up_coupen_expire_date" value="">
                </div>				
				<div class="hs_input">				
					<input type="hidden" value="" name="old_offer_id" id="old_offer_id">		
					<button type="submit" id="add"  class="btn">Actualizar</button>
				</div>
				 
            </div>
			</form>
            </div> 
			
        </div>
    </div>
</div>
</div>
<!-- update offer popup end -->	
 <!-- page body end -->
	
	
	