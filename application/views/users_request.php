<div class="row">

    <div class="col-md-12">

        <div class="hs_heading medium">

            <h3>Users Wallet Recharge Request (<?php echo count($userList); ?>)  </h3>

        </div>

        <div class="hs_datatable_wrapper table-responsive">

            <table class="hs_datatable table table-bordered">

                <thead>

                    <tr>

                        <th>#</th>

                        <th>Name</th>

                       <!-- <th>Email</th>-->

                        <th>Mobile</th>
						
						<th>Amount Request</th>
						
						<th>Remarks</th>
						
						<th>Status</th>
						
                        <th>Action</th>

                    </tr>

                </thead>

                <tbody>

				 <?php if($userList){$cnt=0; ?>

				   <?php foreach($userList as $soloUser){

					   

					  $cnt++;

					  $user_id=$soloUser['user_id'];

                      $user_name=$soloUser['user_name'];

					  $username_arr=explode(' ',$user_name);

					  $name_initiails=substr($username_arr[0] , 0, 1); 

					  if(isset($username_arr[1])){

						$name_initiails.=substr($username_arr[1] , 0, 1); 

					  }

					  if($soloUser['status'] == '1')
					  {
						   $request_status = 'Pending';

					  }elseif($soloUser['status'] == '2')
					  {
							
						   $request_status = 'Done';
							 
					  }else{
							
						   $request_status = '';
							
					  }
					   
					
					 ?>

				    <tr>

                        <td><?php echo $cnt;?></td>

                        <td width="200">

                            <div class="hs_user">

                                <div class="user_img">

                                    <span class="hs_user_initials"><?php echo $name_initiails; ?></span>

                                </div>

                                <div class="user_name">

                                    <p><?php echo $user_name; ?></p>

                                    <span>Created at - <?php echo date("d-m-Y", strtotime($soloUser['created_at'])); ?></span>

                                </div>

                            </div>

                        </td>

						<td><?php echo $soloUser['user_mobile']?></td>
						
						<td><?php echo  $soloUser['amount']?></td>
						
						<td><?php echo  $soloUser['remarks']?></td>
						
						<td><?php echo  $request_status; ?></td>
						
                        <td width="200">
							<a class="btn" title="Add Wallet" onclick="openWalletcreditPopup(<?php echo $user_id; ?> , '<?php echo $user_name; ?>', <?php echo $soloUser['id']; ?>)"><i class="fa fa-money" aria-hidden="true"></i></a>
						</td>

                     </tr> 

				   <?php } ?> 

				 <?php } ?> 

                </tbody>

            </table>

        </div>

    </div>

</div>



<!-- Wallet Credit start -->

<div id="wallet_credit" class="modal fade" role="dialog">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal">&times;</button>

                <h4 class="modal-title" id="myModalLabel">Credit to <span id="user_unameTxt"> </span>'s Wallet</h4>

		        <p>Wallet Balance <span id="wallet_amountTxt">0</span></p>

            </div>

            <div class="modal-body">

                <div class="hs_input">

                    <label>Amount</label>

					 <input type="text" class="form-control" id="wc_amount" />

                </div>

				<div class="hs_input">

                    <label>Note (Description)</label>

                    <textarea class="form-control" id="wc_note"></textarea>

				</div>

				
                <div class="hs_input">

					<input type="hidden" id="wallet_user_id" value="0">
					<input type="hidden" id="wallet_request_id" value="0">
					
					<button type="button" class="btn" onclick="update_wallet_fun()">Add</button>

				</div>

            </div>

			

        </div>

    </div>

</div>

<!-- Wallet Credit popup end -->







 </div>

    <!-- page body end -->

